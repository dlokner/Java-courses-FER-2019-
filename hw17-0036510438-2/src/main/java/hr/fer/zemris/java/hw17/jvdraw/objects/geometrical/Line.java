package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical;

import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit.GeometricalObjectEditor;
import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit.LineEditor;

import java.awt.*;

/**
 * {@code GeometricalObject} that represents basic line.
 *
 * @author Domagoj Lokner
 */
public class Line extends GeometricalObject {

    /**
     * Line starting point.
     */
    private Point start;

    /**
     * Line ending point.
     */
    private Point end;

    /**
     * Color of the line.
     */
    private Color color;

    /**
     * Constructs {@code Line}.
     *
     * @param start line starting point.
     * @param end line ending point.
     * @param color line color.
     */
    public Line(Point start, Point end, Color color) {
        this.start = start;
        this.end = end;
        this.color = color;
    }

    @Override
    public void accept(GeometricalObjectVisitor v) {
        v.visit(this);
    }

    @Override
    public GeometricalObjectEditor createGeometricalObjectEditor() {
        return new LineEditor(this);
    }

    /**
     * Gets line starting point.
     *
     * @return start point.
     */
    public Point getStart() {
        return start;
    }

    /**
     * Gets ending point of the line.
     *
     * @return end point.
     */
    public Point getEnd() {
        return end;
    }

    /**
     * Gets color of the line.
     *
     * @return line color.
     */
    public Color getColor() {
        return color;
    }

    /**
     * Sets line start.
     *
     * @param start point to be set.
     */
    public void setStart(Point start) {
        this.start = start;
        fireChanged();
    }

    /**
     * Sets line end.
     *
     * @param end point to be set.
     */
    public void setEnd(Point end) {
        this.end = end;
        fireChanged();
    }

    /**
     * Sets line color.
     *
     * @param color color to be set.
     */
    public void setColor(Color color) {
        this.color = color;
        fireChanged();
    }

    /**
     * Change line to given {@code l}.
     *
     * @param l line to be copied to this object.
     */
    public void changeLine(Line l) {
        this.start = l.getStart();
        this.end = l.getEnd();
        this.color = l.getColor();
        fireChanged();
    }

    @Override
    public String toString() {
        return String.format("Line (%d,%d)-(%d,%d)", start.x, start.y, end.x, end.y);
    }
}
