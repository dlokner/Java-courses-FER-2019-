package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical;

/**
 * Implements object capable to visit {@code GeometricalObject}s.
 *
 * @author Domagoj Lokner
 */
public interface GeometricalObjectVisitor {

    /**
     * Visits {@code Line}.
     *
     * @param line line to be visited.
     */
    void visit(Line line);

    /**
     * Visits {@code Circle}.
     *
     * @param circle circle to be visited.
     */
    void visit(Circle circle);

    /**
     * Visits {@code FilledCircle}.
     *
     * @param filledCircle circle to be visited.
     */
    void visit(FilledCircle filledCircle);
}
