package hr.fer.zemris.java.hw17.jvdraw.tools;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Object that implements drawing tool.
 *
 * @author Domagoj Lokner
 */
public interface Tool {

    /**
     * Action that will be performed when mouse pressed
     * action is registered.
     *
     * @param e mouse event.
     */
    void mousePressed(MouseEvent e);

    /**
     * Action that will be performed when mouse release
     * action is registered.
     *
     * @param e mouse event.
     */
    void mouseReleased(MouseEvent e);

    /**
     * Action that will be performed when mouse click
     * action is registered.
     *
     * @param e mouse event.
     */
    void mouseClicked(MouseEvent e);

    /**
     * Action that will be performed when mouse is
     * moved.
     *
     * @param e mouse event.
     */
    void mouseMoved(MouseEvent e);

    /**
     * Action that will be performed when
     * mouse is dragged.
     *
     * @param e mouse event.
     */
    void mouseDragged(MouseEvent e);

    /**
     * Method that will perform defined
     * painting action.
     *
     * @param g2d object that will perform painting.
     */
    void paint(Graphics2D g2d);
}
