package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit;

import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.Line;

import javax.swing.*;
import java.awt.*;

/**
 * Editor for {@code Line} {@code GeometricalObject}.
 *
 * @author Domagoj Lokner
 */
public class LineEditor extends GeometricalObjectEditor {

	private static final long serialVersionUID = -7098118733704925742L;

	/**
     * Line that can be edited by this object.
     */
    private Line line;

    /**
     * Edited version of {@code line}.
     */
    private Line editedLine;

    /**
     * Input component for starting point of the line.
     */
    private PointInput startInput;

    /**
     * Input component for ending point of the line.
     */
    private PointInput endInput;

    /**
     * Input for line color.
     */
    private ColorInput colorInput;

    /**
     * Constructs {@code LineEditor}.
     *
     * @param line line that will be edited by this object.
     */
    public LineEditor(Line line) {
        this.line = line;

        initGUI();
    }

    /**
     * Initialize editors GUI.
     */
    private void initGUI() {
        startInput = new PointInput(line.getStart().x, line.getStart().y);
        endInput = new PointInput(line.getEnd().x, line.getEnd().y);

        colorInput = new ColorInput("Line color: ", line.getColor());

        startInput.setBorder(BorderFactory.createTitledBorder("starting point coordinate"));
        endInput.setBorder(BorderFactory.createTitledBorder("ending point coordinate"));
        colorInput.setBorder(BorderFactory.createTitledBorder("Choose color"));

        setLayout(new GridLayout(0, 1));

        add(startInput);
        add(endInput);
        add(colorInput);

    }

    @Override
    public void checkEditing() {
        Point start;
        Point end;
        try {
            start = new Point(
                    Integer.parseInt(startInput.xText()),
                    Integer.parseInt(startInput.yText()));
            end = new Point(
                    Integer.parseInt(endInput.xText()),
                    Integer.parseInt(endInput.yText()));
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException(ex);
        }

        editedLine = new Line(start, end, colorInput.getColor());
    }

    @Override
    public void acceptEditing() {
        if (editedLine == null) {
            throw new UnsupportedOperationException();
        }
        line.changeLine(editedLine);
    }
}
