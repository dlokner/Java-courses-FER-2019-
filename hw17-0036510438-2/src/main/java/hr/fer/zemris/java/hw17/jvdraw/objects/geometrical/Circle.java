package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical;

import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit.CircleEditor;
import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit.GeometricalObjectEditor;

import java.awt.*;

/**
 * Object that implements circle {@code GeometricalObject}.
 *
 * @author Domagoj Lokner
 */
public class Circle extends GeometricalObject {

    /**
     * Outline color of the circle.
     */
    protected Color outlineColor;

    /**
     * Center point.
     */
    protected Point center;

    /**
     * Circle's radius.
     */
    protected int radius;

    /**
     * Constructs {@code Circle}.
     *
     * @param center center point.
     * @param radius circle's radius.
     * @param color circle color.
     */
    public Circle(Point center, int radius, Color color) {
        this.outlineColor = color;
        this.center = center;
        this.radius = radius;
    }

    /**
     * Gets outline color of the circle.
     *
     * @return outline color.
     */
    public Color getOutlineColor() {
        return outlineColor;
    }

    /**
     * Gets circle's center.
     *
     * @return center of the circle.
     */
    public Point getCenter() {
        return center;
    }

    /**
     * Gets circle's radius.
     *
     * @return radius.
     */
    public int getRadius() {
        return radius;
    }

    /**
     * Sets circle's color.
     *
     * @param color color to be set.
     */
    public void setOutlineColor(Color color) {
        this.outlineColor = color;
    }

    /**
     * Sets center point.
     *
     * @param center center point of the circle.
     */
    public void setCenter(Point center) {
        this.center = center;
        fireChanged();
    }

    /**
     * Sets radius.
     * @param radius radius to be set.
     */
    public void setRadius(int radius) {
        this.radius = radius;
        fireChanged();
    }

    /**
     * Changes this {@code Circle} with given {@code c} object.
     *
     * @param c circle to be copied.
     */
    public void changeCircle(Circle c) {
        this.outlineColor = c.outlineColor;
        this.center = c.center;
        this.radius = c.radius;
        fireChanged();
    }

    @Override
    public void accept(GeometricalObjectVisitor v) {
        v.visit(this);
    }

    @Override
    public GeometricalObjectEditor createGeometricalObjectEditor() {
        return new CircleEditor(this);
    }

    @Override
    public String toString() {
        return String.format("Circle (%d,%d), %d", center.x, center.y, radius);
    }
}
