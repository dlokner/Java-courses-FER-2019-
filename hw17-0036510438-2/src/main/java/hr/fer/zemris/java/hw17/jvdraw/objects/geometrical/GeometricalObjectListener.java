package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical;

/**
 * Implements object that listen to changes of {@code GeometricalObject}.
 *
 * @author Domagoj Loker
 */
public interface GeometricalObjectListener {

 /**
  * Indicates change of object {@code o}.
  *
  * @param o changed object.
  */
 void geometricalObjectChanged(GeometricalObject o);
}