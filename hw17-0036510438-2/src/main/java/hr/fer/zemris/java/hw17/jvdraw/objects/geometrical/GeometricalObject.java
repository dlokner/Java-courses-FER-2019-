package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical;

import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit.GeometricalObjectEditor;

import java.util.LinkedList;
import java.util.List;

/**
 * Object that implements single geometrical object.
 *
 * @author Domagoj Lokner
 *
 */
public abstract class GeometricalObject {

    /**
     * List of {@code GeometricalObjectListener}s.
     */
    List<GeometricalObjectListener> listeners = new LinkedList<>();

    /**
     * Method through which given {@code v} visitor object
     * will get this object.
     *
     * @param v visitor.
     */
    public abstract void accept(GeometricalObjectVisitor v);

    /**
     * Creates editor for this {@code GeometricalObject}.
     *
     * @return editor for this object.
     */
    public abstract GeometricalObjectEditor createGeometricalObjectEditor();

    /**
     * Adds listener to this object.
     *
     * @param l listener to be add.
     */
    public void addGeometricalObjectListener(GeometricalObjectListener l) {
        listeners.add(l);
    }

    /**
     * Removes listener from this object.
     *
     * @param l listener to this object.
     */
    public void removeGeometricalObjectListener(GeometricalObjectListener l) {
        listeners.remove(l);
    }

    /**
     * Fires objects change.
     */
    public void fireChanged() {
        for (GeometricalObjectListener l : listeners) {
            l.geometricalObjectChanged(this);
        }
    }

}
