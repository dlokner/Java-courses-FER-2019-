package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical;

import java.awt.*;

/**
 * Visitor that calculate bounding box of
 * all visited objects.
 *
 * @author Domagoj Lokner
 */
public class GeometricalObjectBBCalculator implements GeometricalObjectVisitor {

    /**
     * Bounding box.
     */
    private Rectangle boundingBox;

    /**
     * Bounds of currently visited object.
     */
    private Rectangle currentObjectBounds;

    /**
     * Constructs {@code GeometricalObjectBBCalculator}.
     */
    public GeometricalObjectBBCalculator() {
        currentObjectBounds = new Rectangle();
    }

    @Override
    public void visit(Line line) {
        currentObjectBounds.setBounds(
                Math.min(line.getStart().x, line.getEnd().x),
                Math.min(line.getStart().y, line.getEnd().y),
                Math.abs(line.getStart().x - line.getEnd().x),
                Math.abs(line.getStart().y - line.getEnd().y)
        );
        addToBox(currentObjectBounds);
    }

    @Override
    public void visit(Circle circle) {
        Point center = circle.getCenter();
        int radius = circle.getRadius();

        currentObjectBounds.setBounds(
                center.x - radius,
                center.y - radius,
                radius * 2,
                radius * 2
        );
        addToBox(currentObjectBounds);
    }

    @Override
    public void visit(FilledCircle filledCircle) {
        visit((Circle)filledCircle);
    }

    /**
     * Adds bounding rectangle to bounds of the box.
     *
     * @param bound bound to be added.
     */
    private void addToBox(Rectangle bound) {
        if (boundingBox == null) {
            boundingBox = new Rectangle(bound);
            return;
        }

        int xLeft = Math.min(bound.x, boundingBox.x);
        int xRight = Math.max(bound.x + bound.width, boundingBox.x + boundingBox.width);

        int yTop = Math.min(bound.y, boundingBox.y);
        int yBottom = Math.max(bound.y + bound.height, boundingBox.y + boundingBox.height);

        boundingBox.setBounds(xLeft, yTop,
                xRight - xLeft, yBottom - yTop);
    }

    /**
     * Gets bounding box.
     *
     * @return bounding box.
     */
    public Rectangle getBoundingBox() {
        return boundingBox;
    }
}
