package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Parses .jvd file format lines to {@code GeometricalObject} list.
 *
 * @author Domagoj Lokner
 */
public class JVDParser {

    /**
     * Lines of data to be parsed.
     */
    List<String> data;

    /**
     * List of parsed objects.
     */
    List<GeometricalObject> objects;

    /**
     * Constructs {@code JVDParser}.
     *
     * @param data lines of data to be parsed.
     */
    public JVDParser(List<String> data) {
        this.data = data;
        objects = new LinkedList<>();

        parse();
    }

    /**
     * Gets list of parsed geometrical objects.
     *
     * @return list of parsed objects.
     */
    public List<GeometricalObject> getObjects() {
        return objects;
    }

    /**
     * Parses {@code data}.
     *
     * @throws JVDParserException if defined object can't be interpreted as {@code GeometricalObject}.
     */
    private void parse() {

        for (String line : data) {
            String object = line.strip().substring(0, line.indexOf(' '));
            String objectData = line.substring(object.length() + 1).trim();

            switch (object) {
                case ("LINE"):
                    addLine(objectData);
                    break;
                case ("CIRCLE"):
                    addCircle(objectData);
                    break;
                case("FCIRCLE"):
                    addFcircle(objectData);
                    break;
                default:
                    throw new JVDParserException("Invalid key word: " + object);
            }
        }
    }

    /**
     * Adds filled circle represented with {@code objectData}.
     *
     * @param objectData {@code FilledCircle} record.
     * @throws JVDParserException if record arguments are not valid.
     */
    private void addFcircle(String objectData) {
        int[] args = extractArgs("FCIRCLE", objectData, 9);

        objects.add(
                new FilledCircle(
                        new Point(args[0], args[1]),
                        args[2],
                        new Color(args[3], args[4], args[5]),
                        new Color(args[6], args[7], args[8])
                )
        );
    }

    /**
     * Adds circle represented with {@code objectData}.
     *
     * @param objectData {@code Circle} record.
     * @throws JVDParserException if record arguments are not valid.
     */
    private void addCircle(String objectData) {
        int[] args = extractArgs("CIRCLE", objectData, 6);

        objects.add(
                new Circle(
                        new Point(args[0], args[1]),
                        args[2],
                        new Color(args[3], args[4], args[5])
                )
        );
    }

    /**
     * Adds line object represented with {@code objectData}.
     *
     * @param objectData {@code Line} record.
     * @throws JVDParserException if record arguments are not valid.
     */
    private void addLine(String objectData) {
        int[] args = extractArgs("LINE", objectData, 7);

        objects.add(
                new Line(
                        new Point(args[0], args[1]),
                        new Point(args[2], args[3]),
                        new Color(args[4], args[5], args[6])
                )
        );
    }

    /**
     * Extracts arguments from {@code GeometricalObject} record.
     *
     * @param objectName name of the object.
     * @param data object record.
     * @param expectedLength expected number of arguments.
     * @return array of object arguments.
     * @throws JVDParserException if record arguments are not valid.
     */
    private int[] extractArgs(String objectName, String data, int expectedLength) throws JVDParserException {
        String[] stringArgs = data.split("\\s+");

        if (stringArgs.length != expectedLength) {
            throw new JVDParserException("Invalid number of arguments for " + objectName + " " + data);
        }

        try {
            return parstToIntArray(stringArgs);
        } catch (NumberFormatException ex) {
            throw new JVDParserException("Invalid arguments for " + objectName + " " + data);
        }
    }

    /**
     * Parse array of strings to array of ints.
     *
     * @param numbers array of string to be parsed.
     * @return array of parsed ints.
     * @throws NumberFormatException if any element can't be parsed.
     */
    private int[] parstToIntArray(String[] numbers) throws NumberFormatException {
        int[] result = new int[numbers.length];

        for (int i = 0; i < result.length; ++i) {
            result[i] = Integer.parseInt(numbers[i]);
        }

        return result;
    }
}
