package hr.fer.zemris.java.hw17.jvdraw.components;

import hr.fer.zemris.java.hw17.jvdraw.objects.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.objects.DrawingModelListener;
import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.GeometricalObjectPainter;
import hr.fer.zemris.java.hw17.jvdraw.tools.Tool;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Supplier;

/**
 * Swing components that presents drawing canvas.
 *
 * @author Domagoj Lokner
 */
public class JDrawingCanvas extends JComponent {

	private static final long serialVersionUID = 2336361195310748901L;

	/**
     * Drawing model
     */
    DrawingModel model;

    /**
     * Supplier for current drawing tool.
     */
    Supplier<Tool> toolSupplier;

    /**
     * Constructs {@code JDrawingCanvas}.
     *
     * @param model drawing model.
     * @param toolSupplier supplier for drawing tool.
     */
    public JDrawingCanvas(DrawingModel model, Supplier<Tool> toolSupplier) {
        this.model = model;
        this.toolSupplier = toolSupplier;

        addListeners();
    }

    /**
     * Adds listeners for mouse events.
     */
    private void addListeners() {
        addMouseListener(mouseListener);
        addMouseMotionListener(mouseListener);
        model.addDrawingModelListener(dmListener);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        objPainter.setG2d(g2d);

        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, getWidth(), getHeight());

        for (int i = 0; i < model.getSize(); ++i) {
            model.getObject(i).accept(objPainter);
        }

        toolSupplier.get().paint(g2d);
    }


    /*
                =================
                    CONSTANTS
                =================
     */

    /**
     * Definition  of {@code DrawingModelListener} for this canvas.
     */
    private final DrawingModelListener dmListener = new DrawingModelListener() {
        @Override
        public void objectsAdded(DrawingModel source, int index0, int index1) {
            repaint();
        }

        @Override
        public void objectsRemoved(DrawingModel source, int index0, int index1) {
            repaint();
        }

        @Override
        public void objectsChanged(DrawingModel source, int index0, int index1) {
            repaint();
        }
    };

    /**
     * Mouse event listener for this canvas.
     */
    private final MouseAdapter mouseListener = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            toolSupplier.get().mouseClicked(e);
        }

        @Override
        public void mousePressed(MouseEvent e) {
            toolSupplier.get().mousePressed(e);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            toolSupplier.get().mouseReleased(e);
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            toolSupplier.get().mouseDragged(e);
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            toolSupplier.get().mouseMoved(e);
        }
    };

    /**
     * Object that paints all objects from {@code model}.
     */
    private final GeometricalObjectPainter objPainter = new GeometricalObjectPainter((Graphics2D)getGraphics());
}
