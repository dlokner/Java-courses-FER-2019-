package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical;

import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit.FilledCircleEditor;
import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit.GeometricalObjectEditor;

import java.awt.*;

/**
 * Object that implements circle with filled background {@code GeometricalObject}.
 *
 * @author Domagoj Lokner
 */
public class FilledCircle extends Circle {

    /**
     * Color of the circle's background.
     */
    private Color areaColor;

    /**
     * Constructs {@code FilledCircle}.
     *
     * @param center center point of the circle.
     * @param radius circles radius.
     * @param color color of the circle outline.
     * @param areaColor background color.
     */
    public FilledCircle(Point center, int radius, Color color, Color areaColor) {
        super(center, radius, color);
        this.areaColor = areaColor;
    }

    /**
     * Constructs filled circle.
     *
     * @param circle object to be copied to this object.
     * @param areaColor background color of the circle.
     */
    public FilledCircle(Circle circle, Color areaColor) {
        super(circle.center, circle.radius, circle.outlineColor);
        this.areaColor = areaColor;
    }

    /**
     * Gets background color of the circle.
     *
     * @return background color.
     */
    public Color getAreaColor() {
        return areaColor;
    }

    /**
     * Sets circle's background color.
     *
     * @param areaColor background color to be set.
     */
    public void setAreaColor(Color areaColor) {
        this.areaColor = areaColor;
        fireChanged();
    }

    /**
     * Changes this {@code Circle} with given {@code c} object.
     *
     * @param c circle to be copied.
     */
    public void changeFilledCircle(FilledCircle c) {
        this.areaColor = c.areaColor;
        changeCircle(c);
        fireChanged();
    }

    @Override
    public String toString() {
        return String.format("Filled circle (%d,%d), %d, #%s",
                center.x, center.y, radius, areaColorString());
    }

    /**
     * Gets background color as string.
     *
     * @return background color.
     */
    protected String areaColorString() {
        StringBuilder sb = new StringBuilder();

        sb.append(toHex(areaColor.getRed()));
        sb.append(toHex(areaColor.getGreen()));
        sb.append(toHex(areaColor.getBlue()));

        return sb.toString();
    }

    /**
     * Coverts given {@code n} to hex.
     *
     * @param n integer to be converted to hex.
     * @return converted integer.
     */
    private String toHex(int n) {
        StringBuilder sb = new StringBuilder();

        sb.append(Integer.toString(n, 16));
        if (sb.length() == 1) {
            sb.insert(0, "0");
        }
        return sb.toString();
    }

    @Override
    public void accept(GeometricalObjectVisitor v) {
        v.visit(this);
    }

    @Override
    public GeometricalObjectEditor createGeometricalObjectEditor() {
        return new FilledCircleEditor(this);
    }
}
