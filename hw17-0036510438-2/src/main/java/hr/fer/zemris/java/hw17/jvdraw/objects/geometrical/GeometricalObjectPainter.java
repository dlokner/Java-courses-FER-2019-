package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical;

import java.awt.*;

/**
 * Visitor that paints visited objects.
 *
 * @author Domagoj Lokner
 */
public class GeometricalObjectPainter implements GeometricalObjectVisitor {

    /**
     * Object that will paint visited objects.
     */
    private Graphics2D g2d;

    /**
     * Constructs {@code GeometricalObjectPainter}.
     *
     * @param g2d object that will paint all visited objects.
     */
    public GeometricalObjectPainter(Graphics2D g2d) {
        this.g2d = g2d;
    }

    @Override
    public void visit(Line line) {
        Color c = g2d.getColor();
        g2d.setColor(line.getColor());
        g2d.drawLine(
                line.getStart().x, line.getStart().y,
                line.getEnd().x, line.getEnd().y
        );
        g2d.setColor(c);
    }

    @Override
    public void visit(Circle circle) {
        Color c = g2d.getColor();
        g2d.setColor(circle.getOutlineColor());
        g2d.drawOval(circle.getCenter().x - circle.getRadius(),
                circle.getCenter().y - circle.getRadius(),
                circle.getRadius() * 2 - 1,
                circle.getRadius() * 2 - 1);
        g2d.setColor(c);
    }

    @Override
    public void visit(FilledCircle filledCircle) {
        Color c = g2d.getColor();

        g2d.setColor(filledCircle.getAreaColor());
        g2d.fillOval(filledCircle.getCenter().x - filledCircle.getRadius(),
                filledCircle.getCenter().y - filledCircle.getRadius(),
                filledCircle.getRadius() * 2 - 1,
                filledCircle.getRadius() * 2 - 1);

        g2d.setColor(filledCircle.getOutlineColor());
        g2d.drawOval(filledCircle.getCenter().x - filledCircle.getRadius(),
                filledCircle.getCenter().y - filledCircle.getRadius(),
                filledCircle.getRadius() * 2 - 1,
                filledCircle.getRadius() * 2 - 1);

        g2d.setColor(c);
    }

    /**
     * Sets painting object.
     *
     * @param g2d painting object.
     */
    public void setG2d(Graphics2D g2d) {
        this.g2d = g2d;
    }
}
