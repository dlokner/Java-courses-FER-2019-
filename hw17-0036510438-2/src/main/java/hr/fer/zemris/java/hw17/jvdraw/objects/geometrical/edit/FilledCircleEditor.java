package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit;

import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.FilledCircle;

import javax.swing.*;

/**
 * Editor for {@code FilledCircle} {@code GeometricalObject}.
 *
 * @author Domagoj Lokner
 */
public class FilledCircleEditor extends CircleEditor {

	private static final long serialVersionUID = -6159807968995388667L;

	/**
     * Input for circles background color.
     */
    private ColorInput areaColor;

    /**
     * Object that will be edited by this class.
     */
    private FilledCircle filledCircle;

    /**
     * Edited version of {@code filledCircle}.
     */
    private FilledCircle editedCircle;

    /**
     * Constructs {@code FilledCircleEditor}.
     *
     * @param circle object that will be edited by this object.
     */
    public FilledCircleEditor(FilledCircle circle) {
        super(circle);
        filledCircle = circle;

        areaColor = new ColorInput("Background color: ", circle.getAreaColor());
        areaColor.setBorder(BorderFactory.createTitledBorder("background color"));

        add(areaColor);
    }

    @Override
    public void checkEditing() {
        super.checkEditing();
        editedCircle = new FilledCircle(super.editedCircle, areaColor.getColor());
    }

    @Override
    public void acceptEditing() {
        if (editedCircle == null) {
            throw new UnsupportedOperationException();
        }
        filledCircle.changeFilledCircle(editedCircle);
    }
}
