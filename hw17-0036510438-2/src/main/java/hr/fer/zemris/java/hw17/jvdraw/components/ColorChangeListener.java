package hr.fer.zemris.java.hw17.jvdraw.components;

import java.awt.*;

/**
 * Implements object that listen to changing of color.
 */
public interface ColorChangeListener {

    /**
     * Method that indicate change of color.
     *
     * @param source color provider
     * @param oldColor old color
     * @param newColor new color
     */
    void newColorSelected(IColorProvider source, Color oldColor, Color newColor);
}
