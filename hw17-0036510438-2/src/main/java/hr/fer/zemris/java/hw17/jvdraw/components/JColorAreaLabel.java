package hr.fer.zemris.java.hw17.jvdraw.components;

import javax.swing.*;
import java.awt.*;

/**
 * Label that displays foreground and background colors' RGB-components.
 *
 * @author Domagoj Lokner
 */
public class JColorAreaLabel extends JLabel {

	private static final long serialVersionUID = 6156671880731213295L;

	/**
     * Foreground color provider
     */
    @SuppressWarnings("unused")
	private IColorProvider fgColorArea;

    /**
     * Background color provider
     */
    @SuppressWarnings("unused")
	private IColorProvider bgColorArea;

    /**
     * Format of foreground color
     */
    private static final String fgFormat = "Foreground color: (%d, %d, %d)";

    /**
     * Format of background color
     */
    private static final String  bgFormat = "background color: (%d, %d, %d)";

    /**
     * Currently displayed text for foreground color
     */
    private String fgText = "";

    /**
     * Currently displayed text for background color
     */
    private String bgText = "";


    /**
     * Constructs {@code JColorAreaLabel}.
     *
     * @param fgColorArea foreground color provider.
     * @param bgColorArea background color provider.
     */
    public JColorAreaLabel(IColorProvider fgColorArea, IColorProvider bgColorArea) {
        this.fgColorArea = fgColorArea;
        this.bgColorArea = bgColorArea;

        fgColorArea.addColorChangeListener((c, o, n) -> setFgText(n));
        bgColorArea.addColorChangeListener((c, o, n) -> setBgText(n));

        setTexts(
                fgColorArea.getCurrentColor(),
                bgColorArea.getCurrentColor()
        );
        setLabelText();
    }

    /**
     * Sets label text.
     *
     * @param fgColor foreground color.
     * @param bgColor background color.
     */
    private void setTexts(Color fgColor, Color bgColor) {
        setFgText(fgColor);
        setBgText(bgColor);
    }

    /**
     * Sets text of label.
     */
    private void setLabelText() {
        setText(fgText + ", " + bgText);
    }

    /**
     * Sets foreground color.
     *
     * @param c color to be set.
     */
    private void setFgText(Color c) {
        fgText = String.format(fgFormat, c.getRed(), c.getGreen(), c.getBlue());
        setLabelText();
    }

    /**
     * Sets background color.
     *
     * @param c color to be set.
     */
    private void setBgText(Color c) {
        bgText = String.format(bgFormat, c.getRed(), c.getGreen(), c.getBlue());
        setLabelText();
    }
}
