package hr.fer.zemris.java.hw17.jvdraw.components;

import hr.fer.zemris.java.hw17.jvdraw.objects.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.objects.DrawingModelListener;
import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.GeometricalObject;

import javax.swing.*;

/**
 * List of {@code GeometricalObject}s defined by {@code DrawingModel}.
 * This class is used as adapter to {@code DrawingModel}.
 *
 * @author Domagoj Lokner
 */
public class DrawingObjectListModel extends AbstractListModel<GeometricalObject> {

	private static final long serialVersionUID = -7489805525850612789L;
	
	/**
     * Drawing model this class adapt.
     */
    DrawingModel model;

    /**
     * Constructs {@code DrawingObjectListModel}.
     *
     * @param model drawing model thiss object adapt.
     */
    public DrawingObjectListModel(DrawingModel model) {
        this.model = model;
        model.addDrawingModelListener(dmListener);
    }

    @Override
    public int getSize() {
        return model.getSize();
    }

    @Override
    public GeometricalObject getElementAt(int index) {
        return  model.getObject(index);
    }


    /**
     * Definition of {@code DrawingModelListener}.
     */
    private DrawingModelListener dmListener = new DrawingModelListener() {
        @Override
        public void objectsAdded(DrawingModel source, int index0, int index1) {
            fireIntervalAdded(this, index0, index1);
        }

        @Override
        public void objectsRemoved(DrawingModel source, int index0, int index1) {
            fireIntervalRemoved(this, index0, index1);
        }

        @Override
        public void objectsChanged(DrawingModel source, int index0, int index1) {
            fireIntervalRemoved(this, index0, index1);
        }
    };

}
