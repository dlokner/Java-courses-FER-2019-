package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical;

/**
 * Indicates error in {@code JVDParser}.
 *
 * @author Domagoj Lokner
 */
public class JVDParserException extends RuntimeException {

	private static final long serialVersionUID = -4545218088524967175L;

	/**
     * Constructs {@code JVDParserException}.
     */
    public JVDParserException() {
    }

    /**
     * Constructs {@code JVDParserException} with
     * specified detail message about error.
     *
     * @param message error details message.
     */
    public JVDParserException(String message) {
        super(message);
    }

    /**
     * Constructs {@code JVDParserException} with specified detail
     * message about error and throwable object that indicated error.
     *
     * @param message error details message.
     * @param cause throwable object that indicated error
     */
    public JVDParserException(String message, Throwable cause) {
        super(message, cause);
    }
}
