package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit;

import hr.fer.zemris.java.hw17.jvdraw.components.JColorArea;

import javax.swing.*;
import java.awt.*;

/**
 * Swing component that represents color input.
 *
 * @author Domagoj Lokner
 */
public class ColorInput extends JPanel {

	private static final long serialVersionUID = 3152966557819390216L;

	/**
     * Color area object.
     */
    private JColorArea color;

    /**
     * Constructs {@code ColorInput}.
     *
     * @param text component's text.
     * @param color color that will be selected as current in the
     *              moment of construction.
     */
    public ColorInput(String text, Color color) {

        this.color = new JColorArea(color);
        this.color.setSize(30, 30);

        add(new JLabel(text + ": "));
        add(this.color);
    }

    /**
     * Gets color of the component.
     *
     * @return currently selected color.
     */
    public Color getColor() {
        return color.getCurrentColor();
    }
}
