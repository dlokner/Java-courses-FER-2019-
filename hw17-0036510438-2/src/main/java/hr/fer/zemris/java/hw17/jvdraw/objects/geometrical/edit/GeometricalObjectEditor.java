package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit;

import javax.swing.*;

/**
 * Swing component that represents {@code GeometricalObject} editor dialog.
 *
 * @author Domagoj Lokner
 */
public abstract class GeometricalObjectEditor extends JPanel {

	private static final long serialVersionUID = -3843783616417337601L;
	
	/**
	  * Checks if editing is legal.
	  */
	 public abstract void checkEditing();
	
	 /**
	  * Changes objects components.
	  */
	 public abstract void acceptEditing();
}