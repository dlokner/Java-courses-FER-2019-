package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit;

import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.Circle;

import javax.swing.*;
import java.awt.*;

/**
 * Editor for {@code Circle} {@code GeometricalObject}.
 *
 * @author Domagoj Lokner
 */
public class CircleEditor extends GeometricalObjectEditor {

	private static final long serialVersionUID = 8819412113126884661L;

	/**
     * Circle that will be edited by this object.
     */
    protected Circle circle;

    /**
     * Edited version of {@code circle}.
     */
    protected Circle editedCircle;

    /**
     * Input component for circle center point.
     */
    protected PointInput centerInput;

    /**
     * Input component for circle's radius length.
     */
    protected JTextField radiusInput;

    /**
     * Input for outline color of circle.
     */
    protected ColorInput outlineColor;


    /**
     * Constructs {@code CircleEditor}.
     *
     * @param circle circle that will be edited by this class.
     */
    public CircleEditor(Circle circle) {
        this.circle = circle;
        initGUI();
    }

    /**
     * Initialize editors GUI.
     */
    private void initGUI() {

        setLayout(new GridLayout(0, 1));

        //INPUTS INIT
        centerInput = new PointInput(circle.getCenter().x, circle.getCenter().y);

        radiusInput = new JTextField(Integer.toString(circle.getRadius()), 5);
        JPanel radius = new JPanel(new FlowLayout());
        radius.add(new JLabel("Radius: "));
        radius.add(radiusInput);

        outlineColor = new ColorInput("Outline color: ", circle.getOutlineColor());

        //BORDERS
        centerInput.setBorder(BorderFactory.createTitledBorder("center point of the circle"));
        radius.setBorder(BorderFactory.createTitledBorder("circle radius"));
        outlineColor.setBorder(BorderFactory.createTitledBorder("outline color"));

        add(centerInput);
        add(radius);
        add(outlineColor);
    }

    @Override
    public void checkEditing() {
        Point center;
        int radius;

        try {
            center = new Point(
                    Integer.parseInt(centerInput.xText()),
                    Integer.parseInt(centerInput.yText())
            );
            radius = Integer.parseInt(radiusInput.getText());
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException(ex);
        }

        editedCircle = new Circle(center, radius, outlineColor.getColor());
    }

    @Override
    public void acceptEditing() {
        if (editedCircle == null) {
            throw new UnsupportedOperationException();
        }
        circle.changeCircle(editedCircle);
    }
}
