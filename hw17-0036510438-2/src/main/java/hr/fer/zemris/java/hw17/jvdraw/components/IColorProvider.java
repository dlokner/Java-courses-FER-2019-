package hr.fer.zemris.java.hw17.jvdraw.components;

import java.awt.*;

/**
 * Object that provides color.
 *
 * @author Domagoj Lokner
 */
public interface IColorProvider {

    /**
     * Gets current color.
     *
     * @return current color.
     */
    Color getCurrentColor();

    /**
     * Adds {@code ColorChangeListener}.
     *
     * @param l listener to be added
     */
    void addColorChangeListener(ColorChangeListener l);

    /**
     * Removes {@code ColorChangeListener}.
     *
     * @param l listener to be removed.
     */
    void removeColorChangeListener(ColorChangeListener l);
}
