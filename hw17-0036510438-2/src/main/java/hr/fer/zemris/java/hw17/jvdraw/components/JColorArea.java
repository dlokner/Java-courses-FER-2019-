package hr.fer.zemris.java.hw17.jvdraw.components;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Swing component that color chooser button.
 *
 * @author Domagoj Lokner
 */
public class JColorArea extends JComponent implements IColorProvider {

	private static final long serialVersionUID = -7597996280428877523L;

	/**
     * Currently selected color
     */
    private Color selectedColor;

    /**
     * List of {@code ColorChangeListener} listeners.
     */
    private List<ColorChangeListener> listeners;

    /**
     * Constructs {@code JColorArea}.
     *
     * @param color color to be set as current color of
     *              this component in moment of construction.
     */
    public JColorArea(Color color) {
        this.selectedColor = color;
        setBorder(BorderFactory.createLineBorder(Color.BLACK));

        addMouseListener(new MouseAdapter() {
            @SuppressWarnings("static-access")
			@Override
            public void mouseClicked(MouseEvent e) {
                JColorChooser choser = new JColorChooser(selectedColor);
                Color oldColor = selectedColor;
				Color newColor = choser.showDialog(getRootPane(), "Select color", selectedColor);

                if (newColor != null) {
                    selectedColor = newColor;
                }

                if(!oldColor.equals(selectedColor)) {
                    fire(oldColor, selectedColor);
                }
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(selectedColor);
        g.fillRect(0, 0, getWidth(), getHeight());
    }

    private void fire(Color oldColor, Color newColor) {
        repaint();

        if (Objects.isNull(listeners)) {
            return;
        }

        for (ColorChangeListener l : listeners) {
            l.newColorSelected(this, oldColor, newColor);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(15, 15);
    }


    @Override
    public Color getCurrentColor() {
        return new Color(selectedColor.getRGB());
    }

    @Override
    public void addColorChangeListener(ColorChangeListener l) {
        if (Objects.isNull(listeners)) {
            listeners = new LinkedList<>();
        }
        listeners.add(l);
    }

    @Override
    public void removeColorChangeListener(ColorChangeListener l) {
        listeners.remove(l);
    }
}
