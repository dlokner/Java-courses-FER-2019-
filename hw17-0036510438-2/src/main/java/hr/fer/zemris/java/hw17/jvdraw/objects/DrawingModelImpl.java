package hr.fer.zemris.java.hw17.jvdraw.objects;

import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.GeometricalObject;
import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.GeometricalObjectListener;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Default implementation of {@code DrawingModel}.
 *
 * @author Domagoj Lokner
 */
public class DrawingModelImpl implements DrawingModel {

    /**
     * List of objects this model contains.
     */
    private List<GeometricalObject> objects;

    /**
     * Flag that indicates modification in this model.
     */
    private boolean modified;

    /**
     * List of all registered listeners.
     */
    private List<DrawingModelListener> listeners;

    /**
     * Constructs {@code DrawingModelImpl}.
     */
    public DrawingModelImpl() {
        this.objects = new LinkedList<>();
    }

    @Override
    public int getSize() {
        return objects.size();
    }

    @Override
    public GeometricalObject getObject(int index) {
        return objects.get(index);
    }

    @Override
    public void add(GeometricalObject object) {
        objects.add(object);
        object.addGeometricalObjectListener(objChangedListener);
        setModified();
        fireAdded(objects.size()-1, objects.size()-1);
    }

    @Override
    public void remove(GeometricalObject object) {
        int index = objects.indexOf(object);

        if (index == -1) {
            return;
        }

        objects.remove(object);
        setModified();
        fireRemoved(index, index);
    }

    @Override
    public void changeOrder(GeometricalObject object, int offset) {
        int index = objects.indexOf(object);

        if (index == -1) {
            return;
        }

        int insIndex = index + offset;

        if (insIndex < 0 || insIndex > objects.size() - 1) {
            return;
        }

        objects.remove(index);
        objects.add(insIndex, object);
        setModified();
        fireChanged(index, index);
    }

    @Override
    public int indexOf(GeometricalObject object) {
        return objects.indexOf(object);
    }

    @Override
    public void clear() {
        objects.clear();
        setModified();
    }

    @Override
    public void clearModifiedFlag() {
        modified = false;
    }

    @Override
    public boolean isModified() {
        return modified;
    }

    @Override
    public void addDrawingModelListener(DrawingModelListener l) {
        if (Objects.isNull(listeners)) {
            listeners = new LinkedList<>();
        }
        listeners.add(l);
    }

    @Override
    public void removeDrawingModelListener(DrawingModelListener l) {
        if (Objects.isNull(listeners)) {
            return;
        }
        listeners.remove(l);
    }

    /**
     * Fire information to all listeners that objects are added.
     *
     * @param index0 starting index of added objects to list.
     * @param index1 ending index of added objects to list.
     */
    private void fireAdded(int index0, int index1) {
        if (!Objects.isNull(listeners)) {
            for (DrawingModelListener l : listeners) {
                l.objectsAdded(this, index0, index1);
            }
        }
    }

    /**
     * Fire information to all listeners that objects are removed from model.
     *
     * @param index0 index of first removed object.
     * @param index1 index of last removed object.
     */
    private void fireRemoved(int index0, int index1) {
        if (!Objects.isNull(listeners)) {
            for (DrawingModelListener l : listeners) {
                l.objectsRemoved(this, index0, index1);
            }
        }
    }

    /**
     * Fire information to all listeners that
     * objects from this model are changed.
     *
     * @param index0 index of first changed object.
     * @param index1 index of last changed object.
     */
    private void fireChanged(int index0, int index1) {
        if (!Objects.isNull(listeners)) {
            for (DrawingModelListener l : listeners) {
                l.objectsChanged(this, index0, index1);
            }
        }
    }

    /**
     * Sets modification flag to true.
     */
    private void setModified() {
        modified = true;
    }

    /**
     * Listener object that will be added to all objects
     * from this model.
     */
    private GeometricalObjectListener objChangedListener = o -> {
        int index = indexOf(o);

        if (index == -1) {
            return;
        }

        fireChanged(index, index);
    };
}
