package hr.fer.zemris.java.hw17.jvdraw.objects;

/**
 * Class that implements object capable to listen on
 * changes in {@code DrawingModel}.
 *
 * @author Domagoj Lokner
 */
public interface DrawingModelListener {

 /**
  * Action that will be performed when adding to model is registered.
  *
  * @param index0 index of first added object.
  * @param index1 index of last added object.
  */
 void objectsAdded(DrawingModel source, int index0, int index1);

 /**
  * Action that will be performed when removing from model is registered.
  *
  * @param index0 index of first removed object.
  * @param index1 index of last removed object.
  */
 void objectsRemoved(DrawingModel source, int index0, int index1);

 /**
  * Action that will be performed when change of objects is registered in model.
  *
  * @param index0 index of first changed object.
  * @param index1 index of last changed object.
  */
 void objectsChanged(DrawingModel source, int index0, int index1);
}