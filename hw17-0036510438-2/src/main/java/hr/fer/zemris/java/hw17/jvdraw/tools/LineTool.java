package hr.fer.zemris.java.hw17.jvdraw.tools;

import hr.fer.zemris.java.hw17.jvdraw.components.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.hw17.jvdraw.objects.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.Line;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Line drawing tool.
 *
 * @author Domagoj Lokner
 */
public class LineTool extends ToolAdapter {

    /**
     * Flag that indicates that mouse was clicked.
     */
    boolean clicked = false;

    /**
     * Starting point of the line.
     */
    private Point start;

    /**
     * ending point of the line.
     */
    private Point end;

    /**
     * Model to which this line will be
     * added when drawing is completed.
     */
    private DrawingModel model;

    /**
     * Foreground color provider.
     */
    private IColorProvider fg;

    /**
     * Background color provider.
     */
    @SuppressWarnings("unused")
	private IColorProvider bg;

    /**
     * Canvas on which this tool will draw.
     */
    private JDrawingCanvas canvas;

    /**
     * Constructs {@code LineTool}
     *
     * @param model model to which this line will be added when drawing is completed.
     * @param fg foreground color provider.
     * @param bg background color provider.
     * @param canvas canvas on which this tool will draw.
     */
    public LineTool(DrawingModel model, IColorProvider fg, IColorProvider bg, JDrawingCanvas canvas) {
        this.model = model;
        this.fg = fg;
        this.bg = bg;
        this.canvas = canvas;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (!clicked) {
            start = end = e.getPoint();
            clicked = true;
        } else {
            end = e.getPoint();
            clicked = false;

            Line line = new Line(start, end, fg.getCurrentColor());
            model.add(line);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (clicked) {
            end = e.getPoint();
            canvas.repaint();
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        if(clicked) {
            Color c = g2d.getColor();
            g2d.setColor(fg.getCurrentColor());
            g2d.drawLine(start.x, start.y, end.x, end.y);
            g2d.setColor(c);
        }
    }
}
