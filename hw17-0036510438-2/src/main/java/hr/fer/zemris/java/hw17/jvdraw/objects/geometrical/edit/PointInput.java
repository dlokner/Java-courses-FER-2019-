package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit;

import javax.swing.*;
import java.awt.*;

/**
 * Swing component represents input
 * for single integer point (x, y)
 *
 * @author Domagoj Lokner
 */
public class PointInput extends JPanel {

	private static final long serialVersionUID = -8885440728229505375L;

	/**
     * x coordinate
     */
    @SuppressWarnings("unused")
	private int x;

    /**
     * y coordinate
     */
    @SuppressWarnings("unused")
	private int y;

    /**
     * Field for x coordinate input
     */
    private JTextField xField;

    /**
     * Field for y coordinate input
     */
    private JTextField yField;

    /**
     * Constructs {@code PointInput}.
     *
     * @param x x coordinate to be set on component.
     * @param y y coordinate to be set on component.
     */
    public PointInput(int x, int y) {
        this.x = x;
        this.y = y;
        this.xField = new JTextField(Integer.toString(x), 5);
        this.yField = new JTextField(Integer.toString(y), 5);

        setLayout(new FlowLayout());

        initGUI();
    }

    /**
     * Initialize this component's GUI.
     */
    private void initGUI() {
        JLabel xLabel = new JLabel("x coordinate: ");
        JLabel yLabel = new JLabel("y coordinate: ");

        add(xLabel);
        add(xField);
        add(yLabel);
        add(yField);
    }

    /**
     * Gets x coordinate text.
     *
     * @return x coordinate.
     */
    public String xText() {
        return xField.getText();
    }

    /**
     * Gets y coordinate text.
     *
     * @return y coordinate.
     */
    public String yText() {
        return yField.getText();
    }
}
