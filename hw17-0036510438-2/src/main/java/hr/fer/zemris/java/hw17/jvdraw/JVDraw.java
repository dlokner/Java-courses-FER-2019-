package hr.fer.zemris.java.hw17.jvdraw;

import hr.fer.zemris.java.hw17.jvdraw.components.*;
import hr.fer.zemris.java.hw17.jvdraw.objects.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.objects.DrawingModelImpl;
import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.*;
import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.edit.GeometricalObjectEditor;
import hr.fer.zemris.java.hw17.jvdraw.tools.CircleTool;
import hr.fer.zemris.java.hw17.jvdraw.tools.FilledCircleTool;
import hr.fer.zemris.java.hw17.jvdraw.tools.LineTool;
import hr.fer.zemris.java.hw17.jvdraw.tools.Tool;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Enumeration;

/**
 * Program that offers user GUI in which circles and lines
 * can be drawn by pointer.
 *
 * <ul>
 *  <li>Line is defined by two clicks (starting and ending point).</li>
 *  <li>Circle's center is defined with first click, second click will
 *  determine radius length.</li>
 * </ul>
 *
 * @author Domagoj Lokner
 *
 */
public class JVDraw extends JFrame {

	private static final long serialVersionUID = -8602753234564144375L;

	/**
     * Currently selected drawing tool.
     */
    private Tool currentTool;

    /**
     * Drawing model that is used in this program.
     */
    private DrawingModel model;

    /**
     * Drawing canvas.
     */
    private JDrawingCanvas canvas;

    /**
     * Path to currently opened drawing.
     * {@code null} if current drawing is not already saved.
     */
    private Path filePath;

    /**
     * Constructs {@code JVDraw}.
     */
    public JVDraw() {
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setLocation(20, 20);
        setSize(1500, 1000);
        setTitle("JVDraw");

        initGUI();
    }

    /**
     * Initialize GUI.
     */
    private void initGUI() {
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        model = new DrawingModelImpl();
        canvas = new JDrawingCanvas(model, () -> currentTool);

        createMenus();
        cp.add(createToolBar(), BorderLayout.NORTH);

        cp.add(createList(), BorderLayout.EAST);

        cp.add(canvas, BorderLayout.CENTER);


        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                exit();
            }
        });
    }

    /**
     * Creates list of geometrical objects defined in {@code model}.
     * This list will change order of objects by clicking on <i>+</i>
     * and <i>-</i> key.
     * Double click on list object will open object's editor dialog.
     *
     * @return list of objects.
     */
    private JComponent createList() {

        DrawingObjectListModel GOList = new DrawingObjectListModel(model);
        JList<GeometricalObject> list = new JList<>(GOList);

        // mouse listener that checks for double click and opens editor
        list.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() != 2) {
                    return;
                }

                GeometricalObjectEditor editor = list.getSelectedValue().createGeometricalObjectEditor();

                if (JOptionPane.showConfirmDialog(JVDraw.this, editor, "Edit", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
                    try {
                        editor.checkEditing();
                        editor.acceptEditing();
                    } catch (IllegalArgumentException ex) {
                        JOptionPane.showMessageDialog(JVDraw.this, "Invalid arguments!", "Warning",JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
        });

        // action for + and - keys
        list.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyChar() == '+' || e.getKeyChar() == '-') {
                    int index = list.getSelectedIndex();
                    int offset = e.getKeyChar() == '-' ? -1 : 1;

                    model.changeOrder(list.getSelectedValue(), offset);
                    list.setSelectedIndex(index + offset);
                }
            }
        });

        // action for "del" key
        list.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    int indexToSelect = list.getSelectedIndex()-1;
                    model.remove(list.getSelectedValue());
                    list.setSelectedIndex(indexToSelect < 0 ? 0 : indexToSelect);
                }
            }
        });

        return new JScrollPane(list);
    }

    /**
     * Starting method of the program.
     *
     * @param args - command line arguments.
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new JVDraw().setVisible(true));
    }

    /**
     * This method creates menu.
     * Menu offers open, save, save as, and export option.
     */
    private void createMenus() {
        JMenuBar mb = new JMenuBar();

        JMenu file = new JMenu("File");
        mb.add(file);

        file.add(OPEN);
        file.addSeparator();
        file.add(SAVE);
        file.add(SAVE_AS);
        file.addSeparator();
        file.add(EXPORT);
        file.addSeparator();
        file.add(EXIT);

        setJMenuBar(mb);
    }

    /**
     * Creates tool bar.
     *
     * @return created tool bar.
     */
    private JToolBar createToolBar() {

        JPanel toolBarPanel = new JPanel();
        toolBarPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        JColorArea fgColorArea = new JColorArea(Color.RED);
        JColorArea bgColorArea = new JColorArea(Color.BLUE);

        toolBarPanel.add(fgColorArea);
        toolBarPanel.add(bgColorArea);

        addBottomBar(fgColorArea, bgColorArea);

        // drawing tool buttons
        ButtonGroup toolButtons = createToolButtons(fgColorArea, bgColorArea);
        Enumeration<AbstractButton> buttons = toolButtons.getElements();
        while (buttons.hasMoreElements()) {
            toolBarPanel.add(buttons.nextElement());
        }


        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(true);
        toolBar.add(toolBarPanel);

        return toolBar;
    }

    /**
     * Creates group of drawing tool toggle buttons.
     *
     * @param fgColorProvider foreground color provider.
     * @param bgColorProvider background color provider.
     * @return group of drawing tool buttons.
     */
    private ButtonGroup createToolButtons(IColorProvider fgColorProvider,
                                             IColorProvider bgColorProvider) {
        ButtonGroup bg = new ButtonGroup();

        JToggleButton line = new JToggleButton(new AbstractAction() {

			private static final long serialVersionUID = -5578085974874474502L;

			@Override
            public void actionPerformed(ActionEvent e) {
                currentTool = new LineTool(model, fgColorProvider, bgColorProvider, canvas);
            }
        });
        JToggleButton circle = new JToggleButton(new AbstractAction() {

			private static final long serialVersionUID = -8554030038724628689L;

			@Override
            public void actionPerformed(ActionEvent e) {
                currentTool = new CircleTool(model, fgColorProvider, bgColorProvider, canvas);
            }
        });
        JToggleButton filledCircle = new JToggleButton(new AbstractAction() {

			private static final long serialVersionUID = -9059441570089473821L;

			@Override
            public void actionPerformed(ActionEvent e) {
                currentTool = new FilledCircleTool(model, fgColorProvider, bgColorProvider, canvas);
            }
        });

        line.setText("Line");
        circle.setText("Circle");
        filledCircle.setText("Filled Circle");

        bg.add(line);
        bg.add(circle);
        bg.add(filledCircle);

        // sets line as current tool
        line.getAction().actionPerformed(null);
        line.setSelected(true);

        return bg;
    }

    /**
     * Addes button bar that displays RGB elements of foreground and
     * background colors.
     *
     * @param fgColorArea foreground color provider.
     * @param bgColorArea background color provider.
     */
    private void addBottomBar(JColorArea fgColorArea, JColorArea bgColorArea) {
        JColorAreaLabel bottomColorInfo = new JColorAreaLabel(fgColorArea, bgColorArea);
        getContentPane().add(bottomColorInfo, BorderLayout.SOUTH);
    }


    /*
             =========================
                     ACTIONS
             =========================
     */

    /**
     * Saves current drawing.
     */
    private final Action SAVE = new AbstractAction() {

		private static final long serialVersionUID = 9152644422233607283L;
		{
            putValue(NAME, "Save");
            putValue(SHORT_DESCRIPTION, "Saves current drawing");
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            save();
        }
    };

    /**
     * Opens drawing saved in .jvd file.
     */
    private final Action OPEN = new AbstractAction() {

		private static final long serialVersionUID = 8423451021877667503L;
		{
            putValue(NAME, "Open");
            putValue(SHORT_DESCRIPTION, "Opens *.jvd file");
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            open();
        }
    };

    /**
     * Opens Save As drawing dialog.
     */
    private final Action SAVE_AS = new AbstractAction() {

		private static final long serialVersionUID = 3715382565443345795L;
		{
            putValue(NAME, "Save As...");
            putValue(SHORT_DESCRIPTION, "Saves current drawing as...");
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            saveModelAs();
        }
    };

    /**
     * Program exit action.
     */
    private final Action EXIT = new AbstractAction() {

		private static final long serialVersionUID = -83481833466420513L;
		{
            putValue(NAME, "Exit");
            putValue(SHORT_DESCRIPTION, "Exit program");
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            exit();
        }
    };

    /**
     * Exports file as .png, .jpg or .gif file.
     */
    private final Action EXPORT = new AbstractAction() {

		private static final long serialVersionUID = -5646512628170369692L;
		{
            putValue(NAME, "Export");
            putValue(SHORT_DESCRIPTION, "Export drawing");
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            export();
        }
    };

    /*
            ======================================
            Methods used in definitions of actions
            ======================================
     */

    /**
     * Exits program. Saving dialog will be displayed if
     * currently opened document is not already saved.
     */
    private void exit() {
        if (!model.isModified()) {
            return;
        }

        int status = JOptionPane.showOptionDialog(this,
                        "Do you want to save drawing before exit?",
                        "Drawing is not saved",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.WARNING_MESSAGE,
                        null, null, null);
        
        if(status == JOptionPane.NO_OPTION) {
        	dispose();
        } else if (status == JOptionPane.OK_OPTION) {
        	save();
        	dispose();
        } else {
        	return;
        }   
    }

    /**
     * Saves current document on {@code filePath}.
     * If {@code filePath} is {@code null} <i>Save As</i>
     * option will be called.
     */
    private void save() {
        if (!model.isModified() && model.getSize() != 0) {
            JOptionPane.showMessageDialog(this,
                    "Drawing is already saved",
                    "Info",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        if (filePath == null) {
            saveModelAs();
        } else {
            saveModel(filePath);
        }
    }

    /**
     * Opens .jvd file from disk.
     */
    private void open() {
        JFileChooser jfc = new JFileChooser();
        jfc.setDialogTitle("Open");
        jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfc.setFileFilter(new FileNameExtensionFilter(".jvd","jvd"));

        if (jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            openInCurrentModel(jfc.getSelectedFile().toPath());
        }
    }

    /**
     * Opens .jvd file in current {@code DrawingModel}.
     *
     * @param filePath path to .jvd file to be opened in {@code model}.
     */
    private void openInCurrentModel(Path filePath) {
        JVDParser parser;
        try {
            parser = new JVDParser(Files.readAllLines(filePath));
        } catch (IOException ex) {
            showErrorDialog("Error occurred during opening file: " + filePath.getFileName().toString(), this);
            return;
        } catch (JVDParserException ex) {
            showErrorDialog(ex.getMessage(), this);
            return;
        }

        model.clear();

        for (GeometricalObject o : parser.getObjects()) {
            model.add(o);
        }
        model.clearModifiedFlag();
        this.filePath = filePath;
    }

    /**
     * Exports file as .png, .jpg or .gif file.
     */
    private void export() {
        JFileChooser jfc = new JFileChooser();
        jfc.setDialogTitle("Export");
        jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfc.setFileFilter(new FileNameExtensionFilter("Extensions: ","png", "jpg", "gif"));

        if (jfc.showSaveDialog(this) != JFileChooser.APPROVE_OPTION) {
            return;
        }

        File selected = jfc.getSelectedFile();

        String extension = selected.getName().substring(selected.getName().lastIndexOf('.') + 1);
        if(!checkExportExtensio(extension)) {
            JOptionPane.showOptionDialog(this, "Unsupported export file extension",
                    "Extension",
                    JOptionPane.OK_OPTION,
                    JOptionPane.WARNING_MESSAGE,
                    null, null, null);
            export();
            return;
        }
        exportTo(selected.toPath(), extension);
    }

    /**
     * Exports current drawing to file with {@code extension}
     * stored on {@code filePath}.
     *
     * @param filePath path of exported drawing.
     * @param extension extension of exported file.
     */
    private void exportTo(Path filePath, String extension) {
        GeometricalObjectBBCalculator bb = new GeometricalObjectBBCalculator();

        for (int i = 0; i < model.getSize(); ++i) {
            model.getObject(i).accept(bb);
        }

        Rectangle box = bb.getBoundingBox();
        BufferedImage image = new BufferedImage(
                box.width, box.height, BufferedImage.TYPE_3BYTE_BGR
        );
        Graphics2D g = image.createGraphics();
        g.translate(-box.x, -box.y);

        GeometricalObjectPainter painter = new GeometricalObjectPainter(g);
        for (int i = 0; i < model.getSize(); ++i) {
            model.getObject(i).accept(painter);
        }
        g.dispose();

        try {
            ImageIO.write(image, extension, filePath.toFile());
        } catch (IOException ex) {
            showErrorDialog("Error during image export", this);
            return;
        }

        JOptionPane.showMessageDialog(this, "Image is successfully exported");
    }

    /**
     * Checks if {@code extension} is supported.
     * Supported extensions are .jpg, .png and .gif
     *
     * @param extension file extension.
     * @return {@code true} if extension is supported otherwise {@code false}.
     */
    private boolean checkExportExtensio(String extension) {
        switch (extension) {
            case ("png"):
            case ("gif"):
            case ("jpg"):
                return true;
            default:
                return false;
        }
    }

    /**
     * Displays <i>Save As</i> dialog and saves current drawing
     * on selected file path.
     */
    private void saveModelAs() {
        JFileChooser jfc = new JFileChooser();
        jfc.setDialogTitle("Save As...");
        jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfc.setFileFilter(new FileNameExtensionFilter(".jvd","jvd"));

        if (jfc.showSaveDialog(this) != JFileChooser.APPROVE_OPTION) {
            JOptionPane.showMessageDialog(this,
                    "Saving is canceled", "Info",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        File selectedFile = jfc.getSelectedFile();

        // if file extension is not .jvd
        if (!selectedFile.getName().endsWith(".jvd")) {
            JOptionPane.showMessageDialog(this,
                    "Wrong file extension!", "Warning",
                    JOptionPane.WARNING_MESSAGE);
            saveModelAs();
            return;
        }

        saveModel(jfc.getSelectedFile().toPath());
    }

    /**
     * Saves drawing from current model to {@code filePath}.
     *
     * @param filePath path to file.
     */
    private void saveModel(Path filePath) {

        if (Files.isDirectory(filePath)) {
            JOptionPane.showMessageDialog(this,
                    "Saving is canceled", "Info",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        JVDFileCreator creator = new JVDFileCreator();

        for (int i = 0; i < model.getSize(); ++i) {
            model.getObject(i).accept(creator);
        }

        try {
            Files.write(filePath, creator.getLines());
        } catch (IOException ex) {
            showErrorDialog("Error occurred during saving file to: " + filePath.toString(), this);
            return;
        }

        this.filePath = filePath;
        model.clearModifiedFlag();
    }

    /**
     * Displays error dialog.
     *
     * @param message error message.
     * @param parent parent component.
     */
    private void showErrorDialog(String message, Component parent) {
        JOptionPane.showMessageDialog(parent,
                message,
                "Info",
                JOptionPane.ERROR_MESSAGE);
    }
}
