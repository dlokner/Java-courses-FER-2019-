package hr.fer.zemris.java.hw17.jvdraw.objects.geometrical;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

/**
 * {@code GeometricalObjectVisitor} that will creat list
 * of all visited objects in .jvd file format.
 *
 * <u>
 *     LINE x0 y0 x1 y1 red green blue
 *     CIRCLE centerx centery radius red green blue
 *     FCIRCLE centerx centery radius red green blue red green blue
 * </u>
 *
 * @author Domagoj Lokner
 */
public class JVDFileCreator implements GeometricalObjectVisitor {

    /**
     * List of lines that represents
     * visited objects.
     */
    private List<String> lines;

    /**
     * Format for {@code Line} object.
     */
    public static final String LINE_FORMAT = "LINE %d %d %d %d %d %d %d";

    /**
     * Format for {@code Circle} object.
     */
    public static final String CIRCLE_FORMAT = "CIRCLE %d %d %d %d %d %d";

    /**
     * Format for {@code FilledCircle} object.
     */
    public static final String FCIRCLE_FORMAT = "FCIRCLE %d %d %d %d %d %d %d %d %d";

    /**
     * Constructs {@code JVDFileCreator}.
     */
    public JVDFileCreator() {
        lines = new LinkedList<>();
    }

    @Override
    public void visit(Line line) {
        Point start = line.getStart();
        Point end = line.getEnd();
        Color color = line.getColor();

        String fileLine = String.format(LINE_FORMAT,
                start.x, start.y,
                end.x, end.y,
                color.getRed(), color.getGreen(), color.getBlue());
        lines.add(fileLine);
    }

    @Override
    public void visit(Circle circle) {
        Point center = circle.getCenter();
        int radius = circle.getRadius();
        Color color = circle.getOutlineColor();

        String fileLine = String.format(CIRCLE_FORMAT,
                center.x, center.y, radius,
                color.getRed(), color.getGreen(), color.getBlue());
        lines.add(fileLine);
    }

    @Override
    public void visit(FilledCircle filledCircle) {
        Point center = filledCircle.getCenter();
        int radius = filledCircle.getRadius();
        Color outline = filledCircle.getOutlineColor();
        Color area = filledCircle.getAreaColor();

        String fileLine = String.format(FCIRCLE_FORMAT,
                center.x, center.y, radius,
                outline.getRed(), outline.getGreen(), outline.getBlue(),
                area.getRed(), area.getGreen(), area.getBlue());
        lines.add(fileLine);
    }

    /**
     * Gets lines that represents all visited objects.
     *
     * @return list of objects.
     */
    public List<String> getLines() {
        return lines;
    }

    /**
     * Creates .jvd file content from all visited lines.
     *
     * @return .jvd file content.
     */
    public String createFileContent() {
        if (lines == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        String newLine = String.format("%n");

        for (String line : lines) {
            sb.append(line);
            sb.append(newLine);
        }

        return sb.toString();
    }
}
