package hr.fer.zemris.java.hw17.jvdraw.tools;

import hr.fer.zemris.java.hw17.jvdraw.components.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.hw17.jvdraw.objects.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.FilledCircle;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Tool for drawing filled circle objects.
 *
 * @author Domagoj Lokner
 */
public class FilledCircleTool extends CircleTool {

    /**
     * Constructs {@code FilledCircleTool}
     *
     * @param model model to which this line will be added when drawing is completed.
     * @param fg foreground color provider.
     * @param bg background color provider.
     * @param canvas canvas on which this tool will draw.
     */
    public FilledCircleTool(DrawingModel model, IColorProvider fg, IColorProvider bg, JDrawingCanvas canvas) {
        super(model, fg, bg, canvas);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (!clicked) {
            super.mouseClicked(e);
        } else {
            clicked = false;
            FilledCircle c = new FilledCircle(center, radius,
                    fg.getCurrentColor(), bg.getCurrentColor()
            );
            model.add(c);
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        if(clicked) {
            Color c = g2d.getColor();

            g2d.setColor(bg.getCurrentColor());
            g2d.fillOval(center.x - radius,
                    center.y - radius,
                    radius * 2 - 1,
                    radius * 2 - 1);

            g2d.setColor(fg.getCurrentColor());
            g2d.drawOval(center.x - radius,
                    center.y - radius,
                    radius * 2 - 1,
                    radius * 2 - 1);

            g2d.setColor(c);
        }
    }
}
