package hr.fer.zemris.java.hw17.jvdraw.tools;

import hr.fer.zemris.java.hw17.jvdraw.components.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.hw17.jvdraw.objects.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.Circle;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Tool for drawing circle geometrical objects.
 *
 * @author Domagoj Lokner
 */
public class CircleTool implements Tool {

    /**
     * Flag that indicates that mouse was clicked.
     */
    boolean clicked = false;

    /**
     * Center point of the circle.
     */
    protected Point center;

    /**
     * Length of the circle radius.
     */
    protected int radius;

    /**
     * Model to which this line will be
     * added when drawing is completed.
     */
    protected DrawingModel model;

    /**
     * Foreground color provider.
     */
    protected IColorProvider fg;

    /**
     * Background color provider.
     */
    protected IColorProvider bg;

    /**
     * Canvas on which this tool will draw.
     */
    protected JDrawingCanvas canvas;

    /**
     * Constructs {@code CircleTool}
     *
     * @param model model to which this line will be added when drawing is completed.
     * @param fg foreground color provider.
     * @param bg background color provider.
     * @param canvas canvas on which this tool will draw.
     */
    public CircleTool(DrawingModel model, IColorProvider fg, IColorProvider bg, JDrawingCanvas canvas) {
        this.model = model;
        this.fg = fg;
        this.bg = bg;
        this.canvas = canvas;
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (!clicked) {
            clicked = true;
            center = e.getPoint();
        } else {
            clicked = false;
            Circle c = new Circle(center, radius, fg.getCurrentColor());
            model.add(c);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (clicked) {
            radius = calcRadius(center, e.getPoint());
            canvas.repaint();
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void paint(Graphics2D g2d) {
        if(clicked) {
            Color c = g2d.getColor();
            g2d.setColor(fg.getCurrentColor());

            g2d.drawOval(center.x - radius,
                    center.y - radius,
                    radius * 2 - 1,
                    radius * 2 - 1);

            g2d.setColor(c);
        }
    }

    /**
     * Calculates circle radius.
     *
     * @param center center of the circle.
     * @param edge edge point of the circle.
     * @return length of the radius.
     */
    protected static int calcRadius(Point center, Point edge) {
        double rad = Math.sqrt(
                Math.pow(center.x-edge.x, 2) + Math.pow(center.y-edge.y, 2)
        );

        return (int)Math.round(rad);
    }
}
