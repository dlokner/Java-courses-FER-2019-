package hr.fer.zemris.java.hw17.jvdraw.objects;

import hr.fer.zemris.java.hw17.jvdraw.objects.geometrical.GeometricalObject;

/**
 * Object that represents scene of drawn {@code GeometricalObject}s.
 *
 * @author Domagoj Lokner
 */
public interface DrawingModel {

    /**
     * Gets number of objects in the model.
     *
     * @return number of the objects.
     */
    int getSize();

    /**
     * Gets object on the given index in the model.
     *
     * @param index index of the object.
     * @return object on given index or {@code null} if
     *         there is no object on that index.
     */
    GeometricalObject getObject(int index);

    /**
     * Adds given object to this model.
     *
     * @param object to be added to model.
     */
    void add(GeometricalObject object);

    /**
     * Removes given object from the model.
     *
     * @param object to be removed from this model.
     */
    void remove(GeometricalObject object);

    /**
     * Changes position of object {@code object}
     * for given {@code offset}.
     *
     * @param object object which position will be changed.
     * @param offset offset for which object will be repositioned.
     */
    void changeOrder(GeometricalObject object, int offset);

    /**
     * Returns index of given object.
     *
     * @param object object which index will be returned.
     * @return index ob object {@code object} or -1 if that
     *         object is not present.
     */
    int indexOf(GeometricalObject object);

    /**
     * Removes all elements from model.
     */
    void clear();

    /**
     * Sets modified flag on false.
     */
    void clearModifiedFlag();

    /**
     * Checks if model is modified.
     *
     * @return {@code true} if model is modified otherwise {@code false}.
     */
    boolean isModified();

    /**
     * Adds {@code DrawingModelListener} to this model.
     *
     * @param l listener to be added.
     */
    void addDrawingModelListener(DrawingModelListener l);

    /**
     * Removes {@code DrawingModelListener} from this model.
     *
     * @param l listener to be removed.
     */
    void removeDrawingModelListener(DrawingModelListener l);
}
