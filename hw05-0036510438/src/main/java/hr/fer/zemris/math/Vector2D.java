package hr.fer.zemris.math;

/**
 * Object represent 2D vector starting from (0, 0) coordinate.
 * 
 * @author Domagoj Lokner
 *
 */
public class Vector2D {
	
	/**
	 * x coordinate of vector.
	 */
	private double x;
	
	/**
	 * y coordinate of vector.
	 */
	private double y;
	
	/**
	 * Constructs {@code Vector2D} of coordinate ({@code x}, {@code y}).
	 * 
	 * @param x - x coordinate.
	 * @param y - y coordinate.
	 */
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Gets vector's x coordinate.
	 * 
	 * @return x coordinate.
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * Gets vector's y coordinate.
	 * 
	 * @return y coordinate.
	 */
	public double getY() {
		return y;
	}
	
	/**
	 * Translate this vector by given offset.
	 * 
	 * @param offset - offset to be translated for.
	 */
	public void translate(Vector2D offset) {
		this.x += offset.getX();
		this.y += offset.getY();
	}
	
	/**
	 * Returns vector created as translation of this vector by given offset.
	 * This vector will stay unchanged.
	 * 
	 * @param offset - offset to be translated for.
	 * @return translated vector.
	 */
	public Vector2D translated(Vector2D offset) {
		return new Vector2D(this.x + offset.getX(), this.y + offset.getY());
	}
	
	/**
	 * Rotate this vector by given angle.
	 * 
	 * @param angle - angle to be rotated for.
	 */
	public void rotate(double angle) {
		double x2, y2;
		
		x2 = this.x * Math.cos(angle) - this.y * Math.sin(angle);
		y2 = this.x * Math.sin(angle) + this.y * Math.cos(angle);
				
		this.x = x2;
		this.y = y2;
	}
	
	/**
	 * Returns vector created as rotation of this vector by given angle.
	 * This vector will stay unchanged.
	 * 
	 * @param angle - angle to be rotated for. 
	 * @return rotated vector.
	 */
	public Vector2D rotated(double angle) {
		
		return new Vector2D(this.x * Math.cos(angle) - this.y * Math.sin(angle), 
				this.x * Math.sin(angle) + this.y * Math.cos(angle));
	}
	
	/**
	 * Scale this vector by given scalar.
	 * 
	 * @param scaler - number to be scaled for.
	 */
	public void scale(double scaler) {
		this.x *= scaler;
		this.y *= scaler;
	}
	
	/**
	 * Returns vector created by scaling this vector by given scaler.
	 * 
	 * @param scaler - number to be scaled for.
	 * @return scaled vector.
	 */
	public Vector2D scaled(double scaler) {
		return new Vector2D(this.x * scaler, this.y * scaler);
	}
	
	/**
	 * Returns copy of this vector.
	 * 
	 * @return copy of vector.
	 */
	public Vector2D copy() {
		return new Vector2D(this.x, this.y);
	}
}
