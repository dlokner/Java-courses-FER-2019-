package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Copies last stored state and push it on stack.
 * 
 * @author Domagoj Lokner
 *
 */
public class PushCommand implements Command {

	/**
	 * Stores copy of current turtle state in given context.
	 * 
	 * @param ctx - turtle context.
	 * @param painter - {@code Painter} object (isn't used in this method).
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.pushState(ctx.getCurrentState().copy());
	}

}
