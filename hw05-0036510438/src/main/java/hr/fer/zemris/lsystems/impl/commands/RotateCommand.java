package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Rotate turtle.
 * 
 * @author Domagoj Lokner
 *
 */
public class RotateCommand implements Command {

	private double angle;
	
	/**
	 * Constructs {@code RotateCommand} with angle turtle will be rotated.
	 * 
	 * @param angle - degrees angle for which turtle will be rotated for.
	 */
	public RotateCommand(double angle) {
		this.angle = Math.PI * angle / 180d;
	}

	/**
	 * Rotate current turtle state by angle property of this object.
	 * 
	 * @param ctx - turtle context.
	 * @param painter - {@code Painter} object (isn't used in this method).
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().getAngle().rotate(angle);
	}
}
