package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;
import hr.fer.zemris.math.Vector2D;

/**
 * Command turtle to draw line by calculated distance and certain color.<br>
 * New turtle state will be stored in current turtle state.
 * 
 * @author Domagoj Lokner
 *
 */
public class DrawCommand implements Command{
	
	private double step;
	
	/**
	 * Constructs {@code DrawCommand} with given step for which turtle will be moved.
	 * 
	 * @param step
	 */
	public DrawCommand(double step) {
		this.step = step;
	}

	/**
	 * Draw line with current turtle sate. 
	 * Line length will be unit length multiplied 
	 * by step property of this object.
	 * 
	 * @param ctx - turtle state context.
	 * @param painter - object that perform draw action. 
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState current = ctx.getCurrentState();
		
		Vector2D target = current.getPosition()
				.translated(current.getAngle().scaled(current.getUnitLength()*step));
	
		painter.drawLine(current.getPosition().getX(), 
				current.getPosition().getY(), 
				target.getX(), 
				target.getY(), 
				current.getColor(), 
				1f);	
		
		current.setPosition(target);
	}

}
