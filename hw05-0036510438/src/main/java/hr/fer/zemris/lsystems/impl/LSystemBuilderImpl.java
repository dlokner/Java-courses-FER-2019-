package hr.fer.zemris.lsystems.impl;

import java.awt.Color;

import hr.fer.zemris.java.custom.collections.Dictionary;
import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.commands.ColorCommand;
import hr.fer.zemris.lsystems.impl.commands.DrawCommand;
import hr.fer.zemris.lsystems.impl.commands.PopCommand;
import hr.fer.zemris.lsystems.impl.commands.PushCommand;
import hr.fer.zemris.lsystems.impl.commands.RotateCommand;
import hr.fer.zemris.lsystems.impl.commands.ScaleCommand;
import hr.fer.zemris.lsystems.impl.commands.SkipCommand;
import hr.fer.zemris.math.Vector2D;

/**
 * Object used for defining concrete configuration of Lindemayer system.<br>
 * Class offers methods for modifying LSystem attributes.
 * 
 * <p>
 * All attributes will be set on default values if they were not modified:
 * 	<ul>
 * 	<li>unitLength to 0.1
 *  <li>unitLengthDegreeScaler to 1
 *  <li>origin to (0, 0) vector
 *  <li>angle to 0
 *  <li>axiom to an empty string.
 * 	</ul>
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class LSystemBuilderImpl implements LSystemBuilder {

	private static final double DEFAULT_UNIT_LENGTH = 0.1;
	private static final double DEFAULT_UNIT_LENGTH_DEGREE_SCALER = 1;
	private static final Vector2D DEFAULT_ORIGIN = new Vector2D(0, 0);
	private static final double DEFAULT_ANGLE = 0;
	private static final String DEFAULT_AXIOM = "";
	private static final double ORIGIN_LOWER_BOUND = 0;
	private static final double ORIGIN_UPPER_BOUND = 1;
	
	/**
	 * Collection stores commands defined for certain symbols.
	 */
	private Dictionary<Character , Command> commands;
	
	/**
	 * Collection stores productions for defined symbols.
	 */
	private Dictionary<Character, String> productions;
	
	/**
	 * Unit length of turtle movement.
	 */
	private double unitLength;
	
	/**
	 * Double value used for scaling unit length<br>
	 * On defined deep unit length will be scaled by this value powered by deep.
	 */
	private double unitLengthDegreeScaler;
	
	/**
	 * Starting point of turtle.
	 */
	private Vector2D origin;
	
	/**
	 * Starting angle of turtle.
	 */
	private double angle;
	
	/**
	 * Array of chars defines fractal for deep 0.
	 */
	private String axiom;
	
	/**
	 * Constructs {@code LSystemBuilderImpl} and 
	 * set all properties on default values.
	 */
	public LSystemBuilderImpl() {
		unitLength = DEFAULT_UNIT_LENGTH;
		unitLengthDegreeScaler = DEFAULT_UNIT_LENGTH_DEGREE_SCALER;
		origin = DEFAULT_ORIGIN;
		angle = DEFAULT_ANGLE;
		axiom = DEFAULT_AXIOM;
		
		commands = new Dictionary<>();
		productions = new Dictionary<>();
	}
	
	/**
	 * Returns concrete Lindenmayer system based on 
	 * configuration of this object.
	 * 
	 * @return configured{@code LSystem}.
	 */
	@Override
	public LSystem build() {
		return new LSystemImpl();
	}

	/**
	 * Configure LSystem based on given strings.
	 * 
	 * <p>
	 * Examples of strings that represents valid configuration commands:
	 *	<ul>
	 *	<li>"origin 0.05 0.4"
	 *	<li>"angle 0"
	 *	<li>"unitLengthDegreeScaler 1.0 / 3.0",
	 *	<li>"command F draw 1"
	 *	<li>"command + rotate 60",
	 *	<li>"axiom F"
	 *	<li>"production F F+F--F+F"
	 *	<li> empty strings will be ignored
	 *	</ul>
	 *</p>
	 *
	 * @param arg0 - list of strings that represents commands for configuring LSystem.
	 * @return reference on this object configured.
	 * @throws UnsupportedOperationException if some of given string 
	 * 		   can't be interpreted as valid command.
	 */
	@Override
	public LSystemBuilder configureFromText(String[] arg0) {
		for(String arg : arg0) {
						
			arg = arg.strip();
			
			if(arg.length() == 0) {
				continue;
			}
			
			String[] line = arg.split("\\s+");
			
			switch(line[0]) {
				case("origin"):
					setOriginText(line);
					break;
				case("angle"):
					setAngleText(line);
					break;
				case("unitLength"):
					setUnitLengthText(line);
					break;
				case("unitLengthDegreeScaler"):
					setUnitLengthDegreeScaler(line);
					break;
				case("command"):
					setCommandText(line);
					break;
				case("axiom"):
					setAxiomText(line);
					break;
				case("production"):
					setProductionText(line);
					break;
				default:
					throw new UnsupportedOperationException();
			}
		}
		return this;
	}

	/**
	 * Register command for given symbol.
	 * 
	 * <p>
	 * Examples of strings that represents valid commands:
	 *	<ul>
	 *	<li>"draw 0.1"
	 *	<li>"skip 0.3"
	 *	<li>"scale 0.5",
	 *	<li>"rotate 45"
	 *	<li>"push",
	 *	<li>"pop"
	 *	<li>"color 00ff00"
	 *	</ul>
	 *</p>
	 *
	 * @param arg0 - symbol that represent command evaluated from second argument.
	 * @param arg1 - string representing command.
	 * @return reference on this object with new command registered.
	 * @throws IllegalArgumentException if unvalid expression is given as second argument.
	 */
	@Override
	public LSystemBuilder registerCommand(char arg0, String arg1) {
		String[] arguments = arg1.strip().split("\\s+");
		
		try {
			switch(arguments[0]) {
				case("pop"):
					commands.put(arg0, new PopCommand());
					break;
				case("push"):
					commands.put(arg0, new PushCommand());
					break;
				case ("draw"):
					commands.put(arg0, new DrawCommand(singleDouble(arguments[1])));
					break;
				case ("skip"):
					commands.put(arg0, new SkipCommand(singleDouble(arguments[1])));
					break;
				case ("scale"):
					commands.put(arg0, new ScaleCommand(singleDouble(arguments[1])));
					break;
				case ("rotate"):
					commands.put(arg0, new RotateCommand(singleDouble(arguments[1])));
					break;
				case ("color"):
					commands.put(arg0, new ColorCommand(new Color(Integer.parseInt(arguments[1], 16))));
					break;
			}
		} catch (IndexOutOfBoundsException e) {
			throw new IllegalArgumentException("Unvalid command expression");
		}
		return this;
	}

	/**
	 * Register production of given symbol.
	 * 
	 * @param arg0 - symbol which production is given.
	 * @param arg1 - production.
	 * @return reference on this object with registered production.
	 */
	@Override
	public LSystemBuilder registerProduction(char arg0, String arg1) {
		productions.put(arg0, arg1);
		return this;
	}

	/**
	 * Sets angle to given {@code arg0}.<br>
	 * Given number will be interpreted in degrees.
	 * 
	 * @param arg0 - angle value to me set.
	 * @return reference on this object with angle set on new value.
	 */
	@Override
	public LSystemBuilder setAngle(double arg0) {
		angle = arg0 * Math.PI / 180d;
		return this;
	}

	/**
	 * Sets axiom on given {@code arg0}.
	 * 
	 * @param arg0 - string represents axiom to be set.
	 * @return reference on this object with axiom set on new value.
	 */
	@Override
	public LSystemBuilder setAxiom(String arg0) {
		axiom = arg0;
		return this;
	}

	/**
	 * Sets origin vector to given x and y coordinate.
	 * 
	 * @param arg0 - x coordinate of origin.
	 * @param arg1 - y coordinate of origin.
	 * @return reference on this object with set origin value.
	 */
	@Override
	public LSystemBuilder setOrigin(double arg0, double arg1) {
		origin = new Vector2D(isInBounds(arg0), isInBounds(arg1));
		return this;
	}

	/**
	 * Sets unit length on given {@code arg0}.
	 * 
	 * @param arg0 - unit length to be set.
	 * @return reference on this object with set unit length.
	 */
	@Override
	public LSystemBuilder setUnitLength(double arg0) {
		unitLength = arg0;
		return this;
	}

	/**
	 * Sets unit length degree scaler.
	 * 
	 * @param arg0 - scaler to be set.
	 * @return reference on this class with set unit length degree scaler.
	 */
	@Override
	public LSystemBuilder setUnitLengthDegreeScaler(double arg0) {
		unitLengthDegreeScaler = arg0;
		return this;
	}
	
	/**
	 * Objects represent concrete implementation of LSystem.
	 * 
	 * @author Domagoj Lokner
	 *
	 */
	private class LSystemImpl implements LSystem {

		/**
		 * Draw fractal of given deep.
		 * 
		 * @param arg0 - deep of fractal to be drawn.
		 * @param arg1 - object to draw fractal.
		 */
		@Override
		public void draw(int arg0, Painter arg1) {
			
			Context ctx = new Context();
			
			ctx.pushState(
					new TurtleState(
						origin, 
						new Vector2D(1, 0).rotated(angle), 
						new Color(0), 
						unitLength * Math.pow(unitLengthDegreeScaler, arg0)
						)
					);
			
			char[] generatedSymbols = generate(arg0).toCharArray();
			
			for(char symbol : generatedSymbols) {
				
				Command current = commands.get(symbol);
				
				if(current != null) {
					current.execute(ctx, arg1);
				}
			}
			
			
		}


		/**
		 * Generates string that represents axiom transformed by productions for {@code arg0} deep.<br>
		 * For deep 0 axiom will be returned.
		 * 
		 * @param arg0 - deep of string to be generated.
		 * @return generated string for given deep. 
		 */
		@Override
		public String generate(int arg0) {
			if(arg0 == 0) {
				return axiom;
			}
			
			String result = axiom;
			
			for(int i = 0; i < arg0; ++i) {
				char[] array = result.toCharArray();
				StringBuilder sb = new StringBuilder();
				
				for(char symbol : array) {
					String next = productions.get(symbol);
					
					if(next != null) {
						sb.append(next);
					} else {
						sb.append(symbol);
					}
				}
				
				result = sb.toString();
			}
			return result;
		}
	}
	
	private void setProductionText(String[] line) {
		if(line.length == 3) {
			if(line[1].length() == 1) {
				registerProduction(line[1].charAt(0), line[2]);
				return;
			}
		}
		throw new IllegalArgumentException();
	}

	private void setAxiomText(String[] line) {
		if(line.length == 2) {
			setAxiom(line[1]);
			return;
		}
		throw new IllegalArgumentException();
	}
	
	
	private void setCommandText(String[] line) {
		StringBuilder sb = new StringBuilder();
		
		for(int i = 2; i < line.length; ++i) {
			sb.append(line[i]);
			sb.append(' ');
		}
		
		registerCommand(isSymbol(line[1]), sb.toString());
	}

	private void setUnitLengthDegreeScaler(String[] line) {
		for(int i = 2; i < line.length; ++i) {
			line[1] += line[i];
		}
		
		String[] splitedLine = line[1].split("/");
		
		if(splitedLine.length == 1) {
			setUnitLengthDegreeScaler(singleDouble(splitedLine[0]));
			return;
		} else if(splitedLine.length == 2) {
			setUnitLengthDegreeScaler(
					singleDouble(
							splitedLine[0].strip())/singleDouble(splitedLine[1].strip())
					);
			return;
		}
		throw new IllegalArgumentException();
	}

	private void setUnitLengthText(String[] line) {
		if(line.length == 2) {
			setUnitLength(singleDouble(line[1]));
			return;
		}
		throw new IllegalArgumentException();
	}

	private void setAngleText(String[] line) {
		if(line.length == 2) {
			setAngle(singleDouble(line[1]));
			return;
		}
		throw new IllegalArgumentException();
	}

	private void setOriginText(String[] line) {
		if(line.length == 3) {
			setOrigin(singleDouble(line[1]), singleDouble(line[2]));
			return;
		}
		throw new IllegalArgumentException();
	}
	
	/**
	 * Parse string to double.
	 * 
	 * @param s - string to be parsed to double.
	 * @return parsed double value.
	 * @throws IllegalArgumentException if string can't be parsed to double.
	 */
	private static double singleDouble(String s) {
		try {
			return Double.parseDouble(s);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Expresion can't be parsed to double");
		}
	}
	
	/**
	 * Checks if given value is in defined bounds for origin.
	 * 
	 * @param arg - value to be checked.
	 * @return same value given as argument.
	 * @throws IllegalArgumentException if number is out of bounds.
	 */
	private static double isInBounds(double arg) {
		if(arg < ORIGIN_LOWER_BOUND || arg > ORIGIN_UPPER_BOUND) {
			throw new IllegalArgumentException("Origin coordinats must be numbers in range [0,1]");
		}
		return arg;
	}
	
	/**
	 * Checks if given string can be represented with Character object.
	 * 
	 * @param symbol - string to be checked.
	 * @return string transformed to Character.
	 * @throws IllegalArgumentException if string can't be represented with single character.
	 */
	private static Character isSymbol(String symbol) {
		
		if(symbol.length() == 1) {
			return symbol.charAt(0);
		}
		throw new IllegalArgumentException("Multiple signs symbol");
	}
}
