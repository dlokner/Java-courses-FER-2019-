package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.lsystems.Painter;

/**
 * Implements objects that do actions on turtle states.
 * 
 * @author Domagoj Lokner
 *
 */
@FunctionalInterface
public interface Command {
	
	/**
	 * Action to be done with last turtle state stored in given context.
	 * 
	 * @param ctx - turtle state context. 
	 * @param painter - object that performs turtle actions.
	 */
	void execute(Context ctx, Painter painter);

}
