package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Removes last state stored on stack
 * 
 * @author Domagoj Lokner
 *
 */
public class PopCommand implements Command{

	/**
	 * Pop current turtle state in given context.
	 * 
	 * @param ctx - turtle context.
	 * @param painter - {@code Painter} object (isn't used in this method).
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.popState();
	}

}
