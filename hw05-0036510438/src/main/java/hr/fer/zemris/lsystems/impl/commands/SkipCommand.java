package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;

/**
 * Commands turtle to move without drawing line.
 * 
 * @author Domagoj Lokner
 *
 */
public class SkipCommand implements Command {

	private double step;
	
	/**
	 * Constructs {@code SkipCommand} able to change turtle position 
	 * by unit length multiplied by given {@code step}.
	 * 
	 * @param step
	 */
	public SkipCommand(double step) {
	}
	
	/**
	 * Change current turtle position by unit length multiplied 
	 * by step property of this object.
	 * 
	 * @param ctx - turtle context.
	 * @param painter - {@code Painter} object (isn't used in this method).
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState current = ctx.getCurrentState();
		
		current.getPosition()
				.translate(current.getAngle().scaled(current.getUnitLength()*step));

	}

}
