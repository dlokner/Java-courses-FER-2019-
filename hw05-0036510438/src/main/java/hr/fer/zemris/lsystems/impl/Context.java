package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * Objects of this class represents objects capable to display fractals.
 * 
 * @author Domagoj Lokner
 *
 */
public class Context {
	
	/**
	 * Stack for storing turtle states.
	 */
	private ObjectStack<TurtleState> stack;
	
	public Context() {
		stack = new ObjectStack<>();
	}
	
	/**
	 * Returns current state of turtle (last state pushed on stack).
	 * 
	 * @return current turtle state.
	 */
	public TurtleState getCurrentState() {
		return stack.peek();
	}
	
	/**
	 * Push given turtle state on stack.
	 * 
	 * @param state - state to be pushed on stack.
	 */
	public void pushState(TurtleState state) {
		stack.push(state);
	}
	
	/**
	 * Removes last pushed turtle state.
	 */
	public void popState() {
		stack.pop();
	}
}
