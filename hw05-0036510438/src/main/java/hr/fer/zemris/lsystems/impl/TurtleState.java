package hr.fer.zemris.lsystems.impl;

import java.awt.Color;

import hr.fer.zemris.math.Vector2D;

/**
 * Objects records current turtle state (position, angle, color and unit length to move).
 * 
 * @author Domagoj Lokner
 *
 */
public class TurtleState {
	
	/**
	 * Position of turtle.
	 */
	private Vector2D position;
	
	/**
	 * Angle of turtle.
	 */
	private Vector2D angle;
	
	/**
	 * Color of lines turtle draw.
	 */
	private Color color;
	
	/**
	 * Unit length of turtle movement.
	 */
	private double unitLength; 
	
	/**
	 * Constructs {@code TurtleState}.
	 * 
	 * @param position - position of turtle.
	 * @param angle - turtle's angle.
	 * @param color - color of lines turtle is drawing.
	 * @param unitLength - unit length for which turtle will be moved by.
	 */
	public TurtleState(Vector2D position, Vector2D angle, Color color, double unitLength) {
		this.position = position;
		this.angle = angle;
		this.color = color;
		this.unitLength = unitLength;
	}

	/**
	 * Returns copy of this {@code TurtleState}.
	 * 
	 * @return copy of this state.
	 */
	public TurtleState copy() {
		return new TurtleState(position.copy(), angle.copy(), new Color(color.getRGB()), unitLength);
	}

	/**
	 * Gets turtle position.
	 * 
	 * @return turtle position.
	 */
	public Vector2D getPosition() {
		return position;
	}

	/**
	 * Sets turtle position on given vector.
	 * 
	 * @param position - vector of position to be set.
	 */
	public void setPosition(Vector2D position) {
		this.position = position;
	}

	/**
	 * Gets turtle angle.
	 * 
	 * @return turtle angle.
	 */
	public Vector2D getAngle() {
		return angle;
	}

	/**
	 * Sets turtle angle.
	 * 
	 * @param angle - angle to be set.
	 */
	public void setAngle(Vector2D angle) {
		this.angle = angle;
	}

	/**
	 * Gets turtle color
	 * 
	 * @return turtle color.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Sets color to given {@code color}.
	 * 
	 * @param color - color to be set.
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * Gets unit length.
	 * 
	 * @return unit length of turtle.
	 */
	public double getUnitLength() {
		return unitLength;
	}

	/**
	 * Sets unit length of turtle.
	 * 
	 * @param unitLength - unit length to be set.
	 */
	public void setUnitLength(double unitLength) {
		this.unitLength = unitLength;
	}
	
	
}
