package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Updates current's turtle unit length with length scaled by given factor.
 * 
 * @author Domagoj Lokner
 *
 */
public class ScaleCommand implements Command {
	
	private double factor;
	
	/**
	 * Constructs {@code ScaleCommand} able to scale unit length by given factor.
	 * 
	 * @param factor - scaling factor.
	 */
	public ScaleCommand(double factor) {
		this.factor = factor;
	}

	/**
	 * Scales current turtle state unit length by factor property of this object.
	 * 
	 * @param ctx - turtle context.
	 * @param painter - {@code Painter} object (isn't used in this method).
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().setUnitLength(
				ctx.getCurrentState().getUnitLength() * factor
		);
	}
}
