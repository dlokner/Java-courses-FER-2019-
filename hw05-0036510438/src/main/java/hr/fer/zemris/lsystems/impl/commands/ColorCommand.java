package hr.fer.zemris.lsystems.impl.commands;

import java.awt.Color;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Updates current turtle state by setting new color.
 * 
 * @author Domagoj Lokner
 *
 */
public class ColorCommand implements Command {
	
	private Color color;
	
	/**
	 * Constructs new color command.
	 * 
	 * @param color
	 */
	public ColorCommand(Color color) {
		this.color = color;
	}

	/**
	 * Sets new color in current turtle state.
	 * 
	 * @param ctx - turtle state context.
	 * @param painter - {@code Painter} object (isn't used in this method).
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().setColor(color);
	}
}
