package hr.fer.zemris.lsystems.impl.demo;

import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilderProvider;
import hr.fer.zemris.lsystems.gui.LSystemViewer;
import hr.fer.zemris.lsystems.impl.LSystemBuilderImpl;

/**
 * Demo program that draws fractal in new window.<br>
 * In this program LSystem is defined list of strings that represents 
 * LSystem configuration commands. 
 * 
 * @author Domagoj Lokner
 *
 */
public class Glavni2 {

	/**
	 * Starting method of the program.
	 * 
	 * @param args - command line arguments.
	 */
	public static void main(String[] args) {
		LSystemViewer.showLSystem(createKochCurve2(LSystemBuilderImpl::new));
	}

	/**
	 * Configure LSystem for Koch curve.
	 * 
	 * @param provider
	 * @return Koch curve LSystem.
	 */
	private static LSystem createKochCurve2(LSystemBuilderProvider provider) {
		String[] data = new String[] {
			"origin 0.05 0.4",
			"angle 0",
			"unitLength 0.9",
			"unitLengthDegreeScaler 1.0 / 3.0",
			"",
			"command F draw 1",
			"command + rotate 60",
			"command - rotate -60",
			"",
			"axiom F",
			"",
			"production F F+F--F+F"
		};
		return provider.createLSystemBuilder().configureFromText(data).build();
	}
}
