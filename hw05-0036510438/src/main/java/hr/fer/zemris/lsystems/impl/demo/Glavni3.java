package hr.fer.zemris.lsystems.impl.demo;

import hr.fer.zemris.lsystems.gui.LSystemViewer;
import hr.fer.zemris.lsystems.impl.LSystemBuilderImpl;

/**
 * Demo program that draws fractal in new window.<br>
 * Program will pop new windows with button that offers 
 * reading LSystem configuration commands from text file. 
 * 
 * @author Domagoj Lokner
 *
 */
public class Glavni3 {

	/**
	 * Starting method of the program.
	 * 
	 * @param args - command line arguments.
	 */
	public static void main(String[] args) {
		LSystemViewer.showLSystem(LSystemBuilderImpl::new);
	}

}
