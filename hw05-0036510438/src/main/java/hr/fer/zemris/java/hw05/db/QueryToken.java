package hr.fer.zemris.java.hw05.db;

/**
 * Defines token for {@code QueryLexer} and {@code QueryParser}.
 * 
 * @author Domagoj Lokner
 *
 */
public class QueryToken {
	
	/**
	 * Type of token.
	 */
	private QueryTokenType type;
	
	/**
	 * Value of token.
	 */
	private String value;
	
	/**
	 * COnstructs token with given type and value.
	 * 
	 * @param type - type of token.
	 * @param value - value of token.
	 */
	public QueryToken(QueryTokenType type, String value) {
		this.type = type;
		this.value = value;
	}

	/**
	 * Gets token type.
	 * 
	 * @return type of token.
	 */
	public QueryTokenType getType() {
		return type;
	}

	/**
	 * Gets token value.
	 * 
	 * @return value of token.
	 */
	public String getValue() {
		return value;
	}
	
	
}
