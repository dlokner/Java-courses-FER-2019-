package hr.fer.zemris.java.hw05.db;

import java.util.List;

/**
 * Object filters student record.
 * 
 * @author Domagoj Lokner
 *
 */
public class QueryFilter implements IFilter{
	
	/**
	 * Collection of comparison expressions.
	 */
	private List<ConditionalExpression> query;
	
	/**
	 * Constructs {@code QueryFilter}.
	 * 
	 * @param query . list of conditional expressions on which filter will be base.
	 */
	public QueryFilter(List<ConditionalExpression> query) {
		this.query = query;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Returns true if record satisfies all conditional expressions.
	 */
	@Override
	public boolean accepts(StudentRecord record) {
		for(ConditionalExpression expression : query) {
			if(!expression.getComparisonOperator().satisfied(
					expression.getFieldGetter().get(record), 
					expression.getStringLiteral())) {
				return false;
			}
		}
		return true;
	}

}
