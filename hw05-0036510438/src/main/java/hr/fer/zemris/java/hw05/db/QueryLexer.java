package hr.fer.zemris.java.hw05.db;

/**
 * Converts string given through constructor in tokens.
 * 
 * <ul>
 * 	<li>words starting with letter will be interpreted as ARGUMENT type.<br>
		Except "LIKE", it will be tokenized as OPERATOR and 
		"AND" will be tokenized as AND type token.
 * 	<li>text under quotes will be interpreted as STRING type.
 * 	<li>other symbols will be tokenized as OPERATOR type if they can 
 * 		be interpreted as comparison operators. 
 * </ul>
 * 
 * @see {@link QueryToken}, {@link QueryTokenType}.
 * 
 * @author Domagoj Lokner
 *
 */
public class QueryLexer {
	
	/**
	 * Text to be tokenized.
	 */
	private char[] data;
	
	/**
	 * Current token.
	 */
	private QueryToken token;
	
	/**
	 * Index of next char in array to be tokenized.
	 */
	private int currentIndex;
	
	/**
	 * Constructs {@code QueryLexer}.
	 * 
	 * @param data - text to me converted to tokens.
	 */
	public QueryLexer(String data) {
		this.data = data.toCharArray();
		token = new QueryToken(null, null);
	}
	
	/**
	 * Returns last generated token.
	 * 
	 * @return last token.
	 */
	public QueryToken getToken() {
		return token;
	}
	
	/**
	 * Generate and returns next token.
	 * 
	 * @return next token.
	 * @throws QueryLexerException if an error occurs.
	 */
	public QueryToken nextToken() {
		
		if(token.getType() == QueryTokenType.EOF) {
			throw new QueryLexerException();
		}
		
		skipBlanks();
		
		if(data.length <= currentIndex) {
			return token = new QueryToken(QueryTokenType.EOF, null);
		}
		
		if(Character.isLetter(data[currentIndex])) {
			String text = extractText();
			
			if(text.equals("LIKE")) {
				return token = new QueryToken(QueryTokenType.OPERATOR, "LIKE");
			} else if(text.equalsIgnoreCase("and")) {
				return token = new QueryToken(QueryTokenType.AND, null);
			}
			return token = new QueryToken(QueryTokenType.ARGUMENT, text);
		}
		
		if(data[currentIndex] == '"') {
			return token = new QueryToken(QueryTokenType.STRING, extractString());
		}
		
		if(isOperatorSign(data[currentIndex])) {
			return token = new QueryToken(QueryTokenType.OPERATOR, extractOperator());
		}
		
		throw new QueryLexerException();
	}
	
	/**
	 * Extract value of OPERATOR token starting from current index.
	 * 
	 * @return string representation of an operator.
	 */
	private String extractOperator() {
		
		if(data[currentIndex+1] == '=') {
			currentIndex += 2;
			return data[currentIndex-2] + "=";
		} else {
			++currentIndex;
			return Character.toString(data[currentIndex-1]);
		}
		
		
	}

	/**
	 * Extract value of STRING type operator
	 * 
	 * @return string under quotes.
	 * @throws QueryLexerException if quotes are not closed or 
	 * 		   backslash is found.
	 */
	private String extractString() {
		++currentIndex;
		boolean closed = false;
		
		StringBuilder sb = new StringBuilder();
		
		do {
			if(data[currentIndex] == '\\') {
				throw new QueryLexerException("Escapeing is not allowed in string argument!");
			}
			
			if(data[currentIndex] != '"') {
				sb.append(data[currentIndex++]);
				continue;
			} else {
				closed = true;
				break;
			}
			
		} while(currentIndex < data.length);
		
		if(!closed) {
			throw new QueryLexerException("Unclosed quotes!");
		}
		
		currentIndex++;
		
		return sb.toString();
	}

	/**
	 * Returns string created of data characters from current index
	 * to index of first character that is not letter or digit.
	 * 
	 * @return
	 */
	private String extractText() {
		
		StringBuilder sb = new StringBuilder();
		
		do {
			
			if(Character.isLetterOrDigit(data[currentIndex])) {
				sb.append(data[currentIndex++]);
				continue;
			}
			break;
			
		} while(currentIndex < data.length);
		
		return sb.toString();
	}

	/**
	 * Set {@code currentIndex} on first position thats not whitespace.
	 */
	private void skipBlanks() {
		while(currentIndex < data.length && Character.isWhitespace(data[currentIndex])) {
			++currentIndex;
		}
	}
	
	/**
	 * Checks if given character is valid operator.
	 * 
	 * @param c - character to be checked.
	 * @return {@code true} if {@code c} is valid operator sign otherwise {@code false}.
	 */
	private boolean isOperatorSign(char c) {
		
		if(c == '<') {
			return true;
		}
		if(c == '>') {
			return true;
		}
		if(c == '!') {
			return true;
		}
		if(c == '=') {
			return true;
		}
		return false;
	}
}
