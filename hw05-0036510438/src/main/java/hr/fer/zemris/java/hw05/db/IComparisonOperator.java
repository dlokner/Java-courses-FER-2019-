package hr.fer.zemris.java.hw05.db;

/**
 * Implements comparison operator objects.
 * 
 * @author Domagoj Lokner
 *
 */
@FunctionalInterface
public interface IComparisonOperator {
	
	/**
	 * Checks if given arguments satisfies defined operator.
	 * 
	 * @param value1 - first argument of comparison.
	 * @param value2 - second argument of comparison.
	 * @return {@code true} if operator is satisfied otherwise {@code false}.
	 */
	public boolean satisfied(String value1, String value2);
	
}
