package hr.fer.zemris.java.hw05.db;

/**
 * Thrown to indicate an error in {@code QueryParser}.
 *
 * @author Domagoj Lokner
 *
 */
public class QueryParserException extends RuntimeException {
	
	private static final long serialVersionUID = -8679464280162218007L;

	/**
	 * Constructs {@code QueryParserException}.
	 */
	public QueryParserException() {
	}
	
	/**
	 * Constructs {@code QueryParserException} with specified detail message.
	 * 
	 * @param message - given message.
	 */
	public QueryParserException(String message) {
		super(message);
	}
}
