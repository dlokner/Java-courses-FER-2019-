package hr.fer.zemris.java.custom.collections;

/**
 * Interface models object capable of performing some operation on the passed object.
 * 
 * @author Domagoj Lokner
 * 
 * @param <E> - type of objects to be processed by {@code Processor}.
 */
@FunctionalInterface
public interface Processor<E> {

	/**
	 * Performs action on given {@code value}.
	 * <p>
	 * Action is not defined here.
	 * </p>
	 * 
	 * @param value - value over which action will be performed.
	 */
	public void process(E value);
	
}
