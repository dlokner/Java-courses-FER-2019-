package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Class implements simple hash table.
 * 
 * <p>
 * Records stored in this table are represented with key and value.<br>
 * Key must not be null, but value stored under that key can be.
 * </p>
 * 
 * @author Domagoj Lokner
 *
 * @param <K> - type of key objects.
 * @param <V> - type of value objects.
 */
public class SimpleHashtable<K, V> implements Iterable<SimpleHashtable.TableEntry<K,V>> {

	/**
	 * Constant defines percent of 
	 */
	private static final double PERCENT = 0.75;
	private static final int DEFAULT_TABLE_SIZE = 16;
	
	private TableEntry<K,V>[] table;
	
	/**
	 * Number of records stored into table.
	 */
	private int size;
	
	/**
	 * Counter for table modifications
	 */
	private int modificationCount;
	
	/**
	 * Constructs {@code SimpleHashtable} by default size.
	 */
	public SimpleHashtable() {
		this(DEFAULT_TABLE_SIZE);
	}
	
	/**
	 * Constructs {@code SimpleHashtable} with given number of empty slots.
	 * 
	 * @param capacity - number of slots in table.
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashtable(int capacity) {
		capacity = firstPowOf2(capacity);
		
		table = (TableEntry<K, V>[])new TableEntry[capacity];
	}
	
	/**
	 * Adds new {@code key} and {@code value} to table.<br>
	 * If table already contains given {@code key} value will be overwritten with given {@code value}.
	 * 
	 * @param key - key to be added.
	 * @param value - value to be added under given key.
	 * @throws NullPointerException if given key is null.
	 */
	public void put(K key, V value) {
		Objects.requireNonNull(key);
		
		int slot = findSlot(key);
		
		if(table[slot] != null) {
			TableEntry<K, V> tmp = table[slot];
			
			while(tmp.next != null) {
				if(tmp.key.equals(key)) {
					tmp.setValue(value);
					return;
				}
				tmp = tmp.next;
			}
			/* pronaci bolje rjesenje */
			if(tmp.key.equals(key)) {
				tmp.setValue(value);
				return;
			}
			
			tmp.next = new TableEntry<K, V>(key, value);
			++size;
			++modificationCount;
			return;
		}
		
		table[slot] = new TableEntry<K, V>(key, value);
		
		++modificationCount;
		++size;
		checkAndExpand();
		
	}
	
	/**
	 * Gets value stored by given {@code key} in table.
	 * 
	 * @param key - key of value to be gotten;
	 * @return value of key if table contains given key otherwise null.
	 */
	public V get(Object key) {
		if(key == null) {
			return null;
		}
		
		int slot = findSlot(key);

		TableEntry<K, V> tmp = table[slot];
			
		while(tmp != null) {
			if(tmp.getKey().equals(key)) {
				return tmp.getValue();
			}
			tmp = tmp.next;
		}

		return null;
		
	}
	
	/**
	 * Returns number of records stored into table.
	 * 
	 * @return number of records.
	 */
	public int size() {
		return size;
	}
	
	/**
	 * Checks if table contains given {@code key}.
	 * 
	 * @param key - key to be checked.
	 * @return {@code true} if table contains given key otherwise {@code false}.
	 */
	public boolean containsKey(Object key) {
		if(key == null) {
			return false;
		}
		
		int slot = findSlot(key);
		
		TableEntry<K, V> tmp = table[slot];
		
		while(tmp != null) {
			if(tmp.getKey().equals(key)) {
				return true;
			}
			tmp = tmp.next;
		}

		return false;
		
	}
	
	/**
	 * Checks if table contains given {@code value}.
	 * 
	 * @param value - value to be checked.
	 * @return {@code true} if table contains given value otherwise {@code false}.
	 */
	public boolean containsValue(Object value) {
		
		for(int i = 0; i < table.length; ++i) {
			TableEntry<K, V> tmp = table[i];
			
			while(tmp != null) {
				if(tmp.getValue() == value || tmp.getValue().equals(value)) {
					return true;
				}
				tmp = tmp.next;
			}
		}
		
		return false;
	}
	
	/**
	 * Removes given {@code key} from table.
	 * 
	 * @param key - key to be removed.
	 */
	public void remove(Object key) {
		if(key == null) {
			return;
		}
		
		int slot = findSlot(key);

		if(table[slot] == null) {
			return;
		}
	
		TableEntry<K, V> tmp = table[slot];	
		
		if(tmp.getKey().equals(key)) {
			table[slot] = tmp.next;
			--size;
			++modificationCount;
			return;
		}
			
		 do {
			 TableEntry<K, V> previous = tmp;
			 tmp = tmp.next;
			 
			 if(tmp == null) {
				 return;
			 }
			 
			 if(tmp.getKey().equals(key)) {
				previous.next = tmp.next;
				--size;
				++modificationCount;
				return;
			 }
		} while(true);
	}
	
	/**
	 * Returns true if table contains no elements.
	 * 
	 * @return {@code true} if table is empty otherwise {@code false}.
	 */
	public boolean isEmpty() {
		return size == 0;
	}
	
	public String toString() {
		
		if(size == 0) {
			return "[]";
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for(int i = 0; i < table.length; ++i) {
			TableEntry<K, V> tmp = table[i];
			
			while(tmp != null) {
				sb.append(tmp + ", ");
				tmp = tmp.next;
			}		
		}
		return sb.toString().substring(0, sb.toString().length()-2) + "]";
	}
	
	/**
	 * Removes all elements stored in table.
	 */
	public void clear() {
		
		if(isEmpty()) {
			return;
		}
		
		for(int i = 0; i < table.length; ++i) {
			table[i] = null;
		}
		
		++modificationCount;
		size = 0;
	}
	
	@Override
	public Iterator<TableEntry<K, V>> iterator() {
		return new IteratorImpl();
	}
	
	/**
	 * Defines objects able to iterate by {@code SimpleHashtable} collection.
	 * 
	 * @author Domagoj Lokner
	 *
	 */
	private class IteratorImpl implements Iterator<SimpleHashtable.TableEntry<K,V>> {

		/**
		 * Stores reference to last returned entry by {@code next()} method.
		 */
		private TableEntry<K, V> current;
		
		/**
		 * Stores reference to next entry {@code next()} method should return.
		 */
		private TableEntry<K, V> next;
		
		/**
		 * Stores index of hash table of reference saved in variable {@code next}.
		 */
		private int currentSlot;
		
		/**
		 * Stores {@code modificationCount} of this Hash table in moment of constructing this iterator. 
		 */
		private int savedModificationCount;
		
		/**
		 * Constructs implementation of iterator for {@code SimpleHashtable} class.
		 */
		public IteratorImpl() {
			
			this.savedModificationCount = modificationCount;
			
			while(currentSlot < table.length) {
				if(table[currentSlot] != null) {
					next = table[currentSlot];
					break;
				}
				++currentSlot;
			}
		}
		
		/**
		 * {@inheritDoc}
		 * 
		 * @throws ConcurrentModificationException if collection is modified during the iteration.
		 */
		@Override
		public boolean hasNext() {
			checkForModifications();
			return next != null;
		}
		
		/**
		 * {@inheritDoc}
		 * 
		 * @throws ConcurrentModificationException if collection is modified during the iteration.
		 */
		@Override
		public TableEntry<K, V> next() {
			
			checkForModifications();
			
			if(hasNext()) {
				current = next;
				next = next.next;
			} else {
				throw new NoSuchElementException();
			}
			
			if(next != null) {
				return current;
			}
			
			++currentSlot;
			
			while(currentSlot < table.length) {
				if(table[currentSlot] != null) {
					next = table[currentSlot];
					break;
				}
				++currentSlot;
			}	
			return current;
		}
		
		/**
		 * {@inheritDoc}
		 * 
		 * @throws ConcurrentModificationException if collection is modified during the iteration.
		 */
		@Override
		public void remove() {
			
			checkForModifications();
			
			if(current == null) {
				throw new IllegalStateException();
			}
			SimpleHashtable.this.remove(current.getKey());
			current = null;
			++savedModificationCount;
		}
		
		/**
		 * Private method that checks if collection is modified outside iterator.
		 * 
		 * @throws ConcurrentModificationException if collection is modified.
		 */
		private void checkForModifications() {
			if(savedModificationCount != modificationCount) {
				throw new ConcurrentModificationException();
			}
		}
	}
	
	/**
	 * This class objects represents single record in {@code SimpleHashtable}.
	 * 
	 * <p>
	 * Every record contains key and value stored under that key.
	 * </p>
	 * 
	 * @author Domagoj Lokner
	 *
	 * @param <K> - type of key objects.
	 * @param <V> - type of value objects.
	 */
	public static class TableEntry<K,V> {
		private K key;
		private V value;
		private TableEntry<K, V> next;
		
		/**
		 * Constructs {@code TableEntry} with given {@code key} and {@code value}.
		 * 
		 * @param key - entry's key.
		 * @param value - value to be stored.
		 */
		public TableEntry(K key, V value) {
			this.key = key;
			this.value = value;
		}
		
		@Override
		public String toString() {
			return key + "=" + value;
		}
		
		/**
		 * Gets entry's value.
		 * 
		 * @return value of entry.
		 */
		public V getValue() {
			return value;
		}
		
		/**
		 * Sets entry's value.
		 * 
		 * @param value value to be stored in entry.
		 */
		public void setValue(V value) {
			this.value = value;
		}
		
		/**
		 * Gets entry's key.
		 * 
		 * @return key of the entry.
		 */
		public K getKey() {
			return key;
		}
	}
	
	/**
	 * Calculate first power of two greater or equals to given number.
	 * 
	 * @param number
	 * @return calculated power of two.
	 */
	private static int firstPowOf2(int number) {
		int result = 1;
		
		while(result < number) {
			result *= 2;
		}
		
		return result;
	}
	
	/**
	 * Calculate index of slot in table by given {@code key}.
	 * 
	 * @param key - key for which index of slot will be calculated for.
	 * @return index of slot in hash table.
	 */
	private int findSlot(Object key) {
		return Math.abs(key.hashCode()) % table.length;
	}

	/**
	 * Checks if number of elements stored in table is greater than size of table 
	 * multiplied by {@code PERCENT} constant and than expands table's size by twice
	 */
	@SuppressWarnings("unchecked")
	private void checkAndExpand() {
		
		if(size < PERCENT * table.length) {
			return;
		}
				
		TableEntry<K, V>[] newTable = (TableEntry<K, V>[])new TableEntry[table.length*2];
		
		for(int i = 0; i < table.length; ++i) {
			TableEntry<K, V> tmp = table[i];
			
			while(tmp != null) {
				putEntryIntoTable(newTable, tmp);
				tmp = tmp.next;
			}
		}
		
		table = newTable;
	}
	
	/**
	 * Puts given {@code entry} into given {@code table}. 
	 * 
	 * @param table - table in which entry will be stored.
	 * @param entry - entry to be stored.
	 */
	private void putEntryIntoTable(TableEntry<K, V>[] table, TableEntry<K, V> entry) {
	
		int slot = Math.abs(entry.getKey().hashCode()) % table.length;
		
		if(table[slot] != null) {
			TableEntry<K, V> tmp = table[slot];
			
			while(tmp.next != null) {
				tmp = tmp.next;
			}
			
			tmp.next = entry;
			return;
		}
		
		table[slot] = entry;
	}
	
}
