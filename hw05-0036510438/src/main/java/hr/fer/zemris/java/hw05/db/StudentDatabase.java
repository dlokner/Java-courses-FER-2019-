package hr.fer.zemris.java.hw05.db;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class StudentDatabase {
	
	/**
	 * Collection that stores student records. Jmbag is used as key.
	 */
	private Map<String, StudentRecord> records;
	
	/**
	 * Constructs student databases.
	 * 
	 * @param data - array of Strings where every String represents single student data.
	 * @throws IllegalArgumentException if unvalide record is given.
	 */
	public StudentDatabase(String[] data) {

		records = new LinkedHashMap<>();
		
		for(String record : data) {
			addRecord(record);
		}	
	}

	/**
	 * Adds given {@code record} to map of student records. Sets {@code jmbag} as key of the record.
	 * 
	 * @param record - string representation single student record.
	 * @throws IllegalArgumentException if unvalide expression is given, 
	 * 		   grade is not integer number or grade is not in range [1-5].
	 */
	private void addRecord(String record) {
		String[] recordParts = record.strip().split("\\s+");
		
		String jmbag = recordParts[0];
		String lastName = recordParts[1];
		
		for(int i = 2; i < recordParts.length-2; ++i) {
			lastName += " " + recordParts[i];
		}
		
		String firstName = recordParts[recordParts.length-2];
		String gradeStr = recordParts[recordParts.length-1];
		
		int grade;
		
		try {
			grade = Integer.parseInt(gradeStr);	
		} catch(NumberFormatException ex) {
			throw new IllegalArgumentException("Grade from database can't be parsed to integer!");
		}
		
		if(grade < 1 || grade > 5) {
			throw new IllegalArgumentException("Grade must be number in range [1-5].");
		}
		
		if(records.put(jmbag, new StudentRecord(jmbag, lastName, firstName, grade)) == null) {
		} else {
			throw new IllegalArgumentException("Duplicated records are not allowed!");
		}
	}
	
	/**
	 * Returns student record with given jmbag.
	 * 
	 * @param jmbag - jmbag of requested record.
	 * @return student record with given jmbag, 
	 * 		   if there is no student stored under 
	 * 		   given jmbag {@code null} will be returned.
	 */
	public StudentRecord forJMBAG(String jmbag) {
		return records.get(jmbag);
	}
	
	/**
	 * Returns list of student records with all records for 
	 * which {@code filter.accepts} method results with {@code true}. 
	 * 
	 * @param filter - filter on which will be based decision 
	 * 				   whether record will be returned.
	 * @return filtered list of student records from this database.
	 */
	public List<StudentRecord> filter(IFilter filter){
		List<StudentRecord> filteredRecords = new LinkedList<>();
		
		records.forEach((k, v) -> {
			if(filter.accepts(v)) {
				filteredRecords.add(v);
			}
		});
		
		return filteredRecords;
	}
	
	
}
