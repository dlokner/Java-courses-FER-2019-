package hr.fer.zemris.java.hw05.db;

/**
 * Thrown to indicate an error in {@code QueryLexer}.
 *
 * @author Domagoj Lokner
 *
 */
public class QueryLexerException extends RuntimeException {
	
	private static final long serialVersionUID = 6358382930207113996L;

	/**
	 * Constructs {@code QueryLexerException}.
	 */
	public QueryLexerException() {
	}
	
	/**
	 * Constructs {@code QueryLexerException} with specified detail message.
	 * 
	 * @param message - given message.
	 */
	public QueryLexerException(String message) {
		super(message);
	}

}
