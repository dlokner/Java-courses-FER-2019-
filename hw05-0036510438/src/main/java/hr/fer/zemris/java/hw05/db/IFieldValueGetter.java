package hr.fer.zemris.java.hw05.db;

/**
 * Implements objects capable to get single student record property.
 * 
 * @author Domagoj Lokner
 *
 */
@FunctionalInterface
public interface IFieldValueGetter {

	/**
	 * Returns defined student value property.
	 *  
	 * @param record - which property will be gotten.
	 * @return student record property.
	 */
	public String get(StudentRecord record);
	
}
