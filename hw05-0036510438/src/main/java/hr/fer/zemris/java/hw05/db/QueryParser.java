package hr.fer.zemris.java.hw05.db;

import java.util.LinkedList;
import java.util.List;

/**
 * Parse text given through constructor to list of {@code ConditionalExpression}s.
 * 
 * <p>
 * Conditions that must be satisfied for successful parsing:
 * 	<ul>
 * 	<li> tokens should be delivered in specified order: ARGUMENT, OPERATOR, STRING and optionally AND.
 * 	<li> query can't end with AND token.
 * 	</ul>
 * </p>
 * 
 * @see {@link QueryLexer}, {@link QueryToken}, {@link QueryTokenType}.
 * 
 * @author Domagoj Lokner
 *
 */
public class QueryParser {
 
	/**
	 * Collection stores parsed expressions
	 */
	private List<ConditionalExpression> query;
	
	/**
	 * Constructs {@code QueryParser} and parse given {@code data}.
	 * 
	 * @param data - string to be parsed.
	 * @throws QueryParserException if any error occur.
	 */
	public QueryParser(String data) {
		
		query = new LinkedList<>();
		
		QueryLexer lexer = new QueryLexer(data);
		try {
			parse(lexer);
		} catch(QueryLexerException ex) {
			throw new QueryParserException(ex.getMessage());
		}
	}

	/**
	 * Returns true if parsed query is direct query 
	 * (result can be reached in O(1) complexity).
	 * 
	 * @return {@code true} if query is direct otherwise {@code false}.
	 */
	public boolean isDirectQuery() {
		if(query.size() == 1) {
			if(query.get(0).getFieldGetter() == FieldValueGetters.JMBAG) {
				if(query.get(0).getComparisonOperator() == ComparisonOperators.EQUALS) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Gets JMBAG sent as argument of comparison in direct query.
	 * 
	 * @return jmbag.
	 * @throws IllegalStateException if query is not direct.
	 */
	public String getQueriedJMBAG() {
		if(isDirectQuery()) {
			return query.get(0).getStringLiteral();
		}
		throw new IllegalStateException();
	}
	
	/**
	 * Returns query as list of {@code ConditionalExpression}.
	 * 
	 * @return list of {@code ConditionalExpression}.
	 */
	public List<ConditionalExpression> getQuery(){
		return query;
	}
	
	/**
	 * Parse tokens from given lexer in {@code ConditionalExpression}s.
	 * 
	 * @param lexer
	 */
	private void parse(QueryLexer lexer) {
		
		QueryToken token = lexer.nextToken();
		
		while(lexer.getToken().getType() != QueryTokenType.EOF) {
			
			IFieldValueGetter fieldGetter;
			IComparisonOperator operator;
			String stringLiteral;
			
			if(token.getType() != QueryTokenType.ARGUMENT) {
				throw new QueryParserException("Unvalide query is given!");
			} else {
				fieldGetter = extractRecordValue(token);
			}
			
			
			token = lexer.nextToken();
			
			if(token.getType() != QueryTokenType.OPERATOR) {
				throw new QueryParserException("Unsuported argument order!");
			} else {
				operator = extractOperator(token);
			}
			
			token = lexer.nextToken();
			
			if(token.getType() != QueryTokenType.STRING) {
				throw new QueryParserException("Unsuported argument order!");
			} else {
				stringLiteral = token.getValue();
			}
			
			query.add(new ConditionalExpression(fieldGetter, stringLiteral, operator));
			
			
			token = lexer.nextToken();
			
			if(token.getType() == QueryTokenType.AND) {
				token = lexer.nextToken();
				if(token.getType() == QueryTokenType.EOF) {
					throw new QueryParserException("Query can't end with AND!");
				}
				continue;
			} else if(token.getType() == QueryTokenType.EOF) {
				continue;
			} else {
				throw new QueryParserException("Unsuported argument order!");
			}
			
		}
		
		
	}

	/**
	 * Returns {@code IComparisonOperators} equivalent to given token.
	 * 
	 * @param token - token to be interpreted as operator.
	 * @return token equivalent {@code IComparisonOperators}.
	 * @throws QueryParserException if given token does not represent valid operator.
	 */
	private static IComparisonOperator extractOperator(QueryToken token) {
		switch(token.getValue()) {
			case("="):
				return ComparisonOperators.EQUALS;
			case("!="):
				return ComparisonOperators.NOT_EQUALS;
			case("<"):
				return ComparisonOperators.LESS;
			case("<="):
				return ComparisonOperators.LESS_OR_EQUALS;
			case(">"):
				return ComparisonOperators.GREATER;
			case(">="):
				return ComparisonOperators.GREATER_OR_EQUALS;
			case("LIKE"):
				return ComparisonOperators.LIKE;
			default:
				throw new QueryParserException("Unvalid operator: \"" + token.getValue() + "\".");
		}
	}

	/**
	 * Returns {@code IFieldValueGetter} equivalent to given token.
	 * 
	 * @param token - token to be interpreted as field value.
	 * @return token equivalent {@code IFieldValueGetter}.
	 * @throws QueryParserException if given token does not represent existing field value.
	 */
	private static IFieldValueGetter extractRecordValue(QueryToken token) {
		switch(token.getValue()) {
			case("firstName"):
				return FieldValueGetters.FIRST_NAME;
			case("lastName"):
				return FieldValueGetters.LAST_NAME;
			case("jmbag"):
				return FieldValueGetters.JMBAG;
			default:
				throw new QueryLexerException("Nonexistent attribute: \"" + token.getValue() + "\".");
		}
	}
}
