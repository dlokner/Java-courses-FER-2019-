package hr.fer.zemris.java.hw05.db;

public class ComparisonOperators {
	
	/**
	 * Implementation of less operator.
	 */
	public static final IComparisonOperator LESS = (s1,s2) -> s1.compareTo(s2) < 0;
	
	/**
	 * Implementation of less or equals operator.
	 */
	public static final IComparisonOperator LESS_OR_EQUALS = (s1, s2) -> s1.compareTo(s2) <= 0;
	
	/**
	 * Implementation of greater operator.
	 */
	public static final IComparisonOperator GREATER = (s1,s2) -> s1.compareTo(s2) > 0;
	
	/**
	 * Implementation of greater or equals operator.
	 */
	public static final IComparisonOperator GREATER_OR_EQUALS = (s1,s2) -> s1.compareTo(s2) >= 0;
	
	/**
	 * Implementation of equals operator.
	 */
	public static final IComparisonOperator EQUALS = (s1,s2) -> s1.compareTo(s2) == 0;
	
	/**
	 * Implementation of not equlas operator.
	 */
	public static final IComparisonOperator NOT_EQUALS = (s1,s2) -> s1.compareTo(s2) != 0;
	
	
	/**
	 * Implementation of LIKE operator<br>
	 * 
	 * <p>
	 * This operator allows wildcard '*' in second argument of comparison. 
	 * There must not be more than one wildcard sign or 
	 * {@link IllegalArgumentException} will be throwen.
	 * </p>
	 */
	public static final IComparisonOperator LIKE = (s1, s2) -> {
		
		if(!s2.contains("*")) {
			return EQUALS.satisfied(s1, s2);
		}
		
		if(s2.indexOf('*') != s2.lastIndexOf('*')) {
			throw new IllegalArgumentException("LIKE allows only one wildcard!");
		}
		
		if(s2.endsWith("*")) {
			s2 = s2.substring(0, s2.length()-1);
			return s1.startsWith(s2);
		}
		
		if(s2.startsWith("*")) {
			s2 = s2.substring(1);
			return s1.endsWith(s2);
		}
		
		String[] patterns = s2.split("\\*");
		
		if(s1.startsWith(patterns[0])) {
			s1 = s1.substring(patterns[0].length());
			return s1.endsWith(patterns[1]);
		}
		
		return false;
	};
}
