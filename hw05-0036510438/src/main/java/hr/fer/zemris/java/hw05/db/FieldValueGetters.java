package hr.fer.zemris.java.hw05.db;

/**
 * Class defines field value getters for firstName, lastName and jmbag.
 * 
 * @author Domagoj Lokner
 *
 */
public class FieldValueGetters {
	
	/**
	 * First name getter.
	 */
	public static final IFieldValueGetter FIRST_NAME = StudentRecord::getFirstName;
	
	/**
	 * Last name getter.
	 */
	public static final IFieldValueGetter LAST_NAME = StudentRecord::getLastName;
	
	/**
	 * JMBAG getter.
	 */
	public static final IFieldValueGetter JMBAG = StudentRecord::getJmbag;
	
}
