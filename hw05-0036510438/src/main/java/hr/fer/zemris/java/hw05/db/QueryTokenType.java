package hr.fer.zemris.java.hw05.db;

/**
 * Token types enumeration.
 * 
 * @author Domagoj Lokner
 *
 */
public enum QueryTokenType {
	ARGUMENT,
	STRING,
	OPERATOR,
	AND,
	EOF
}
