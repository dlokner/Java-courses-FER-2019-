package hr.fer.zemris.java.hw05.db;

/**
 * Instance of this class represents single student record.
 * 
 * @author Domagoj Lokner
 *
 */
public class StudentRecord {
	
	/**
	 * Student's jmbag.
	 */
	private String jmbag;
	
	/**
	 * Student's last name.
	 */
	private String lastName;
	
	/**
	 * Student's first name.
	 */
	private String firstName;

	/**
	 * Student's grade
	 */
	private int grade;
	
	/**
	 * Constructs {@code StudentRecord} for student with given parameters.
	 * @param jmbag - student's jmbag.
	 * @param lastName - student's last name.
	 * @param firstName - student's first name.
	 * @param grade - student's grade.
	 */
	public StudentRecord(String jmbag, String lastName, String firstName, int grade) {
		this.jmbag = jmbag;
		this.lastName = lastName;
		this.firstName = firstName;
		this.grade = isGradeInBounds(grade);
	}
	
	/**
	 * Gets student's jmbag.
	 * 
	 * @return jmbag.
	 */
	public String getJmbag() {
		return jmbag;
	}


	/**
	 * Gets student's last name.
	 * 
	 * @return last name.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Gets student's first name.
	 * 
	 * @return first name.
	 */
	public String getFirstName() {
		return firstName;
	}


	/**
	 * Gets student's grade.
	 * 
	 * @return grade.
	 */
	public int getGrade() {
		return grade;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jmbag == null) ? 0 : jmbag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof StudentRecord)) {
			return false;
		}
		StudentRecord other = (StudentRecord) obj;
		if (jmbag == null) {
			if (other.jmbag != null) {
				return false;
			}
		} else if (!jmbag.equals(other.jmbag)) {
			return false;
		}
		return true;
	}	
	
	/**
	 * Returns grade if it is in range [1-5].
	 * 
	 * @param grade - grade to be checked for bounds.
	 * @return grade.
	 * @throws IllegalArgumentException if grade is out of bounds.
	 */
	private static int isGradeInBounds(int grade) {
		if(grade < 1  || grade > 5) {
			throw new IllegalArgumentException("Grade must be number in range [1-5]!");
		}
		return grade;
	}
}
