package hr.fer.zemris.java.hw05.db;

/**
 * Instance of this class represents complete conditional expressions used in query.<br>
 * Containing fieldValue, operator of comparison and string literal to be compared. 
 * 
 * @author Domagoj Lokner
 *
 */
public class ConditionalExpression {
	
	private IFieldValueGetter fieldGetter;
	private String stringLiteral;
	private IComparisonOperator comparisonOperator;
	
	/**
	 * Constructs new {@code ConditionalExpression}.
	 * 
	 * @param fieldGetter - getter for field value.
	 * @param stringLiteral - string to be compared with operator.
	 * @param comparisonOperator - operator of comparing.
	 */
	public ConditionalExpression(IFieldValueGetter fieldGetter, String stringLiteral, IComparisonOperator comparisonOperator) {
		this.fieldGetter = fieldGetter;
		this.stringLiteral = stringLiteral;
		this.comparisonOperator = comparisonOperator;
	}

	/**
	 * Gets field getter.
	 * 
	 * @return field getter.
	 */
	public IFieldValueGetter getFieldGetter() {
		return fieldGetter;
	}

	/**
	 * Gets string literal.
	 * 
	 * @return string literal.
	 */
	public String getStringLiteral() {
		return stringLiteral;
	}

	/**
	 * Gets comparison operator.
	 * 
	 * @return comparison operator.
	 */
	public IComparisonOperator getComparisonOperator() {
		return comparisonOperator;
	}
}
