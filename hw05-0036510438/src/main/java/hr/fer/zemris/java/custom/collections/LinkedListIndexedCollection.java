package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Class that implements linked list backed collection.
 * <p>
 * Storage of element value null is not allowed in this implementation.<br>
 * Duplicated elements are allowed.
 * <p>
 * <p>
 * <b>Average complexity</b> of:
 * <ul>
 * 	<li>{@code void add(T value)} - O(1)</li>
 * 	<li>{@code Object get(int index)} - the greatest complexity (size/2+1)</li>
 * 	<li>{@code void insert(T value, int position)} - O((size/2+1)/2)</li>
 * 	<li>{@code int indexOf(Object value)} - O(size/2)</li>
 * </ul>
 * </p>
 * 
 * @author Domagoj Lokner
 * 
 * @param <T> - type of objects to be stored into collection.
 *
 */
public class LinkedListIndexedCollection<T> implements List<T> {

	private int size;
	private ListNode<T> first;
	private ListNode<T> last;
	private long modificationCount;

	/*
	 * Private class that represents one node of the linked list.
	 * 
	 * Contains pointers on previous and next node in the list and reference for
	 * value storage.
	 * 
	 * Class also have default constructor and two more constructors for
	 * initializing attributes.
	 */
	public static class ListNode<T> {
		private ListNode<T> previous;
		private ListNode<T> next;
		private T value;

		private ListNode() {
		}

		private ListNode(T value) {
			this.value = value;
		}

		private ListNode(T value, ListNode<T> previous, ListNode<T> next) {
			this.value = value;
			this.previous = previous;
			this.next = next;
		}
	}

	/**
	 * Default constructor. Attributes {@code first} and {@code last} stay on
	 * {@code null} value. Size of the collection is 0.
	 */
	public LinkedListIndexedCollection() {
	}

	/**
	 * Constructor that copy elements stored in {@code other} collection to new
	 * {@code LinkedListIndexedCollection}.
	 * 
	 * @param other - collection to be copied into the list.
	 * @throws NullPointerException if null is sent as {@code other} parameter.
	 */
	public LinkedListIndexedCollection(Collection<T> other) {
		Objects.requireNonNull(other, "Collection parameter can't be null");

		this.addAll(other);
	}
	
	/**
	 * Implementation of {@code ElementsGetter} adjusted to work with {@code LinkedListIndexedCollection}.
	 * 
	 * @author Domagoj Lokner
	 *
	 */
	private static class ListElementsGetter<T> implements ElementsGetter<T> {
		private ListNode<T> next;
		private long savedModificationCount;
		private LinkedListIndexedCollection<T> collection;
		
		/**
		 * Constructs instance of {@code ListElementsGetter}.
		 * 
		 * @param first - first node of list.
		 */
		public ListElementsGetter(LinkedListIndexedCollection<T> collection) {
			this.next = collection.first;
			this.savedModificationCount = collection.modificationCount;
			this.collection = collection;
		}
		
		/**
		 * Returns {@code true} if there is more elements in list to be gotten.
		 * 
		 * @return {@code true} if there is more elements otherwise {@code false}.
		 */
		@Override
		public boolean hasNextElement() {
			ifModified();
			return next != null;
		}
		
		/**
		 * Gets value of next element of the list.
		 * 
		 * @return value of next element.
		 * @throws NoSuchElementException if there is no more elements in list.
		 */
		@Override
		public T getNextElement() {
			ifModified();
			if(next == null) {
				throw new NoSuchElementException();
			}
			
			T result = next.value;
			next = next.next;
			
			return result;
		}
		
		/**
		 * Throws exception if iterated collection is modified.
		 * 
		 * @throws ConcurrentModificationException if collection is modified.
		 */
		private void ifModified() {
			if(collection.modificationCount != savedModificationCount) {
				throw new ConcurrentModificationException();
			}
		}
	}
	
	@Override
	public ElementsGetter<T> createElementsGetter() {
		return new ListElementsGetter<T>(this);
	}
	
	/**
	 * {@inheritDoc} 
	 * 
	 * @throws NullPointerException if given value is null.
	 */
	@Override
	public void add(T value) {
		Objects.requireNonNull(value);

		ListNode<T> newNode = new ListNode<T>(value, last, null);
		++size;
		++modificationCount;
		
		if (first == null) {
			first = newNode;
		} else {
			last.next = newNode;
		}
		last = newNode;
	}
	
	@Override
	public int size() {
		return size;
	}
	
	@Override
	public boolean contains(Object value) {
		return indexOf(value) == -1 ? false : true;
	}
	
	@Override
	public boolean remove(Object value) {
		
		int index = indexOf(value);
		
		if(index == -1) {
			return false;
		} 
		
		remove(index);
		return true;
	}
	
	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		
		ListNode<T> tmp = first;
		
		for(int i = 0; i < size; ++i) {
			array[i] = tmp.value;
			tmp = tmp.next;
		}
		
		return array;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * @throws IndexOutOfBoundsException if {@code index} is not in range [0, size-1].
	 */
	@Override
	public T get(int index) {
		
		isLegalIndex(0, size-1, index);
		
		return getNodeOnIndex(index).value;
		
	}
	
	@Override
	public void clear() {
		first = null;
		last = null;
		
		size = 0;
		++modificationCount;
	}
	

	/**
	 * {@inheritDoc}
	 * 
	 * @throws IndexOutOfBoundsException if {@code} is not in range [0, size].
	 */
	@Override
	public void insert(T value, int position) {
		
		Objects.requireNonNull(value);
		
		isLegalIndex(0, size, position);
		
		if(position == size) {
			add(value);
			return;
		}
		
		ListNode<T> newNode = new ListNode<T>(value);
		ListNode<T> current = getNodeOnIndex(position);
	
		if(current.previous == null) {
			current.previous = newNode;
			first = newNode;
		} else {
			newNode.previous = current.previous;
			current.previous.next = newNode;
			current.previous = newNode;
		}
		
		
		newNode.next = current;
		
		++size;
		++modificationCount;
	}
	
	@Override
	public int indexOf(Object value) {
		
		ListNode<T> tmp = first;
		
		int index = 0;
		while(tmp != null) {
			if(tmp.value.equals(value)) {
				return index;
			}
			
			tmp = tmp.next;
			++index;
		}
		
		return -1;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws IndexOutOfBoundsException if {@code index} is not in range [0, size-1].
	 */
	@Override
	public void remove(int index) {
		isLegalIndex(0, size-1, index);
		
		ListNode<T> node = getNodeOnIndex(index);
		
		if(node.previous == null) {
			node.next.previous = null;
			first = node.next;
		} else {
			node.previous.next = node.next;
			if(node.next == null) {
				last = node.previous;
			} else {
				node.next.previous = node.previous;
			}
		}
		
		--size;
		++modificationCount;
	}

	/**
	 * Private method that throws IndexOutOfBound exception if index is not value 
	 * in range [{@code lower}, {@code upper}].
	 * 
	 * @param lower - lower bound.
	 * @param upper - upper bound.
	 * @param index - index to checked.
	 */
	private void isLegalIndex(int lower, int upper, int index) {
		if(index < lower || index > upper) {
			throw new IndexOutOfBoundsException();
		}
	}
	
	/*
	 * Private method that returns ListNode at given index in linked list.
	 */
	
	/**
	 * Private method that gets node at given index in linked list.
	 * 
	 * @param index - index of node to be gotten.
	 * @return {@code ListNode} on {@code index}.
	 */
	private ListNode<T> getNodeOnIndex(int index) {
		
		int median = size/2;
		
		ListNode<T> tmp;
		if(index <= median) {
			tmp = first;
			
			for(int i = 0; i < index; ++i) {
				tmp = first.next;
			}
			
		} else {
			tmp = last;
			
			for(int i = size-1; i > index; --i) {
				tmp = tmp.previous;
			}
		}
		
		return tmp;
	}
	
}
