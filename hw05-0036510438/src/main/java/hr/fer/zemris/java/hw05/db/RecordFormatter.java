package hr.fer.zemris.java.hw05.db;

import java.util.LinkedList;
import java.util.List;

/**
 * Format student records to string.
 * 
 * @author Domagoj Lokner
 *
 */
public class RecordFormatter {
	
	/**
	 * Form list of strings that represent table of given records.
	 * 
	 * @param records - records to be formated.
	 * @return list of formated strings.
	 */
	public static List<String> format(List<StudentRecord> records){
		List<String> formattedRecords = new LinkedList<>();
				
		if(records.size() == 0) {
			return formattedRecords;
		}
		
		int[] fieldsSize = fields(records);
		
		StringBuilder frame = new StringBuilder();
		
		frame.append('+');
		for(int i = 0; i < 4; ++i) {
			for(int j = 0; j < fieldsSize[i]; ++j) {
				frame.append('=');
			}
			frame.append('+');
		}
		
		formattedRecords.add(frame.toString());
		
		for(StudentRecord record : records) {
			StringBuilder formatRecord = new StringBuilder();
			
			formatRecord.append("| ");
			formatRecord.append(record.getJmbag() + whitespaces(fieldsSize[0] - record.getJmbag().length()-2));
			formatRecord.append(" | ");
			formatRecord.append(record.getLastName() + whitespaces(fieldsSize[1] - record.getLastName().length()-2));
			formatRecord.append(" | ");
			formatRecord.append(record.getFirstName() + whitespaces(fieldsSize[2] - record.getFirstName().length()-2));
			formatRecord.append(" | ");
			formatRecord.append(record.getGrade());
			formatRecord.append(" |");
			
			formattedRecords.add(formatRecord.toString());
		}
		
		formattedRecords.add(frame.toString());

		
		return formattedRecords;
	}

	/**
	 * Construct int array initialized on table slot length 
	 * value for every property of student record.
	 * 
	 * @param records - student records to be formated.
	 * @return int array.
	 */
	private static int[] fields(List<StudentRecord> records) {
		int[] result = new int[4];
		
		result[0] = records.stream()
				.max((s1, s2) -> s1.getJmbag().length() - s2.getJmbag().length())
				.get()
				.getJmbag()
				.length() + 2;
		
		result[1] = records.stream()
				.max((s1, s2) -> s1.getLastName().length() - s2.getLastName().length())
				.get()
				.getLastName()
				.length() + 2;
		
		result[2] = records.stream()
				.max((s1, s2) -> s1.getFirstName().length() - s2.getFirstName().length())
				.get()
				.getFirstName()
				.length() + 2;
		
		result[3] = 3;
		
		return result;
	}
	
	/**
	 * Return string contains only n whitespoaces.
	 * 
	 * @param n - number of whitespaces.
	 * @return string with n whitespaces.
	 */
	private static String whitespaces(int n) {
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0 ; i < n; ++i) {
			sb.append(' ');
		}
		
		return sb.toString();
	}
}
