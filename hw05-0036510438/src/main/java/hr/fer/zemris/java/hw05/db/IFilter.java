package hr.fer.zemris.java.hw05.db;

/**
 * Implements objects that filters student records.
 * 
 * @author Domagoj Lokner
 *
 */
@FunctionalInterface
public interface IFilter {
	
	/**
	 * Checks if record is acceptable based on defined conditions. 
	 * 
	 * @param record - record to be chacked.
	 * @return {@code true} if record is accepted otherwise {@code false}.
	 */
	public boolean accepts(StudentRecord record);
}
