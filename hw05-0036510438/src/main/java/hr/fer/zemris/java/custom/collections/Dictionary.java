package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * Objects of this class stores keys with their value.
 * 
 * <p>
 * This class is parameterized with two object types {@code K} and {@code V}.<br>
 * {@code K} - is type of key objects.<br>
 * {@code V} - is type of value objects.<br>
 * </p>
 * 
 * @author Domagoj Lokner
 * 
 * @param <K> - type of key object.
 * @param <V> - type of key given value.
 *
 */
public class Dictionary<K, V> {
	
	/**
	 * Collection for storing {@code DictionaryEntry} objects.
	 */
	private ArrayIndexedCollection<DictionaryEntry<K, V>> collection;
	
	public Dictionary() {
		this.collection = new ArrayIndexedCollection<DictionaryEntry<K,V>>();
	}
	
	/**
	 * Returns {@code true} if this collection contains no elements.
	 * 
	 * @return {@code ture} if collection contains no elements otherwise {@code false}.
	 */
	public boolean isEmpty() {
		return collection.isEmpty();
	}
	
	/**
	 * Returns number of elements this collection contains.
	 * 
	 * @return number of elements.
	 */
	public int size() {
		return collection.size();
	}
	
	/**
	 * Remove all elements from collection.
	 */
	public void clear() {
		collection.clear();
	}
	
	/**
	 * Adds given {@code key} and {@code value} to collection.<br>
	 * If given key already exists in collection it's {@code value} will be overwritten with given {@code value}.
	 * 
	 * @param key - key to be added.
	 * @param value - value to be added to given {@code key}.
	 * @throws NullPointerException if given {@code key} is null.
	 */
	public void put(K key, V value) {
		DictionaryEntry<K, V> entry = new DictionaryEntry<K, V>(key, value);
		
		int index = collection.indexOf(entry);
		
		if(index == -1) {
			collection.add(entry);
		} else {
			collection.get(index).setValue(value);
		}
	}
	
	/**
	 * Gets value stored under given {@code key}.
	 * 
	 * @param key - key of value to be returned.
	 * @return value stored under given {@code key}. 
	 * 		   If collection does not contains given key null will be returned.
	 */
	public V get(Object key) {
		int index;
		
		try {
			index = collection.indexOf(new DictionaryEntry<Object, V>(key, null));
		} catch(NullPointerException ex) {
			return null;
		}
		
		return index == -1 ? null : collection.get(index).getValue();
	}
	
	/**
	 * Object represents one entry capable for storing in {@code Dictionary}.<br>
	 * Key can't be null.
	 * 
	 * @author Domagoj Lokner
	 *
	 * @param <K> - type of key object.
	 * @param <V> - type of key given value.
	 */
	private static class DictionaryEntry<K, V> {
		private K key;
		private V value;
		
		/**
		 * Constructs one {@code Dictionary} entry.
		 * 
		 * @param key - key
		 * @param value - key value.
		 * 
		 * @throws NullPointerException if given key is null.
		 */
		public DictionaryEntry(K key, V value) {
			Objects.requireNonNull(key, "Key must not be null!");
			
			this.key = key;
			this.value = value;
		}

		/**
		 * Gets entry's {@code value}.
		 * 
		 * @return {@code value}.
		 */
		public V getValue() {
			return value;
		}

		/**
		 * Sets entry {@code value} to given {@code value}.
		 * 
		 * @param value - value to be set.
		 */
		public void setValue(V value) {
			this.value = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((key == null) ? 0 : key.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof DictionaryEntry)) {
				return false;
			}
			@SuppressWarnings("unchecked")
			DictionaryEntry<K, V> other = (DictionaryEntry<K, V>) obj;
			if (!key.equals(other.key)) {
				return false;
			}
			return true;
		}	
	}
}
