package hr.fer.zemris.java.custom.collections;

/**
 * Interface that test given objects.
 * 
 * @author Domagoj Lokner
 * 
 * @param <E> - type of objects to be tested.
 */
@FunctionalInterface
public interface Tester<E> { 
	
	/**
	 * Test given object.
	 * 
	 * @param obj - object to be tested.
	 * @return {@code true} if object is acceptable otherwise {@code false}.
	 */
	boolean test(E obj);
}
