package hr.fer.zemris.java.hw05.db;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * Program that reads records from database.txt placed in project folder and require 
 * database query from standard input.<br>
 * 
 * Program will process queries continuously 
 * until the "exit" is entered instead of query. 
 * 
 * @author Domagoj Lokner
 *
 */
public class StudentDB {

	/**
	 * Starting method of the program.
	 * 
	 * @param args - command line arguments.
	 */
	public static void main(String[] args) {
		
		StudentDatabase db = null;
		
		try {
			db = new StudentDatabase(Files.readAllLines(
					 Paths.get("./database.txt"),
					 StandardCharsets.UTF_8).toArray(new String[1]));
		} catch(IOException ex1) {
			System.out.println("Can't found database!");
			System.exit(1);
		} catch(IllegalArgumentException ex2) {
			System.out.println(ex2.getMessage());
			System.exit(1);
		}
		
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			
			QueryParser parser;
			
			System.out.print("> ");
			
			String keyWord = sc.next();
			
			if(keyWord.equalsIgnoreCase("exit")){
				sc.close();
				System.out.println("Goodbye!");
				return;
			}
			
			if(!keyWord.equals("query")) {
				System.out.println("Query must start with key word \"query\"!");
				sc.nextLine();
				continue;
			} else {
				try {
					parser = new QueryParser(sc.nextLine());
				} catch (QueryParserException ex) {
					System.out.println("Unvalid query! " + ex.getMessage());
					continue;
				}
			}
			
			List<StudentRecord> records = new LinkedList<>();
			List<String> output = new LinkedList<>();
			
			if(parser.isDirectQuery()) {
				
				System.out.println("Using index for record retrieval.");
				
				StudentRecord record = db.forJMBAG(parser.getQueriedJMBAG());
				if(!Objects.isNull(record)) {
					records.add(record);
				}
				
			} else {
				try {
					for(StudentRecord r : db.filter(new QueryFilter(parser.getQuery()))) {
						 records.add(r);
					}
				} catch(IllegalArgumentException ex) {
					System.out.println(ex.getMessage());
					continue;
				}
			}
						
			output = RecordFormatter.format(records);
			output.forEach(System.out::println);
			
			System.out.println("Records selected: " + records.size());
			
		}
	}

}
