package hr.fer.zemris.lsystems.impl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilderProvider;

class LSystemBuilderImplTest {

	private LSystemBuilderProvider provider = LSystemBuilderImpl::new;
	
	private LSystem system = provider.createLSystemBuilder()
		.registerCommand('F', "draw 1")
		.registerCommand('+', "rotate 60")
		.registerCommand('-', "rotate -60")
		.setOrigin(0.05, 0.4)
		.setAngle(0)
		.setUnitLength(0.9)
		.setUnitLengthDegreeScaler(1.0/3.0)
		.registerProduction('F', "F+F--F+F")
		.setAxiom("F")
		.build();
	
	@Test
	void testGenerateLevel0() {
		assertEquals("F", system.generate(0));
	}
		
	@Test
	void testGenerateLevel1() {
		assertEquals("F+F--F+F", system.generate(1));
	}
	
	@Test
	void testGenerateLevel2() {
		assertEquals("F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F", system.generate(2));
	}
	
	@Test
	void testGenerateLevel3() {
		assertEquals("F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F+F+F--F+F+F+F"+
					 "--F+F--F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F--F+F-"+
					 "-F+F+F+F--F+F+F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F", system.generate(3));
	}

}
