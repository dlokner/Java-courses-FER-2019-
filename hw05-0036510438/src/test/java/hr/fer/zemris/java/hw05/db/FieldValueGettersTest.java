package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;
import static hr.fer.zemris.java.hw05.db.FieldValueGetters.*;

import org.junit.jupiter.api.Test;

class FieldValueGettersTest {

	@Test
	void firstNameTest() {
		assertEquals("Štefko", FIRST_NAME.get(new StudentRecord("0036557968", "Štefić", "Štefko", 4)));
		assertEquals("Siniša", FIRST_NAME.get(new StudentRecord("1136157968", "Sinke", "Siniša", 5)));
		assertEquals("Damir", FIRST_NAME.get(new StudentRecord("1625526226", "Danić", "Damir", 1)));
		
		assertNotEquals("Štefica", FIRST_NAME.get(new StudentRecord("1136157968", "Sinke", "Siniša", 5)));
		assertNotEquals("damir", FIRST_NAME.get(new StudentRecord("1625526226", "Danić", "Damir", 1)));
	}
	
	@Test
	void lastNameTest() {
		assertEquals("Štefić", LAST_NAME.get(new StudentRecord("0036557968", "Štefić", "Štefko", 4)));
		assertEquals("Sinke", LAST_NAME.get(new StudentRecord("1136157968", "Sinke", "Siniša", 5)));
		assertEquals("Danić", LAST_NAME.get(new StudentRecord("1625526226", "Danić", "Damir", 1)));
		
		assertNotEquals("Štefica", LAST_NAME.get(new StudentRecord("1136157968", "Sinke", "Siniša", 5)));
		assertNotEquals("damir", LAST_NAME.get(new StudentRecord("1625526226", "Danić", "Damir", 1)));
	}
	
	@Test
	void JMBAGTest() {
		assertEquals("0036557968", JMBAG.get(new StudentRecord("0036557968", "Štefić", "Štefko", 4)));
		assertEquals("1136157968", JMBAG.get(new StudentRecord("1136157968", "Sinke", "Siniša", 5)));
		assertEquals("1625526226", JMBAG.get(new StudentRecord("1625526226", "Danić", "Damir", 1)));
		
		assertNotEquals("1625526226", JMBAG.get(new StudentRecord("1136157968", "Sinke", "Siniša", 5)));
		assertNotEquals("0036557968", JMBAG.get(new StudentRecord("1625526226", "Danić", "Damir", 1)));
	}
}
