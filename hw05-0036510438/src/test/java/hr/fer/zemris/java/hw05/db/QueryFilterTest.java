package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Test;

class QueryFilterTest {

	List<ConditionalExpression> list;
	
	public QueryFilterTest() {
		list = new LinkedList<>();
		list.add(new ConditionalExpression(FieldValueGetters.FIRST_NAME, "Josip",ComparisonOperators.EQUALS));
		list.add(new ConditionalExpression(FieldValueGetters.LAST_NAME, "*ć",ComparisonOperators.LIKE));
	}
	
	@Test
	void emptyList() {
		QueryFilter filter = new QueryFilter(new LinkedList<ConditionalExpression>());
		
		assertTrue(filter.accepts(new StudentRecord("0025478456248", "Šišić", "Siniša", 4)));
		assertTrue(filter.accepts(new StudentRecord("0052566+6566", "tadam", "student", 3)));
		assertTrue(filter.accepts(new StudentRecord(null, null, null, 3)));

	}
	
	@Test
	void testFilter() {
		QueryFilter filter = new QueryFilter(list);
		
		assertTrue(filter.accepts(new StudentRecord("0025478456248", "Šišić", "Josip", 4)));
		
		assertFalse(filter.accepts(new StudentRecord("0025478456248", "Šišić", "Siniša", 4)));
		assertFalse(filter.accepts(new StudentRecord("0025478456248", "Šišir", "Josip", 4)));
	}

}
