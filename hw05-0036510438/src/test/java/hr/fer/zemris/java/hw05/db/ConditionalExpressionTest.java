package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ConditionalExpressionTest {
	
	@Test
	void testFalse() {
		ConditionalExpression exp = new ConditionalExpression(
				FieldValueGetters.FIRST_NAME,
				"Karlo",
				ComparisonOperators.EQUALS);
		
		StudentRecord rec = new StudentRecord("0085562656", "Ivić", "Ivica", 4);
		
		assertFalse(exp.getComparisonOperator().satisfied(exp.getFieldGetter().get(rec), exp.getStringLiteral()));
	}
	
	@Test
	void testTrue() {
		ConditionalExpression exp = new ConditionalExpression(
				FieldValueGetters.FIRST_NAME,
				"Karlo",
				ComparisonOperators.EQUALS);
		
		StudentRecord rec = new StudentRecord("0085562656", "Ivić", "Karlo", 4);
		
		assertTrue(exp.getComparisonOperator().satisfied(exp.getFieldGetter().get(rec), exp.getStringLiteral()));
	}
	
	@Test
	void testConstructor() {
		ConditionalExpression exp = new ConditionalExpression(
				FieldValueGetters.LAST_NAME,
				"Siniša",
				ComparisonOperators.LESS);
		
		assertEquals(FieldValueGetters.LAST_NAME, exp.getFieldGetter());
		assertEquals(ComparisonOperators.LESS, exp.getComparisonOperator());
		assertEquals("Siniša", exp.getStringLiteral());
	}

}
