package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class QueryLexerTest {

	@Test
	void multipleOperators() {
		QueryLexer lexer = new QueryLexer("jmBag=<=>\"0085324826\" ANd Štefica    LIKE  \"A*\"");
		
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.ARGUMENT, "jmBag"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.OPERATOR, "="));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.OPERATOR, "<="));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.OPERATOR, ">"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.STRING, "0085324826"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.AND, null));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.ARGUMENT, "Štefica"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.OPERATOR, "LIKE"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.STRING, "A*"));
	}
	@Test
	void likeLowerCase() {
		QueryLexer lexer = new QueryLexer("jmBag=\"0085324826\" aNd firstName    like  \"A*\"");
		
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.ARGUMENT, "jmBag"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.OPERATOR, "="));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.STRING, "0085324826"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.AND, null));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.ARGUMENT, "firstName"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.ARGUMENT, "like"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.STRING, "A*"));
	}
	
	@Test
	void unclosedQuotes() {
		QueryLexer lexer = new QueryLexer("jmBag=\"0085324826\" aNd firstName    LIKE  \"A*");
		
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.ARGUMENT, "jmBag"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.OPERATOR, "="));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.STRING, "0085324826"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.AND, null));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.ARGUMENT, "firstName"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.OPERATOR, "LIKE"));
		
		assertThrows(QueryLexerException.class, () -> lexer.nextToken());
	}
	
	@Test
	void unsuportedSign() {
		QueryLexer lexer = new QueryLexer("jmBag=\"0085324826\" aNd firstName  /  \"A*\"");
		
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.ARGUMENT, "jmBag"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.OPERATOR, "="));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.STRING, "0085324826"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.AND, null));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.ARGUMENT, "firstName"));
		
		assertThrows(QueryLexerException.class, () -> lexer.nextToken());
	}
	
	@Test
	void getTOkenAndEOFTest() {
		QueryLexer lexer = new QueryLexer("jmBag=\"0085324826\" aNd firstName    LIKE  \"A*\"");
		
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.ARGUMENT, "jmBag"));
		checkToken(lexer.getToken(), new QueryToken(QueryTokenType.ARGUMENT, "jmBag"));
		
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.OPERATOR, "="));
		checkToken(lexer.getToken(), new QueryToken(QueryTokenType.OPERATOR, "="));
		
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.STRING, "0085324826"));
		checkToken(lexer.getToken(), new QueryToken(QueryTokenType.STRING, "0085324826"));
		
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.AND, null));
		checkToken(lexer.getToken(), new QueryToken(QueryTokenType.AND, null));

		
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.ARGUMENT, "firstName"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.OPERATOR, "LIKE"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.STRING, "A*"));
		checkToken(lexer.nextToken(), new QueryToken(QueryTokenType.EOF, null));

		assertThrows(QueryLexerException.class, () -> lexer.nextToken());
	}

	
	private void checkToken(QueryToken actual, QueryToken expected) {
		String msg = "Token are not equal.";
		assertEquals(expected.getType(), actual.getType(), msg);
		assertEquals(expected.getValue(), actual.getValue(), msg);
}

}
