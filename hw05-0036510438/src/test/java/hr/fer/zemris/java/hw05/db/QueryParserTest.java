package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class QueryParserTest {

	@Test
	void notDirectQuery() {
		QueryParser parser = new QueryParser(" jmbag =\"0123456789\" and firstName LIKE \"A*\" ");
		
		assertFalse(parser.isDirectQuery());
		assertThrows(IllegalStateException.class, () -> parser.getQueriedJMBAG());
	}
	
	@Test
	void directQuery() {
		QueryParser parser = new QueryParser(" jmbag =\"0123456789\"");
		
		assertTrue(parser.isDirectQuery());		
		assertEquals("0123456789", parser.getQueriedJMBAG());
		assertEquals(1, parser.getQuery().size());
	}
	
	@Test
	void jmbagQuery() {
		QueryParser parser = new QueryParser(" jmbag <\"0123456789\"");
		
		assertFalse(parser.isDirectQuery());		
		assertEquals(parser.getQuery().get(0).getFieldGetter(), FieldValueGetters.JMBAG);
		assertEquals(parser.getQuery().get(0).getComparisonOperator(), ComparisonOperators.LESS);
		assertEquals(parser.getQuery().get(0).getStringLiteral(), "0123456789");
		assertEquals(1, parser.getQuery().size());
	}
	
	@Test
	void firstNameQuery() {
		QueryParser parser = new QueryParser(" firstName>=     \"Štefica\"");
		
		assertFalse(parser.isDirectQuery());		
		assertEquals(parser.getQuery().get(0).getFieldGetter(), FieldValueGetters.FIRST_NAME);
		assertEquals(parser.getQuery().get(0).getComparisonOperator(), ComparisonOperators.GREATER_OR_EQUALS);
		assertEquals(parser.getQuery().get(0).getStringLiteral(), "Štefica");
		assertEquals(1, parser.getQuery().size());
	}
	
	@Test
	void lastNameQuery() {
		QueryParser parser = new QueryParser(" lastName  !=     \"Prezime\"");
		
		assertFalse(parser.isDirectQuery());		
		assertEquals(parser.getQuery().get(0).getFieldGetter(), FieldValueGetters.LAST_NAME);
		assertEquals(parser.getQuery().get(0).getComparisonOperator(), ComparisonOperators.NOT_EQUALS);
		assertEquals(parser.getQuery().get(0).getStringLiteral(), "Prezime");
		assertEquals(1, parser.getQuery().size());
	}

	@Test
	void multipleQuries() {
		QueryParser parser = 
				new QueryParser(" lastName  !=  \"Prezime\"  anD  firstName<=     \"Štefica\"  AnD jmbag <\"0123456789\"  ");
		
		assertFalse(parser.isDirectQuery());	
		
		assertEquals(parser.getQuery().get(0).getFieldGetter(), FieldValueGetters.LAST_NAME);
		assertEquals(parser.getQuery().get(0).getComparisonOperator(), ComparisonOperators.NOT_EQUALS);
		assertEquals(parser.getQuery().get(0).getStringLiteral(), "Prezime");
		
		assertEquals(parser.getQuery().get(1).getFieldGetter(), FieldValueGetters.FIRST_NAME);
		assertEquals(parser.getQuery().get(1).getComparisonOperator(), ComparisonOperators.LESS_OR_EQUALS);
		assertEquals(parser.getQuery().get(1).getStringLiteral(), "Štefica");
		
		assertEquals(parser.getQuery().get(2).getFieldGetter(), FieldValueGetters.JMBAG);
		assertEquals(parser.getQuery().get(2).getComparisonOperator(), ComparisonOperators.LESS);
		assertEquals(parser.getQuery().get(2).getStringLiteral(), "0123456789");
		
		assertEquals(3, parser.getQuery().size());
	}	

}
