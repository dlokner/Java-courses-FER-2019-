package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;
import static hr.fer.zemris.java.hw05.db.ComparisonOperators.*;

import org.junit.jupiter.api.Test;

class ComparisonOperatorsTest {

	@Test
	void LikeTest() {
		assertTrue(LIKE.satisfied("AAA", "AA*"));
		assertTrue(LIKE.satisfied("bnh3jklmaskjnKdkm", "bnh3*"));
		assertTrue(LIKE.satisfied("lkN7jsd89", "*d89"));
		
		assertTrue(LIKE.satisfied("OvoJeProba", "OvoJeProba"));
		
		assertTrue(LIKE.satisfied("AAA", "A*A"));
		assertTrue(LIKE.satisfied("AAAA", "AA*AA"));
		
		assertFalse(LIKE.satisfied("AAA", "AA*AA"));
		
		assertTrue(LIKE.satisfied("", ""));
		
		assertTrue(LIKE.satisfied("Štefica", "*"));
		
		assertThrows(IllegalArgumentException.class, () -> LIKE.satisfied("Štefica", "Šte**ca"));
		assertThrows(IllegalArgumentException.class, () -> LIKE.satisfied("Štefica", "Št*fi*a"));
	}
	
	@Test
	void LessTest() {
		assertTrue(LESS.satisfied("abc", "bbc"));
		assertTrue(LESS.satisfied("Josip", "Marko"));
		
		assertFalse(LESS.satisfied("abc", "abc"));
		
		assertFalse(LESS.satisfied("Štefica", "Štefanija"));
	}
	
	@Test
	void LessOrEqulasTest() {
		assertTrue(LESS_OR_EQUALS.satisfied("abc", "bbc"));
		assertTrue(LESS_OR_EQUALS.satisfied("Josip", "Marko"));
		
		assertTrue(LESS_OR_EQUALS.satisfied("", ""));
		assertTrue(LESS_OR_EQUALS.satisfied("abc", "abc"));
		
		assertFalse(LESS_OR_EQUALS.satisfied("Štefica", "Štefanija"));
	}
	
	@Test
	void GreaterTest() {
		assertFalse(GREATER.satisfied("abc", "bbc"));
		assertFalse(GREATER.satisfied("Josip", "Marko"));
		
		assertFalse(GREATER.satisfied("abc", "abc"));
		
		assertTrue(GREATER.satisfied("Štefica", "Štefanija"));
	}
	
	@Test
	void GreaterOrEqulasTest() {
		assertFalse(GREATER_OR_EQUALS.satisfied("abc", "bbc"));
		assertFalse(GREATER_OR_EQUALS.satisfied("Josip", "Marko"));
		
		assertTrue(GREATER_OR_EQUALS.satisfied("", ""));
		assertTrue(GREATER_OR_EQUALS.satisfied("abc", "abc"));
		
		assertTrue(GREATER_OR_EQUALS.satisfied("Štefica", "Štefanija"));	
	}
	
	@Test
	void EqualsTest() {
		assertTrue(EQUALS.satisfied("", ""));
		assertTrue(EQUALS.satisfied("abc", "abc"));
		assertTrue(EQUALS.satisfied("HdJHsdGD3gbdd457", "HdJHsdGD3gbdd457"));
		
		assertFalse(EQUALS.satisfied("abc", "bbc"));
		assertFalse(EQUALS.satisfied("Josip", "Marko"));
		assertFalse(EQUALS.satisfied("Štefica", "Štefanija"));
	}
	
	@Test
	void NotEqualsTest() {
		assertFalse(NOT_EQUALS.satisfied("", ""));
		assertFalse(NOT_EQUALS.satisfied("abc", "abc"));
		assertFalse(NOT_EQUALS.satisfied("HdJHsdGD3gbdd457", "HdJHsdGD3gbdd457"));
		
		assertTrue(NOT_EQUALS.satisfied("abc", "bbc"));
		assertTrue(NOT_EQUALS.satisfied("Josip", "Marko"));
		assertTrue(NOT_EQUALS.satisfied("Štefica", "Štefanija"));
	}

}
