package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;

class StudentDatabaseTest {
	

	StudentDatabase db;
	
	public StudentDatabaseTest() {
		try {
			db = new StudentDatabase(Files.readAllLines(
					 Paths.get("./database.txt"),
					 StandardCharsets.UTF_8).toArray(new String[1]));
		} catch (IOException e) {
			System.out.println("Can't found path to file!");
		}
	}
	
	@Test
	void forJMBAG() {
		assertEquals(new StudentRecord("0000000036", null, null, 4), db.forJMBAG("0000000036"));
		assertEquals(new StudentRecord("0000000001", null, null, 4), db.forJMBAG("0000000001"));
		assertEquals(new StudentRecord("0000000040", null, null, 4), db.forJMBAG("0000000040"));
		assertNull(db.forJMBAG("0000000000"));
		assertNull(db.forJMBAG("0000000065"));
		assertNull(db.forJMBAG("Štefica"));
	}

	@Test 
	void filterAllRecords(){
		List<StudentRecord> list = db.filter((s) -> true);
		
		assertEquals(63, list.size());
		
		assertEquals(new StudentRecord("0000000036", null, null, 4), list.get(35));
		assertEquals(new StudentRecord("0000000001", null, null, 4), list.get(0));
		assertEquals(new StudentRecord("0000000040", null, null, 4), list.get(39));	
	}
	
	@Test 
	void filterNoRecords(){
		List<StudentRecord> list = db.filter((s) -> false);
		
		assertEquals(0, list.size());
	}
	
}
