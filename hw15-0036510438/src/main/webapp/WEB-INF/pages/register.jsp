<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Register</title>

    <style type="text/css">
        .error {
            font-family: Arial;
            font-weight: bold;
            font-size: 0.9em;
            color: #FF0000;
            padding-left: 110px;
        }
        .formLabel {
            display: inline-block;
            width: 100px;
            font-weight: bold;
            text-align: right;
            padding-right: 10px;
        }
        .formControls {
            margin-top: 10px;
        }
    </style>

</head>
<body>

    <%@include file="loggedUser.jsp"%>
    <hr>

    <h1>Register</h1>

    <form action="register" method="post">

        <div>
            <c:if test="${userFormular.containsError('firstName')}">
                <div class="error">
                    <c:out value="${userFormular.getErrorMessage('firstName')}"/>
                </div>
            </c:if>

            <span class="formLabel">first name</span><input type="text" name="firstName" value="<c:out value='${userFormular.firstName}'/>" size="20">
        </div>

        <div>
            <c:if test="${userFormular.containsError('lastName')}">
                <div class="error">
                    <c:out value="${userFormular.getErrorMessage('lastName')}"/>
                </div>
            </c:if>

            <span class="formLabel">last name</span><input type="text" name="lastName" value="<c:out value='${userFormular.lastName}'/>" size="20">
        </div>

        <div>
            <c:if test="${userFormular.containsError('email')}">
                <div class="error">
                    <c:out value="${userFormular.getErrorMessage('email')}"/>
                </div>
            </c:if>

            <span class="formLabel">e-mail</span><input type="text" name="email" value="<c:out value='${userFormular.email}'/>" size="20">
        </div>

        <div>
            <c:if test="${userFormular.containsError('nick')}">
                <div class="error">
                    <c:out value="${userFormular.getErrorMessage('nick')}"/>
                </div>
            </c:if>

            <span class="formLabel">nickname</span><input type="text" name="nick" value="<c:out value='${userFormular.nick}'/>" size="20">
        </div>

        <div>
            <c:if test="${userFormular.containsError('passwordHash')}">
                <div class="error">
                    <c:out value="${userFormular.getErrorMessage('passwordHash')}"/>
                </div>
            </c:if>

            <span class="formLabel">password</span><input type="password" name="password" size="20">
        </div>

        <div class="formControls">
            <span class="formLabel">&nbsp;</span>
            <input type="submit" value="Register">
        </div>

    </form>

    <hr>
    <a href="${pageContext.request.contextPath}/index.jsp">main page</a>

</body>
</html>
