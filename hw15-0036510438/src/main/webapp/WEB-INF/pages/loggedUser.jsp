<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:choose>
    <c:when test="${sessionScope.get('current.user.id') eq null}">
        <b>not logged in</b></p>
    </c:when>
    <c:otherwise>
        <p>Logged as: <b><%= session.getAttribute("current.user.nick")%></b></p>
        <a href="${pageContext.request.contextPath}/logout">Logout</a>
    </c:otherwise>
</c:choose>
