<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <title>${blogEntry.title}</title>
    <style type="text/css">
      .error {
        font-family: Arial;
        font-weight: bold;
        font-size: 0.9em;
        color: #FF0000;
        padding-left: 110px;
      }
      .formLabel {
        display: inline-block;
        width: 100px;
        font-weight: bold;
        text-align: right;
        padding-right: 10px;
      }
      .formControls {
        margin-top: 10px;
      }
    </style>
  </head>
  <body>

      <%@include file="loggedUser.jsp"%>
      <hr>

      <h1><c:out value="${blogEntry.title}"/></h1>
      <p><c:out value="${blogEntry.text}"/></p>

      <c:if test="${!blogEntry.comments.isEmpty()}">

        <br>

        <h2>Comments:</h2>
        <ul>
          <c:forEach var="e" items="${blogEntry.comments}">
            <hr>
            <div><b><c:out value="${e.usersEMail}"/></b> - <i><c:out value="${e.postedOn}"/></i></div>
            <div style="padding-left: 10px;"><c:out value="${e.message}"/></div>
          </c:forEach>
        </ul>
      </c:if>

      <form action="${pageContext.request.contextPath}/servleti/author/${blogEntry.creator.nick}/${blogEntry.id}" method="post">
        <div>
          <c:if test="${formular.containsError('message')}">
            <div class="error">
              <c:out value="${formular.getErrorMessage('message')}"/>
            </div>
          </c:if>
          <p>Comment: </p><textarea name="message" rows="5" cols="75">${formular.message}</textarea>
        </div>

        <c:if test="${not logged}">
          <div>
            <c:if test="${formular.containsError('email')}">
              <div class="error">
                <c:out value="${formular.getErrorMessage('email')}"/>
              </div>
            </c:if>
            <p>E-Mail: </p></span><textarea name="email" rows="1" cols="75">${formular.usersEmail}</textarea>
          </div>
        </c:if>

        <div class="formControls">
          <input type="submit" value="Comment">
        </div>

        <input type="hidden" name="blogID" value="${blogEntry.id}">
      </form>

  <c:choose>
    <c:when test="${blogEntry.creator.id eq sessionScope.get('current.user.id')}">
      <hr>
      <a href="${pageContext.request.contextPath}/servleti/author/<%=session.getAttribute("current.user.nick")%>/edit?blogID=${blogEntry.id}">Edit blog</a>
    </c:when>
    <c:otherwise>
      <hr>
      <a href="${pageContext.request.contextPath}/servleti/author/${blogEntry.creator.nick}">
        see other blogs created by ${blogEntry.creator.nick}
      </a>
    </c:otherwise>
  </c:choose>

  <hr>
  <a href="${pageContext.request.contextPath}/index.jsp">main page</a>

  </body>
</html>
