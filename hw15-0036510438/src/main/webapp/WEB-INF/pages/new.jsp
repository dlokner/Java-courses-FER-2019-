<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>${method}</title>

    <style type="text/css">
        .formLabel {
            display: inline-block;
            width: 100px;
            font-weight: bold;
            text-align: right;
            padding-right: 10px;
        }
        .formControls {
            margin-top: 10px;
        }
        .error {
            font-family: Arial;
            font-weight: bold;
            font-size: 0.9em;
            color: #FF0000;
            padding-left: 110px;
        }
    </style>
</head>
<body>

    <%@include file="loggedUser.jsp"%>
    <hr>

    <h1>${method} blog</h1>

    <form action="./${method}" method="post">
        <div>
            <c:if test="${formular.containsError('title')}">
                <div class="error">
                    <c:out value="${formular.getErrorMessage('title')}"/>
                </div>
            </c:if>
            <span class="formLabel">Title</span><input type="text" name="title" value="${formular.title}" size="20">
        </div>

        <div>
            <span class="formLabel">Text</span><textarea name="text" rows="10" cols="100">${formular.text}</textarea>
        </div>

        <div class="formControls">
            <span class="formLabel">&nbsp;</span>
            <input type="submit" value="Post">
        </div>

        <input type="hidden" name="blogID" value="${formular.id}">
    </form>

</body>
</html>
