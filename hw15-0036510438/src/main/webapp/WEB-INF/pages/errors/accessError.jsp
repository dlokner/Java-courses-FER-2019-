<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Error</title>
</head>
<body>

<h1>Error</h1>

<p>Access to this page has been denied.</p><br>

<a href="${pageContext.request.contextPath}/index.jsp">main page</a>

</body>
</html>
