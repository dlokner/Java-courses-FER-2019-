<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<html>
<head>
    <title>${nick}</title>
</head>
    <body>

        <%@include file="loggedUser.jsp"%>
        <hr>

        <h1>Blogs created by ${nick}</h1>

        <c:if test="${empty blogs}">
            <p>There is no blogs created by ${nick}</p>
        </c:if>

        <c:if test="${not empty blogs}">
            <ul>
                <c:forEach var="b" items="${blogs}">
                    <li><a href="${nick}/${b.id}">${b.title}</a></li>
                </c:forEach>
            </ul>
        </c:if>

        <c:if test="${logged}">
            <hr>
            <a href="${nick}/new">Create new blog</a>
        </c:if>

        <hr>
        <a href="${pageContext.request.contextPath}/index.jsp">main page</a>

    </body>
</html>
