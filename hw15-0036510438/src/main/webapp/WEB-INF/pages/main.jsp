<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<html>
<head>
    <title>Sign in</title>

    <style type="text/css">
        .error {
            font-family: Arial;
            font-weight: bold;
            font-size: 0.9em;
            color: #FF0000;
            padding-left: 110px;
        }
        .formLabel {
            display: inline-block;
            width: 100px;
            font-weight: bold;
            text-align: right;
            padding-right: 10px;
        }
        .formControls {
            margin-top: 10px;
        }
    </style>

</head>
<body>

    <%@include file="loggedUser.jsp"%>
    <hr>

    <h1>Login</h1>

    <c:if test="${userFormular.containsError('loginError')}">
        <div class="error">
            <b><c:out value="${userFormular.getErrorMessage('loginError')}"/></b>
        </div>
        <br>
    </c:if>

    <form action="main" method="post">

        <div>
            <c:if test="${userFormular.containsError('nick')}">
                <div class="error">
                    <c:out value="${userFormular.getErrorMessage('nick')}"/>
                </div>
            </c:if>

            <span class="formLabel">nickname</span><input type="text" name="nick" value="<c:out value='${userFormular.nick}'/>" size="20">
        </div>

        <div>
            <c:if test="${userFormular.containsError('passwordHash')}">
                <div class="error">
                    <c:out value="${userFormular.getErrorMessage('passwordHash')}"/>
                </div>
            </c:if>

            <span class="formLabel">password</span><input type="password" name="password" size="20">
        </div>

        <div class="formControls">
            <span class="formLabel">&nbsp;</span>
            <input type="submit" value="Login">
        </div>

    </form>

    <a href="register">Register</a>

    <c:if test="${not empty users}">
        <hr>

        <h2>Registered users:</h2>

        <ul>
            <c:forEach var="user" items="${users}">
                <li><a href="author/${user.nick}">${user.nick}</a></li>
            </c:forEach>
        </ul>
    </c:if>

</body>
</html>
