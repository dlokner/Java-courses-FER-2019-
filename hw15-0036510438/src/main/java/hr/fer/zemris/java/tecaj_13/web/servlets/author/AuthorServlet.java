package hr.fer.zemris.java.tecaj_13.web.servlets.author;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * Router servlet that redirect all request with <i>"/servleti/author/*"</i> addresses.
 *
 * @author Domagoj Lokner
 */
@WebServlet("/servleti/author/*")
public class AuthorServlet extends HttpServlet {

	private static final long serialVersionUID = 8210646013035281516L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] URLInfo = parseURL(req.getPathInfo().substring(1));

        if (Objects.isNull(URLInfo)) {
            req.getRequestDispatcher("/WEB-INF/pages/errors/accessError.jsp").forward(req, resp);
            return;
        }

        switch (URLInfo.length) {
            case (1):
                req.getRequestDispatcher("../user?nick=" + URLInfo[0]).forward(req, resp);
                break;
            case (2):
                req.getRequestDispatcher("../../user/action?nick=" + URLInfo[0] + "&action=" + URLInfo[1]).forward(req, resp);
                break;
            default:
                req.getRequestDispatcher("/WEB-INF/pages/errors/accessError.jsp").forward(req, resp);
        }
    }

    /**
     * Parses given URL. URL will be splitted by last '/'.
     *
     * @param URLInfo URL to be parsed
     * @return splitted URL info.
     */
    private String[] parseURL(String URLInfo) {
        int index = URLInfo.indexOf('/');

        if (index == -1) {
            return new String[]{URLInfo};
        }

        if (index == 0 || index == URLInfo.length() - 1) {
            return null;
        }

        return new String[]{URLInfo.substring(0, index), URLInfo.substring(index+1)};
    }
}
