package hr.fer.zemris.java.tecaj_13.web.servlets;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogComment;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;
import hr.fer.zemris.java.tecaj_13.web.servlets.formulars.BlogCommentFormular;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * Servlet that collect all informations needed to display blog to user.
 *
 * @author Domagoj Lokner
 */
@WebServlet("/servleti/user/eid")
public class EidServlet extends HttpServlet {

	private static final long serialVersionUID = -6270410853639990961L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BlogEntry entry = getEntry(req, resp);
        Object nick = req.getAttribute("nick");

        if (Util.sendErrorIfNull(req, resp, nick, entry)) {
            return;
        }

        boolean logged = !Objects.isNull(req.getSession().getAttribute("current.user.id"));

        if (!nick.toString().equals(entry.getCreator().getNick())) {
            Util.sendErrorIfNull(req, resp, (Object)null);
            return;
        }

        req.setAttribute("formular", new BlogCommentFormular());
        req.setAttribute("blogEntry", entry);
        req.setAttribute("logged", logged);

        req.getRequestDispatcher("/WEB-INF/pages/displayBlog.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Object comment = req.getParameter("message");
        Object blogID = req.getParameter("blogID");
        Object nick = req.getParameter("nick");

        String email = getEmail(req, resp);


        if (Util.sendErrorIfNull(req, resp, email, comment, blogID, nick)) {
            return;
        }

        BlogEntry entry = DAOProvider.getDAO().getBlogEntry(Long.parseLong(blogID.toString()));

        if (Util.sendErrorIfNull(req, resp, entry)) {
            return;
        }

        BlogCommentFormular cf = new BlogCommentFormular();
        cf.setId("");
        cf.setUsersEmail(email.toString());
        cf.setMessage(comment.toString());

        cf.validate();

        if(cf.containsErrors()) {
            req.setAttribute("formular", cf);
            req.setAttribute("blogEntry", entry);
            req.setAttribute("logged", !Objects.isNull(req.getSession().getAttribute("current.user.id")));

            req.getRequestDispatcher("/WEB-INF/pages/displayBlog.jsp").forward(req, resp);
            return;
        }

        BlogComment com = new BlogComment();
        cf.fillBlogComment(com);
        com.setBlogEntry(entry);

        DAOProvider.getDAO().newComment(com);

        resp.sendRedirect(req.getContextPath() + "/servleti/author/" + nick + "/" + entry.getId());

    }

    private String getEmail(HttpServletRequest req, HttpServletResponse resp) {
        Object paramEmail = req.getParameter("email");

        if (paramEmail != null) {
            return paramEmail.toString();
        }

        Object user = req.getSession().getAttribute("current.user.nick");

        if (user == null) {
            return null;
        }

        BlogUser bUser = DAOProvider.getDAO().getBlogUser(user.toString());

        if (bUser == null) {
            return null;
        }
        return bUser.getEmail();
    }

    private BlogEntry getEntry(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        Object idObj = req.getParameter("blogID");

        if(Util.sendErrorIfNull(req, resp, idObj)) {
            return null;
        }

        Long blogID;

        try {
            blogID = Long.parseLong(idObj.toString());
        } catch (NumberFormatException e) {
            Util.sendErrorIfNull(req, resp, (Object)null);
            return null;
        }

        BlogEntry entry = DAOProvider.getDAO().getBlogEntry(blogID);

        if (Util.sendErrorIfNull(req, resp, entry)) {
            return null;
        }

        return entry;
    }
}
