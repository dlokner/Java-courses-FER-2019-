package hr.fer.zemris.java.tecaj_13.model;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "BlogUser.forNick",
                query = "select u from BlogUser as u where u.nick=:nick"),
        @NamedQuery(name = "BlogUser.getUsers",
                query = "select u from BlogUser as u")
})

/**
 * Object that represents single blog user.
 *
 * @author Domagoj Lokner
 */
@Entity
@Table(name="blog_users")
public class BlogUser {

    /**
     * User identificator.
     */
    private Long id;

    /**
     * User's first name.
     */
    private String firstName;

    /**
     * User's last name.
     */
    private String lastName;

    /**
     * User's nickname
     */
    private String nick;

    /**
     * User's email address.
     */
    private String email;

    /**
     * Hash of user's password
     */
    private String passwordHash;

    /**
     * List of blogs this user created.
     */
    private List<BlogEntry> blogs = new LinkedList<>();

    /**
     * Constructs {@code BlogUser}.
     *
     * @param id user's identificator.
     * @param firstName user's name.
     * @param lastName user's last name.
     * @param nick nickname.
     * @param email email address.
     * @param passwordHash hash of user's password.
     */
    public BlogUser(Long id, String firstName, String lastName, String nick, String email, String passwordHash) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nick = nick;
        this.email = email;
        this.passwordHash = passwordHash;
    }

    /**
     * Constructs {@code BlogUser}.
     */
    public BlogUser() {
    }

    /**
     * Gets user's identificator.
     *
     * @return user's ID.
     */
    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    /**
     * Sets user's ID.
     *
     * @param id ID to be set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets first name.
     *
     * @return first name.
     */
    @Column(length = 50, nullable=true)
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName first name to be set.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets last name.
     *
     * @return last name.
     */
    @Column(length = 50, nullable=true)
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName last name to be set.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets nickname.
     *
     * @return nickname.
     */
    @Column(length = 50, nullable=false, unique=true)
    public String getNick() {
        return nick;
    }

    /**
     * Sets nickname.
     *
     * @param nick nickname to be set.
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     * Gets e-mail.
     *
     * @return e-mail.
     */
    @Column(nullable=false)
    public String getEmail() {
        return email;
    }

    /**
     * Sets e-mail.
     *
     * @param email e-mail to be set.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets hash of password.
     *
     * @return hash of password.
     */
    @Column(nullable=false)
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * Sets password hash.
     *
     * @param passwordHash password hash to be set.
     */
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    /**
     * Gets list of all blogs this user created.
     *
     * @return list of blogs.
     */
    @OneToMany(mappedBy="creator", fetch=FetchType.LAZY, cascade=CascadeType.PERSIST, orphanRemoval=true)
    public List<BlogEntry> getBlogs() {
        return blogs;
    }

    /**
     * Sets list of blogs this user created.
     *
     * @param blogs list of blogs to be set.
     */
    public void setBlogs(List<BlogEntry> blogs) {
        this.blogs = blogs;
    }
}
