package hr.fer.zemris.java.tecaj_13.web.servlets;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;
import hr.fer.zemris.java.tecaj_13.web.servlets.formulars.BlogUserFormular;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * Current that dispatch request to main page.
 * This servlet performs login of registered users.
 *
 * @author Domagoj Lokner
 */
@WebServlet(urlPatterns = "/servleti/main")
public class MainServlet extends HttpServlet {

	private static final long serialVersionUID = 8067748696702751032L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BlogUser user = new BlogUser();

        BlogUserFormular formular = new BlogUserFormular();
        formular.fillUsingBlogUser(user);

        req.setAttribute("userFormular", formular);

        List<BlogUser> users = DAOProvider.getDAO().getUsers();
        req.getServletContext().setAttribute("users", users);

        req.getRequestDispatcher("/WEB-INF/pages/main.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        BlogUserFormular f = new BlogUserFormular();
        f.fillUsingHttpRequest(req);

        Util.setHashPassword(req, f);

        if (f.getNick().isEmpty() || f.getPasswordHash().isEmpty() || !checkUser(f)) {
            f.loginError();
            req.setAttribute("userFormular", f);
            req.getRequestDispatcher("/WEB-INF/pages/main.jsp").forward(req, resp);
            return;
        }

        BlogUser user = DAOProvider.getDAO().getBlogUser(f.getNick());
        HttpSession session = req.getSession();

        setCurrentUser(user, session);

        resp.sendRedirect(req.getContextPath() + "/index.jsp");
    }

    /**
     * Sets current user.
     *
     * @param user user to be logged in.
     * @param session Http session.
     */
    private static void setCurrentUser(BlogUser user, HttpSession session) {
        session.setAttribute("current.user.id", user.getId());
        session.setAttribute("current.user.fm", user.getFirstName());
        session.setAttribute("current.user.ln", user.getLastName());
        session.setAttribute("current.user.nick", user.getNick());
    }

    /**
     * Checks if user which record is represented with given formular
     * have valid username and password.
     *
     * @param f formular.
     * @return {@code true} if user id registered in database otherwise {@code false}.
     */
    private static boolean checkUser(BlogUserFormular f) {
        String nick = f.getNick();
        String pass = f.getPasswordHash();

        BlogUser user = DAOProvider.getDAO().getBlogUser(nick);

        if (Objects.isNull(user)) {
            return false;
        }

        return user.getPasswordHash().equals(pass);
    }
}
