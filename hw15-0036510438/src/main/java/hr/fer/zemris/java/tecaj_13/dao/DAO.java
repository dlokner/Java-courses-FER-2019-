package hr.fer.zemris.java.tecaj_13.dao;

import hr.fer.zemris.java.tecaj_13.model.BlogComment;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;
import hr.fer.zemris.java.tecaj_13.web.servlets.formulars.BlogEntryFormular;

import java.util.List;

/**
 * Interface to date persistence layer.
 *
 * @author Domagoj Lokner
 */
public interface DAO {

	/**
	 * Gets {@code BlogEntry} with given {@code id}.
	 * If that entry is not present {@code null} will be returned.
	 *
	 * @param id entry identificator.
	 * @return user with {@code id} idnetificator.
	 * @throws DAOException if error occurs during getting arguments
	 * 						stored in database.
	 */
	BlogEntry getBlogEntry(Long id) throws DAOException;

	/**
	 * Gets {@code BlogUser} with given {@code id}.
	 * If that user is not present {@code null} will be returned.
	 *
	 * @param id user's identificator.
	 * @return user with {@code id} idnetificator.
	 * @throws DAOException if error occurs during getting arguments
	 * 						stored in database.
	 */
	BlogUser getBlogUser(Long id) throws DAOException;

	/**
	 * Gets {@code BlogUser} with given nickname.
	 * If that user is not present {@code null} will be returned.
	 *
	 * @param nickname user's nickname.
	 * @return user with nick argument equals to {@code nickname}.
	 * @throws DAOException if error occurs during getting arguments
	 * 						stored in database.
	 */
	BlogUser getBlogUser(String  nickname) throws DAOException;

	/**
	 * Gets list of all users.
	 * If there is no registered users {@code null}
	 * will be returned.
	 *
	 * @return list of all users.
	 * @throws DAOException if error occurs during getting arguments
	 * 						stored in database.
	 */
	List<BlogUser> getUsers() throws DAOException;

	/**
	 * Registers given user.
	 *
	 * @param user user to be registered.
	 * @throws DAOException if error occurs during getting arguments
	 * 						stored in database.
	 */
	void registerUser(BlogUser user) throws DAOException;

	/**
	 * Stores given {@code blog} into database.
	 *
	 * @param blog blog to be stored into database.
	 * @throws DAOException if error occurs during getting arguments
	 * 						stored in database.
	 */
	void newBlog(BlogEntry blog) throws DAOException;

	/**
	 * Updates blog stored in database represented with
	 * given formular.
	 *
	 * @param ef blog formular. Blog will be updated with informations from
	 *           this formular.
	 * @throws DAOException if error occurs during getting arguments
	 * 						stored in database.
	 */
	void editBlog(BlogEntryFormular ef) throws DAOException;

	/**
	 * Stores new comment into database.
	 *
	 * @param com comment to be stored into database.
	 * @throws DAOException if error occurs during getting arguments
	 * 						stored in database.
	 */
	void newComment(BlogComment com) throws DAOException;
	
}