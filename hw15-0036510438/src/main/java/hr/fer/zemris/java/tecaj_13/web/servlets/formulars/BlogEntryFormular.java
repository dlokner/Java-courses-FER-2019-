package hr.fer.zemris.java.tecaj_13.web.servlets.formulars;

import hr.fer.zemris.java.tecaj_13.model.BlogEntry;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class BlogEntryFormular {

    /**
     * Entry identificator.
     */
    private String id;

    /**
     * Blog title.
     */
    private String title;

    /**
     * Blog body.
     */
    private String text;

    /**
     * Error map.
     * Expected key is property name and expected value is error message.
     */
    Map<String, String> errors = new HashMap<>();

    /**
     * Constructs {@code BlogUser}.
     */
    public BlogEntryFormular() {
    }

    /**
     * Gets error message for requested property.
     *
     * @param name property name.
     * @return error message or {@code null} if there is no error for
     *         requested property.
     */
    public String getErrorMessage(String name) {
        return errors.get(name);
    }

    /**
     * Checks if there are any errors in this formular.
     *
     * @return {@code true} if formular contains errors.
     */
    public boolean containsErrors() {
        return !errors.isEmpty();
    }

    /**
     * Checks if formular contains error for given property name.
     *
     * @param name property name.
     * @return {@code true} if formular contains error for given property name.
     */
    public boolean containsError(String name) {
        return errors.containsKey(name);
    }

    /**
     * Fills this formular using HTTP request parameters.
     *
     * @param req HTTP request.
     */
    public void fillUsingHttpRequest(HttpServletRequest req) {
        this.id = prepare(req.getParameter("id"));
        this.title = prepare(req.getParameter("title"));
        this.text = req.getParameter("text");
    }

    /**
     * Converts given string in more convenient format.
     *
     * @param s string to be converted.
     * @return prepared string or an empty string if {@code null} is given.
     */
    private String prepare(String s) {
        if (Objects.isNull(s)) {
            return "";
        }
        return s.strip();
    }

    /**
     * Fills this formular using {@code BlogEntry} parameters.
     *
     * @param e blog entry from which formular will be filled.
     */
    public void fillUsingBlogEntry(BlogEntry e) {
        if (Objects.isNull(e.getId())) {
            this.id = "";
        } else {
            this.id = e.getId().toString();
        }

        this.title = e.getTitle();
        this.text = e.getText();
    }

    /**
     * Fill {@code BlogEntry} object using this formular.
     *
     * @param e {@code BlogEntry} to be filled.
     */
    public void fillBlogEntry(BlogEntry e) {
        if (this.id.isEmpty()) {
            e.setId(null);
        } else {
            e.setId(Long.valueOf(this.id));
        }

        e.setTitle(this.title);
        e.setText(this.text);
    }

    /**
     * Validates this formular. Method will check all properties
     * and register all errors.
     */
    public void validate() {
        errors.clear();

        if (this.title.length() > 50) {
            errors.put("title", "Title must contains less than 50 signs.");
        } else if (this.title.isEmpty()) {
            errors.put("title", "Title is required.");
        }
    }

    /**
     * Gets user's identificator.
     *
     * @return user's ID.
     */
    public String getId() {
        return id;
    }

    /**
     * Sets user's ID.
     *
     * @param id ID to be set.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets title.
     *
     * @return title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title title to be set.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets text.
     *
     * @return text.
     */
    public String getText() {
        return text;
    }

    /**
     * Sets text.
     *
     * @param text text. to be set.
     */
    public void setText(String text) {
        this.text = text;
    }
}
