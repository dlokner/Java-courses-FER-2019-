package hr.fer.zemris.java.tecaj_13.crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

/**
 * Class that offers static methods to work with hex numbers and byte arrays.
 * 
 * @author Domagoj Lokner
 *
 */
public class Encoder {

	
	/**
	 * Converts given hex number to byte array.
	 * 
	 * @param keyText - hex number.
	 * @return byte array that represents given hex number.
	 */
	public static byte[] hextobyte(String keyText) {
		Objects.requireNonNull(keyText);
		
		byte[] result = new byte[keyText.length()/2];
		
		if(keyText.length() % 2 != 0) {
			throw new IllegalArgumentException("Odd hex number length!");
		}
				
		for(int i = 0, j = 0, n = keyText.length(); i < n; i +=2, ++j) {
			
			try {
				result[j] = (byte) (
						Integer.parseInt(keyText, i, i+2, 16) 
				);
				
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Given expression can not be inpreated as hex number!");
			}
		}
		
		return result;
	}
	
	/**
	 * Converts given byte array to hex number.
	 * 
	 * @param bytearray - byte array to be converted to hex number.
	 * @return string that represents hex number.
	 */
	public static String bytetohex(byte[] bytearray) {
		Objects.requireNonNull(bytearray);

		StringBuilder result = new StringBuilder();
		
		for(int i = 0, n = bytearray.length; i < n; ++i) {
			
			String digit = Integer.toString(Byte.toUnsignedInt(bytearray[i]), 16);
			
			if(digit.length() == 1) {
				result.append('0');
			}
			
			result.append(digit);
		}
		
		return result.toString();
	}

	/**
	 * Compute SHA-1 digest by given string.
	 *
	 * @param s string for which digest will be computed.
	 * @return byte array represents encoded parameter {@code s}.
	 */
	public static byte[] computeSHA(String s) {
		Objects.requireNonNull(s);

		MessageDigest md;

		try {
			md = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException ex) {
			throw new IllegalArgumentException("Given algorithm is not recognized!");
		}

		md.update(s.getBytes());

		byte[] passwordSHA = md.digest();

		return passwordSHA;
	}
}