package hr.fer.zemris.java.tecaj_13.web.servlets;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.web.servlets.formulars.BlogEntryFormular;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * Servlet that checks if user is allowed to edit requested blog, if user is allowed servlet will
 * dispatch request to scriptlet used for blog editing.
 *
 * @author Domagoj Lokner
 */
@WebServlet("/servleti/user/edit")
public class EditServlet extends HttpServlet {

	private static final long serialVersionUID = 5474919476481331338L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Object nickO = req.getAttribute("nick");
        Object entryO = req.getParameter("blogID");

        if (Util.sendErrorIfNull(req, resp, nickO, entryO)) {
            return;
        }

        String nick = nickO.toString();
        BlogEntry e = DAOProvider.getDAO().getBlogEntry(Long.parseLong(entryO.toString()));

        if (!e.getCreator().getNick().equals(nick) || !nick.equals(req.getSession().getAttribute("current.user.nick"))) {
            Util.sendErrorIfNull(req, resp, (Objects)null);
        }

        if (Util.isLogged(nick, req.getSession()) && !Objects.isNull(e)) {

            BlogEntryFormular ef = new BlogEntryFormular();
            ef.fillUsingBlogEntry(e);

            req.setAttribute("formular", ef);
            req.setAttribute("method", "edit");

            req.getRequestDispatcher("/WEB-INF/pages/new.jsp").forward(req, resp);
        } else {
            req.getRequestDispatcher("/WEB-INF/pages/errors/accessError.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("title");
        String text = req.getParameter("text");

        if (Util.sendErrorIfNull(req, resp, text, title)) {
            return;
        }

        req.setCharacterEncoding("UTF-8");

        BlogEntryFormular ef = new BlogEntryFormular();
        ef.fillUsingHttpRequest(req);
        ef.setId(req.getParameter("blogID"));

        ef.validate();

        if (ef.containsErrors()) {
            req.setAttribute("formular", ef);
            req.setAttribute("method", "edit");
            req.getRequestDispatcher("/WEB-INF/pages/new.jsp").forward(req, resp);
            return;
        }

        DAOProvider.getDAO().editBlog(ef);

        resp.sendRedirect(req.getContextPath() + "/servleti/author/" + req.getSession().getAttribute("current.user.nick").toString());
    }
}
