package hr.fer.zemris.java.tecaj_13.web.servlets.author;

import hr.fer.zemris.java.tecaj_13.web.servlets.Util;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Router servlet that dispatch requests with actions specific for defined user.
 *
 * @author Domagoj Lokner
 */
@WebServlet("/servleti/user/action")
public class UserActionServlet extends HttpServlet {

	private static final long serialVersionUID = -8117779734956103319L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    /**
     * Method that handle all requests this servlet receive.
     * Used to process bot POST and GET method.
     *
     * @param req request
     * @param resp response
     * @throws ServletException
     * @throws IOException
     */
    private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nick = req.getParameter("nick");
        String action = req.getParameter("action");

        if (Util.sendErrorIfNull(req, resp, nick, action)) {
            return;
        }

        req.setAttribute("nick", nick);

        switch (action) {
            case ("new"):
                req.getRequestDispatcher("new").forward(req, resp);
                return;
            case ("edit"):
                req.getRequestDispatcher("edit").forward(req, resp);
                return;
            default:
                req.setAttribute("eid", action);
                req.getRequestDispatcher("eid?blogID=" + action).forward(req, resp);
                return;
        }
    }

}
