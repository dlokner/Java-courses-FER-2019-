package hr.fer.zemris.java.tecaj_13.web.servlets.formulars;

import hr.fer.zemris.java.tecaj_13.model.BlogComment;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Formular used for detecting errors in blog comments.
 *
 * @author Domagoj Lokner
 */
public class BlogCommentFormular {

    /**
     * Regex used for detecting invalid email address.
     */
    private static final String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:" +
            "[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")" +
            "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\" +
            "[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?" +
            "|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|" +
            "\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    /**
     * Entry identificator.
     */
    private String id;

    /**
     * User's email.
     */
    private String usersEmail;

    /**
     * Comment message.
     */
    private String message;

    /**
     * Error map.
     * Expected key is property name and expected value is error message.
     */
    Map<String, String> errors = new HashMap<>();

    /**
     * Constructs {@code BlogCommentFormular}.
     */
    public BlogCommentFormular() {
    }

    /**
     * Gets error message for requested property.
     *
     * @param name property name.
     * @return error message or {@code null} if there is no error for
     *         requested property.
     */
    public String getErrorMessage(String name) {
        return errors.get(name);
    }

    /**
     * Checks if there are any errors in this formular.
     *
     * @return {@code true} if formular contains errors.
     */
    public boolean containsErrors() {
        return !errors.isEmpty();
    }

    /**
     * Checks if formular contains error for given property name.
     *
     * @param name property name.
     * @return {@code true} if formular contains error for given property name.
     */
    public boolean containsError(String name) {
        return errors.containsKey(name);
    }

    /**
     * Fills this formular using HTTP request parameters.
     *
     * @param req HTTP request.
     */
    public void fillUsingHttpRequest(HttpServletRequest req) {
        this.id = prepare(req.getParameter("id"));
        this.usersEmail = prepare(req.getParameter("email"));
        this.message = req.getParameter("message");
    }

    /**
     * Converts given string in more convenient format.
     *
     * @param s string to be converted.
     * @return prepared string or an empty string if {@code null} is given.
     */
    private String prepare(String s) {
        if (Objects.isNull(s)) {
            return "";
        }
        return s.strip();
    }

    /**
     * Fills this formular using {@code BlogEntry} parameters.
     *
     * @param c blog entry from which formular will be filled.
     */
    public void fillUsingBlogEntry(BlogComment c) {
        if (Objects.isNull(c.getId())) {
            this.id = "";
        } else {
            this.id = c.getId().toString();
        }

        this.usersEmail = c.getUsersEMail();
        this.message = c.getMessage();
    }

    /**
     * Fill {@code BlogEntry} object using this formular.
     *
     * @param c {@code BlogEntry} to be filled.
     */
    public void fillBlogComment(BlogComment c) {
        if (this.id.isEmpty()) {
            c.setId(null);
        } else {
            c.setId(Long.valueOf(this.id));
        }

        c.setUsersEMail(this.usersEmail);
        c.setMessage(this.message);
    }

    /**
     * Validates this formular. Method will check all properties
     * and register all errors.
     */
    public void validate() {
        errors.clear();

        if (this.message.isEmpty()) {
            errors.put("message", "Message is required.");
        }

        if (!this.usersEmail.matches(EMAIL_REGEX)) {
            errors.put("email", "Invalid E-Mail");
        }
    }

    /**
     * Gets user's identificator.
     *
     * @return user's ID.
     */
    public String getId() {
        return id;
    }

    /**
     * Sets user's ID.
     *
     * @param id ID to be set.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets users e-mail address.
     * @return e-mail.
     */
    public String getUsersEmail() {
        return usersEmail;
    }

    /**
     * Sets users e-mail address.
     *
     * @param usersEmail e-mail address to be set.
     */
    public void setUsersEmail(String usersEmail) {
        this.usersEmail = usersEmail;
    }

    /**
     * Gets comment.
     * @return comment.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets comment.
     *
     * @param message comment to be set.
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
