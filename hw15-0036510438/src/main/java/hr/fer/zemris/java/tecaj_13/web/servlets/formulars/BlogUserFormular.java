package hr.fer.zemris.java.tecaj_13.web.servlets.formulars;

import hr.fer.zemris.java.tecaj_13.crypto.Encoder;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Formular used for detecting invalid arguments in {@code BlogUser}.
 *
 * @author Domagoj Lokner
 */
public class BlogUserFormular {

    /**
     * Hash of empty string.
     */
    private static final String EMPTY_HASH = Encoder.bytetohex(Encoder.computeSHA(""));

    /**
     * Regular expression used for detecting invalid E-Mail addresses.
     */
    private static final String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:" +
            "[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")" +
            "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\" +
            "[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?" +
            "|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|" +
            "\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    /**
     * User identificator.
     */
    private String id;

    /**
     * User's first name.
     */
    private String firstName;

    /**
     * User's last name.
     */
    private String lastName;

    /**
     * User's nickname
     */
    private String nick;

    /**
     * User's email address.
     */
    private String email;

    /**
     * Hash of user's password
     */
    private String passwordHash;

    /**
     * Error map.
     * Expected key is property name and expected value is error message.
     */
    Map<String, String> errors = new HashMap<>();

    /**
     * Constructs {@code BlogUser}.
     */
    public BlogUserFormular() {
    }

    /**
     * Gets error message for requested property.
     *
     * @param name property name.
     * @return error message or {@code null} if there is no error for
     *         requested property.
     */
    public String getErrorMessage(String name) {
        return errors.get(name);
    }

    /**
     * Checks if there are any errors in this formular.
     *
     * @return {@code true} if formular contains errors.
     */
    public boolean containsErrors() {
        return !errors.isEmpty();
    }

    /**
     * Checks if formular contains error for given property name.
     *
     * @param name property name.
     * @return {@code true} if formular contains error for given property name.
     */
    public boolean containsError(String name) {
        return errors.containsKey(name);
    }

    /**
     * Fills this formular using HTTP request parameters.
     *
     * @param req HTTP request.
     */
    public void fillUsingHttpRequest(HttpServletRequest req) {
        this.id = prepare(req.getParameter("id"));
        this.firstName = prepare(req.getParameter("firstName"));
        this.lastName = prepare(req.getParameter("lastName"));
        this.nick = prepare(req.getParameter("nick"));
        this.email = prepare(req.getParameter("email"));
        this.passwordHash = prepare(req.getParameter("passwordHash"));
    }

    /**
     * Converts given string in more convenient format.
     *
     * @param s string to be converted.
     * @return prepared string or an empty string if {@code null} is given.
     */
    private String prepare(String s) {
        if (Objects.isNull(s)) {
            return "";
        }
        return s.strip();
    }

    /**
     * Fills this formular using {@code BlogUser} parameters.
     *
     * @param bu blog user from which formular will be filled.
     */
    public void fillUsingBlogUser(BlogUser bu) {
        if (Objects.isNull(bu.getId())) {
            this.id = "";
        } else {
            this.id = bu.getId().toString();
        }

        this.firstName = bu.getFirstName();
        this.lastName = bu.getLastName();
        this.nick = bu.getNick();
        this.email = bu.getEmail();
        this.passwordHash = bu.getPasswordHash();
    }

    /**
     * Fill {@code BlogUser} object using this formular.
     *
     * @param bu {@code BlogUser} to be filled.
     */
    public void fillBlogUser(BlogUser bu) {
        if (this.id.isEmpty()) {
            bu.setId(null);
        } else {
            bu.setId(Long.valueOf(this.id));
        }

        bu.setFirstName(this.firstName);
        bu.setLastName(this.lastName);
        bu.setNick(this.nick);
        bu.setEmail(this.email);
        bu.setPasswordHash(this.passwordHash);
    }

    /**
     * Validates this formular. Method will check all properties
     * and register all errors.
     */
    public void validate() {
        errors.clear();

        if (!this.id.isEmpty()) {
            try {
                Long.parseLong(this.id);
            } catch (NumberFormatException ex) {
                errors.put("id", "Invalid ID value.");
            }
        }

        if (this.firstName.length() > 50 ) {
            errors.put("firstName", "Name must contains less than 50 signs.");
        }

        if (this.lastName.length() > 50) {
            errors.put("lastName", "Last name must contains less than 50 signs.");
        }

        if (this.nick.length() > 50) {
            errors.put("nick", "Nickname must contains less than 50 signs.");
        } else if (this.nick.isEmpty()) {
            errors.put("nick", "Nickname is required.");
        }

        if (this.email.isEmpty()) {
            errors.put("email", "E-Mail is required.");
        } else if (!this.email.matches(EMAIL_REGEX)) {
            errors.put("email", "Invalid E-Mail address.");
        }

        if (this.passwordHash.equals(EMPTY_HASH)) {
            errors.put("passwordHash", "Password is required.");
        }
    }

    /**
     * Register login error stored under <i>loginError</i> key.
     */
    public void loginError() {
        validate();
        if (!containsError("nick") && !containsError("passwordHash")) {
            errors.put("loginError", "Invalid nickname or password");
        }
    }

    /**
     * Register login error stored under <i>loginError</i> key.
     */
    public void setUsedNickError() {
        validate();
        errors.put("nick", "nickname is already used");
    }

    /**
     * Gets user's identificator.
     *
     * @return user's ID.
     */
    public String getId() {
        return id;
    }

    /**
     * Sets user's ID.
     *
     * @param id ID to be set.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets first name.
     *
     * @return first name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName first name to be set.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets last name.
     *
     * @return last name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName last name to be set.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets nickname.
     *
     * @return nickname.
     */
    public String getNick() {
        return nick;
    }

    /**
     * Sets nickname.
     *
     * @param nick nickname to be set.
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     * Gets e-mail.
     *
     * @return e-mail.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets e-mail.
     *
     * @param email e-mail to be set.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets hash of password.
     *
     * @return hash of password.
     */
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * Sets password hash.
     *
     * @param passwordHash password hash to be set.
     */
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
}
