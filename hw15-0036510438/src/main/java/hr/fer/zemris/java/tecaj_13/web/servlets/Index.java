package hr.fer.zemris.java.tecaj_13.web.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Redirects request to main page (<i>/servleti/main</i>).
 *
 * @author Domagoj Lokner
 */
@WebServlet(urlPatterns = "/index.jsp")
public class Index extends HttpServlet {

	private static final long serialVersionUID = 8170180513814741436L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("servleti/main");
    }
}
