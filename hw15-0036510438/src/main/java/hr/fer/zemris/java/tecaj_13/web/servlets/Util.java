package hr.fer.zemris.java.tecaj_13.web.servlets;

import hr.fer.zemris.java.tecaj_13.crypto.Encoder;
import hr.fer.zemris.java.tecaj_13.web.servlets.formulars.BlogUserFormular;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

public class Util {

    /**
     * Checks if user with given {@code nick} is currently logged user.
     *
     * @param nick nickname of user to be checked.
     * @param session Http session.
     * @return {@code true} if user with given nick is currently logged.
     */
    public static boolean isLogged(String nick, HttpSession session) {

        Object loggedUser = session.getAttribute("current.user.nick");

        if (Objects.isNull(loggedUser)) {
            return false;
        }

        return nick.equals(loggedUser.toString());
    }

    /**
     * Dispatch request to error page if any of arguments
     * given as {@code o} is {@code null}.
     *
     * @param req http request.
     * @param resp http response.
     * @param o array of objects to checked.
     * @return {@code true} if response has been sent.
     * @throws IOException
     * @throws ServletException
     */
    public static boolean sendErrorIfNull(HttpServletRequest req, HttpServletResponse resp, Object... o)
            throws IOException, ServletException {

        boolean flag = false;

        for (Object obj : o) {
            if (Objects.isNull(obj)) {
                flag = true;
            }
        }

        if (flag) {
            req.getRequestDispatcher("/WEB-INF/pages/errors/accessError.jsp").forward(req, resp);
        }
        return flag;
    }

    /**
     * Compute hash of password form requests parameters.
     * Password is expected to be mapped by <i>password</i> key in req parameters.
     * If <i>password</i> request is not present empty string will be used as
     * password.
     *
     * @param req Http request.
     * @param f formular in which password will be stored.
     */
    public static void setHashPassword(HttpServletRequest req, BlogUserFormular f) {
        String pass = req.getParameter("password");

        if (Objects.isNull(pass)) {
            f.setPasswordHash("");
            return;
        }

        pass = Encoder.bytetohex(Encoder.computeSHA(pass));
        f.setPasswordHash(pass);
    }
}
