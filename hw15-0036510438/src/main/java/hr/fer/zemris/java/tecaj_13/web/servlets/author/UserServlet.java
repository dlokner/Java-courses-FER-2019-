package hr.fer.zemris.java.tecaj_13.web.servlets.author;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;
import hr.fer.zemris.java.tecaj_13.web.servlets.Util;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * Servlet used for preparing parameters used to display home page of
 * specified user.
 * Users {@code nick} is expected as parameter mapped by <i>"nick"</i>.
 *
 * @author Domagoj Lokener
 */
@WebServlet("/servleti/user")
public class UserServlet extends HttpServlet {

	private static final long serialVersionUID = -4628706418628167874L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nick = req.getParameter("nick");
        if (Objects.isNull(nick)) {
            req.getRequestDispatcher("/WEB-INF/pages/errors/accessError.jsp").forward(req, resp);
            return;
        }

        BlogUser user = DAOProvider.getDAO().getBlogUser(nick);
        if (Objects.isNull(user)) {
            req.getRequestDispatcher("/WEB-INF/pages/errors/accessError.jsp").forward(req, resp);
            return;
        }

        List<BlogEntry> blogs = user.getBlogs();

        Boolean logged = Util.isLogged(nick, req.getSession());


        req.setAttribute("nick", nick);
        req.setAttribute("blogs", blogs);
        req.setAttribute("logged", logged);

        req.getRequestDispatcher("/WEB-INF/pages/list.jsp").forward(req, resp);
    }
}
