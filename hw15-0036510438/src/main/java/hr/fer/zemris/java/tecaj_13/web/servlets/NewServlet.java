package hr.fer.zemris.java.tecaj_13.web.servlets;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.web.servlets.formulars.BlogEntryFormular;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet handle request for creating new blog.
 * Servlet will check if user is allowed to create blog and will
 * checks if blog is valid (blog title is required).
 *
 * @author Domagoj Lokner
 */
@WebServlet("/servleti/user/new")
public class NewServlet extends HttpServlet {

	private static final long serialVersionUID = -536022249540833173L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Object nickO = req.getAttribute("nick");
        if (Util.sendErrorIfNull(req, resp, nickO)) {
            return;
        }

        String nick = nickO.toString();

        if (Util.isLogged(nick, req.getSession())) {
            BlogEntry e = new BlogEntry();
            BlogEntryFormular ef = new BlogEntryFormular();
            ef.fillUsingBlogEntry(e);

            req.setAttribute("formular", ef);
            req.setAttribute("method", "new");

            req.getRequestDispatcher("/WEB-INF/pages/new.jsp").forward(req, resp);
        } else {
            req.getRequestDispatcher("/WEB-INF/pages/errors/accessError.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("title");
        String text = req.getParameter("text");

        if (Util.sendErrorIfNull(req, resp, text, title)) {
            return;
        }

        req.setCharacterEncoding("UTF-8");

        BlogEntryFormular ef = new BlogEntryFormular();
        ef.fillUsingHttpRequest(req);

        ef.validate();

        if (ef.containsErrors()) {
            req.setAttribute("formular", ef);
            req.setAttribute("method", "new");
            req.getRequestDispatcher("/WEB-INF/pages/new.jsp").forward(req, resp);
            return;
        }


        BlogEntry blog = new BlogEntry();
        ef.fillBlogEntry(blog);
        blog.setCreator(DAOProvider.getDAO().getBlogUser(
                req.getSession().getAttribute("current.user.nick").toString()
        ));

        DAOProvider.getDAO().newBlog(blog);
        resp.sendRedirect("../" + req.getSession().getAttribute("current.user.nick").toString());
    }
}
