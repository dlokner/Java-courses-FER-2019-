package hr.fer.zemris.java.tecaj_13.dao.jpa;

import hr.fer.zemris.java.tecaj_13.dao.DAO;
import hr.fer.zemris.java.tecaj_13.dao.DAOException;
import hr.fer.zemris.java.tecaj_13.model.BlogComment;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;
import hr.fer.zemris.java.tecaj_13.web.servlets.formulars.BlogEntryFormular;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class JPADAOImpl implements DAO {

	@Override
	public BlogEntry getBlogEntry(Long id) throws DAOException {
		Objects.requireNonNull(id);

		BlogEntry blogEntry = JPAEMProvider.getEntityManager().find(BlogEntry.class, id);
		return blogEntry;
	}

	@Override
	public BlogUser getBlogUser(Long id) throws DAOException {
		Objects.requireNonNull(id);

		BlogUser user = JPAEMProvider.getEntityManager().find(BlogUser.class, id);
		return user;
	}

	@Override
	public BlogUser getBlogUser(String nickname) throws DAOException {
		Objects.requireNonNull(nickname);

		EntityManager em = JPAEMProvider.getEntityManager();

		try {
			return em.createNamedQuery("BlogUser.forNick", BlogUser.class)
					.setParameter("nick", nickname)
					.getSingleResult();

		} catch (NoResultException ex) {
			return null;
		}
	}

	@Override
	public List<BlogUser> getUsers() throws DAOException {
		EntityManager em = JPAEMProvider.getEntityManager();

		try {
			return em.createNamedQuery("BlogUser.getUsers", BlogUser.class)
					.getResultList();
		} catch (NoResultException ex) {
			return new LinkedList<>();
		}
	}

	@Override
	public void registerUser(BlogUser user) throws DAOException {
		Objects.requireNonNull(user);

		JPAEMProvider.getEntityManager().persist(user);
	}

	@Override
	public void newBlog(BlogEntry blog) throws DAOException {
		Objects.requireNonNull(blog);

		JPAEMProvider.getEntityManager().persist(blog);
	}

	@Override
	public void editBlog(BlogEntryFormular ef) throws DAOException {
		Objects.requireNonNull(ef);
		Long id;

		try {
			id = Long.parseLong(ef.getId());
		} catch (NumberFormatException ex) {
			throw new DAOException("Invalid BlogEntryFormat. ID can't be parsed to long", ex);
		}

		BlogEntry blogEntry = JPAEMProvider.getEntityManager().find(BlogEntry.class, id);
		ef.fillBlogEntry(blogEntry);
	}

	@Override
	public void newComment(BlogComment com) throws DAOException {
		Objects.requireNonNull(com);

		JPAEMProvider.getEntityManager().persist(com);
	}

}