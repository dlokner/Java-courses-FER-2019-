package hr.fer.zemris.java.tecaj_13.web.servlets;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;
import hr.fer.zemris.java.tecaj_13.web.servlets.formulars.BlogUserFormular;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet for blog user registration.
 * This servlet will store new user im database.
 * Before user is registered all information user provides will be
 * checked (nickname and e-mail are required).
 *
 * @author Domagoj Lokner
 */
@WebServlet(urlPatterns = "/servleti/register")
public class Registration extends HttpServlet {

	private static final long serialVersionUID = 4152198374385854151L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BlogUser user = new BlogUser();

        BlogUserFormular formular = new BlogUserFormular();
        formular.fillUsingBlogUser(user);

        req.setAttribute("userFormular", formular);

        req.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        BlogUserFormular f = new BlogUserFormular();
        f.fillUsingHttpRequest(req);
        Util.setHashPassword(req, f);

        if (registerUser(f)) {
            resp.sendRedirect(req.getContextPath() + "/index.jsp");
            return;
        }

        req.setAttribute("userFormular", f);
        req.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(req, resp);
    }

    /**
     * Registers user represented by given formular.
     *
     * @param f user formular.
     * @return {@code true} if user is successfully registered.
     */
    private boolean registerUser(BlogUserFormular f) {
        f.validate();

        if (f.containsErrors()) {
            return false;
        }

        if (DAOProvider.getDAO().getBlogUser(f.getNick()) != null) {
            f.setUsedNickError();
            return false;
        }

        BlogUser user = new BlogUser();

        f.fillBlogUser(user);
        DAOProvider.getDAO().registerUser(user);
        return true;
    }
}
