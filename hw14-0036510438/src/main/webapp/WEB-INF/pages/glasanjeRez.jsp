
<%@ page import="java.util.List" %>
<%@ page import="java.util.LinkedList" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <title>Rezultati</title>
    <style type="text/css">
        table.rez td {text-align: center;}
    </style>
</head>
<body>

    <h1>Voting results</h1>
    <p>There are results of voting.</p>

    <table border="1" cellspacing="0" class="rez">
        <thead><tr><th>Voting options</th><th>Number of votes</th></tr></thead>
        <tbody>
        <c:forEach var="option" items="${options}">
            <tr><td>${option.title}</td><td>${option.votesCount}</td></tr>
        </c:forEach>
        </tbody>
    </table>

    <h2>Graphic view of results</h2>
    <img alt="Pie-chart" src="glasanje-grafika?pollID=${pollID}" width="500" height="300" />

    <h2>Download results in XLS format</h2>
    <p>XLS doc can be downloaded <a href="glasanje-xls?pollID=${pollID}">here</a></p>

    <h2>Links</h2>
    <p>Related links:</p>

    <ul>
        <c:forEach var="winner" items="${winners}">
            <li><a href="${winner.link}" targer="_blank">${winner.title}</a></li>
        </c:forEach>
    </ul>

</body>
</html>
