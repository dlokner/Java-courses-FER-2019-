<%@ page import="hr.fer.zemris.java.p12.model.Poll" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Glasanje</title>
</head>
<body>
    <body>
    <h1>${pollID.title}</h1>

    <p>
        ${pollID.message}
    </p>

    <ol>
        <c:forEach var="option" items="${options}">
            <li><a href="glasanje-glasaj?id=${option.id}&pollID=${pollID.id}">${option.title}</a></li>
        </c:forEach>
    </ol>

    </body>

</body>
</html>
