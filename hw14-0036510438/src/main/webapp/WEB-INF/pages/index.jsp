<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Poll</title>
</head>
<body>

<h1>Home page</h1>

<p>
    <c:forEach var="poll" items="${polls}">
        <li><a href="glasanje?pollID=${poll.id}">${poll.title}</a></li>
    </c:forEach>

</p>

</body>
</html>
