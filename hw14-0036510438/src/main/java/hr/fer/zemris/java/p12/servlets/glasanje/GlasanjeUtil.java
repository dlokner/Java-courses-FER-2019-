package hr.fer.zemris.java.p12.servlets.glasanje;

import hr.fer.zemris.java.p12.dao.DAOProvider;
import hr.fer.zemris.java.p12.model.PollOption;

import java.util.*;

public class GlasanjeUtil {

    public static List<PollOption> getPollOptions(long id) {
        List<PollOption> options = DAOProvider.getDao().getPollOptions();

        options.removeIf((o) -> o.getPollId() != id);

        return options;
    }

    public static List<PollOption> getPollOptionsSorted(long id) {
        List<PollOption> options =  getPollOptions(id);
        options.sort((o1, o2) -> Integer.compare(o2.getVotesCount(), o1.getVotesCount()));
        return options;
    }

    public static  List<PollOption> getWinners(long id) {
        List<PollOption> options = getPollOptionsSorted(id);
        List<PollOption> winners = new LinkedList<>();

        int highestResult = options.get(0).getVotesCount();

        if (highestResult != 0) {
            for (PollOption o : options) {
                if (o.getVotesCount() == highestResult) {
                    winners.add(o);
                    continue;
                }
                break;
            }
        }

        return winners;
    }
}
