package hr.fer.zemris.java.p12.servlets.glasanje;

import hr.fer.zemris.java.p12.model.PollOption;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet that prepares informations about results of voting
 * and dispatches tham to jsp to produce page with results.
 *
 * @author Domagoj Lokner
 */
@WebServlet(name = "glasanjeRezultati", urlPatterns = "/servleti/glasanje-rezultati")
public class GlasanjeRezultatiServlet extends HttpServlet {

	private static final long serialVersionUID = 3250094496095229122L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    long pollID = Long.parseLong(req.getParameter("pollID"));

	    List<PollOption> options = GlasanjeUtil.getPollOptionsSorted(pollID);
	    List<PollOption> winners = GlasanjeUtil.getWinners(pollID);

        req.setAttribute("options", options);
        req.setAttribute("winners", winners);
        req.setAttribute("pollID", pollID);
        req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
    }


}
