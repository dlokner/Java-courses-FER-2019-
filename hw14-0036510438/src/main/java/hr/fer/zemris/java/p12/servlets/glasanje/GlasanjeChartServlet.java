package hr.fer.zemris.java.p12.servlets.glasanje;

import hr.fer.zemris.java.p12.model.PollOption;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet that constructs and sends HTTP response to request for image of chart.
 * Chart will show results of voting for favourite band.
 *
 * @author Domagoj Lokner
 */
@WebServlet(name = "glasanjeChart", urlPatterns = "/servleti/glasanje-grafika")
public class GlasanjeChartServlet extends HttpServlet {

	private static final long serialVersionUID = -5572255753412361698L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long pollID = Long.parseLong(req.getParameter("pollID"));
        List<PollOption> options = GlasanjeUtil.getPollOptionsSorted(pollID);

        JFreeChart chart = createChart(createDataset(options), null);

        byte[] img = ChartUtils.encodeAsPNG(chart.createBufferedImage(500, 300));

        resp.setContentType("image/png");
        resp.setContentLength(img.length);

        resp.getOutputStream().write(img);
    }

    /**
     * Creates dataset for chart.
     */
    private PieDataset createDataset(List<PollOption> options) {
        DefaultPieDataset result = new DefaultPieDataset();
        for (PollOption o : options) {
            if (o.getVotesCount() == 0) {
                continue;
            }
            result.setValue(o.getTitle(), o.getVotesCount());
        }
        return result;
    }

    /**
     * Creates a chart.
     */
    private JFreeChart createChart(PieDataset dataset, String title) {

        JFreeChart chart = ChartFactory.createPieChart3D(
                title,                  // chart title
                dataset,                // data
                true,             // include legend
                true,
                false
        );

        PiePlot3D plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
        return chart;

    }
}
