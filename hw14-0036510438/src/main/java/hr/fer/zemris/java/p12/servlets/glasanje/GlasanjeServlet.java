package hr.fer.zemris.java.p12.servlets.glasanje;

import hr.fer.zemris.java.p12.dao.DAOProvider;
import hr.fer.zemris.java.p12.model.Poll;
import hr.fer.zemris.java.p12.model.PollOption;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Prepares list of all bands for which user can vote and sends
 * request to jsp page that will construct voting page.
 *
 * @author Domagoj Lokner
 */
@WebServlet(name = "glasanje", urlPatterns = "/servleti/glasanje")
public class GlasanjeServlet extends HttpServlet {

	private static final long serialVersionUID = 4694141553261047774L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("pollID"));

        List<PollOption> options = GlasanjeUtil.getPollOptions(id);
        Poll poll = DAOProvider.getDao().getPoll(id);

        req.setAttribute("pollID", poll);
        req.setAttribute("options", options);
        req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
    }


}
