package hr.fer.zemris.java.p12.model;

/**
 * Model that defines single voting option in the poll.
 *
 * @author Domagoj Lokner
 */
public class PollOption {

    /**
     * Identificator number of this option.
     */
    private Long id;

    /**
     * Option title.
     */
    private String title;

    /**
     * URL link to page related with this poll option.
     */
    private String link;

    /**
     * ID of poll to which this option belong.
     */
    private Long pollId;

    /**
     * Number of votes this option earned.
     */
    private int votesCount;

    /**
     * Constructs {@code PollOption}.
     *
     * @param id identificator number of this option.
     * @param title option title.
     * @param link URL link to page related with this poll option.
     * @param pollId ID of poll to which this option belong.
     * @param votesCount number of votes this option earned.
     */
    public PollOption(Long id, String title, String link, Long pollId, int votesCount) {
        this.id = id;
        this.title = title;
        this.link = link;
        this.pollId = pollId;
        this.votesCount = votesCount;
    }

    /**
     * Gets option ID.
     *
     * @return ID.
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets option title.
     *
     * @return title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets link to page related with this poll option.
     *
     * @return link.
     */
    public String getLink() {
        return link;
    }

    /**
     * Gets ID of poll to which this option belong.
     *
     * @return ID of poll.
     */
    public Long getPollId() {
        return pollId;
    }

    /**
     * Gets number of votes this option earned.
     *
     * @return number of votes.
     */
    public int getVotesCount() {
        return votesCount;
    }
}
