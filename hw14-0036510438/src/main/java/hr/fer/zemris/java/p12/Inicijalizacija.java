package hr.fer.zemris.java.p12;

import java.beans.PropertyVetoException;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;
import hr.fer.zemris.java.p12.dao.sql.SQLConnectionProvider;

/**
 * Web Listener that provides initialization of database during starting of application.
 *
 * @author Domagoj Lokner
 */
@WebListener
public class Inicijalizacija implements ServletContextListener {

	public static final String PROPERTIES = "dbsettings.properties";

	@Override
	public void contextInitialized(ServletContextEvent sce) {

		String propertiesFile = sce.getServletContext().getRealPath("/WEB-INF/" + PROPERTIES);
		Properties prop = createProperties(Paths.get(propertiesFile));

		DBInfo info;
		try {
			info = new DBInfo(prop);
		} catch (IllegalArgumentException ex) {
			throw new RuntimeException(ex.getMessage());
		}

		String connectionURL = "jdbc:derby://" + info.host + ":" +
				info.port +	"/" + info.name + ";" +
				"user=" + info.user + ";password=" + info.password;

		ComboPooledDataSource cpds = new ComboPooledDataSource();
		try {
			cpds.setDriverClass("org.apache.derby.jdbc.ClientDriver");
		} catch (PropertyVetoException e1) {
			throw new RuntimeException("Pogreška prilikom inicijalizacije poola.", e1);
		}
		cpds.setJdbcUrl(connectionURL);

		try {
			SQLConnectionProvider.setConnection(cpds.getConnection());
			PollTablesCreator.setInitialPolls();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			SQLConnectionProvider.setConnection(null);
		}

		sce.getServletContext().setAttribute("hr.fer.zemris.dbpool", cpds);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ComboPooledDataSource cpds = (ComboPooledDataSource)sce.getServletContext().getAttribute("hr.fer.zemris.dbpool");
		if(cpds!=null) {
			try {
				DataSources.destroy(cpds);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Creates {@code Properties} object under file defined with given {@code filePath}.
	 *
	 * @param filePath - path on which is property file.
	 * @return newly created {@code Property} object.
	 * @throws IllegalArgumentException if error occurs during opening input stream under given file or
	 * 					   				if file on given path can't be reached or if file on given
	 * 					   				path can't be reached.
	 */
	public Properties createProperties(Path filePath) {
		Objects.requireNonNull(filePath);

		if (!Files.isReadable(filePath)) {
			throw new IllegalArgumentException("File can't be reached");
		}

		Properties p = new Properties();

		try {
			p.load(new BufferedInputStream(
					Files.newInputStream(filePath)
			));
		} catch (IOException ex) {
			throw new IllegalArgumentException("Error occurs opening file.");
		}

		return p;
	}

	/**
	 * Object that stores informations about database.
	 *
	 * @author Domagoj Lokner
	 */
	private static class DBInfo {
		String host;
		String port;
		String name;
		String user;
		String password;

		/**
		 * Constructs {@code DBInfo} object.
		 *
		 * @param p object from which all informations will be read.
		 * @throws IllegalArgumentException if any property can't be reached.
		 * @throws NullPointerException if {@code p} is {@code null}.
		 */
		public DBInfo(Properties p) {
			Objects.requireNonNull(p);

			host = getProperty("host", p);
			port = getProperty("port", p);
			name = getProperty("name", p);
			user = getProperty("user", p);
			password = getProperty("password", p);
		}

		/**
		 * Gets property stored by given key.
		 *
		 * @param key key under which property is stored.
		 * @param p object that provides properties.
		 * @return value of the property.
		 * @throws IllegalArgumentException if property is not found.
		 */
		private String getProperty(String key, Properties p) {
			String result = p.getProperty(key);

			if (Objects.isNull(result)) {
				throw new IllegalArgumentException("Invalid property file. " +
						key + " property can't be found.");
			}

			return result;
		}

	}

}