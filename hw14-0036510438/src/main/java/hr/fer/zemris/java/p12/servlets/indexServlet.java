package hr.fer.zemris.java.p12.servlets;

import hr.fer.zemris.java.p12.dao.DAO;
import hr.fer.zemris.java.p12.dao.DAOProvider;
import hr.fer.zemris.java.p12.model.Poll;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet that collect all parameters for index page end dispatch request to it.
 * 
 * @author Domagoj Lokner
 */
@WebServlet(name = "index", urlPatterns = "/servleti/index.html")
public class indexServlet extends HttpServlet {

	private static final long serialVersionUID = -8661064569918235982L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DAO dao = DAOProvider.getDao();
        List<Poll> polls = dao.getPolls();

        req.setAttribute("polls", polls);

        req.getRequestDispatcher("/WEB-INF/pages/index.jsp").forward(req, resp);
    }
}
