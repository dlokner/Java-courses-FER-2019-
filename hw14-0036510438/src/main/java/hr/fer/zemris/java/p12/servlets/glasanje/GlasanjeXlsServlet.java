package hr.fer.zemris.java.p12.servlets.glasanje;

import hr.fer.zemris.java.p12.model.PollOption;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet that send xls file with with table that shows voting results for all
 * bands as HTTP response.
 *
 * @author Domagoj Lokner
 */
@WebServlet(name = "glsanjeXls", urlPatterns = "/servleti/glasanje-xls")
public class GlasanjeXlsServlet extends HttpServlet {

	private static final long serialVersionUID = 8360887296127326002L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long pollID = Long.parseLong(req.getParameter("pollID"));
        List<PollOption> options = GlasanjeUtil.getPollOptionsSorted(pollID);

        HSSFWorkbook hwb = new HSSFWorkbook();
        constructPage(hwb, options);

        resp.setContentType("application/vnd.ms-excel");
        resp.setHeader("Content-Disposition", "attachment; filename=\"pollResults.xls\"");

        hwb.write(resp.getOutputStream());
    }

    /**
     * Constructs page of xls file.
     *
     * @param hwb workbook in which page will be created.
     * @param options list of votes for all bands which results should be
     *              added to file.
     */
    private void constructPage(HSSFWorkbook hwb, List<PollOption> options) {
        HSSFSheet sheet = hwb.createSheet("Rezultati glasanja");

        HSSFRow rowhead = sheet.createRow(0);
        rowhead.createCell(1).setCellValue("Bend");
        rowhead.createCell(2).setCellValue("Broj glasova");

        int i = 1;
        for (PollOption o : options) {
            HSSFRow row = sheet.createRow(i);

            row.createCell(0).setCellValue(i);
            row.createCell(1).setCellValue(o.getTitle());
            row.createCell(2).setCellValue(o.getVotesCount());
            ++i;
        }
    }
}
