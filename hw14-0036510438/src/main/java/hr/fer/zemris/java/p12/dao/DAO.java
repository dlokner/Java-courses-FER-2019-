package hr.fer.zemris.java.p12.dao;

import hr.fer.zemris.java.p12.model.Poll;
import hr.fer.zemris.java.p12.model.PollOption;

import java.util.List;

/**
 * Interface to date persistence layer.
 * 
 * @author marcupic
 *
 */
public interface DAO {
	/**
	 * Gets list of all polls.
	 *
	 * @return list of available polls.
	 * @throws DAOException if error occurs during
	 * 						getting poll records.
	 */
	List<Poll> getPolls() throws DAOException;

	/**
	 * Gets all defined poll options.
	 *
	 * @return poll options.
	 * @throws DAOException if error occurs during
	 * 	 * 					getting poll options.
	 */
	List<PollOption> getPollOptions() throws  DAOException;

	/**
	 * Stores given poll.
	 *
	 * @param poll poll to be stored.
	 * @return ID of added poll.
	 * @throws DAOException if error occurs during
	 * 						storing new poll.
	 */
	Long setPoll(Poll poll) throws DAOException;

	/**
	 * Stores given poll option related to poll with given {@code pollID}.
	 *
	 * @param option poll option to be added.
	 * @param pollID identificator of poll in which new option should be added.
	 * @return ID of added option.
	 * @throws DAOException if error occurs during
	 * 						storing new poll option.
	 */
	Long setPollOption(PollOption option, long pollID) throws DAOException;

	/**
	 * Gets poll determined with given {@code id}.
	 *
	 * @param id identificator of poll.
	 * @return poll with given id or {@code null} if poll with given id
	 * 		   is not present.
	 * @throws DAOException if error occurs during getting poll.
	 */
	Poll getPoll(long id) throws DAOException;

	/**
	 * Increments poll option determined by given {@code id}.
	 *
	 * @param id identificator of poll option which votesCount should be incremented.
	 * @throws DAOException if error occurs during updating of voteCount.
	 */
	void incrementVote(long id) throws DAOException;
}