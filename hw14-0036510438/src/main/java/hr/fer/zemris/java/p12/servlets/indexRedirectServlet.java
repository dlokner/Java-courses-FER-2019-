package hr.fer.zemris.java.p12.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet that redirects user to index page.
 *  
 * @author Domagoj Lokner
 */
@WebServlet(urlPatterns = "/index.html")
public class indexRedirectServlet extends HttpServlet {

	private static final long serialVersionUID = -6092195394266441594L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect(req.getContextPath() + "/servleti/index.html");
    }
}
