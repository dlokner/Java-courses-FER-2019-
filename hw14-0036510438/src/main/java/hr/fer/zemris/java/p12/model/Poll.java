package hr.fer.zemris.java.p12.model;

/**
 * Model that defines single poll.
 *
 * @author Domagoj Lokner
 */
public class Poll {

    /**
     * Identificator of this poll.
     */
    private Long id;

    /**
     * Poll title.
     */
    private String title;

    /**
     * Poll message.
     */
    private String message;

    /**
     * Constructs {@code Poll}.
     *
     * @param id identificator of poll.
     * @param title poll title.
     * @param message poll message.
     */
    public Poll(Long id, String title, String message) {
        this.id = id;
        this.title = title;
        this.message = message;
    }

    /**
     * Gets poll ID.
     *
     * @return poll ID.
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets poll title.
     *
     * @return poll title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets poll message.
     *
     * @return message.
     */
    public String getMessage() {
        return message;
    }
}
