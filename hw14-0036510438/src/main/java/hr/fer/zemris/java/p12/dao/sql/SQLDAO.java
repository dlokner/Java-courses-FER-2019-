package hr.fer.zemris.java.p12.dao.sql;

import hr.fer.zemris.java.p12.dao.DAO;
import hr.fer.zemris.java.p12.dao.DAOException;
import hr.fer.zemris.java.p12.model.Poll;
import hr.fer.zemris.java.p12.model.PollOption;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Ovo je implementacija podsustava DAO uporabom tehnologije SQL. Ova
 * konkretna implementacija očekuje da joj veza stoji na raspolaganju
 * preko {@link SQLConnectionProvider} razreda, što znači da bi netko
 * prije no što izvođenje dođe do ove točke to trebao tamo postaviti.
 * U web-aplikacijama tipično rješenje je konfigurirati jedan filter 
 * koji će presresti pozive servleta i prije toga ovdje ubaciti jednu
 * vezu iz connection-poola, a po zavrsetku obrade je maknuti.
 *  
 * @author marcupic
 */
public class SQLDAO implements DAO {
	@Override
	public List<Poll> getPolls() throws DAOException {
		List<Poll> polls = new ArrayList<>();
		Connection con = SQLConnectionProvider.getConnection();

		PreparedStatement pst;

		try {
			pst = con.prepareStatement("select * from Polls");
			try {
				ResultSet rs = pst.executeQuery();
				try {
					while(rs!=null && rs.next()) {
						Long id = rs.getLong(1);
						String title = rs.getString(2);
						String message = rs.getString(3);

						Poll poll = new Poll(id, title, message);
						polls.add(poll);
					}
				} finally {
					try { rs.close(); } catch(Exception ignorable) {}
				}
			} finally {
				try { pst.close(); } catch(Exception ignorable) {}
			}
		} catch(Exception ex) {
			throw new DAOException("Error occurs during getting polls.", ex);
		}
		return polls;

	}

	@Override
	public List<PollOption> getPollOptions() throws DAOException {
		List<PollOption> options = new ArrayList<>();
		Connection con = SQLConnectionProvider.getConnection();

		PreparedStatement pst;

		try {
			pst = con.prepareStatement("select * from PollOptions");
			try {
				ResultSet rs = pst.executeQuery();
				try {
					while(rs!=null && rs.next()) {
						Long id = rs.getLong(1);
						String optionTitle = rs.getString(2);
						String optionLink = rs.getString(3);
						Long pollID = rs.getLong(4);
						int votesCount = rs.getInt(5);

						PollOption option = new PollOption(id, optionTitle, optionLink, pollID, votesCount);
						options.add(option);
					}
				} finally {
					try { rs.close(); } catch(Exception ignorable) {}
				}
			} finally {
				try { pst.close(); } catch(Exception ignorable) {}
			}
		} catch(Exception ex) {
			throw new DAOException("Error occurs during getting polls.", ex);
		}
		return options;
	}

	@Override
	public Long setPoll(Poll poll) throws DAOException {
		Connection con = SQLConnectionProvider.getConnection();

		Long id = null;

		PreparedStatement pst = null;

		try {
			pst = con.prepareStatement("INSERT INTO Polls (title, message) VALUES (?, ?)",
					Statement.RETURN_GENERATED_KEYS);

			pst.setString(1, poll.getTitle());
			pst.setString(2, poll.getMessage());


			pst.executeUpdate();

			ResultSet rs = null;
			try {
				rs = pst.getGeneratedKeys();
				if (rs != null && rs.next()) {
					id = rs.getLong(1);
				}
			} finally {
				try {
					rs.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return id;
	}

	@Override
	public Long setPollOption(PollOption option, long pollID) throws DAOException {
		Connection con = SQLConnectionProvider.getConnection();

		Long id = null;

		PreparedStatement pst = null;

		try {
			pst = con.prepareStatement("INSERT INTO PollOptions (optionTitle, optionLink, pollID, votesCount)" +
							" VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

			pst.setString(1, option.getTitle());
			pst.setString(2, option.getLink());
			pst.setLong(3, pollID);
			pst.setInt(4, option.getVotesCount());


			pst.executeUpdate();

			ResultSet rs = null;
			try {
				rs = pst.getGeneratedKeys();
				if (rs != null && rs.next()) {
					id = rs.getLong(1);
				}
			} finally {
				try {
					rs.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return id;
	}

	@Override
	public Poll getPoll(long id) throws DAOException {
		Connection con = SQLConnectionProvider.getConnection();

		PreparedStatement pst;

		try {
			pst = con.prepareStatement("select * from Polls where id = ?");
			pst.setLong(1, id);
			try {
				ResultSet rs = pst.executeQuery();
				try {
					while(rs!=null && rs.next()) {
						String title = rs.getString(2);
						String message = rs.getString(3);

						return new Poll(id, title, message);
					}
				} finally {
					try { rs.close(); } catch(Exception ignorable) {}
				}
			} finally {
				try { pst.close(); } catch(Exception ignorable) {}
			}
		} catch(Exception ex) {
			throw new DAOException("Error occurs during getting polls.", ex);
		}

		return null;
	}

	@Override
	public void incrementVote(long id) throws DAOException {
		Connection con = SQLConnectionProvider.getConnection();


		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("UPDATE PollOptions " +
					"SET votesCount = votesCount + 1" +
					"WHERE id = ?");
			ps.setLong(1, id);
			ps.executeUpdate();

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

	}

}