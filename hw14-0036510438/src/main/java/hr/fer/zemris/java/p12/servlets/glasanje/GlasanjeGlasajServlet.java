package hr.fer.zemris.java.p12.servlets.glasanje;

import hr.fer.zemris.java.p12.dao.DAOProvider;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * Servlet that updates database that stores voting results.
 * Vote which id is sent as <i>"id"</i> argument will be incremented or
 * created with value 1 if it is not present.
 *
 * @author DomagojLokner
 */
@WebServlet(name = "glasanjeGlasaj", urlPatterns = "/servleti/glasanje-glasaj")
public class GlasanjeGlasajServlet extends HttpServlet {

	private static final long serialVersionUID = -8866108361878134703L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String pollID = req.getParameter("pollID");

        if (Objects.isNull(id) || Objects.isNull(pollID)) {
            req.getRequestDispatcher("/error?message=Invalid band ID.").forward(req, resp);
            return;
        }

        DAOProvider.getDao().incrementVote(Long.parseLong(id));

        resp.sendRedirect(req.getContextPath() + "/servleti/glasanje-rezultati?pollID=" + pollID);
    }
}
