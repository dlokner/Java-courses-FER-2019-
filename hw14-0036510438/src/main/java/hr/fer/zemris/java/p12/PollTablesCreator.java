package hr.fer.zemris.java.p12;

import hr.fer.zemris.java.p12.dao.DAO;
import hr.fer.zemris.java.p12.dao.DAOException;
import hr.fer.zemris.java.p12.dao.DAOProvider;
import hr.fer.zemris.java.p12.dao.sql.SQLConnectionProvider;
import hr.fer.zemris.java.p12.model.Poll;
import hr.fer.zemris.java.p12.model.PollOption;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Class that offers static methods for initializing poll database.
 * All methods expects that connection to database is established.
 *
 * @author Domagoj Lokner
 */
public class PollTablesCreator {

    /**
     * Sets two polls (Favourite OS and Favourite band polls).
     */
    public static void setInitialPolls() {
        createTables();
        initTables();
    }

    /**
     * Creates <i>Polls</i> and <i>PollOptions</i> tables if
     * they do not already exists.
     */
    public static void createTables() {
        createPollsTable("Polls");
        createPollOptionsTable("PollOptions");
    }


    /**
     * Sets initial records into <i>Polls</i> and <i>PollOptions</i>
     * tables.
     *
     * @throws DAOException if error occurs during database opening or while records
     *                      are inserted into database.
     */
    public static void initTables() throws DAOException {
        DAO dao = DAOProvider.getDao();

        List<Poll> polls = pollsCollection();

        long pollID;
        if (isEmpty(SQLConnectionProvider.getConnection(), "Polls")) {
            pollID = dao.setPoll(polls.get(0));
            for (PollOption o : bandPollOptionsCollection()) {
                dao.setPollOption(o, pollID);
            }

            pollID = dao.setPoll(polls.get(1));
            for (PollOption o : OSPollOptionsCollection()) {
                dao.setPollOption(o, pollID);
            }
        }
    }

    /**
     * Creates <i>Polls</i> table in which all poll definitions will be stored.
     *
     * @param tableName name of the table.
     * @throws DAOException if error occurs while table is created.
     */
    private static void createPollsTable(String tableName) throws DAOException {

        Connection con = SQLConnectionProvider.getConnection();

        if (tableExists(con, tableName)) {
            return;
        }

        PreparedStatement pst;

        try {
            pst = con.prepareStatement("CREATE TABLE "+ tableName +
                    " (id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                    " title VARCHAR(150) NOT NULL," +
                    " message CLOB(2048) NOT NULL" +
                    ")");
            try {
                pst.executeUpdate();
            } finally {
                try { pst.close(); } catch(Exception ignorable) {}
            }
        } catch(Exception ex) {
            throw new DAOException("Error occurs during getting polls.", ex);
        }

    }

    /**
     * Creates <i>PollOptions</i> table in which all poll definitions will be stored.
     *
     * @param tableName name of the table.
     * @throws DAOException if error occurs while table is created.
     */
    private static void createPollOptionsTable(String tableName) throws DAOException  {

        Connection con = SQLConnectionProvider.getConnection();

        if (tableExists(con, tableName)) {
            return;
        }

        PreparedStatement pst;

        try {
            pst = con.prepareStatement("CREATE TABLE "+ tableName +
                    " (id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                    " optionTitle VARCHAR(100) NOT NULL," +
                    " optionLink VARCHAR(150) NOT NULL," +
                    " pollID BIGINT," +
                    " votesCount BIGINT," +
                    " FOREIGN KEY (pollID) REFERENCES Polls(id)" +
                    ")");
            try {
                pst.executeUpdate();
            } finally {
                try {
                    pst.close();
                } catch(Exception ignorable) {
                }
            }
        } catch(Exception ex) {
            throw new DAOException("Error occurs during getting polls.", ex);
        }

    }

    /**
     * Checks if table is empty.
     *
     * @param con connection to database.
     * @param tableName name of the table.
     * @return {@code true} if table is empty otherwise {@code false}.
     * @throws DAOException if error occurs during reading of the database.
     */
    private static boolean isEmpty(Connection con, String tableName) throws DAOException  {
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement("SELECT * FROM " + tableName);

            ResultSet rs = null;
            try {
                rs = ps.executeQuery();

                while (rs.next()) {
                    return false;
                }
            } finally {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    throw new DAOException();
                }
            }

        } catch (SQLException ex) {
            throw new DAOException();
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DAOException();
            }

        }
        return true;
    }

    /**
     * Checks if table with given name exists in database.
     *
     * @param con connection to database.
     * @param tableName name of the table.
     * @return {@code true} if table exists otherwise {@code false}.
     * @throws DAOException if error occurs during reading of the database.
     */
    private static boolean tableExists(Connection con, String tableName) {
        int tables = 0;
        try {
            DatabaseMetaData dbmd = con.getMetaData();
            ResultSet rs = dbmd.getTables( null, null, tableName.toUpperCase(), null);

            while(rs.next()){
                ++tables;
            }

        } catch (SQLException ex) {
            throw new DAOException();
        }
        return tables  > 0;
    }

    /**
     * Collection of polls that will be added during database initialization.
     *
     * @return list of polls.
     */
    private static List<Poll> pollsCollection() {
        List<Poll> polls = new LinkedList<>();

        polls.add(new Poll((long)1,
                "Vote for your favorite band!",
                "Which is your favorite band? Click on the link to vote!"));

        polls.add(new Poll((long)2,
                "Vote for your favorite OS!",
                "Which is your favorite OS? Click on the link to vote!"));

        return polls;
    }

    /**
     * Collection of poll options that will be added during database initialization.
     * This collection contains options for <i>Favourite band poll</i>.
     *
     * @return list of polls options.
     */
    private static List<PollOption> bandPollOptionsCollection() {
        List<PollOption> options = new LinkedList<>();

        options.add(new PollOption(null,
                "The Beatles",
                "https://www.youtube.com/watch?v=z9ypq6_5bsg",
                null,
                0));

        options.add(new PollOption(null,
                "The Platters",
                "https://www.youtube.com/watch?v=H2di83WAOhU",
                null,
                0));


        options.add(new PollOption(null,
                "The Beach Boys",
                "https://www.youtube.com/watch?v=2s4slliAtQU",
                null,
                0));


        options.add(new PollOption(null,
                "The Four Seasons",
                "https://www.youtube.com/watch?v=y8yvnqHmFds",
                null,
                0));


        options.add(new PollOption(null,
                "The Marcels",
                "https://www.youtube.com/watch?v=qoi3TH59ZEs",
                null,
                0));

        options.add(new PollOption(null,
                "The Everly Brothers",
                "https://www.youtube.com/watch?v=tbU3zdAgiX8",
                null,
                0));

        options.add(new PollOption(null,
                "The Mamas And The Papas",
                "https://www.youtube.com/watch?v=N-aK6JnyFmk",
                null,
                0));

        return options;
    }

    /**
     * Collection of poll options that will be added during database initialization.
     * This collection contains options for <i>Favourite OS poll</i>.
     *
     * @return list of polls options.
     */
    private static List<PollOption> OSPollOptionsCollection() {
        List<PollOption> options = new LinkedList<>();

        options.add(new PollOption(null,
                "Ubuntu",
                "https://ubuntu.com/",
                null,
                0));

        options.add(new PollOption(null,
                "Mint",
                "https://linuxmint.com/",
                null,
                0));

        options.add(new PollOption(null,
                "Windows 10",
                "https://www.microsoft.com/hr-hr/windows/get-windows-10",
                null,
                0));

        options.add(new PollOption(null,
                "Windows 7",
                "https://windows-7-home-premium.en.softonic.com/",
                null,
                0));


        options.add(new PollOption(null,
                "MacOS",
                "https://www.apple.com/hr/macos/mojave/",
                null,
                0));

        return options;
    }
}
