package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

import java.io.IOException;
import java.util.Set;

/**
 * Worker which generate HTML script with table of all parameters and their values.
 *
 * @author Domagoj Lokner
 */
public class EchoParams implements IWebWorker {

    @Override
    public void processRequest(RequestContext context) throws Exception {
        Set<String> parameters = context.getParameterNames();

        try {
            context.write("<html>" +
                    "<head>" +
                    "<title>EchoParams</title>" +
                    "</head>" +
                    "<body>");

            context.write("<h1>Param table</h1>");
            context.write("<table border=\"1\"");

            context.write("<tr>" +
                    "<th>Name</th>" +
                    "<th>Value</th>" +
                    "</tr>");

            for (String param : parameters) {
                context.write(createTableRow(param, context.getParameter(param)));
            }

            context.write("</table>");
            context.write("</body></html>");
        } catch(IOException ex) {
            // Log exception to servers log...
            ex.printStackTrace();
        }
    }

    /**
     * Creates single table row with given arguments.
     *
     * @param name parameter name.
     * @param value parameter's value.
     * @return string representing row of HTML table with given arguments.
     */
    private String createTableRow(String name, String value) {
        StringBuilder row = new StringBuilder();

        row.append("<tr>");

        row.append("<td>");
        row.append(name);
        row.append("</td>");

        row.append("<td>");
        row.append(value);
        row.append("</td>");

        row.append("</tr>");

        return row.toString();
    }
}
