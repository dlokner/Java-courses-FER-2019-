package hr.fer.zemris.java.custom.scripting.demo;

import hr.fer.zemris.java.custom.scripting.nodes.*;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Program that prints reconstruction of parsed SmartScript.
 * Path to SmartScript to be parsed is expected as first command line argument.
 *
 * @author Domagoj Lokner
 */
public class TreeWriter {


    /**
     * Starting method of the program.
     *
     * @param args command line arguments.
     */
    public static void main(String[] args) {
        String docBody = null;
        try {
            docBody = Files.readString(Paths.get(args[0]));
        } catch (IOException e1) {
            System.err.println("File does not exist!!");
            System.exit(1);
        } catch (IndexOutOfBoundsException e2) {
			System.err.println("Program expects single command line argument.");
			System.exit(1);
		}

        SmartScriptParser parser = null;

        try {
            parser = new SmartScriptParser(docBody);
        } catch (SmartScriptParserException e) {
            System.err.println("Unable to parse document!");
            System.exit(-1);
        }

        WriterVisitor visitor = new WriterVisitor();
        parser.getDocumentNode().accept(visitor);
    }

    /**
     * {@code INodeVisitor} implementation used for writing
     * nodes on standard output.
     */
    private static class WriterVisitor implements INodeVisitor {

        @Override
        public void visitTextNode(TextNode node) {
            System.out.print(node.toString());
        }

        @Override
        public void visitForLoopNode(ForLoopNode node) {
            System.out.print(node.toString());
            for (int i = 0, n = node.numberOfChildren(); i < n; ++i) {
                node.getChild(i).accept(this);
            }
            System.out.print("{$END$}");
        }

        @Override
        public void visitEchoNode(EchoNode node) {
            System.out.print(node.toString());
        }

        @Override
        public void visitDocumentNode(DocumentNode node) {
            for (int i = 0, n = node.numberOfChildren(); i < n; ++i) {
                node.getChild(i).accept(this);
            }
        }
    }

}
