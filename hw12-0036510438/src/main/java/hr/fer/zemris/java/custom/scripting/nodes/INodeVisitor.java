package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Visitor object for nodes that inherits {@code Node} class.
 *
 * @author Domagoj Lokner
 */
public interface INodeVisitor {

    /**
     * Visitors action for {@code TextNode}.
     *
     * @param node visited node.
     */
    void visitTextNode(TextNode node);

    /**
     * Visitors action for {@code ForLoopNode}.
     *
     * @param node visited node.
     */
    void visitForLoopNode(ForLoopNode node);

    /**
     * Visitors action for {@code EchoNode}.
     *
     * @param node visited node.
     */
    void visitEchoNode(EchoNode node);

    /**
     * Visitors action for {@code DocumentNode}.
     *
     * @param node visited node.
     */
    void visitDocumentNode(DocumentNode node);
}
