package hr.fer.zemris.java.webserver;

import java.io.IOException;

/**
 * Program starts server. Used only for testing server from IDE.
 * 
 * @author Domagoj Lokner 
 */
public class SmartHttpServerDemo {

	/**
	 * Starting method of the program.
	 * 
	 * @param args command line arguments.
	 * @throws IOException 
	 */
    public static void main(String[] args) throws IOException {
        SmartHttpServer server = new SmartHttpServer(
                "./config/server.properties");
        server.start();
    }
}
