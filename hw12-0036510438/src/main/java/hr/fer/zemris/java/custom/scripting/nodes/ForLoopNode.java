package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;

/**
 * {@code Node} representing single for loop construct.
 * 
 * @author Domagoj Lokner
 *
 */
public class ForLoopNode extends Node {
	private ElementVariable variable;
	private Element startExpression;
	private Element endExpression;
	private Element stepExpression;
	
	/**
	 * Constructs {@code ForLoopNode} with all properties.
	 * 
	 * @param variable - variable 
	 * @param startExpression - starting value of {@code variable}.
	 * @param endExpression - ending value of {@code variable}.
	 * @param stepExpression - for loop step.
	 */
	public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression, Element stepExpression) {
		this.variable = variable;
		this.startExpression = startExpression;
		this.endExpression = endExpression;
		this.stepExpression = stepExpression;
	}
	
	/**
	 * COnstructs {@code ForLoopNode} with all necessary properties.
	 * 
	 * @param variable - variable.
	 * @param startExpression - starting value of {@code variable}.
	 * @param endExpression - ending value of {@code variable}.
	 */
	public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression) {
		this.variable = variable;
		this.startExpression = startExpression;
		this.endExpression = endExpression;
	}

	/**
	 * Gets for loop variable. 
	 * 
	 * @return variable.
	 */
	public ElementVariable getVariable() {
		return variable;
	}

	/**
	 * Gets starting expression of for loop.
	 * 
	 * @return starting expression.
	 */
	public Element getStartExpression() {
		return startExpression;
	}

	/**
	 * Gets ending expression of for loop.
	 * 
	 * @return ending expression.
	 */
	public Element getEndExpression() {
		return endExpression;
	}

	/**
	 * Gets step expression of for loop.
	 * 
	 * @return step expression.
	 */
	public Element getStepExpression() {
		return stepExpression;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("{$ FOR ");
		
		sb.append(variable.asText() + " " + startExpression.asText() + " " + endExpression.asText() + " ");

		if(stepExpression != null) {
			sb.append(stepExpression.asText());
		}
		
		sb.append(" $}");
		
//		for(int i = 0; i <  numberOfChildren(); ++i) {
//			sb.append(getChild(i).toString());
//		}
//
//		sb.append("$END$");
		
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((endExpression == null) ? 0 : endExpression.hashCode());
		result = prime * result + ((startExpression == null) ? 0 : startExpression.hashCode());
		result = prime * result + ((stepExpression == null) ? 0 : stepExpression.hashCode());
		result = prime * result + ((variable == null) ? 0 : variable.hashCode());
		return result;
	}

	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitForLoopNode(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ForLoopNode)) {
			return false;
		}
		ForLoopNode other = (ForLoopNode) obj;
		if (endExpression == null) {
			if (other.endExpression != null) {
				return false;
			}
		} else if (!endExpression.equals(other.endExpression)) {
			return false;
		}
		if (startExpression == null) {
			if (other.startExpression != null) {
				return false;
			}
		} else if (!startExpression.equals(other.startExpression)) {
			return false;
		}
		if (stepExpression == null) {
			if (other.stepExpression != null) {
				return false;
			}
		} else if (!stepExpression.equals(other.stepExpression)) {
			return false;
		}
		if (variable == null) {
			if (other.variable != null) {
				return false;
			}
		} else if (!variable.equals(other.variable)) {
			return false;
		}
		return true;
	}
	
	
}
