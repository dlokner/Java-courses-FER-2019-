package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

import java.util.Objects;

/**
 * Worker that generate HTML script with all server functionalities.
 *
 * @author Domagoj Lokner
 */
public class Home implements IWebWorker {
    @Override
    public void processRequest(RequestContext context) throws Exception {
        String bgcolor = context.getPersistentParameter("bgcolor");

        bgcolor = Objects.isNull(bgcolor) ? "7F7F7F" : bgcolor;
        context.setTemporaryParameter("background", bgcolor);

        context.getDispatcher().dispatchRequest("/private/pages/home.smscr");
    }
}
