package hr.fer.zemris.java.custom.scripting.exec;

import hr.fer.zemris.java.custom.scripting.elems.*;
import hr.fer.zemris.java.custom.scripting.nodes.*;
import hr.fer.zemris.java.webserver.RequestContext;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Objects;
import java.util.Stack;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Engine for SmartScript (executes SmartScript).
 *
 * @author Domagoj Lokner
 */
public class SmartScriptEngine {

    /**
     * Document that can be executed.
     */
    private DocumentNode documentNode;

    /**
     * Context of HTTP request.
     */
    private RequestContext requestContext;

    /**
     * Stack on which engine stores variables.
     */
    private ObjectMultistack multistack;

    /**
     * Constructs {@code SmartScriptEngine}.
     *
     * @param documentNode - node to be executed with this engine.
     * @param requestContext - context of HTTP request.
     */
    public SmartScriptEngine(DocumentNode documentNode, RequestContext requestContext) {
        this.documentNode = documentNode;
        this.requestContext = requestContext;
        this.multistack = new ObjectMultistack();
    }

    /**
     * Executes document.
     */
    public void execute() {
        documentNode.accept(visitor);
    }


    /**
     * Implementation of node visitor.
     */
    private INodeVisitor visitor = new INodeVisitor() {

        @Override
        public void visitTextNode(TextNode node) {
            try {
                requestContext.write(node.getText());
            } catch (IOException e) {
                throw new SmartScriptEngineException("Error on output stream");
            }
        }

        @Override
        public void visitForLoopNode(ForLoopNode node) {
            String varName = node.getVariable().asText();
            ValueWrapper varValue = new ValueWrapper(node.getStartExpression().asText());

            String bound = resolveVar(node.getEndExpression().asText());
            String step = Objects.isNull(node.getStepExpression()) ?
                    "1" : resolveVar(node.getStepExpression().asText());

            multistack.push(varName, varValue);

            while (multistack.peek(varName).numCompare(bound) <= 0) {
                for (int i = 0, n = node.numberOfChildren(); i < n; ++i) {
                    node.getChild(i).accept(this);
                }

                multistack.peek(varName).add(step);
            }

            multistack.pop(varName);
        }

        @Override
        public void visitEchoNode(EchoNode node) {
            Stack<Object> stack = new Stack<>();

            for (Element e : node.getElements()) {
                processElement(e, stack);
            }

            for (Object o : stack) {
                try {
                    requestContext.write(o.toString());
                } catch (IOException e) {
                    throw new SmartScriptEngineException("Error on output stream");
                }
            }
        }

        @Override
        public void visitDocumentNode(DocumentNode node) {
            for (int i = 0, n = node.numberOfChildren(); i < n; ++i) {
                try {
                    node.getChild(i).accept(this);
                }catch (RuntimeException ex) {
                    throw new SmartScriptEngineException(ex.getMessage());
                }
            }

        }

        /**
         * Process given element.
         *
         * @param e element to be processed.
         * @param stack on from which processed element will get context
         *              and on which process result will be stored.
         */
        private void processElement(Element e, Stack<Object> stack) {
            if (e instanceof ElementOperator) {
                compute(e.asText(), stack);
            } else if (e instanceof ElementVariable) {
                processVariable(e.asText(), stack);
            } else if (e instanceof ElementFunction) {
                processFunction(e.asText(), stack);
            } else {
                stack.push(e.asText());
            }
        }

        /**
         * Processes function element.
         *
         * @param function function to be processed.
         * @param stack on from which processed element will get context
         *              and on which process result will be stored.
         */
        private void processFunction(String function, Stack<Object> stack) {
            function = function.substring(1);

            switch (function) {
                case ("sin"):
                    sinFunction(stack);
                    break;
                case ("decfmt"):
                    formatFunction(stack);
                    break;
                case ("dup"):
                    stack.push(stack.peek());
                    break;
                case ("swap"):
                    Object first = stack.pop();
                    Object second = stack.pop();

                    stack.push(first);
                    stack.push(second);
                    break;
                default:
                    paramFunction(function, stack);
            }
        }

        /**
         * Processes function that works with parameters of request context.
         *
         * @param function function to be processed.
         * @param stack on from which processed element will get context
         *              and on which process result will be stored.
         */
        private void paramFunction(String function, Stack<Object> stack) {
            switch (function) {
                case ("setMimeType"):
                    requestContext.setMimeType(stack.pop().toString());
                    break;
                case ("paramGet"):
                    getParameter((n) -> requestContext.getParameter(n), stack);
                    break;
                case ("pparamGet"):
                    getParameter((n) -> requestContext.getPersistentParameter(n), stack);
                    break;
                case ("tparamGet"):
                    getParameter((n) -> requestContext.getTemporaryParameter(n), stack);
                    break;

                case ("pparamSet"):
                    setParameter((n, v) -> requestContext.setPersistentParameter(n, v), stack);
                    break;
                case ("tparamSet"):
                    setParameter((n, v) -> requestContext.setTemporaryParameter(n, v), stack);
                    break;

                case ("pparamDel"):
                    delParameter(n -> requestContext.removePersistentParameter(n), stack);
                    break;
                case ("tparamDel"):
                    delParameter(n -> requestContext.removeTemporaryParameter(n), stack);
                    break;
                default:
                    throw new SmartScriptEngineException("Unsupported function " + function);
            }
        }

        /**
         * Method used for deleting parameters.
         *
         * @param cons consumer that will receive string key of parameter to be deleted.
         * @param stack on from which processed element will get context
         *              and on which process result will be stored.
         */
        private void delParameter(Consumer<String> cons, Stack<Object> stack) {
            cons.accept(stack.pop().toString());
        }

        /**
         * Method adds parameter in request context.
         *
         * @param cons consumer that will receive key and value of parameter to be added.
         * @param stack on from which processed element will get context
         *              and on which process result will be stored.
         */
        private void setParameter(BiConsumer<String, String> cons, Stack<Object> stack) {
            String name = stack.pop().toString();
            String value = stack.pop().toString();

            cons.accept(name, value);
        }

        /**
         * Method gets parameter from request context.
         *
         * @param fun function that will receive name and default value of parameter to be gotten.
         * @param stack on from which processed element will get context
         *              and on which process result will be stored.
         */
        private void getParameter(Function<String, String> fun, Stack<Object> stack) {
            String dv = stack.pop().toString();
            String name = stack.pop().toString();

            String value = fun.apply(name);

            stack.push(Objects.isNull(value) ? dv : value);
        }

        /**
         * Implementation of <i>@decfmt</i> function from SmartScript.
         * Formats decimal number.
         *
         * @param stack on from which processed element will get context
         *              and on which process result will be stored.
         */
        private void formatFunction(Stack<Object> stack) {
            Object f = stack.pop();
            Object x = stack.pop();

            DecimalFormat format = new DecimalFormat(f.toString());

            String result = format.format(Double.parseDouble(x.toString()));

            stack.push(result);

        }

        /**
         * Compute sin of last element pushed on stack.
         * Result is pushed back on stack.
         *
         * @param stack on from which processed element will get context
         *              and on which process result will be stored.
         */
        private void sinFunction(Stack<Object> stack) {
            stack.push(
                    Math.sin(Double.parseDouble(stack.pop().toString()) * Math.PI / 180)
            );
        }

        /**
         * Push value of variable with given name on stack.
         *
         * @param varName variable name.
         * @param stack stack on which result will be pushed.
         */
        private void processVariable(String varName, Stack<Object> stack) {
            Object value = multistack.peek(varName).getValue();

            Objects.requireNonNull(value, "Variable " + varName + " is not defined.");

            stack.push(value);
        }

        /**
         * Returns string representation of variable value.
         *
         * @param variable - variable name.
         * @return value of variable. If variable with that name is not present
         *         {@code variable} will be returned back
         */
        private String resolveVar(String variable) {
            String value = requestContext.getParameter(variable);

            return Objects.isNull(value) ? variable : value;
        }

        /**
         * Compute arithmetical operation defined by {@code operator}.
         *
         * @param operator arithmetical operator (+, *, -, / operators are supported).
         * @param stack stack from which function will get operands and on
         *              which result will be stored.
         */
        private void compute(String operator, Stack<Object> stack) {
            ValueWrapper val1 = new ValueWrapper(stack.pop());
            Object val2 = stack.pop();

            switch (operator) {
                case ("+"):
                    val1.add(val2);
                    break;
                case ("-"):
                    val1.subtract(val2);
                    break;
                case ("*"):
                    val1.multiply(val2);
                    break;
                case("/"):
                    val1.divide(val2);
                    break;
                default:
                    throw new SmartScriptEngineException("Invalid operator " + operator);
            }

            stack.push(val1.getValue());
        }

    };
}
