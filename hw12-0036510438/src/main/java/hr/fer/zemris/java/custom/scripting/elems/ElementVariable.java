package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Represents variable.
 * 
 * @author Domagoj Lokner
 *
 */
public class ElementVariable extends Element {
	private String name;
	
	/**
	 * Constructs {@code ElementVariable} with {@code name}.
	 * 
	 * @param name - name of variable.
	 */
	public ElementVariable(String name) {
		this.name = name;
	}
	
	/**
	 * Returns variable name.
	 * 
	 * @return name.
	 */
	@Override
	public String asText() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ElementVariable)) {
			return false;
		}
		ElementVariable other = (ElementVariable) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
	
	
}
