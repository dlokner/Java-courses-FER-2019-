package hr.fer.zemris.java.custom.collections;

/**
 * Interface models object capable of performing some operation on the passed object.
 * 
 * @author Domagoj Lokner
 */
@FunctionalInterface
public interface Processor {

	/**
	 * Performs action on given {@code value}.
	 * <p>
	 * Action is not defined here.
	 * </p>
	 * 
	 * @param value - value over which action will be performed.
	 */
	public void process(Object value);
	
}
