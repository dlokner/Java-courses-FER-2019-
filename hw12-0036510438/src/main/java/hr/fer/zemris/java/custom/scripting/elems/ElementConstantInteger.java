package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Represents integer value expression.
 * 
 * @author Domagoj Lokner
 *
 */
public class ElementConstantInteger extends Element {
	private int value;
	
	/**
	 * Constructs {@code ElementConstantInteger} with {@code value}.
	 * 
	 * @param value - integer value.
	 */
	public ElementConstantInteger(int value) {
		this.value = value;
	}
	
	/**
	 * Returns {@code String} representation of integer value.
	 * 
	 * @return int value as {@code String}.
	 */
	@Override
	public String asText() {
		return Integer.valueOf(value).toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ElementConstantInteger)) {
			return false;
		}
		ElementConstantInteger other = (ElementConstantInteger) obj;
		if (value != other.value) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return asText();
	}
}
