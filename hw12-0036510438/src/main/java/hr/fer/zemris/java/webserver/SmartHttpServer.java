package hr.fer.zemris.java.webserver;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Object that implements simple HTTP server.
 *
 * @author Domagoj Lokner
 */
public class SmartHttpServer {

    /**
     * Agreed package for workers. Will be used for resolving <i>/ext</i> url path requests.
     */
    private static final String DEFAULT_WORKERS_PACKAGE = "hr.fer.zemris.java.webserver.workers";

    /**
     * Address from which server will receive requests.
     */
    private String address;

    /**
     * Domain on which this server will receive requests.
     */
    private String domainName;

    /**
     * Port on which this server will receive requests.
     */
    private int port;

    /**
     * Maximum number of worker threads that can be in the pool.
     */
    private int workerThreads;

    /**
     * Time in which session will expire.
     */
    private int sessionTimeout;

    /**
     * Map of mime types this server supports.
     */
    private Map<String,String> mimeTypes = new HashMap<>();

    /**
     * Thread on which server runs.
     */
    private ServerThread serverThread;

    /**
     * Thread pool of workers.
     */
    private ExecutorService threadPool;

    /**
     * Root path to documents that can be requested.
     */
    private Path documentRoot;

    /**
     * Map that contains all available workers.
     */
    private Map<String,IWebWorker> workersMap = new HashMap<>();

    /**
     * Map in which sessions are stored.
     */
    private Map<String, SessionMapEntry> sessions = new HashMap<String, SmartHttpServer.SessionMapEntry>();

    /**
     * {@code Random} object used for generating session IDs.
     */
    private Random sessionRandom = new Random();

    /**
     * Flag which indicate that thread on which server runs should be stopped.
     */
    private boolean stopServerThread;

    /**
     * Constructs {@code SmartHttpServer}.
     *
     * @param configFileName path to server configuration property file.
     * @throws IOException if error occurs opening server configuration file.
     */
    public SmartHttpServer(String configFileName) throws IOException {

        Path configFile = Paths.get(configFileName);
        Properties configProperties = createProperties(configFile);
        initProperties(configProperties);

        Path mimePath = Paths.get(
                configProperties.getProperty("server.mimeConfig")
        );
        Properties mimeProperties = createProperties(mimePath);
        mimeProperties.stringPropertyNames().stream()
                .forEach(k -> mimeTypes.put(k, mimeProperties.getProperty(k)));

        fillWorkersMap(configProperties.getProperty("server.workers"));

        serverThread = new ServerThread();
    }

    /**
     * Fills workers map with workers properties.
     *
     * @param path path to properties file.
     * @throws IOException if error occurs reading from given properties file.
     * @throws IllegalArgumentException if workers class can't be found or if
     *                                  properties contains duplicated path.
     *
     */
    private void fillWorkersMap(String path) throws IOException {
        Path workersPath = Paths.get(path);

        Properties workersProperties = createProperties(workersPath);

        workersProperties.forEach((k, v) -> {
            String workerPath = (String)k;
            String fqcn = (String)v;

            IWebWorker iww = getWebWorker(fqcn);

            if (Objects.isNull(iww)) {
                throw new IllegalArgumentException("Worker class can't be found");
            }

            if (!Objects.isNull(workersMap.put(workerPath, iww))) {
                throw new IllegalArgumentException("Duplicated paths in workers properties");
            }
        });
    }

    /**
     * Gets instance of object that implements {@code IWebWorker} with
     * given fully qualified class name.
     *
     * @param fqcn fully qualified class name
     * @return instance of {@code IWebWorker} or {@code null} if instance can't be found;
     */
    @SuppressWarnings("deprecation")
	private IWebWorker getWebWorker(String fqcn) {
        Object newObject;

        try {
            Class<?> referenceToClass = this.getClass().getClassLoader().loadClass(fqcn);
            newObject = referenceToClass.newInstance();
        } catch (Exception e1) {
            return null;
        }

        IWebWorker iww = (IWebWorker)newObject;

        return iww;
    }

    /**
     * Starts server.
     */
    protected synchronized void start() {
        stopServerThread = false;

        if (!serverThread.isAlive()) {
            serverThread.start();
        }
        threadPool = Executors.newFixedThreadPool(workerThreads);
    }

    /**
     * Stops server.
     */
    protected synchronized void stop() {
        stopServerThread = true;
        serverThread.interrupt();

        threadPool.shutdown();
    }

    /**
     * Object that implements main server {@code Thread}.
     * This thread accepts clients and send them to thread pool to be executed.
     *
     * @author Domagoj Lokner
     */
    protected class ServerThread extends Thread {
        @Override
        public void run() {

            try(ServerSocket socket = new ServerSocket()) {

                socket.bind(new InetSocketAddress(
                        InetAddress.getByName(address),
                        port
                ));

                //Thread which removes all expired sessions
                Thread cookieMonster = new Thread(() -> {
                	while(true) {
	                    synchronized (SmartHttpServer.this) {
	                        for (Map.Entry<String, SessionMapEntry> entry : sessions.entrySet()) {
	                            SessionMapEntry session = entry.getValue();
	
	                            if (session.validUntil < currentTimeSec()) {
	                                sessions.remove(entry.getKey());
	                            }
	                        }
	                    }
	
	                    try {
	                        Thread.sleep(5 * 60 * 1000);
	                    } catch (InterruptedException e) {
	                    }
                	}
                });
                cookieMonster.setDaemon(true);
                cookieMonster.start();

                while (!stopServerThread) {
                    Socket client = socket.accept();

                    if (stopServerThread) {
                        return;
                    }

                    ClientWorker cw = new ClientWorker(client);
                    threadPool.submit(cw);
                }

            } catch (Exception ex) {
                System.err.println("Error occurred");
            }
        }
    }

    /**
     * Initialize {@code SmartHttpServer} properties defined in given
     * {@code Properties} object.
     *
     * @param p object from which properties will be gotten.
     */
    private void initProperties(Properties p) {
        Objects.requireNonNull(p, "Properties can't be null");


        address = p.getProperty("server.address");
        domainName = p.getProperty("server.domainName");
        port = getIntProperty(p, "server.port");
        workerThreads = getIntProperty(p, "server.workerThreads");
        sessionTimeout = getIntProperty(p, "session.timeout");
        documentRoot = Paths.get(p.getProperty("server.documentRoot"));
    }

    /**
     * Gets int value properties.
     *
     * @param p properties object.
     * @param key key by which property is saved.
     * @return int property.
     */
    private int getIntProperty(Properties p, String key) {
        return Integer.parseInt(
                p.getProperty(key)
        );
    }

    /**
     * Creates {@code Properties} object under file defined with given {@code filePath}.
     *
     * @param filePath - path on which is property file.
     * @return newly created {@code Property} object.
     * @throws IOException if error occurs during opening input stream under given file.
     * @throws IllegalArgumentException if file on given path can't be reached.
     */
    public Properties createProperties(Path filePath) throws IOException {
        Objects.requireNonNull(filePath);

        if (!Files.isReadable(filePath)) {
            throw new IllegalArgumentException();
        }

        Properties p = new Properties();

        p.load(new BufferedInputStream(
                Files.newInputStream(filePath)
        ));

        return p;
    }

    /**
     * Object that implements client serving job.
     *
     * @author Domagoj Lokner
     */
    private class ClientWorker implements Runnable, IDispatcher {

        /**
         * Socket to client.
         */
        private Socket csocket;

        /**
         * Input stream under client's socket.
         */
        private PushbackInputStream istream;

        /**
         * Output stream to client.
         */
        private OutputStream ostream;

        /**
         * Version of HTTP request.
         */
        private String version;

        /**
         * Method of request.
         */
        @SuppressWarnings("unused")
		private String method;

        /**
         * Host on which request have been sent.
         */
        private String host;

        /**
         * Requests parameters.
         */
        private Map<String,String> params = new HashMap<>();

        /**
         * Temporary parameters of request.
         */
        private Map<String,String> tempParams = new HashMap<>();

        /**
         * Permanent parameters of request.
         */
        private Map<String,String> permPrams = new HashMap<>();

        /**
         * List of output cookies.
         */
        private List<RequestContext.RCCookie> outputCookies = new ArrayList<>();

        /**
         * Session identificator.
         */
        private String SID;

        /**
         * Context of request this job does.
         */
        private RequestContext context = null;

        /**
         * Constructs {@code ClientWorker} object.
         *
         * @param csocket socket to client.
         */
        public ClientWorker(Socket csocket) {
            super();
            this.csocket = csocket;
        }

        @Override
        public void run() {
            try {

                istream = new PushbackInputStream(
                        new BufferedInputStream(csocket.getInputStream())
                );
                ostream = new BufferedOutputStream(csocket.getOutputStream());

                List<String> headerLines = readRequest(istream);

                //if there is no first line or first line is empty collection
                if (Objects.isNull(headerLines) || headerLines.isEmpty()) {
                    sendBadRequestMessage();
                    return;
                }

                String firstLine = headerLines.get(0);
                String requestedPath = extractFromFirstLine(firstLine);

                if (Objects.isNull(requestedPath)) {
                    sendBadRequestMessage();
                    return;
                }

                String[] splitedPath = splitRequestedPath(requestedPath);

                setHost(headerLines);

                checkSession(headerLines);

                if (!Objects.isNull(splitedPath[1])) {
                    parseParameters(splitedPath[1]);
                }


                String path = splitedPath[0];

                try {
                    internalDispatchRequest(path, true);
                } catch (Exception ex) {
                    sendBadRequestMessage();
                }

            } catch (IOException ex) {
                throw new RuntimeException("Error occurred during reading from client");
            } finally {
                try {
                    closeStreams();
                    csocket.close();
                } catch (IOException e) {

                }
            }
        }

        /**
         * Construct and send response to request.
         * Execution of request will be dispatched depending on file extension.
         *
         * @param urlPath path to requested file.
         * @param directCall boolean flag.
         * @throws Exception if error occurs.
         */
        public void internalDispatchRequest(String urlPath, boolean directCall) throws Exception {

            if (urlPath.equals("/private") || urlPath.startsWith("/private/")) {
                if (directCall) {
                    sendError(ostream, 404, "Direct Call");
                }
            }


            IWebWorker worker;

            if (urlPath.startsWith("/ext")) {
                worker = getWebWorker(DEFAULT_WORKERS_PACKAGE
                        + "."
                        + urlPath.substring("/ext".length()+1));

                //if requested worker do not exists
                if (Objects.isNull(worker)) {
                    throw new Exception();
                }

            } else {
                worker = workersMap.get(urlPath);
            }


            //WORKER requested
            if (!Objects.isNull(worker)) {
                worker.processRequest(getContext());
                return;
            }

            //determine requested file path
            Path sourcePath = resolvePath(urlPath);

            if (Objects.isNull(sourcePath)) {
                throw new Exception();
            }

            //SMARTSCRIPT is requested
            if (urlPath.endsWith(".smscr")) {
                String fileContent = Files.readString(sourcePath);

                SmartScriptEngine engine = new SmartScriptEngine(
                        new SmartScriptParser(fileContent).getDocumentNode(),
                        getContext()
                );
                engine.execute();
            } else {
                //any other case

                String mimeType = determineMimeType(sourcePath.getFileName().toString());

                RequestContext rc = getContext();

                rc.setMimeType(mimeType);
                rc.setStatusCode(200);

                rc.write(Files.readAllBytes(sourcePath));
            }
        }

        /**
         * Checks if session exists and if it should be updated or replaced
         * by new session.
         *
         * @param headerLines list of {@code String}s representing lines
         *                    from request's header.
         */
        private void checkSession(List<String> headerLines) {
            String sidCandidate = findSidCookie(headerLines);

            if (updateSession(sidCandidate)) {
                return;
            } else {
                sidCandidate = generateSID();
            }

            SID = sidCandidate;

            createNewSidSession();
        }

        /**
         * Creates new session for this worker.
         */
        private void createNewSidSession() {
            SessionMapEntry session = new SessionMapEntry();

            session.validUntil = currentTimeSec() + sessionTimeout;
            session.host = host;
            session.map = new ConcurrentHashMap<>();
            permPrams = session.map;

            synchronized (SmartHttpServer.this) {
                sessions.put(SID, session);
            }

            outputCookies.add(new RequestContext.RCCookie(
                    "sid",
                    SID,
                    null,
                    host,
                    "/")
            );
        }

        /**
         * Updates session of this job. If there is no session under sid of this
         * worker method will return {@code false}. If session is out of date method will
         * also return {@code false}.
         *
         * @param sidCandidate SID of session that should be updated.
         * @return {@code true} if session is successfully updated otherwise {@code false}.
         */
        private boolean updateSession(String sidCandidate) {
            if (Objects.isNull(sidCandidate)) {
                return false;
            }

            SessionMapEntry session = null;

            synchronized (SmartHttpServer.this) {
                session = sessions.get(sidCandidate);
            }

            if (Objects.isNull(session)) {
                return false;
            }

            if (session.host.equals(host)) {
                if (session.validUntil > currentTimeSec()) {
                    session.validUntil = currentTimeSec() + sessionTimeout;
                    permPrams = session.map;

                    return true;
                }
            }

            return false;
        }

        /**
         * Generates session identificator.<br>
         * String of 20 random digits.
         *
         * @return generated SID.
         */
        private String generateSID() {
            StringBuilder sb = new StringBuilder();

            synchronized (SmartHttpServer.this) {
                while (sb.length() < 20) {
                    sb.append(Math.abs(sessionRandom.nextInt()));
                }
            }

            return sb.substring(0, 20);
        }

        /**
         * Finds sid cookie in header if it is present. Cookie value will be
         * returned as result of this method.
         *
         * @param headerLines lines of request's header.
         * @return sid cookie value or {@code null} if sid cookie is not present
         *         in header.
         */
        private String findSidCookie(List<String> headerLines) {
            for (String line : headerLines) {
                if (!line.startsWith("Cookie:")) {
                    continue;
                }

                String sidCandidate;

                String cookieLine = line.substring("Cookie:".length()).strip();

                String[] cookies = cookieLine.split(";");

                for (String cookie : cookies) {
                    if (!cookie.startsWith("sid")) {
                        continue;
                    }

                    int valueIndex = cookie.indexOf('=') + 1;

                    if (valueIndex != -1 && valueIndex != cookie.length()) {
                        sidCandidate = cookie.substring(valueIndex);

                        if (sidCandidate.length() < 3) {
                            continue;
                        }

                        return sidCandidate.substring(1, sidCandidate.length()-1);
                    }
                }
            }

            return null;
        }

        @Override
        public void dispatchRequest(String urlPath) throws Exception {
            internalDispatchRequest(urlPath, false);
        }

        /**
         * Returns {@code context} property if it is initialized otherwise
         * new {@code RequestContext} object will be created and assigned to {@code context}.
         *
         * @return {@code context} property.
         */
        private RequestContext getContext() {
            if (Objects.isNull(context)) {
                context = new RequestContext(
                        ostream,
                        params,
                        permPrams,
                        outputCookies,
                        tempParams,
                        this,
                        SID);
            }
            return context;
        }

        /**
         * Closes streams defined to client.
         *
         * @throws IOException if error occurs while streams are closing.
         */
        private void closeStreams() throws IOException {
            istream.close();
            ostream.close();
        }

        /**
         * Method sets mime type determined with file extension form
         * given file name.
         *
         * @param fileName name of file which extension will be used for
         *                 defining mime type.
         */
        private String determineMimeType(String fileName) {
            String extension = extractExtension(fileName);

            String mimeType = mimeTypes.get(extension);

            return Objects.isNull(mimeType) ? "application/octet-stream" : mimeType;
        }

        /**
         * Resolves requested path against {@code documentRoot} from outer class.
         *
         * @param path path to be resolved
         * @return resolved path or {@code null} if given path is absolute path or
         *         if file on resolved path can't be reached.
         * @throws IOException if error occurs while writing an error message.
         */
        private Path resolvePath(String path) throws IOException {
            Objects.requireNonNull(path);

            if (path.startsWith("/")) {
                path = path.substring(1);
            }

            Path requestedPath = documentRoot.resolve(path);

            if (documentRoot.toString().contains(requestedPath.toString())) {
                sendError(ostream, 403, "Forbidden");
                return null;
            }

            if (!Files.isReadable(requestedPath)) {
                sendError(ostream, 404, "File not found");
                return null;
            }

            return requestedPath;
        }

        /**
         * Parse and fill params map with parameters defined in given {@code String}.
         * If parameter have no defined value in map will be put {@code null} as value for
         * that parameter.
         *
         * @param s string that represents parameters from request.
         *          (example of expected string will be: <i>"name=joe&country=usa"</i>)
         */
        private void parseParameters(String s) {
            Objects.requireNonNull(s);

            String[] parameters = s.split("&");

            for (String parameter : parameters) {
                String[] splited = parameter.split("=");

                if (splited.length == 1) {
                    params.put(splited[0], null);
                } else if (splited.length == 2) {
                    params.put(splited[0], splited[1]);
                } else {

                    //in case that second argument contains '?' sign
                    params.put(
                            splited[0],
                            s.substring(splited[0].length() + 1)
                    );
                }
            }
        }

        /**
         * Helper method that splits path part of header on path and parameters.
         *
         * @param requestedPath path to be split.
         * @return array of strings with two elements, path and parameters if there is no
         *         defined parameters second element will be null.
         */
        private String[] splitRequestedPath(String requestedPath) {
            Objects.requireNonNull(requestedPath);

            String[] result = requestedPath.split("\\?");

            if (result.length == 1) {
                return new String[]{result[0], null};
            } else if (result.length == 2) {
                return new String[]{result[0], result[1]};
            } else {
                return new String[]{requestedPath, null};
            }

        }

        /**
         * Sets host property to value defined in header.
         * If there is no defined host in header host will be set to
         * {@code domainName} from outer class.
         *
         * @param headerLines collection with header lines.
         */
        private void setHost(List<String> headerLines) {
            Objects.requireNonNull(headerLines);

            for (String line : headerLines) {
                if (!line.startsWith("Host:")) {
                    continue;
                }

                String hostArg = line.substring("Host:".length()).strip();
                String[] hostArgsSplited = hostArg.split(":");

                if (hostArgsSplited.length == 2) {
                    try {
                        //argument is in format "name:number" extract name
                        Integer.parseInt(hostArgsSplited[1]);
                        host = hostArgsSplited[0];
                        return;
                    } catch (NumberFormatException ex) {
                    }
                }

                host = hostArg;
                return;
            }

            host = domainName;
        }


        /**
         * Extract all informations from first line of request.
         * Class properties will be set on extracted value. If format of
         * given first line is illegal error message will be sent.
         *
         * @param firstLine first line of request header.
         * @return path requested by client.
         * @throws IOException if error occurs while writing error message.
         */
        private String extractFromFirstLine(String firstLine) throws IOException {
            Objects.requireNonNull(firstLine);

            String[] firstLineArgs = firstLine.split("\\s+");

            if (Objects.isNull(firstLine) || firstLineArgs.length != 3) {
                sendError(ostream, 400, "Bad Request");
                return null;
            }

            String extractedMethod = firstLineArgs[0].toUpperCase();
            if (!checkMethod(extractedMethod)) {
                return null;
            }

            String extractedVersion = firstLineArgs[2].toUpperCase();
            if (!checkVersion(extractedVersion)) {
                return null;
            }

            method = extractedMethod;
            version = extractedVersion;

            String requestedPath = firstLineArgs[1];

            return requestedPath;
        }

        /**
         * Checks if required HTTP version is supported.
         *
         * @param version version to be checked.
         * @return {@code true} if version is supported otherwise {@code false}.
         * @throws IOException if error occurs while writing error message.
         */
        private boolean checkVersion(String version) throws IOException {
            Objects.requireNonNull(version);

            switch (version) {
                case ("HTTP/1.1"):
                case ("HTTP/1.0"):
                    return true;
                default:
                    sendError(ostream, 405, "Method Not Allowed");
                    return false;
            }
        }


        /**
         * Checks if required HTTP method is supported.
         *
         * @param method version to be checked.
         * @return {@code true} if method is supported otherwise {@code false}.
         * @throws IOException if error occurs while writing error message.
         */
        private boolean checkMethod(String method) throws IOException {
            Objects.requireNonNull(method);

            switch (method) {
                case ("GET"):
                    return true;
                default:
                    sendError(ostream, 405, "Method Not Allowed");
                    return false;
            }
        }


        /**
         * Sends "Bad Request" error message as response to client.
         *
         * @throws IOException if message can't be written to output stream.
         */
        private void sendBadRequestMessage() throws IOException {
            sendError(ostream, 400, "Bad Request");
        }


        /**
         * Helper method for sending error message on given output stream.
         *
         * @param os output stream on which error message will be written.
         * @param statusCode error status code.
         * @param statusText error status text.
         * @throws IOException if message can't be written to output stream.
         */
        private void sendError(OutputStream os, int statusCode, String statusText) throws IOException {

            os.write(
                    (version + " " + statusCode + " " + statusText + "\r\n"+
                            "Server: simple java server\r\n"+
                            "Content-Type: text/plain;charset=UTF-8\r\n"+
                            "Content-Length: 0\r\n"+
                            "Connection: close\r\n"+
                            "\r\n").getBytes(StandardCharsets.US_ASCII)
            );
            os.flush();
        }
    }

    /**
     * Structure that represents single session.
     */
    private static class SessionMapEntry {
        @SuppressWarnings("unused")
		String sid;
        String host;
        long validUntil;
        Map<String,String> map;
    }

    /**
     * Extracts extension of file from given file name.
     *
     * @param fileName name of file which extension will be returned.
     * @return extension of file or {@code null} if file have no defined extension.
     */
    private static String extractExtension(String fileName) {
        int dotIndex = fileName.lastIndexOf('.');

        if (dotIndex == -1) {
            return null;
        }

        if (dotIndex == 0 || dotIndex == fileName.length() - 1) {
            return null;
        }

        return fileName.substring(dotIndex + 1);
    }


    /**
     * Read header of HTTP request.
     *
     * @param is input stream from which header will be read.
     * @return header of request as {@code List} of {@code Strings} where every
     *         element of collection represents single request line.
     *         If there is no single line {@code null} will be returned.
     * @throws IOException
     */
    private static List<String> readRequest(InputStream is) throws IOException {

        byte[] requestHeader = readHeaderBytes(is);

        if (Objects.isNull(requestHeader)) {
            return null;
        }

        List<String> headers = new ArrayList<>();
        String currentLine = null;

        String requestHeaderStr = new String(requestHeader, StandardCharsets.US_ASCII);

        for(String s : requestHeaderStr.split("\n")) {
            if(s.isEmpty()) break;
            char c = s.charAt(0);
            if(c==9 || c==32) {
                currentLine += s;
            } else {
                if(currentLine != null) {
                    headers.add(currentLine);
                }
                currentLine = s;
            }
        }
        if(!currentLine.isEmpty()) {
            headers.add(currentLine);
        }
        return headers;
    }

    /**
     * Reads header of HTTP request.
     * Returns complete header as array of {@code bytes}.
     *
     * @param is input stream from which bytes will be read.
     * @return header of HTTP request. If there is no single byte
     *         in stream {@code null} wil be returned.
     * @throws IOException
     */
    private static byte[] readHeaderBytes(InputStream is) throws IOException {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        int state = 0;

        l:		while(true) {

            int b = is.read();

            if(b==-1) {
                return null;
            }


            if(b!=13) {
                bos.write(b);
            }

            switch(state) {
                case 0:
                    if(b==13) { state=1; } else if(b==10) state=4;
                    break;
                case 1:
                    if(b==10) { state=2; } else state=0;
                    break;
                case 2:
                    if(b==13) { state=3; } else state=0;
                    break;
                case 3:
                    if(b==10) { break l; } else state=0;
                    break;
                case 4:
                    if(b==10) { break l; } else state=0;
                    break;
            }
        }
        return bos.toByteArray();
    }

    /**
     * Returns current time in seconds.
     *
     * @return current time.
     */
    private static long currentTimeSec() {
        return System.currentTimeMillis() / 1000;
    }

}