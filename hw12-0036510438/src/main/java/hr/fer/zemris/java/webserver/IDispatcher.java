package hr.fer.zemris.java.webserver;

/**
 * Implements object that dispatch requests to be executed.
 *
 * @code Domagoj Lokner
 */
public interface IDispatcher {

    /**
     * Dispatches request to objects capable to execute it.
     *
     * @param urlPath url path requested object/file.
     * @throws Exception if error occurs.
     */
    void dispatchRequest(String urlPath) throws Exception;
}
