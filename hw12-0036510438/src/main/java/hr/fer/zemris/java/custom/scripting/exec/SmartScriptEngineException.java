package hr.fer.zemris.java.custom.scripting.exec;

/**
 * Indicate error in {@code SmartScriptEngine} class.
 */
public class SmartScriptEngineException extends RuntimeException {

	private static final long serialVersionUID = -3742077800689023447L;

	/**
     * Constructs {@code SmartScriptEngineException}.
     */
    public SmartScriptEngineException() {
    }

    /**
     * Constructs {@code SmartScriptEngineException} with detail message.
     *
     * @param message - detail message.
     */
    public SmartScriptEngineException(String message) {
        super(message);
    }
}
