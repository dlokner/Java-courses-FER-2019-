package hr.fer.zemris.java.custom.collections;

/**
 * Interface that represents some general collection of objects.
 * 
 * @author Domagoj Lokner
 *
 */
public interface Collection {
	
	/**
	 * Returns the number of currently stored objects in collection.
	 * 
	 * @return size of the collection.
	 */
	int size();
	
	/**
	 * Adds given {@code value} into the collection.
	 * 
	 * @param value - element to be added.
	 */
	void add(Object value);
	
	/**
	 * Checks for containment of given {@code value} in collection.
	 * 
	 * @param value - object to be checked for containment.
	 * @return {@code true} if collection contains given {@code value} otherwise {@code false}.
	 */
	boolean contains(Object value);
	
	/**
	 * Remove given {@code value} from collection.
	 * 
	 * @param value - element to be removed.
	 * @return {@code true} if element is removed otherwise {@code false}.
	 */
	boolean remove(Object value);
	
	/**
	 * Allocates new array with size equals to the size of this collections, 
	 * fills it with collection content.
	 * 
	 * @return Newly allocated array.
	 */
	Object[] toArray();
	
	/**
	 * Calls {@code processor.process} for each element this collection contains. 
	 * 
	 * @param processor - {@code Processor} object.
	 */
	default void forEach(Processor processor) {
		ElementsGetter getter = this.createElementsGetter();
		
		getter.processRemaining(processor);
	}
	
	/**
	 * Remove all elements from this collection.
	 */
	void clear();
	
	/**
	 * Creates instance of class that implements {@code ElementsGetter} object.
	 * 
	 * @return new {@code ElementsGetter}.
	 */
	ElementsGetter createElementsGetter();
	
	/**
	 * Checks if collection contains no objects.
	 * 
	 * @return {@code true} if collection is empty and {@code false} otherwise.
	 */
	default boolean isEmpty() {
		return size() == 0;
	}
	
	/**
	 * Adds all elements from given {@code other} collection into this collection.
	 * 
	 * @param other - collection to be added into current collection.
	 */
	default void addAll(Collection other) {
		other.forEach((element) -> add(element));
	}
	
	/**
	 * Add element of given collection depending on {@code tester} to this collection.
	 * 
	 * @param col - given collection.
	 * @param tester - {@code Tester} object.
	 */
	default void addAllSatisfying(Collection col, Tester tester) {
		ElementsGetter getter = col.createElementsGetter();
		
		getter.processRemaining(element -> {
			if(tester.test(element)) {
				this.add(element);
			}
		});
	}
	
}
