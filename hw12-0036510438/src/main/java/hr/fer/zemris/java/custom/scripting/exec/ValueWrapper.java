package hr.fer.zemris.java.custom.scripting.exec;

import java.util.Objects;
import java.util.function.BiFunction;

/**
 * {@code Objects} value wrapper.<br>
 * Class offers arithmetical methods able to change wrapped value if it is instance of 
 * {@code Integer} or {@code Double} class, or if it is {@code String} that 
 * can be parsed to one of mentioned classes.<br>
 * In that case null value will be interpreted as {@code Integer(0)}.
 * 
 * @author Domagoj Lokner
 *
 */
public class ValueWrapper {

	/**
	 * Wrapped {@code Object}. 
	 */
	private Object value;

	/**
	 * Constructs {@code ValueWrapper} with given value.
	 * 
	 * @param value - value to be wrapped.
	 */
	public ValueWrapper(Object value) {
		setValue(value);
	}
	
	/**
	 * Adds given value to wrapped value.
	 * 
	 * @param incValue - value to be added.
	 */
	public void add(Object incValue) {
		value = arithmeticOperation(value, incValue, (n1, n2) -> n1 + n2);
	}
	
	/**
	 * Subtract wrapped value by given value.
	 * 
	 * @param decValue - subtrahend value.
	 */
	public void subtract(Object decValue) {
		value = arithmeticOperation(value, decValue, (n1, n2) -> n1 - n2);
	}
	
	/**
	 * Multiply wrapped value by given value.
	 * 
	 * @param mulValue - multiplier value.
	 */
	public void multiply(Object mulValue) {
		value = arithmeticOperation(value, mulValue, (n1, n2) -> n1 * n2);
	}
	
	/**
	 * Divide wrapped value by given value.
	 * 
	 * @param divValue - divisor.
	 */
	public void divide(Object divValue) {
		value = arithmeticOperation(value, divValue, (n1, n2) -> n1 / n2);
	}
	
	/**
	 * Compare wrapped value to given value.
	 * 
	 * @param withValue - value with which wrapped value will be compared.
	 */
	public int numCompare(Object withValue) {
		ValueWrapper copy = new ValueWrapper(value);
		
		copy.subtract(withValue);
		
		return (Double.compare(((Number)copy.value).doubleValue(), 0d));
	}

	/**
	 * Gets wrapped value.
	 * 
	 * @return
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Sets wrapped value.
	 * 
	 * @param value
	 */
	public void setValue(Object value) {
		this.value = value;
	}
	
	/**
	 * Method performing arithmetic operation.
	 * 
	 * @param arg1 - first argument
	 * @param arg2 - second argument.
	 * @param function - arithmetic function to be performed.
	 * @return result of operation.
	 * @throws RuntimeException if any argument can be interpreted as {@code Integer} or {@code Double}.
	 */
	private static Object arithmeticOperation(Object arg1, Object arg2, BiFunction<Double, Double, Number> function) {
		Number arg1num = extractArg(arg1);
		Number arg2num = extractArg(arg2);
		
		Number result = function.apply(arg1num.doubleValue(), arg2num.doubleValue());
		
		if(arg1num instanceof Double || arg2num instanceof Double) {
			return Double.valueOf(result.doubleValue());
		} else {
			return Integer.valueOf(result.intValue());
		}
	}
	
	/**
	 * Check given argument and return it if argument is instance of {@code Integer} or {@code Double} classes.<br>
	 * If argument is {@code String} that can be interpreted as {@code Integer} or {@code Double} it will be returnes
	 * as instance of that class.
	 * 
	 * @param arg - argument to be checked.
	 * @return argument.
	 */
	private static Number extractArg(Object arg) {
		if(Objects.isNull(arg)) {
			return Integer.valueOf(0);
		}
		
		if(arg instanceof Integer || arg instanceof Double) {
			return (Number)arg;
		}
		
		if(!(arg instanceof String)) {
			throw new RuntimeException("Wrapped value can not be interpreted as number.");
		}
		
		try {
			String stringArg = (String)arg;
			if(stringArg.contains(".") || stringArg.contains("E")) {
				return Double.parseDouble(stringArg);
			}
			return Integer.parseInt(stringArg);
		} catch(NumberFormatException ex) {
			throw new RuntimeException("Wrapped string can not be interpreted as number.");
		}
	}
	
}
