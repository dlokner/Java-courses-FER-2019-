package hr.fer.zemris.java.custom.collections;

public interface ElementsGetter {

	/**
	 * Returns {@code true} if there is more elements to be gotten.
	 * 
	 * @return {@code true} if there is more elements otherwise {@code false}.
	 */
	boolean hasNextElement();

	/**
	 * Gets value of next element in collection.
	 * 
	 * @return value of next element.
	 */
	Object getNextElement();
 
	/**
	 * Call {@code p.process} method for all remaining elements of collection.
	 * 
	 * @param p - {@code Processor}
	 */
	default void processRemaining(Processor p) {
		while(hasNextElement()) {
			p.process(getNextElement());
		}
	}
}
