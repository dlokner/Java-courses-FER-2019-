package hr.fer.zemris.java.webserver;

/**
 * Implements object capable to process request.
 */
public interface IWebWorker {

    /**
     * Processes request and creates content for client.
     *
     * @param context request's context
     * @throws Exception if error occurs
     * */
    public void processRequest(RequestContext context) throws Exception;
}
