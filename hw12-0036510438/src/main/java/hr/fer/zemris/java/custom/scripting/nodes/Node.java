package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;

import java.util.Objects;

/**
 * Class represents all nodes that can be part of graph.
 * 
 * @author Domagoj Lokner
 *
 */
public abstract class Node {
	private ArrayIndexedCollection array;
	private int numOfChildren;
	
	/**
	 * Adds given {@code child} to collection of children.
	 * 
	 * @param child - child {@code Node}.
	 */
	public void addChildNode(Node child) {
		if(Objects.isNull(array)) {
			array = new ArrayIndexedCollection();
		}
		array.add(child);
		++numOfChildren;
	}
	
	/**
	 * Gets number of children.
	 * 
	 * @return number of children.
	 */
	public int numberOfChildren() {
		return numOfChildren;
	}
	
	/**
	 * Gets child on {@code index}.
	 * 
	 * @param index - index of child.
	 * @return child {@code Node}.
	 * @throws IndexOutOfBoundsException if {@code index} is not in range [0, size-1]. 
	 */
	public Node getChild(int index) {
		return (Node)array.get(index);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((array == null) ? 0 : array.hashCode());
		result = prime * result + numOfChildren;
		return result;
	}


	/**
	 * Perform visitor action defined for this node.
	 *
	 * @param visitor implementation of {@code INodeVisitor} interface.
	 */
	public abstract void accept(INodeVisitor visitor);

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Node)) {
			return false;
		}
		Node other = (Node) obj;
		if (array == null) {
			if (other.array != null) {
				return false;
			}
		} else if (!array.equals(other.array)) {
			return false;
		}
		if (numOfChildren != other.numOfChildren) {
			return false;
		}
		return true;
	}
	
	
}
