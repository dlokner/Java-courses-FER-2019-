package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Define working states of {@code SmartScriptLexer}.
 * 
 * @author Domagoj Lokner
 *
 */
public enum SmartScriptLexerState {
		TEXT,
		TAGS
}
