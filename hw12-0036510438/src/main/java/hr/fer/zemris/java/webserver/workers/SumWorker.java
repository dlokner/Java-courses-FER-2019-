package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Worker which generate table with values of parameters <i>a</> and <i>b</i>
 * and their sum.
 * If parameters values are not defined default value will be used.
 *
 * @author Domagoj Lokner
 */
public class SumWorker implements IWebWorker {
    @Override
    public void processRequest(RequestContext context) throws Exception {


        int a = getVarValue("a", 1, context);
        int b = getVarValue("b", 2, context);

        int zbroj = a+b;

        context.setTemporaryParameter("zbroj", Integer.toString(zbroj));
        context.setTemporaryParameter("varA", Integer.toString(a));
        context.setTemporaryParameter("varB", Integer.toString(b));

        context.setTemporaryParameter("imgName",
                zbroj % 2 == 0 ? "firstImage.jpg" : "secondImage.jpg");

        context.getDispatcher().dispatchRequest("/private/pages/calc.smscr");
    }

    private int getVarValue(String varName, int defaultValue, RequestContext context) {
        int var;

        try {
            var = Integer.parseInt(context.getParameter(varName));
        } catch (NumberFormatException | NullPointerException ex) {
            var = defaultValue;
        }

        return var;
    }
}
