package hr.fer.zemris.java.custom.scripting.parser;

/**
 * Thrown to indicate an error in {@code SmartScriptParser}.
 * 
 * @author Domagoj Lokner
 *
 */
public class SmartScriptParserException extends RuntimeException {
	
	private static final long serialVersionUID = 981395893165535867L;

	/**
	 * Constructs {@code SmartScriptParserException}.
	 */
	public SmartScriptParserException() {
		super();
	}
	
	/**
	 * Constructs {@code SmartScriptParserException} with specified detail message.
	 * 
	 * @param message - given message.
	 */
	public SmartScriptParserException(String message) {
		super(message);
	}
}
