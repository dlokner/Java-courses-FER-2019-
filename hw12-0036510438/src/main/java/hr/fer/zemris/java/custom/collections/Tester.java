package hr.fer.zemris.java.custom.collections;

/**
 * Interface that test given objects.
 * 
 * @author Domagoj Lokner
 *
 */
@FunctionalInterface
public interface Tester {
	
	/**
	 * Test given object.
	 * 
	 * @param obj - object to be tested.
	 * @return {@code true} if object is acceptable otherwise {@code false}.
	 */
	boolean test(Object obj);
}
