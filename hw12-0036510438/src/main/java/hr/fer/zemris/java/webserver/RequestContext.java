package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Object that represents context of HTTP request.
 *
 * @author Domagoj Lokner
 */
public class RequestContext {

    private static final String DEFAULT_ENCODING = "UTF-8";
    private static final int DEFAULT_STATUS_CODE = 200;
    private static final String DEFAULT_STATUS_TEXT = "OK";
    private static final String DEFAULT_MIME_TYPE = "text/html";
    private static final String NEW_LINE = "\r\n";

    /**
     * Output stream on which content will be written.
     */
    private OutputStream outputStream;

    /**
     * Charset used for encoding content.
     */
    @SuppressWarnings("unused")
	private Charset charset;

    /**
     * Encoding.
     */
    private String encoding;

    /**
     * Request status code.
     */
    private int statusCode;

    /**
     * Request status text.
     */
    private String statusText;

    /**
     * Mime type.
     */
    private String mimeType;

    /**
     * Length of context
     */
    public Long contentLength;

    /**
     * Map that stores requests parameters.
     */
    private Map<String, String> parameters;

    /**
     * Map that stores temporary parameters of request.
     */
    private Map<String, String> temporaryParameters;

    /**
     * Map that stores persistent parameters of request.
     */
    private Map<String, String> persistentParameters;

    /**
     * List of cookies
     */
    private List<RCCookie> outputCookies;

    /**
     * Indicate that header is generated.
     */
    private boolean headerGenerated;

    /**
     * Session identificator.
     */
    private String sid;

    /**
     * Dispatcher object for executing requests based on this context.
     */
    private IDispatcher dispatcher;

    /**
     * Constructs {@code RequestContext}.
     *
     * @param outputStream output stream on which request content will be written.
     * @param parameters collection of parameters.
     * @param persistentParameters collection of persistent parameters.
     * @param outputCookies collection of cookies.
     * @throws NullPointerException if {@code null} is given as output parameter.
     */
    public RequestContext(OutputStream outputStream, Map<String,String> parameters,
            Map<String,String> persistentParameters, List<RCCookie> outputCookies) {

        Objects.requireNonNull(outputStream);

        this.encoding = DEFAULT_ENCODING;
        this.statusCode = DEFAULT_STATUS_CODE;
        this.statusText = DEFAULT_STATUS_TEXT;
        this.mimeType = DEFAULT_MIME_TYPE;

        this.outputStream = outputStream;

        this.persistentParameters = new HashMap<>();
        this.parameters = new HashMap<>();

        if (!Objects.isNull(parameters)) {
            this.parameters.putAll(parameters);
        }

        this.persistentParameters = persistentParameters;

        this.outputCookies = new LinkedList<>();

        if (!Objects.isNull(outputCookies)) {
            this.outputCookies.addAll(outputCookies);
        }

        this.temporaryParameters = new HashMap<>();
    }

    /**
     * Constructs {@code RequestContext}.
     *
     * @param outputStream output stream on which request content will be written.
     * @param parameters collection of parameters.
     * @param persistentParameters collection of persistent parameters.
     * @param outputCookies collection of cookies.
     * @param temporaryParameters collection of temporary parameters.
     * @param dispatcher dispatcher object for executing requests based on this context.
     * @throws NullPointerException if {@code null} is given as output parameter.
     */
    public RequestContext(OutputStream outputStream, Map<String,String> parameters,
                          Map<String,String> persistentParameters, List<RCCookie> outputCookies,
                          Map<String, String> temporaryParameters, IDispatcher dispatcher, String sessionID) {

        this(outputStream, parameters, persistentParameters, outputCookies);

        this.temporaryParameters = temporaryParameters;
        this.dispatcher = dispatcher;
        this.sid = sessionID;
    }


    /**
     * Gets parameter stored by given {@code name}.
     *
     * @param name name of parameter.
     * @return parameter stored by given name or {@code null} if no association exists.
     */
    public String getParameter(String name) {
        return parameters.get(name);
    }

    /**
     * Constructs read-only set of all parameters this request contains.
     *
     * @return set of parameter names.
     */
    public Set<String> getParameterNames() {
        return Collections.unmodifiableSet(
                parameters.keySet()
        );
    }

    /**
     * Gets dispatcher property.
     *
     * @return dispatcher.
     */
    public IDispatcher getDispatcher() {
        return dispatcher;
    }

    /**
     * Returns persistent parameter stored by given key.
     *
     * @param name key of persistent parameter.
     * @return parameter stored by given name or {@code null} if no association exists.
     */
    public String getPersistentParameter(String name) {
        return persistentParameters.get(name);
    }

    /**
     * Constructs read-only set of all persistent parameters this request contains.
     *
     * @return set of persistent parameters names.
     */
    public Set<String> getPersistentParameterNames() {
        return Collections.unmodifiableSet(
                persistentParameters.keySet()
        );
    }

    /**
     * Stores given persistent parameter.
     *
     * @param name parameter name.
     * @param value value of parameter.
     * @throws NullPointerException if {@code null} is given as name.
     */
    public void setPersistentParameter(String name, String value) {
        Objects.requireNonNull(name);

        persistentParameters.put(name, value);
    }

    /**
     * Removes persistent parameter.
     *
     * @param name name of parameter to be removed.
     */
    public void removePersistentParameter(String name) {
        persistentParameters.remove(name);
    }


    /**
     * Gets temporary parameter stored under given key.
     *
     * @param name temporary parameter name.
     * @return parameter stored by given name or {@code null} if no association exists.
     */
    public String getTemporaryParameter(String name) {
        return temporaryParameters.get(name);
    }

    /**
     * Constructs read-only set of temporary parameters this request contains.
     *
     * @return set of temporary parameters names.
     */
    public Set<String> getTemporaryParameterNames() {
        return Collections.unmodifiableSet(
                temporaryParameters.keySet()
        );
    }

    /**
     * Sets temporary parameter.
     *
     * @param name name of temporary parameter to be set.
     * @param value value of temporary parameter to be set.
     * @throws NullPointerException if {@code null} is given as parameter {@code name}.
     */
    public void setTemporaryParameter(String name, String value) {
        temporaryParameters.put(name, value);
    }

    /**
     * Removes temporary parameter.
     *
     * @param name name of temporary parameter to be removed.
     */
    public void removeTemporaryParameter(String name) {
        temporaryParameters.remove(name);
    }


    /**
     * Retrieves an identifier which is unique for current user session.
     *
     * @return session identifier.
     */
    public String getSessionID() {
        return sid;
    }


    /**
     * Sets encoding.
     *
     * @param encoding string representing encoding to be set.
     */
    public void setEncoding(String encoding) {
        isEditable();
        this.encoding = encoding;
    }

    /**
     * Sets status code.
     *
     * @param statusCode status code to be set.
     */
    public void setStatusCode(int statusCode) {
        isEditable();
        this.statusCode = statusCode;
    }

    /**
     * Sets status text.
     *
     * @param statusText status text to be set.
     */
    public void setStatusText(String statusText) {
        isEditable();
        this.statusText = statusText;
    }

    /**
     * Sets mime type.
     *
     * @param mimeType mime type to be set.
     */
    public void setMimeType(String mimeType) {
        isEditable();
        this.mimeType = mimeType;
    }

    /**
     * SEts length of content.
     *
     * @param contentLength content length.
     */
    public void setContentLength(Long contentLength) {
        isEditable();
        this.contentLength = contentLength;
    }


    /**
     * Checks if property is editable.
     * Following properties can't be edited after heading is generated for this {@code RequestContext}:
     * {@code encoding}, {@code statusCode}, {@code statusText}, {@code mimeType},
     * {@code outputCookies} and {@code contentLength}.
     *
     * @throws RuntimeException if header is generated and properties can't be changed.
     */
    private void isEditable() {
        if(headerGenerated) {
            throw new RuntimeException();
        }
    }


    /**
     * Writes {@code data} into {@code outputStream} in this {@code RequestContext}.<br>
     * Header for the request will be generated if this is first call of {@code write} method.
     *
     * @param data data to be written into {@code outputStream}.
     * @param offset from what index of data to write.
     * @param len length of data to be written.
     * @return reference on {@code RequestContext} in which this data is written.
     * @throws IOException
     */
    public RequestContext write(byte[] data, int offset, int len) throws IOException {
        if (!headerGenerated) {
            generateHeader();
        }

        outputStream.write(Arrays.copyOfRange(data, offset, offset + len));
        outputStream.flush();
        return this;
    }

    /**
     * Writes {@code data} into {@code outputStream} in this {@code RequestContext}.<br>
     * Header for the request will be generated if this is first call of {@code write} method.
     *
     * @param data data to be written into {@code outputStream}.
     * @return reference on {@code RequestContext} in which this data is written.
     * @throws IOException
     */
    public RequestContext write(byte[] data) throws IOException {
        return write(data, 0, data.length);
    }

    /**
     * Writes {@code data} into {@code outputStream} in this {@code RequestContext}.<br>
     * Header for the request will be generated if this is first call of {@code write} method.
     *
     * @param text text to be written into {@code outputStream}.
     *             Text will be encoded with {@code encoding} given in constructor.
     * @return reference on {@code RequestContext} in which this data is written.
     * @throws IOException
     */
    public RequestContext write(String text) throws IOException {
        Objects.requireNonNull(text);

        byte[] data = text.getBytes(Charset.forName(encoding));

        return write(data);
    }

    /**
     * Adds cookie to {@code outputCookies} collection.
     *
     * @param cookie - cookie to be added.
     * @throws NullPointerException if given {@code cookie} is {@code null}.
     */
    public void addRCCookie(RCCookie cookie) {
        Objects.requireNonNull(cookie);

        outputCookies.add(cookie);
    }


    /**
     * Generates header for this request.
     */
    private void generateHeader() throws IOException {
        headerGenerated = true;

        charset = Charset.forName(encoding);

        StringBuilder sb = new StringBuilder();

        sb.append(String.format("HTTP/1.1 %d %s%s", statusCode, statusText, NEW_LINE));

        sb.append(String.format("Content-Type: %s", mimeType));
        if (mimeType.startsWith("text/")) {
            sb.append(String.format("; charset=%s", encoding));
        }
        sb.append(NEW_LINE);

        if (!Objects.isNull(contentLength)) {
            sb.append(String.format("Content-Length:%d", contentLength));
            sb.append(NEW_LINE);
        }

        for (RCCookie cookie : outputCookies) {
            sb.append("Set-Cookie: ");
            sb.append(cookie + "; HttpOnly");
            sb.append(NEW_LINE);
        }

        sb.append(NEW_LINE);

        write(sb.toString().getBytes(Charset.forName("ISO_8859_1")));
    }


    /**
     * Object represents single cookie.
     *
     * @author Domagoj Lokner
     */
    public static class RCCookie {
        private String name;
        private String value;
        private Integer maxAge;
        private String domain;
        private String path;


        public RCCookie(String name, String value, Integer maxAge, String domain, String path) {
            Objects.requireNonNull(name, "Cookie name can't be null");
            Objects.requireNonNull(value, "Cookie value can't be null");

            this.name = name;
            this.value = value;
            this.maxAge = maxAge;
            this.domain = domain;
            this.path = path;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();

            sb.append(name + "=\"");
            sb.append(value);
            sb.append("\";");

            sb.append(generateString(" Domain", domain));
            sb.append(generateString(" Path", path));
            sb.append(generateString(" Max-Age", maxAge));

            sb.deleteCharAt(sb.length()-1);

            return sb.toString();
        }

        /**
         * Helper method for {@code toString} method.<br>
         * Generate string in format "<i>argument=value;</i>".
         *
         * @param argument argument
         * @param value value
         * @return formatted string or empty string if given {@code value} is {@code null}.
         */
        private String generateString(String argument, Object value) {
            if (Objects.isNull(value)) {
                return "";
            }

            return String.format("%s=%s;", argument, value.toString());
        }
    }
}
