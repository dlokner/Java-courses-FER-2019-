package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Represents textual data {@code Node}.
 * 
 * @author Domagoj Lokner
 *
 */
public class TextNode extends Node {
	private String text;
	
	/**
	 * Constructs new {@code TextNode}.
	 * 
	 * @param text - text of text node.
	 */
	public TextNode(String text) {
		this.text = text;
	}

	/**
	 * Gets text of this {@code TextNode}.
	 * 
	 * @return text.
	 */
	public String getText() {
		StringBuilder sb = new StringBuilder();		
		
		char[] array = text.toCharArray();
		
		for(int i = 0; i < array.length; ++i) {
			
			if(array[i] == '\\'){
				sb.append("\\\\");
				continue;
			}
			
			if(array[i] == '{') {
				sb.append("\\{");
				continue;
			}
			
			sb.append(array[i]);
		}
				
		return sb.toString();
	}
	
	@Override
	public String toString() {
		return getText();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitTextNode(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof TextNode)) {
			return false;
		}
		TextNode other = (TextNode) obj;
		if (text == null) {
			if (other.text != null) {
				return false;
			}
		} else if (!text.equals(other.text)) {
			return false;
		}
		return true;
	}
}
