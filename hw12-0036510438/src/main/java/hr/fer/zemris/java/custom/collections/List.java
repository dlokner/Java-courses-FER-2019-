package hr.fer.zemris.java.custom.collections;

/**
 * Interface that implements basic methods to work with collection. 
 * 
 * @author Domagoj Lokner
 *
 */
public interface List extends Collection {
	
	/**
	 * Return object that is stored in collection at position {@code index}.
	 * 
	 * @param index - position of element in collection.
	 * @return element stored on {@code index} position.
	 */
	Object get(int index);
	
	/**
	 * Inserts the given element at given {@code position} into collection.
	 * 
	 * @param value - element to be inserted.
	 * @param position - index of inserting in collection.
	 */
	void insert(Object value, int position);
	
	/**
	 * Search the collection and returns index of the first occurrence of the given {@code value}.
	 * 
	 * @param value - value which is searched in the collection.
	 * @return index of the first occurrence of {@code value} or -1 if {@code value} is not found.
	 */
	int indexOf(Object value);
	
	/**
	 * Removes element at specified {@code index} from collection.
	 * 
	 * @param index - index of element to be removed.
	 * @throws IndexOutOfBoundsException if {@code index} is not in range [0, size-1].
	 */
	void remove(int index);
	
}
