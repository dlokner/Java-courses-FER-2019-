package hr.fer.zemris.java.custom.scripting.exec;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Object that stores stack of Object values by some key.
 * 
 * @author Domagoj Lokner
 *
 */
public class ObjectMultistack {
	
	/**
	 * Collection that store values.
	 */
	private Map<String, MultistackEntry> map;
	
	/**
	 * Constructs {@code ObjectMultistack}.
	 */
	public ObjectMultistack() {
		map = new HashMap<>();
	}

	/**
	 * Push wrapped value on stack with given key.
	 * 
	 * @param keyName - key by which value will be stored.
	 * @param valueWrapper - wrapped value.
	 */
	public void push(String keyName, ValueWrapper valueWrapper) {
		Objects.requireNonNull(keyName, "Key can not be null!");
		Objects.requireNonNull(valueWrapper, "Value can't be null!");
		
		MultistackEntry last = MultistackEntry.lastEntry(map.get(keyName));
		
		if(Objects.isNull(last)) {
			map.put(keyName, new MultistackEntry(valueWrapper));
		} else {
			last.next = new MultistackEntry(valueWrapper);
		}
	}
	
	/**
	 * Pop last value pushed on stack by given key.
	 * 
	 * @param keyName - key of value to be popped.
	 * @return popped value.
	 * @throws EmptyStackException if there is no elements stored by given key.
	 */
	public ValueWrapper pop(String keyName) {
		
		MultistackEntry firstEntry = map.get(keyName);
		
		if(Objects.isNull(firstEntry)) {
			throw new EmptyStackException();
		}
		
		if(Objects.isNull(firstEntry.next)) {
			return map.remove(keyName).value;
		}
		
		MultistackEntry penultimate = MultistackEntry.penultimateEntry(firstEntry);
		MultistackEntry last = penultimate.next;
		
		penultimate.next = null;
		
		return last.value;
	}
	
	/**
	 * Returns last value pushed on stack by given key.
	 * 
	 * @param keyName - key of peeked value.
	 * @return last value stored by given key.
	 * @throws EmptyStackException if there is no elements stored by given key.
	 */
	public ValueWrapper peek(String keyName) {
		Objects.requireNonNull(keyName);
		
		MultistackEntry firstEntry = map.get(keyName);
		
		if(Objects.isNull(firstEntry)) {
			throw new EmptyStackException();
		}
		
		return MultistackEntry.lastEntry(firstEntry).value;
	}
	
	/**
	 * Checks if collection contains elements stored by given key.
	 * 
	 * @param keyName - key of elements to checked.
	 * @return {@code true} if there is no values stored by given key or if null is given as key
	 * 		   otherwise {@code false}.
	 */
	public boolean isEmpty(String keyName) {
		if(Objects.isNull(keyName)) {
			return true;
		}
		
		MultistackEntry entry = map.get(keyName);
		
		if(Objects.isNull(entry)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Object that represent single stack element.
	 * 
	 * @author Domagoj Lokner
	 *
	 */
	private static class MultistackEntry {
		
		/**
		 * Value of entry.
		 */
		private ValueWrapper value;
		
		/**
		 * Reference on next entry stored in stack.
		 */
		private MultistackEntry next;

		/**
		 * Constructs new {@code MultistackEntry}.
		 * 
		 * @param value - value of entry.
		 */
		public MultistackEntry(ValueWrapper value) {
			this.value = value;
		}
		
		/**
		 * Returns last entry linked on given entry.
		 * 
		 * @param entry - starting entry.
		 * @return last entry on stack or {@code null} if {@code null} is given.
		 */
		private static MultistackEntry lastEntry(MultistackEntry entry) {

			MultistackEntry penultimate = penultimateEntry(entry);
			
			if(penultimate == null) {
				return entry;
			}
			
			return penultimate.next;
		}
		
		/**
		 * Returns entry stored before last entry in list.
		 * 
		 * @param entry - starting entry.
		 * @return entry before last on stack.
		 */
		private static MultistackEntry penultimateEntry(MultistackEntry entry) {			
			
			if(Objects.isNull(entry)) {
				return entry;
			}
			
			MultistackEntry previous = null;
			MultistackEntry current = entry;
			
			while(current.next != null) {
				 previous = current;
				 current = current.next;
			}
			
			return previous;
		}
	}
	

}
