package hr.fer.zemris.java.custom.scripting.nodes;

import java.util.Arrays;

import hr.fer.zemris.java.custom.scripting.elems.Element;

/**
 * {@code Node} representing a command which generates some textual output.
 * 
 * @author Domagoj Lokner
 *
 */
public class EchoNode extends Node {
	private Element[] elements;
	
	/**
	 * Constructs {@code EchoNode}.
	 * 
	 * @param elements - elements to be echoed.
	 */
	public EchoNode(Element[] elements) {
		this.elements = elements;
	}

	/**
	 * Gets {@code EchoNode} elements.
	 * 
	 * @return {@code EchoNode} elements.
	 */
	public Element[] getElements() {
		return elements;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("{$= ");
		
		for(int i = 0; i < elements.length; ++i) {
			sb.append(elements[i].asText() + " ");
		}
		
		sb.append("$}");
		
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(elements);
		return result;
	}

	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitEchoNode(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof EchoNode)) {
			return false;
		}
		EchoNode other = (EchoNode) obj;
		if (!Arrays.equals(elements, other.elements)) {
			return false;
		}
		return true;
	}
	
	
}
