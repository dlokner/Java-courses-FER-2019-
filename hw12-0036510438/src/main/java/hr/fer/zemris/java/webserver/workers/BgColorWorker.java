package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

import java.util.Objects;

/**
 * Worker used for changing background color in html script.
 *
 * @author Domagoj Lokner
 */
public class BgColorWorker implements IWebWorker {

    @Override
    public void processRequest(RequestContext context) throws Exception {
        String param = context.getParameter("bgcolor");
        String message = null;

        if (!Objects.isNull(param)) {
            if (isRGBColor(param)) {
                context.setPersistentParameter("bgcolor", param);
                message = "Color is updated!";
            }
        }

        if (Objects.isNull(message)) {
            message = "Color is not updated!";
        }

        context.setTemporaryParameter("message", message);
        context.getDispatcher().dispatchRequest("/index2.html");
        context.removeTemporaryParameter("message");
    }

    /**
     * Checks if given string represents hex rgb color.
     *
     * @param color string to be checked.
     * @return {@code true} if given string is rgb color otherwise {@code false}.
     */
    private boolean isRGBColor(String color) {
        if (color.length() != 6) {
            return false;
        }

        try {
            Integer.parseInt(color, 16);
        } catch (NumberFormatException ex) {
            return false;
        }

        return true;
    }
}
