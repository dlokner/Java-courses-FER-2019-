package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Represents operator expression element.
 * 
 * @author Domagoj Lokner
 *
 */
public class ElementOperator extends Element {
	private String symbol;
	
	/**
	 * Constructs {@code ElementOperator} representing {@code symbol}.
	 * 
	 * @param symbol - symbol of operator.
	 */
	public ElementOperator(String symbol) {
		this.symbol = symbol;
	}
	
	/**
	 * Returns operator symbol as string..
	 * 
	 * @return {@code symbol}.
	 */
	@Override
	public String asText() {
		return symbol;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ElementOperator)) {
			return false;
		}
		ElementOperator other = (ElementOperator) obj;
		if (symbol == null) {
			if (other.symbol != null) {
				return false;
			}
		} else if (!symbol.equals(other.symbol)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return asText();
	}
}
