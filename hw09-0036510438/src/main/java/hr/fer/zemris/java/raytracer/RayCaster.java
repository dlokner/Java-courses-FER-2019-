package hr.fer.zemris.java.raytracer;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.LightSource;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.RayIntersection;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * Program that generate picture of specified scene using ray tracing method and Phong lighting model.
 * 
 * @author Domagoj Lokner
 *
 */
public class RayCaster {

	/**
	 * RGB components of ambient ("color of objects").
	 */
	private final static short[] RGB_AMBIEN = new short[] {15, 15, 15};
	
	/**
	 * Precision in which will be determined if a light source illuminates an object at particular point
	 */
	private final static double PRECISION = 1e-6;
	
	
	/**
	 * Starting method of the program.
	 * 
	 * @param args - command line arguments.
	 */
	public static void main(String[] args) {
		RayTracerViewer.show(getIRayTracerProducer(), 
				new Point3D(10, 0, 0), 
				new Point3D(0, 0, 0),
				new Point3D(0, 0, 10),
				20, 20);
	}

	/**
	 * Creates new object that implements {@code IRayTracerProducer} interface.
	 * 
	 * @return created object as {@code IRayTracerProducer}.
	 * @see IRayTracerProducer
	 */
	private static IRayTracerProducer getIRayTracerProducer() {

		return new IRayTracerProducer() {

			@Override
			public void produce(Point3D eye, Point3D view, Point3D viewUp, double horizontal, double vertical,
					int width, int height, long requestNo, IRayTracerResultObserver observer, AtomicBoolean cancel) {

				System.out.println("Započinjem izračune...");
				short[] red = new short[width * height];
				short[] green = new short[width * height];
				short[] blue = new short[width * height];

				Point3D zAxis = view.sub(eye)
						.normalize();
				Point3D yAxis = viewUp.normalize()
						.sub(zAxis.scalarMultiply(zAxis.scalarProduct(viewUp.normalize())));
				Point3D xAxis = zAxis.vectorProduct(yAxis)
						.normalize();

				Point3D screenCorner = view
						.sub(xAxis.scalarMultiply(horizontal / 2))
						.add(yAxis.scalarMultiply(vertical / 2));

				Scene scene = RayTracerViewer.createPredefinedScene();

				short[] rgb = new short[3];
				int offset = 0;

				for (int y = 0; y < height; y++) {
					for (int x = 0; x < width; x++) {

						Point3D screenPoint = screenCorner
								.add(xAxis.scalarMultiply((horizontal * x) / (width - 1)))
								.sub(yAxis.scalarMultiply((vertical * y) / (height - 1)));

						Ray ray = Ray.fromPoints(eye, screenPoint);

						tracer(scene, ray, rgb, eye);

						red[offset] = rgb[0] > 255 ? 255 : rgb[0];
						green[offset] = rgb[1] > 255 ? 255 : rgb[1];
						blue[offset] = rgb[2] > 255 ? 255 : rgb[2];

						offset++;
					}
				}

				System.out.println("Izračuni gotovi...");
				observer.acceptResult(red, green, blue, requestNo);
				System.out.println("Dojava gotova...");
			}

		};
	}
	
	/**
	 * Traces object intersecting given ray at given scene and generate its color.
	 * 
	 * @param scene - scene in which objects will be searched.
	 * @param ray - ray representing view direction.
	 * @param rgb - array in which generated colors will be stored.
	 * @param eye - point of view.
	 */
	protected static void tracer(Scene scene, Ray ray, short[] rgb, Point3D eye) {
		rgb[0] = 0;
		rgb[1] = 0;
		rgb[2] = 0;
		
		RayIntersection closest = findClosestIntersection(scene, ray);
		
		if (closest.isOuter()) {
			return;
		}
		
		determinateColor(scene, closest, rgb, eye);
	}
	
	/**
	 * Determinate color in given intersection depending on all lights in scene.
	 * 
	 * @param scene - scene where lights and objects are placed.
	 * @param intersection - ray and object intersection point.
	 * @param rgb - array in which generated colors will be stored.
	 * @param eye - point of view.
	 */
	private static void determinateColor(Scene scene, RayIntersection intersection, short[] rgb, Point3D eye) {
		
		rgb[0] = RGB_AMBIEN[0];
		rgb[1] = RGB_AMBIEN[1];
		rgb[2] = RGB_AMBIEN[2];
		
		for(LightSource ls : scene.getLights()) {
			
			Ray lightToObject = Ray.fromPoints(ls.getPoint(), intersection.getPoint());
			RayIntersection lsIntersection = findClosestIntersection(scene, lightToObject);
			
			if(!Objects.isNull(lsIntersection)){
				double objectLightDistance = ls.getPoint().sub(intersection.getPoint()).norm();
				double lightIntersectionDistance  = lsIntersection.getDistance();
				
				if(!areEquals(lightIntersectionDistance, objectLightDistance, PRECISION)) {
					continue;
				}
			}
			
			Point3D rayNegative = lightToObject.direction.negate();
			Point3D normal = intersection.getNormal();
			Point3D rayReflection = normal
					.scalarMultiply(-2 * rayNegative.scalarProduct(normal))
					.add(rayNegative)
					.normalize();
			Point3D rayToEye = eye.sub(intersection.getPoint()).negate().normalize();
			
			setColors(ls, rgb, rayNegative, rayReflection, normal, rayToEye, intersection);
		}
	}

	
	/**
	 * Finds ray's intersection with closest object on scene.
	 * 
	 * @param scene - scene in which objects are placed.
	 * @param ray - ray.
	 * @return closest ray intersection.
	 */
	private static RayIntersection findClosestIntersection(Scene scene, Ray ray) {
		RayIntersection closest = null;
		
		for(GraphicalObject object : scene.getObjects()) {
			
			RayIntersection intersection = object.findClosestRayIntersection(ray);
			if(intersection != null) {
				if(closest == null) {
					closest = intersection;
				} else {
					if(intersection.getDistance() < closest.getDistance()) {
						closest = intersection;
					}
				}
			}
		}
		
		return closest;
	}
	
	/**
	 * Checks if given double numbers are equal.
	 * 
	 * @param d1 - first number.
	 * @param d2 - second number.
	 * @param precision - precision in which equality will be checked.
	 * @return
	 */
	private static boolean areEquals(double d1, double d2, double precision) {
		if(Math.abs(d1 - d2) > precision) {
			return false;
		}
		return true;
	}
	
	/**
	 * Sets colors at given intersection point using Phong lighting model to generate colors.
	 * 
	 * @param ls - light source.
	 * @param rgb - array in which result will be stored.
	 * @param rayNeg - normalized light ray in opposite direction.
	 * @param rayRef - normalized reflection ray.
	 * @param normal - normal to surface of an object.
	 * @param intersection - ray and object intersection point.
	 */
	private static void setColors(LightSource ls, short[] rgb, Point3D rayNeg, Point3D rayRef, 
			Point3D normal, Point3D rayToEye, RayIntersection intersection) {

		double difusion = Math.max(rayNeg.scalarProduct(normal), 0);
		double reflection = Math.pow(Math.max(rayRef.scalarProduct(rayToEye), 0), intersection.getKrn());

		
		rgb[0] += ls.getR()*(difusion*intersection.getKdr() + reflection*intersection.getKrr());
		rgb[1] += ls.getG()*(difusion*intersection.getKdg() + reflection*intersection.getKrg());
		rgb[2] += ls.getB()*(difusion*intersection.getKdb() + reflection*intersection.getKrb());
	}

}
