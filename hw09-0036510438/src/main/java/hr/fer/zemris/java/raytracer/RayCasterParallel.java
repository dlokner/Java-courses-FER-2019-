package hr.fer.zemris.java.raytracer;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * Program that generate picture of specified scene using ray tracing method and Phong lighting model.<br>
 * Program uses recursive method of parallelization.
 * 
 * @author Domagoj Lokner
 *
 */
public class RayCasterParallel {

	/**
	 * Threshold that determinate maximum number of frame rows single process will process.
	 */
	public static final int THRESHOLD = 16;

	/**
	 * Starting method of the program.
	 * 
	 * @param args - command line arguments.
	 */
	public static void main(String[] args) {
		RayTracerViewer.show(new RayCasterProducerParallel(), new Point3D(10, 0, 0), new Point3D(0, 0, 0),
				new Point3D(0, 0, 10), 20, 20);
	}

	/**
	 * Concrete implementation of {@code IRayTracerProducer} that 
	 * executes {@code RayCasterCompute} objects parallelized by {@code ForkJoinPool}.
	 *  
	 * @author Domagoj Lokner
	 * @see {@link RayCasterCompute}, {@link ForkJoinPool}
	 *
	 */
	public static class RayCasterProducerParallel implements IRayTracerProducer {

		@Override
		public void produce(Point3D eye, Point3D view, Point3D viewUp, double horizontal, double vertical, int width,
				int height, long requestNo, IRayTracerResultObserver observer, AtomicBoolean cancel) {

			System.out.println("Zapocinjem izracun...");

			short[] red = new short[width * height];
			short[] green = new short[width * height];
			short[] blue = new short[width * height];

			Point3D zAxis = view.sub(eye).normalize();
			Point3D yAxis = viewUp.normalize().sub(zAxis.scalarMultiply(zAxis.scalarProduct(viewUp.normalize())));
			Point3D xAxis = zAxis.vectorProduct(yAxis).normalize();

			Point3D screenCorner = view.sub(xAxis.scalarMultiply(horizontal / 2))
					.add(yAxis.scalarMultiply(vertical / 2));

			Scene scene = RayTracerViewer.createPredefinedScene();

			ForkJoinPool pool = new ForkJoinPool();
			pool.invoke(new RayCasterCompute(red, green, blue, scene, eye, horizontal, vertical, width, height, 0,
					height - 1, xAxis, yAxis, screenCorner, cancel));
			pool.shutdown();

			System.out.println("Racunanje gotovo. Idem obavijestiti promatraca tj. GUI!");
			observer.acceptResult(red, green, blue, requestNo);

		}
	}


	/**
	 * Compute scene snapshots using ray-tracing technique.
	 * Process will for until number of rows that process 
	 * must compute is lower of specified threshold. 
	 * 
	 * @author Domagoj Lokner
	 *
	 */
	public static class RayCasterCompute extends RecursiveAction {

		
		private static final long serialVersionUID = 7680927574269799222L;

		/**
		 * Array stores shades of red color.
		 */
		private short[] red;
		
		/**
		 * Array stores shades of green color.
		 */
		private short[] green;
		
		/**
		 * Array stores shades of blue color.
		 */
		private short[] blue;
		
		/**
		 * Scene for which snapshot will be generated. 
		 */
		private Scene scene;
		
		/**
		 * Position of human observer.
		 */
		private Point3D eye;
		
		/**
		 * Horizontal width of observed space.
		 */
		private double horizontal;
		
		/**
		 * Vertical height of observed space.
		 */
		private double vertical;
		
		/**
		 * Number of pixels per screen row.
		 */
		private int width;
		
		/**
		 * Number of pixel per screen column.
		 */
		private int height;
		
		/**
		 * Minimum y value.
		 */
		private int yMin;
		
		/**
		 * Maximum y value.
		 */
		private int yMax;
		
		/**
		 * x axis unit vector.
		 */
		private Point3D xAxis;
		
		/**
		 * y axis unit vector.
		 */
		private Point3D yAxis;
		
		/**
		 * Point placed in upper-left corner.
		 */
		private Point3D screenCorner;
		
		/**
		 * Flag that indicates if calculation can be canceled.
		 */
		private AtomicBoolean cancel;

		
		/**
		 * Constructs {@code RayCasterCompute}.
		 * 
		 * @param red - array stores shades of red color.
		 * @param green - array stores shades of green color.
		 * @param blue - array stores shades of blue color.
		 * @param scene - scene for which snapshot will be generated. 
		 * @param eye - position of human observer.
		 * @param horizontal - horizontal width of observed space.
		 * @param vertical - vertical height of observed space.
		 * @param width - number of pixels per screen row.
		 * @param height - number of pixel per screen column.
		 * @param yMin - minimum y value.
		 * @param yMax - maximum y value.
		 * @param xAxis - x axis unit vector.
		 * @param yAxis - y axis unit vector.
		 * @param screenCorner - point placed in upper-left corner.
		 * @param cancel - flag that indicates if calculation can be canceled.
		 */
		public RayCasterCompute(short[] red, short[] green, short[] blue, Scene scene, Point3D eye, 
				double horizontal, double vertical, int width, int height, int yMin, int yMax, 
				Point3D xAxis, Point3D yAxis, Point3D screenCorner, AtomicBoolean cancel) {
			
			this.red = red;
			this.green = green;
			this.blue = blue;
			this.scene = scene;
			this.eye = eye;
			this.horizontal = horizontal;
			this.vertical = vertical;
			this.width = width;
			this.height = height;
			this.yMin = yMin;
			this.yMax = yMax;
			this.xAxis = xAxis;
			this.yAxis = yAxis;
			this.screenCorner = screenCorner;
			this.cancel = cancel;
		}

		@Override
		public void compute() {
			if(yMax-yMin < THRESHOLD) {
				computeDirect();
				return;
			}
			
			invokeAll(
				new RayCasterCompute(red, green, blue, scene, eye, horizontal, vertical, width, height, 
						yMin, yMin+(yMax-yMin)/2, xAxis, yAxis, screenCorner, cancel),
				new RayCasterCompute(red, green, blue, scene, eye, horizontal, vertical, width, height, 
						yMin+(yMax-yMin)/2+1, yMax, xAxis, yAxis, screenCorner, cancel)
			);	
		}
		
		/**
		 * Compute color of scene snapshot using ray-tracing technique and Phong lighting model. 
		 */
		public void computeDirect() {
			for (int y = yMin; y <= yMax; y++) {
				for (int x = 0; x < width; x++) {

					if(cancel.get()){
						return;
					}
					
					Point3D screenPoint = screenCorner
							.add(xAxis.scalarMultiply((horizontal * x) / (width - 1)))
							.sub(yAxis.scalarMultiply((vertical * y) / (height - 1)));

					Ray ray = Ray.fromPoints(eye, screenPoint);

					short[] rgb = new short[3];
					
					RayCaster.tracer(scene, ray, rgb, eye);

					int offset = y * width + x;
					
					red[offset] = rgb[0] > 255 ? 255 : rgb[0];
					green[offset] = rgb[1] > 255 ? 255 : rgb[1];
					blue[offset] = rgb[2] > 255 ? 255 : rgb[2];

					offset++;
				}
			}
		}
	}
}
