package hr.fer.zemris.java.raytracer.model;

import java.util.Objects;

/**
 * Object models sphere as {@code GraphicalObject}.
 * 
 * @author Domagoj Lokner
 *
 */
public class Sphere extends GraphicalObject {

	/**
	 * Center of the sphere.
	 */
	private Point3D center;
	
	/**
	 * Radius of the sphere.
	 */
	private double radius; 
	
	/**
	 * Diffusion coefficient for red color.
	 */
	private double kdr;
	
	/**
	 * Diffusion coefficient for red green.
	 */
	private double kdg;
	
	/**
	 * Diffusion coefficient for red blue.
	 */
	private double kdb; 
	
	/**
	 * Reflection coefficient for red color.
	 */
	private double krr; 
	
	/**
	 * Reflection coefficient for green color.
	 */
	private double krg; 
	
	/**
	 * Reflection coefficient for blue color.
	 */
	private double krb; 
	
	/**
	 * Reflection coefficient.
	 */
	private double krn;
	


	/**
	 * Constructs {@code Sphere} object.
	 * 
	 * @param center - center of the sphere.
	 * @param radius - radius of the sphere.
	 * @param kdr - diffusion coefficient for red color.
	 * @param kdg - diffusion coefficient for green color.
	 * @param kdb - diffusion coefficient for blue color.
	 * @param krr - reflection coefficient for red color.
	 * @param krg - reflection coefficient for green color.
	 * @param krb - reflection coefficient for blue color.
	 * @param krn - reflection coefficient.
	 */
	public Sphere(Point3D center, double radius, 
			double kdr, double kdg, double kdb, 
			double krr, double krg, double krb,
			double krn) {
		
		this.center = center;
		this.radius = radius;
		this.kdr = kdr;
		this.kdg = kdg;
		this.kdb = kdb;
		this.krr = krr;
		this.krg = krg;
		this.krb = krb;
		this.krn = krn;
	}



	@Override
	public RayIntersection findClosestRayIntersection(Ray ray) {
		
		boolean outer = false;
		double distance = Double.MAX_VALUE;
		
		Point3D intersectionPoint = findIntersection(ray);
		
		if(Objects.isNull(intersectionPoint)) {
			outer = true;
		} else {
			distance = intersectionPoint.difference(intersectionPoint, ray.start).norm();
		}
		
		return new RayIntersection(intersectionPoint, distance, outer) {	
			
			@Override
			public Point3D getNormal() {				
				Point3D normal = new Point3D(
						(intersectionPoint.x - center.x) / radius,
						(intersectionPoint.y - center.y) / radius,
						(intersectionPoint.z - center.z) / radius
				);
				
				return normal.normalize();
			}
			
			@Override
			public double getKrr() {
				return krr;
			}
			
			@Override
			public double getKrn() {
				return krn;
			}
			
			@Override
			public double getKrg() {
				return krg;
			}
			
			@Override
			public double getKrb() {
				return krb;
			}
			
			@Override
			public double getKdr() {
				return kdr;
			}
			
			@Override
			public double getKdg() {
				return kdg;
			}
			
			@Override
			public double getKdb() {
				return kdb;
			}
		};
	}

	/**
	 * Finds first intersection of given ray and {@code sphere} sphere. 
	 * 
	 * @param ray - ray to be checked for intersection.
	 * @return point of intersection or {@code null} if ray do not intersect sphere.
	 */
	private Point3D findIntersection(Ray ray) {
		
		double a = 1d;
		double b = ray.direction.scalarProduct(ray.start.sub(center)) * 2;
		
		double tmp = ray.start.sub(center).norm();
		double c = tmp*tmp - radius*radius;
		
		double D = b*b - 4*a*c;
		
		if(D < 0) {
			return null;
		}
	
		double t1 = (-b - Math.sqrt(D)) / 2*a;
		double t2 = (-b + Math.sqrt(D)) / 2*a;
		
		//both points are behind ray origin
		if(t2 < 0) {
			return null;
		}

		//chose first point in front of ray origin
		double t = t1 < 0d ? t2 : t1;
		
		return ray.start.add(ray.direction.scalarMultiply(t));
	}

}
