package hr.fer.zemris.java.fractals;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * Program that displays Newton-Raphson fractal for given polynomial.<br>
 * User will be asked to enter polynomial roots through standard input.
 * 
 * @author Domagoj Lokner
 *
 */
public class Newton {

	/**
	 * Starting method of the program.
	 * 
	 * @param args - command line arguments.
	 */
	public static void main(String[] args) {
		
		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.");
		
		
		Scanner sc = new Scanner(System.in);
			
		ComplexRootedPolynomial polynomial = new ComplexRootedPolynomial(
				Complex.ONE, readRoots(sc).toArray(new Complex[2]));
		
		sc.close();
		
		FractalViewer.show(new NewtonProducer(polynomial));
		
		System.out.println("Image of fractal will appear shortly. Thank you.");
		

	}
	
	/**
	 * Reads polynomial roots from given {@code Scanner} object.
	 * User will be asked to enter roots until "done" is entered. 
	 * 
	 * @param sc - scanner from which roots will be read.
	 * @return collection of polynomial roots.
	 */
	private static List<Complex> readRoots(Scanner sc){
		List<Complex> roots = new LinkedList<>();
		
		System.out.println("Please enter at least two roots, one root per line. Enter 'done' when done.");
		
		int i = 1;
		
		while(true) {
			
			System.out.format("Root %d> ", i);
			
			String root = sc.nextLine();
			
			if(root.equalsIgnoreCase("done")) {
				if(i < 3) {
					System.out.println("At least two roots must be entered!");
					continue;
				}
				break;
			}
			
			try {
				roots.add(Complex.parse(root));
			} catch (NumberFormatException ex) {
				System.out.println("Given expression can't be interpreated as complex number!");
				continue;
			}
			++i;
		}
		
		return roots;
	}

}
