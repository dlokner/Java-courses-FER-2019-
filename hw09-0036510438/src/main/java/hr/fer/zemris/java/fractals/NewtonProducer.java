package hr.fer.zemris.java.fractals;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * Concrete implementation of {@code IFractalProducer}.<br>
 * Generate objects needed for displaying Newton-Raphson fractal.
 * 
 * @author Domagoj Lokner
 *
 */
public class NewtonProducer implements IFractalProducer {
	
	/**
	 * Maximal number of iterations.
	 */
	private static final short m = 16*16*16; 
	
	/**
	 * Polynomial for which fractal will be computed.
	 */
	private ComplexRootedPolynomial polynomial;

	/**
	 * Constructs {@code NewtonProducer}.
	 * 
	 * @param polynomial - polynomial for which fractal will be computed.
	 */
	public NewtonProducer(ComplexRootedPolynomial polynomial) {
		Objects.requireNonNull(polynomial);
		
		this.polynomial = polynomial;
	}


	@Override
	public void produce(double reMin, double reMax, double imMin, double imMax, 
			int width, int height, long requestNo, IFractalResultObserver observer, AtomicBoolean cancel) {
		
		System.out.println("Započinjem izračun...");
		
		short[] data = new short[width * height];
		int availableProcessors = Runtime.getRuntime().availableProcessors();
		final int tracks = 8 * availableProcessors;
		int yPerTrack = height / tracks;
		
		ExecutorService pool = Executors.newFixedThreadPool(availableProcessors, (r) -> { 
			Thread t = new Thread(r);
			t.setDaemon(true);
			return t;
		});
		
		List<Future<Void>> results = new ArrayList<>();
		
		for(int i = 0; i < tracks; ++i) {
			
			int yMin = i * yPerTrack;
			int yMax = (i + 1) * yPerTrack - 1;
			
			if(i == tracks-1) {
				yMax = height-1;
			}
			
			NewtonCalculate job = new NewtonCalculate(reMin, reMax, imMin, imMax, 
					width, height, yMin, yMax, m, data, polynomial, cancel);
			results.add(pool.submit(job));
		}
		
		for(Future<Void> job : results) {
			try {
				job.get();
			} catch (InterruptedException | ExecutionException e) {
			}
		}
		
		pool.shutdown();
		
		System.out.println("Racunanje gotovo. Idem obavijestiti promatraca tj. GUI!");
		observer.acceptResult(data, (short)(polynomial.toComplexPolynom().order()+1), requestNo);
	}
}
