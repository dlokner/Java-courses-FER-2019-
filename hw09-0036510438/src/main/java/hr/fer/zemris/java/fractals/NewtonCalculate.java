package hr.fer.zemris.java.fractals;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * Calculate results for {@code NewtonProducer}.
 * @see NewtonProducer
 * 
 * @author Domagoj Lokner
 *
 */
public class NewtonCalculate implements Callable<Void> {
	
	private static final double CONVERGENCE_THRESHOLD = 1e-3;
	private static final double ROOT_THRESHOLD = 2e-3;

	private double reMin;
	private double reMax;
	private double imMin;
	private double imMax;

	private int width;
	private int height;

	private int yMin;
	private int yMax;

	/**
	 * Maximal number of iterations for checking convergence.
	 */
	private int m;

	/**
	 * Array in which results will be stored.
	 */
	private short[] data;
	
	/**
	 * Polynomial for which fractal will be computed.
	 */
	private ComplexRootedPolynomial polynomial;
	
	/**
	 * 
	 */
	AtomicBoolean cancel;

	/**
	 * Constructs {@code NewtonCalculate}.
	 * 
	 * @param reMin - minimal value of real part of complex number.
	 * @param reMax - maximal value of real part of complex number.
	 * @param imMin - minimal value of imaginary part of complex number.
	 * @param imMax - maximal value of imaginary part of complex number.
	 * @param width - width of raster to which fractal will be displayed.
	 * @param height - height of raster to which fractal will be displayed.
	 * @param yMin - minimal value of y.
	 * @param yMax - maximal value of y.
	 * @param m - maximal number of iterations for checking convergence.
	 * @param data - array in which results will be stored.
	 * @param polynomial - polynomial for which fractal will be computed.
	 * @param cancel - signals if operation can be canceled before it is done.
	 */
	public NewtonCalculate(double reMin, double reMax, double imMin, double imMax, int width, int height, int yMin,
			int yMax, int m, short[] data, ComplexRootedPolynomial polynomial, AtomicBoolean cancel) {

		this.reMin = reMin;
		this.reMax = reMax;
		this.imMin = imMin;
		this.imMax = imMax;
		this.width = width;
		this.height = height;
		this.yMin = yMin;
		this.yMax = yMax;
		this.m = m;
		this.data = data;
		this.polynomial = polynomial;
		this.cancel = cancel;
	}

	@Override
	public Void call() throws Exception {
		
		for(int y = yMin; y <= yMax ; ++y) {
			for(int x = 0; x < width; ++x) {
				
				Complex current = mapToComplexPlain(x, y);
				Complex previous;
								
				int i = 0;
				
				do {
					if(cancel.get()) {
						return null;
					}
					
					ComplexPolynomial compPolynomial = polynomial.toComplexPolynom();
					previous = current;
					
					current = current.sub(
							polynomial.apply(current).divide(compPolynomial.derive().apply(current)));
					
					++i;
					
				} while(current.sub(previous).module() > CONVERGENCE_THRESHOLD && i < m);
				
				int index = polynomial.indexOfClosestRootFor(current, ROOT_THRESHOLD);
				
				data[computeOffset(x, y)] = (short)(index + 1);
				
			}
		}
		
		
		return null;
	}

	/**
	 * Compute index in {@code data} array in which result will be stored for given x and y.
	 * 
	 * @param x - x coordinate on raster.
	 * @param y - y coordinate on raster.
	 * @return offset.
	 */
	private int computeOffset(int x, int y) {
		return width * y + x;
	}

	/**
	 * Map coordinate on raster to complex number.
	 * 
	 * @param x - x coordinate on raster.
	 * @param y - y coordinate on raster.
	 * @return mapped complex number for given coordinates.
	 */
	private Complex mapToComplexPlain(int x, int y) {
		
		double re = x * (reMax - reMin) / (width - 1) + reMin;
		double im = (height - 1  - y) * (imMax - imMin) / (height - 1) + imMin;
		
		return new Complex(re, im);
		
	}



}
