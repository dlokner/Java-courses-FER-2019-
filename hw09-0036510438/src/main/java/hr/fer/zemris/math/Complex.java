package hr.fer.zemris.math;

import java.util.LinkedList;
import java.util.List;

/**
 * Objects of this class represents immutable complex numbers.
 * 
 * @author Domagoj Lokner
 *
 */
public class Complex {
	
	public static final Complex ZERO = new Complex(0,0);
	public static final Complex ONE = new Complex(1,0);
	public static final Complex ONE_NEG = new Complex(-1,0);
	public static final Complex IM = new Complex(0,1);
	public static final Complex IM_NEG = new Complex(0,-1);
	
	/**
	 * Real part of complex number.
	 */
	private double re;
	
	/**
	 * Imaginary part of complex number. 
	 */
	private double im;
	
	
	public Complex() {
	}
	
	/**
	 * Constructs complex number with real part {@code re} and imaginary part {@code im}.
	 * 
	 * @param re - real part of complex number.
	 * @param im - imaginary part of complex number. 
	 */
	public Complex(double re, double im) {
		this.re = re;
		this.im = im;
	}
	
	/**
	 * Calculate module of complex number.
	 * 
	 * @return module of complex number.
	 */
	public double module() {
		return Math.sqrt(re*re + im*im);
	}
	
	/**
	 * Multiplication by given complex number.
	 * 
	 * @param c - multiplier. 
	 * @return new {@code ComplexNumber} initialized on multiplication of this number and given argument.
	 */
	public Complex multiply(Complex c) {
		return new Complex(re*c.re - im*c.im, re*c.im + c.re*im);
	}

	/**
	 * Divides by given complex number.
	 * 
	 * @param c - divisor.
	 * @return new {@code ComplexNumber} initialized on result of division.
	 */
	public Complex divide(Complex c) {
		double thisMag = getMagnitude();
		double thisAng = getAngle();
		
		double cMag = c.getMagnitude();
		double cAng = c.getAngle();
		
		double real = (thisMag/cMag)*Math.cos(thisAng-cAng);
		double imaginary = (thisMag/cMag)*Math.sin(thisAng-cAng);
		return new Complex(real, imaginary);
	}
	
	/**
	 * Addition by given complex number.
	 * 
	 * @param c - {@code ComplexNumber} to be added.
	 * @return new {@code Complex} initialized on sum of this number and given argument.
	 */
	public Complex add(Complex c) {
		return new Complex(this.re + c.re, this.im + c.im);
	}
	
	/**
	 * Subtraction by given complex number.
	 * 
	 * @param c - subtrahend.
	 * @return new {@code Complex} initialized on subtraction of this number and given argument.
	 */
	public Complex sub(Complex c) {
		return new Complex(this.re - c.re, this.im - c.im);
	}
	
	/**
	 * Returns this complex number negative
	 * 
	 * @return negative complex number.
	 */
	public Complex negate() {
		return new Complex(-re, -im);
	}
	
	/**
	 * Powers by given {@code n}.
	 * 
	 * @param n - exponent.
	 * @return new {@code Complex} powered by {@code n}.
	 * @throws IllegalArgumentException if n is 
	 */
	public Complex power(int n) {
		if(n < 0) {
			throw new IllegalArgumentException();
		}
		
		double magnitude = getMagnitude();
		double angle = getAngle();
		
		double real = Math.pow(magnitude, n)*Math.cos(n*angle);
		double imaginary = Math.pow(magnitude, n)*Math.sin(n*angle);
		
		return new Complex(real, imaginary);
	}
	
	/**
	 * Return list of {@code n}-th roots of this complex number.
	 * 
	 * @param n - degree.
	 * @return Array of references on {@code ComplexNumber}.
	 * @throws IllegalArgumentException if degree is equal or less to 0.
	 */
	public List<Complex> root(int n) {
		if(n <= 0) {
			throw new IllegalArgumentException();
		}
		
		List<Complex> result = new LinkedList<>();
		
		double magnitude = getMagnitude();
		double angle = getAngle();
		
		for(int i = 0; i < n; ++i) {
			double real = Math.pow(magnitude, 1d/n)*Math.cos((angle + 2*i*Math.PI)/n);
			double imaginary = Math.pow(magnitude, 1d/n)*Math.sin((angle + 2*i*Math.PI)/n);
			
			result.add(new Complex(real, imaginary));
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append('(');

		sb.append(re);
		
		if(!(im < 0d)) {
			sb.append('+');
		} else {
			sb.append('-');
		}
		sb.append('i');
		sb.append(Math.abs(im));
		
		sb.append(')');
		
		return sb.toString();
	}
	
	/**
	 * Calculates angle of complex number.
	 * 
	 * @return angle of complex number in range [0, 2*PI].
	 */
	private double getAngle() {
		return im < 0 ? Math.atan2(im, re) + Math.PI * 2 : Math.atan2(im, re);
	}
	
	/**
	 * Calculates magnitude of complex number.
	 * 
	 * @return magnitude of complex number.
	 */
	private double getMagnitude() {
		return Math.sqrt(re * re + im * im);
	}
	
	/**
	 * Returns new {@code Complex} initialized to the values represented
	 * by {@code String s}.<br>
	 * (imaginary unit "<i>i</i>" is expected before imaginary part of number e.g. "13 - i15")
	 * 
	 * @param s - string to be parsed.
	 * @return {@code Complex} object represented by the string argument.
	 * @throws NumberFormatException if string argument can not be parsed to {@code ComplexNumber}.
	 */
	public static Complex parse(String s) {
		String reS = "", imS = "";
		double re, im;

		String[] numbers = s.split("[+-]+");
		String[] signs = s.split("[^+-]+");
		
		for(int i = 0; i < numbers.length; ++i) {
			numbers[i] = numbers[i].strip();
		}

		int numLen = numbers.length - 1;
		int sigLen = signs.length - 1;
		
		/*throw exception if there are two signs (+,-) together*/
		for(String signe : signs) {
			if(signe.length() > 1) {
				throw new NumberFormatException();
			}
		}
		
		/*exception is thrown if stri,g endsWith '+' or '-'*/
		if(s.endsWith("-") || s.endsWith("+")) {
			throw new NumberFormatException();
		}
		
		try {
			if (s.contains("i")) {
				
				imS = numbers[numLen].substring(1, numbers[numLen].length());
				imS = imS.isEmpty() ? "1" : imS;
				
				if(sigLen != -1) {
					imS = signs[sigLen].contains("-") ? signs[sigLen] + imS : imS;
				}
				
				im = Double.parseDouble(imS);
				
				if(numLen > 0 && !numbers[numLen-1].isEmpty()) {
					reS = signs[sigLen-1].contains("-") ? signs[sigLen-1] + numbers[numLen-1] : numbers[numLen-1];
					re = Double.parseDouble(reS);
				} else {
					re = 0d;
				}
			} else {
				im = 0d;
				
				reS = numbers[numLen];
				
				if(sigLen != -1) {
					reS = signs[sigLen].contains("-") ? signs[sigLen] + reS : reS;
				}
				
				re = Double.parseDouble(reS);
			}
			
			return new Complex(re, im);
		} catch (IndexOutOfBoundsException | NumberFormatException ex2) {
			throw new NumberFormatException();
		}
		
		
	}

	/**
	 * Gets real part of complex number.
	 * 
	 * @return real part.
	 */
	public double getRe() {
		return re;
	}

	/**
	 * Gets imaginary part of complex number.
	 * 
	 * @return imaginary part.
	 */
	public double getIm() {
		return im;
	}
}
