package hr.fer.zemris.math;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Objects represents polynomial of complex numbers<br>
 * Form of polynomial is - f(z) = z_n * z^n + z_n-1 * z^n + ... + z_1 * z + z_0
 * 
 * @author Domagoj Lokner
 *
 */
public class ComplexPolynomial {
	
	/**
	 * Polynomial factors
	 */
	private Complex[] factors;
	
	/**
	 * Constructs new {@code ComplexPolynomial} with given 
	 * factors ordered ascending (from z_0 to z_n).
	 * 
	 * @param factors - polynomial factors.
	 */
	public ComplexPolynomial(Complex ...factors) {
		this.factors = Arrays.copyOf(factors, factors.length);
	}
	
	/**
	 * Returns order of this polynomial<br>
	 * (e.g. For (7+2i)z^3+2z^2+5z+1 returns 3)
	 * 
	 * @return order of polynomial.
	 */
	public short order() {
		return (short)(factors.length - 1);
	}
	
	/**
	 * Computes a new polynomial as result of multiplying {@code this} and given polynomial.
	 * 
	 * @param p - multiplier.
	 * @return result of multiplication.
	 */
	public ComplexPolynomial multiply(ComplexPolynomial p) {

		List<ComplexPolynomial> addends = new LinkedList<>();
		
		for(int i = p.factors.length - 1; i >= 0; --i) {
			addends.add(this.multiplyFactor(p.factors[i], i));
		}
		
		return sumPolynomials(addends);
		
	}
	
	/**
	 * Compute a new polynomial as result of multiplying {@code this} and given complex number.
	 * 
	 * @param factor - multiplier.
	 * @param order - order of factor in polynomial.
	 * @return result of multiplication.
	 */
	private ComplexPolynomial multiplyFactor(Complex factor, int order) {
		Complex[] result = new Complex[factors.length + order];
		
		for(int i = factors.length - 1; i >= 0; --i) {
			result[i + order] = factors[i].multiply(factor);
		}
		
		for(int i = 0; i < order; ++i) {
			result[i] = Complex.ZERO;
		}
		
		return new ComplexPolynomial(result);
	}
	
	/**
	 * Computes sum of given polynomials.
	 * 
	 * @param addends - collection of polynomials to be summed.
	 * @return sum of polynomials.
	 */
	private static ComplexPolynomial sumPolynomials(List<ComplexPolynomial> addends) {
		addends.sort((p1, p2) -> Short.compare(p2.order(), p1.order()));
		
		Complex[] result = Arrays.copyOf(addends.get(0).factors, addends.remove(0).factors.length);
		
		for(ComplexPolynomial addend : addends) {
			for(int i = 0, n = addend.factors.length; i < n; ++i) {
				result[i] = result[i].add(addend.factors[i]);
			}
		}
		
		return new ComplexPolynomial(result);
	}
	
	// computes first derivative of this polynomial; for example, for
	// (7+2i)z^3+2z^2+5z+1 returns (21+6i)z^2+4z+5
	/**
	 * Computes first derivative of this polynomial.
	 * 
	 * @return first derive of {@code this} polynomial.
	 */
	public ComplexPolynomial derive() {
		Complex[] derive = new Complex[factors.length-1];
		
		for(int i = factors.length-1; i > 0; --i) {
			derive[i-1] = factors[i].multiply(new Complex(i, 0));
		}
		
		return new ComplexPolynomial(derive);
	}
	

	/**
	 * Computes polynomial value at given point.
	 * 
	 * @param z - point in which polynomial value will be computed.
	 * @return polynomial value in given point.
	 */
	public Complex apply(Complex z) {
		
		Complex result = Complex.ZERO;
		
		for(int i = factors.length - 1; i >= 0; --i) {
			result = result.add(factors[i].multiply(z.power(i)));
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for(int i = factors.length - 1; i >= 0; --i) {
			sb.append(factors[i] + "*");
			
			if(i == 1) {
				sb.append("z+");
				continue;
			}
			
			if(i == 0) {
				sb.delete(sb.length()-1, sb.length());
				break;
			}
			
			sb.append("z^" + i);
		}
		return sb.toString();
	}
}
