package hr.fer.zemris.math;

import java.util.Arrays;

/**
 * Objects represents polynomial of complex numbers<br>
 * Form of polynomial is - f(z) = z_0 * (z - z_1) * (z - z_2) * ... * (z - z_n)
 * 
 * @author Domagoj Lokner
 *
 */
public class ComplexRootedPolynomial {
	
	/**
	 * Constant in polynomial (z_0).
	 */
	private Complex constant;
	
	/**
	 * Collection of polynomial roots.
	 */
	private Complex[] roots;
	
	public ComplexRootedPolynomial(Complex constant, Complex ... roots) {
		this.constant = constant;
		this.roots = Arrays.copyOf(roots, roots.length);
	}
	
	/**
	 * Computes polynomial value at given point.
	 * 
	 * @param z - point in which polynomial value will be computed.
	 * @return polynomial value in given point.
	 */
	public Complex apply(Complex z) {
		Complex result = constant;
		
		for(int i = roots.length - 1; i >= 0; --i) {
			result = result.multiply(z.sub(roots[i]));
		}
		
		return result;
	}
	
	/**
	 * Converts this representation to ComplexPolynomial type
	 * 
	 * @return {@code this} polynomial as {@code ComplexPolynomial} type.
	 */
	public ComplexPolynomial toComplexPolynom() {
		ComplexPolynomial result = new ComplexPolynomial(constant);
		
		for(Complex root : roots) {
			result = result.multiply(new ComplexPolynomial(root.negate(), Complex.ONE));
		}
		
		return result;
		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(constant + "*");
		
		for(int i = 0; i < roots.length; ++i) {
			sb.append("(z-" + roots[i] + ")*");
		}
		
		sb.delete(sb.length()-2, sb.length());
		
		return sb.toString();
	}
	

	/**
	 * Finds index of closest root for given complex number z that is within {@code threshold}.
	 * 
	 * @param z - complex number to be checked for closest root.
	 * @param treshold - threshold within root must be to given number to be accepted as result.
	 * @return index of closest root for given complex number. 
	 * 		   If there is no such root, returns -1.
	 */
	public int indexOfClosestRootFor(Complex z, double threshold) {
		int indexOfMin = -1;
		
		for(int i = 0; i < roots.length; ++i) {
			
			double distance = z.sub(roots[i]).module();
			if(Double.compare(distance, threshold) <= 0) {
				threshold = distance;
				indexOfMin = i;
			}
		}
		
		return indexOfMin;
	}
	
}
