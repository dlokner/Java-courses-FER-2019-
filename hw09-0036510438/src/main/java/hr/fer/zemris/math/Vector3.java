package hr.fer.zemris.math;

/**
 * Objects of this class represents three-component immutable vector.<br>
 * All operations this class offers will return new vector.
 * 
 * @author Domagoj Lokner
 *
 */
public class Vector3 {

	/**
	 * Vector's x coordinate.
	 */
	private double x;
	
	/**
	 * Vector's y coordinate.
	 */
	private double y;
	
	/**
	 * Vector's z coordinate.
	 */
	private double z;
	
	/**
	 * Constructs {@code Vector3} object.
	 * 
	 * @param x - x coordinate of vector.
	 * @param y - y coordinate of vector.
	 * @param z - z coordinate of vector.
	 */
	public Vector3(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	} 
	
	/**
	 * Returns norm of {@code this} vector.
	 * 
	 * @return vector norm.
	 */
	public double norm() {
		return Math.sqrt(x*x + y*y + z*z); 
	}
	
	/**
	 * Returns this vector normalized.
	 * 
	 * @return normalized vector.
	 */
	public Vector3 normalized() {
		double norm = norm();
		
		return new Vector3(
				x / norm,
				y / norm,
				z / norm);
	}
	
	/**
	 * Returns new vector computed as sum of {@code this} vector and given vector.
	 * 
	 * @param other - vector to be added to {@code this} vector.
	 * @return sum of vectors.
	 */
	public Vector3 add(Vector3 other) {
		return new Vector3(
				x + other.getX(),
				y + other.getY(),
				z + other.getZ());
	}
	
	/**
	 * Returns new vector computed as subtraction of {@code this} and given vector.
	 * 
	 * @param other - subtrahend.
	 * @return result of subtraction.
	 */
	public Vector3 sub(Vector3 other) {
		return new Vector3(
				x - other.getX(),
				y - other.getY(),
				z - other.getZ());
	} 
	
	
	/**
	 * Constructs new vector calculated as dot product of 
	 * {@code this} and given vector.
	 * 
	 * @param other - multiplier.
	 * @return dot product with given vector.
	 */
	public double dot(Vector3 other) {
		return x*other.getX() + y*other.getY() + z*other.getZ();
	} 
	
	
	/**
	 * Constructs new vector calculated as cross product of {@code this} and given vector.
	 * 
	 * @param other - multiplier.
	 * @return cross product with given vector.
	 */
	public Vector3 cross(Vector3 other) {
		return new Vector3(
				y*other.getZ() - z*other.getY(), 
				z*other.getX() - x*other.getZ(), 
				x*other.getY() - y*other.getX());
	} 
	
	
	/**
	 * Constructs new vector calculated  as this vector scaled for given number.
	 * 
	 * @param s - scaling coefficient.
	 * @return scaled vector.
	 */
	public Vector3 scale(double s) {
		return new Vector3(
				x * s, 
				y * s, 
				z * s);
	}
	
	
	/**
	 * Cosine of angle between {@code this} and given vector.
	 * 
	 * @param other - vector to which angle will be calculated.
	 * @return cosine of angle between vectors.
	 */
	public double cosAngle(Vector3 other) {
		return this.dot(other)/(this.norm() * other.norm());
	}
	
	/**
	 * Gets vector's x coordinate.
	 *  
	 * @return x - coordinate.
	 */
	public double getX() {
		return x;
	} 
	
	/**
	 * Gets vector's y coordinate.
	 *  
	 * @return y - coordinate.
	 */
	public double getY() {
		return y;
	} 
	
	/**
	 * Gets vector's z coordinate.
	 *  
	 * @return z - coordinate.
	 */
	public double getZ() {
		return z;
	}
	
	/**
	 * Returns vector as three-component double array.
	 * 
	 * @return vector as array.
	 */
	public double[] toArray() {
		return new double[] {x, y, z};
	}
	
	@Override
	public String toString() {
		return String.format("(%.6f, %.6f, %.6f)", x, y, z);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Vector3)) {
			return false;
		}
		Vector3 other = (Vector3) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x)) {
			return false;
		}
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y)) {
			return false;
		}
		if (Double.doubleToLongBits(z) != Double.doubleToLongBits(other.z)) {
			return false;
		}
		return true;
	}
}
