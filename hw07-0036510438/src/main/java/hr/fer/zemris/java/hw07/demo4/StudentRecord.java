package hr.fer.zemris.java.hw07.demo4;

/**
 * Object represents record for single student.
 * 
 * @author Domagoj Lokner
 *
 */
public class StudentRecord {
	
	/**
	 * Student's JMBAG,
	 */
	private String jmbag;
	
	/**
	 * Student's last name
	 */
	private String lastName;

	/**
	 * Student's first name
	 */
	private String firstName;
	
	/**
	 * Earned points on MI exam. 
	 */
	private double miPoints;
	
	/**
	 * Earned points on ZI exam.
	 */
	private double ziPoints;
	
	/**
	 * Points earned on labs.
	 */
	private double labPoints;
	
	/**
	 * Final grade.
	 */
	private int grade;
	
	/**
	 * Constructs {@code StudentRecord}.
	 * 
	 * @param jmbag
	 * @param lastName
	 * @param firstName
	 * @param miPoints
	 * @param ziPoints
	 * @param labPoints
	 * @param grade
	 */
	public StudentRecord(String jmbag, String lastName, String firstName, double miPoints, double ziPoints, double labPoints, int grade) {
		this.jmbag = jmbag;
		this.lastName = lastName;
		this.firstName = firstName;
		this.miPoints = miPoints;
		this.ziPoints = ziPoints;
		this.labPoints = labPoints;
		this.grade = grade;
	}
	
	

	@Override
	public String toString() {
		return jmbag + "\t" 
				+ lastName + "\t" 
				+ firstName + "\t" 
				+ miPoints + "\t" 
				+ ziPoints + "\t" 
				+ labPoints + "\t" 
				+ grade; 
	}

	/**
	 * Gets student's jmbag.
	 * 
	 * @return jmbag.
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * Gets student's last name.
	 * 
	 * @return last name.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Gets student's first name.
	 * 
	 * @return first name.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Returns points student have earned on MI exam.
	 * 
	 * @return points earned on MI exam.
	 */
	public double getMiPoints() {
		return miPoints;
	}

	/**
	 * Returns points student have earned on ZI exam.
	 * 
	 * @return points earned on ZI exam.
	 */
	public double getZiPoints() {
		return ziPoints;
	}

	/**
	 * Returns points student have earned on LAB.
	 * 
	 * @return points earned on LAB.
	 */
	public double getLabPoints() {
		return labPoints;
	}

	/**
	 * Returns student's final grade.
	 * 
	 * @return student's final grade.
	 */
	public int getGrade() {
		return grade;
	}
	
	/**
	 * Returns sum of all points student have earned.
	 * 
	 * @return sum of all points.
	 */
	public double getPoints() {
		return miPoints + ziPoints + labPoints;
	}
}
