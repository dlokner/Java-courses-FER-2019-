package hr.fer.zemris.java.hw07.observer2;

/**
 * Counts how many times value has been changed after observers registration.
 * 
 * @author Domagoj Lokner
 *
 */
public class ChangeCounter implements IntegerStorageObserver {

	int counter;
	
	@Override
	public void valueChanged(IntegerStorageChange istorageChange) {
		++counter;
		
		System.out.format("Number of value changes since tracking: %d%n", counter);
	}

}
