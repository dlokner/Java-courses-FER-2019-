package hr.fer.zemris.java.hw07.demo4;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Class that offers static methods for processing list of {@code StudentRecord}s.
 * 
 * @author Domagoj Lokner
 *
 */
public class ProcessStudentRecords {
	
	/**
	 * Returns the number of students who have more than 25 points.
	 * 
	 * @param records - records to be processed.
	 * @return Number of students who have more than 25 points.
	 */
	public static long vratiBodovaViseOd25(List<StudentRecord> records) {
		Objects.requireNonNull(records);
		
		return records.stream()
				.filter((r) -> r.getPoints() > 25)
				.count();
	}
	
	/**
	 * Returns the number of students who have final grade 5.
	 * 
	 * @param records - records to be processed.
	 * @return number of students who have final grade 5.
	 */
	public static long vratiBrojOdlikasa(List<StudentRecord> records) {
		Objects.requireNonNull(records);
		
		return records.stream()
				.filter((r) -> r.getGrade() == 5)
				.count();
	}
	
	/**
	 * Filters students who have final grade 5.
	 * 
	 * @param records - records to be processed.
	 * @return list of students who have final grade 5.
	 */
	public static List<StudentRecord> vratiListuOdlikasa(List<StudentRecord> records) {
		Objects.requireNonNull(records);
		
		return records.stream()
				.filter((r) -> r.getGrade() == 5)
				.collect(Collectors.toList());
	}
	
	/**
	 * Filters students who have final grade 5.
	 * Records will be sorted by number of earned points.
	 * 
	 * @param records - records to be processed.
	 * @return sorted list of students who have final grade 5.
	 */
	public static List<StudentRecord> vratiSortiranuListuOdlikasa(List<StudentRecord> records) {
		Objects.requireNonNull(records);
		
		return records.stream()
				.filter((r) -> r.getGrade() == 5)
				.sorted((r1, r2) -> Double.compare(r1.getPoints(), r2.getPoints()))
				.collect(Collectors.toList());
	}
	
	/**
	 * Filters students who have not passed.
	 * 
	 * @param records - records to be processed.
	 * @return list of students who have not passed.
	 */
	public static List<String> vratiPopisNepolozenih(List<StudentRecord> records) {
		Objects.requireNonNull(records);
		
		return records.stream()
				.filter((r) -> r.getGrade() == 1)
				.map((r) -> r.getJmbag())
				.sorted()
				.collect(Collectors.toList());
	}
	
	/**
	 * Groups student records by grade.
	 * 
	 * @param records - records to be processed.
	 * @return map of students. Key represent grade by which are saved 
	 * 		   records for students that have same grade.
	 */
	public static Map<Integer, List<StudentRecord>> razvrstajStudentePoOcjenama(List<StudentRecord> records) {
		Objects.requireNonNull(records);
		
		return records.stream()
				.collect(Collectors.groupingBy(r -> r.getGrade()));
	}
	
	/**
	 * Counts number of students with some grade.
	 * 
	 * @param records - records to be processed.
	 * @return map stores number of students with same grade stored by that grade.
	 */
	public static Map<Integer, Integer> vratiBrojStudenataPoOcjenama(List<StudentRecord> records) {
		Objects.requireNonNull(records);
		
		return records.stream()
				.collect(Collectors.toMap(r -> r.getGrade(),
						r -> 1,
						(v1, v2) -> v2 = v1+1));
	}
	
	/**
	 * Groups students depending by passing.
	 * 
	 * @param records - records to be processed.
	 * @return map with {@code true} and {@code false} keys.
	 * 		   Students that passsed are stored by {@code true} others are stored by {@code false}.
	 */
	public static Map<Boolean, List<StudentRecord>> razvrstajProlazPad(List<StudentRecord> records) {
		Objects.requireNonNull(records);
		
		return records.stream()
				.collect(Collectors.partitioningBy(r -> r.getGrade() > 1));
	}
	
	
}
