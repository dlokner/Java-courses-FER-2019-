package hr.fer.zemris.java.hw07.observer1;

/**
 * Observer that prints square value of stored value on standard output.
 * 
 * @author Domagoj Lokner
 *
 */
public class SquareValue implements IntegerStorageObserver {

	@Override
	public void valueChanged(IntegerStorage istorage) {
		int value = istorage.getValue();
		
		System.out.format("Provided new value: %d, square is %d%n", value, value*value);
	}

}
