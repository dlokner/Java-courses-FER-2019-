package hr.fer.zemris.java.custom.scripting.demo;

import hr.fer.zemris.java.custom.scripting.exec.ValueWrapper;

/**
 * Second demonstration program of {@code ObjectMultistack} class.
 * 
 * @author Domagoj Lokner
 *
 */
public class ObjectMultistackDemo2 {

	/**
	 * Starting method of the program.
	 * 
	 * @param args - command line arguments.
	 */
	public static void main(String[] args) {
		ValueWrapper v1 = new ValueWrapper(null);
		ValueWrapper v2 = new ValueWrapper(null);
		
		v1.add(v2.getValue()); // v1 now stores Integer(0); v2 still stores null.
		System.out.println("Integer(0) -> " + v1.getValue());
		System.out.println("null -> " + v2.getValue());
		System.out.println("--------------------");
		
		ValueWrapper v3 = new ValueWrapper("1.2E1");
		ValueWrapper v4 = new ValueWrapper(Integer.valueOf(1));
		v3.add(v4.getValue()); // v3 now stores Double(13); v4 still stores Integer(1).
		System.out.println("Double(13) -> " + v3.getValue());
		System.out.println("Integer(1) -> " + v4.getValue());
		System.out.println("--------------------");
		
		ValueWrapper v5 = new ValueWrapper("12");
		ValueWrapper v6 = new ValueWrapper(Integer.valueOf(1));
		v5.add(v6.getValue()); // v5 now stores Integer(13); v6 still stores Integer(1).
		System.out.println("Integer(13) -> " + v5.getValue());
		System.out.println("Integer(1) -> " + v6.getValue());	
		System.out.println("--------------------");
		
		ValueWrapper v7 = new ValueWrapper("Ankica");
		ValueWrapper v8 = new ValueWrapper(Integer.valueOf(1));
		
		try {
			v7.add(v8.getValue()); // throws RuntimeException
		} catch(RuntimeException ex) {
			System.out.println("Exception is thrown :)");
		}

	}

}
