package hr.fer.zemris.java.hw07.observer2;

/**
 * Object that encapsulate context of {@code IntegerStorage} before and after modification. 
 * 
 * @author Domagoj Lokner
 *
 */
public class IntegerStorageChange {
	
	private IntegerStorage istorage;
	
	/**
	 * Previous value of {@code istorage}.
	 */
	private int previousValue;
	
	/**
	 * Current value of {@code istorage};
	 */
	private int currentValue;
	
	/**
	 * Constructs {@code IntegerStorageChange}.
	 *  
	 * @param istorage - {@code IntegerStorage} which context is stored.
	 * @param previousValue - previous value of storage.
	 * @param currentValue - current value of storage.
	 */
	public IntegerStorageChange(IntegerStorage istorage, int previousValue, int currentValue) {
		this.istorage = istorage;
		this.previousValue = previousValue;
		this.currentValue = currentValue;
	}

	/**
	 * Gets {@code IntegerStorage}.
	 * 
	 * @return - {@code IntegerStorage}.
	 */
	public IntegerStorage getIstorage() {
		return istorage;
	}

	/**
	 * Gets previous value of {@code IntegerStorage}.
	 * 
	 * @return - previous value of {@code IntegerStorage}.
	 */
	public int getPreviousValue() {
		return previousValue;
	}

	/**
	 * Gets current value of {@code IntegerStorage}.
	 * 
	 * @return - current value of {@code IntegerStorage}.
	 */
	public int getCurrentValue() {
		return currentValue;
	}
}
