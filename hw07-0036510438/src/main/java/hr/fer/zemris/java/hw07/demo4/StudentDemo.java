package hr.fer.zemris.java.hw07.demo4;

import static hr.fer.zemris.java.hw07.demo4.ProcessStudentRecords.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Demonstration program of {@code ProcessStudentRecords} class.
 * 
 * @author Domagoj Lokner
 *
 */
public class StudentDemo {

	/**
	 * Starting method of the program.
	 * 
	 * @param args - command line arguments.
	 */
	public static void main(String[] args) {
		
		List<String> lines = null;
		try {
			lines = Files.readAllLines(Paths.get("studenti.txt"));
		} catch (IOException e) {
			System.err.println("File can't be reached.");
			System.exit(1);
		}
		
		List<StudentRecord> records = convert(lines);
		
		System.out.println(outputFormat(1));
		System.out.println(vratiBodovaViseOd25(records));
		
		System.out.println(outputFormat(2));
		System.out.println(vratiBrojOdlikasa(records));
		
		System.out.println(outputFormat(3));
		printList(vratiListuOdlikasa(records));
		
		System.out.println(outputFormat(4));
		printList(vratiSortiranuListuOdlikasa(records));
		
		System.out.println(outputFormat(5));
		printList(vratiPopisNepolozenih(records));
		
		System.out.println(outputFormat(6));
		for(Map.Entry<Integer, List<StudentRecord>> entry : razvrstajStudentePoOcjenama(records).entrySet()) {
			System.out.println("KLJUČ: " + entry.getKey());
			printList(entry.getValue());
		}
		
		System.out.println(outputFormat(7));
		for(Map.Entry<Integer, Integer> entry : vratiBrojStudenataPoOcjenama(records).entrySet()) {
			System.out.println(entry.getKey() + " -> " + entry.getValue()); 
		}
		
		System.out.println(outputFormat(8));
		for(Map.Entry<Boolean, List<StudentRecord>> entry : razvrstajProlazPad(records).entrySet()) {
			System.out.format(entry.getKey() ? "PROŠLI: %n" : "PALI: %n");
			printList(entry.getValue());
		}
		
	}

	/**
	 * Convert String lines to list of student records.
	 * 
	 * @param lines - list of strings where every string represents record for single student.
	 * @return list of student records.
	 */
	private static List<StudentRecord> convert(List<String> lines) {
		List<StudentRecord> result = new LinkedList<>();
		
		for(String line : lines) {
			if(line.isBlank()) {
				continue;
			}
			
			String[] record = line.split("\t");
			
			if(record.length != 7) {
				throw new IllegalArgumentException("Uvalid record!");
			}
			
			result.add(new StudentRecord(record[0], 
					record[1], 
					record[2], 
					Double.parseDouble(record[3]), 
					Double.parseDouble(record[4]), 
					Double.parseDouble(record[5]), 
					Integer.parseInt(record[6]))
			);
		}			
		return result;
	}
	
	/**
	 * Returns output format.<br>
	 * <p>
	 * example of output for n = 10:
	 * 	<ul>
	 * 	Zadatak 10<br>
	 * 	=========
	 * 	</ul>
	 * </p>
	 * @param n - number of example.
	 * @return output format.
	 */
	private static String outputFormat(int n) {
		return String.format("Zadatak %d%n=========", n);
	}
	
	/**
	 * Prints list on standard output.
	 * 
	 * @param records - list to be printed.
	 */
	private static <T> void printList(List<T> records) {
		for(T record : records) {
			System.out.println(record);
		}
	}

}
