package hr.fer.zemris.java.hw07.observer2;

import java.util.ArrayList;
import java.util.List;

/**
 * Object that stores single {@code int} value.<br>
 * This implementation is able to iterate registered observers even if 
 * list of registered observers is modified during the iteration.
 * 
 * @author Domagoj Lokner
 *
 */
public class IntegerStorage {
	
	/**
	 * Stored value.
	 */
	private int value;
	
	/**
	 * List of registered observers. 
	 */
	private List<IntegerStorageObserver> observers; // use ArrayList here!!!

	/**
	 * Constructs {@code IntegerStorage} with given value.
	 * 
	 * @param initialValue - value to be stored.
	 */
	public IntegerStorage(int initialValue) {
		this.value = initialValue;
	}

	/**
	 * Registers new observer.
	 * 
	 * @param observer - observer to be registered
	 */
	public void addObserver(IntegerStorageObserver observer) {
		if(observers == null) {
			observers = new ArrayList<>();
		}
		
		if(observers.contains(observer)) {
			return;
		}
		
		observers = new ArrayList<>(observers);
		observers.add(observer);
	}

	/**
	 * Removes given observer if it is registered.
	 * 
	 * @param observer - observer to be removed.
	 */
	public void removeObserver(IntegerStorageObserver observer) {
		observers = new ArrayList<>(observers);
		observers.remove(observer);
	}

	/**
	 * Removes all registered observers.
	 */
	public void clearObservers() {
		observers.clear();
	}

	/**
	 * Gets stored value.
	 * 
	 * @return store {@code int} value.
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets value to given argument.
	 * 
	 * @param value - new value to be stored.
	 */
	public void setValue(int value) {
		
		if (this.value != value) {
			
			int previousValue = this.value;
			
			this.value = value;
			
			if (observers != null) {
				
				IntegerStorageChange ischange = new IntegerStorageChange(this, previousValue, value);
				
				for (IntegerStorageObserver observer : observers) {
					observer.valueChanged(ischange);
				}
			}
		}
	}
	
}