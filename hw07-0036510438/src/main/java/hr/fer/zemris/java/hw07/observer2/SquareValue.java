package hr.fer.zemris.java.hw07.observer2;

/**
 * Observer that prints square value of stored value on standard output.
 * 
 * @author Domagoj Lokner
 *
 */
public class SquareValue implements IntegerStorageObserver {

	@Override
	public void valueChanged(IntegerStorageChange istorageChange) {
		int value = istorageChange.getCurrentValue();
		
		System.out.format("Provided new value: %d, square is %d%n", value, value*value);
	}

}
