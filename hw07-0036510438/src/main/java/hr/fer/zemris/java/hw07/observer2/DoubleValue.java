package hr.fer.zemris.java.hw07.observer2;

/**
 * Observer that prints double value of stored value on standard output.
 * 
 * @author Domagoj Lokner
 *
 */
public class DoubleValue implements IntegerStorageObserver {
	
	/**
	 * Number of calls after which observer will be unregistered.
	 */
	private int n;
	
	/**
	 * Constructs {@code DoubleValue} observer.
	 * 
	 * @param n - number of calls after which observer will be unregistered.
	 */
	public DoubleValue(int n) {
		this.n = n;
	}

	@Override
	public void valueChanged(IntegerStorageChange istorageChange) {
		
		System.out.format("Double value: %d%n", istorageChange.getCurrentValue()*2);

		if(--n <= 0) {
			istorageChange.getIstorage().removeObserver(this);
		}
	}

}
