package hr.fer.zemris.java.hw07.observer1;

import java.util.ArrayList;
import java.util.List;

/**
 * Object that stores single {@code int} value.
 * 
 * @author Domagoj Lokner
 *
 */
public class IntegerStorage {
	
	/**
	 * Stored value.
	 */
	private int value;
	
	/**
	 * List of registered observers. 
	 */
	private List<IntegerStorageObserver> observers; 

	/**
	 * Constructs {@code IntegerStorage} with given value.
	 * 
	 * @param initialValue - value to be stored.
	 */
	public IntegerStorage(int initialValue) {
		this.value = initialValue;
	}

	/**
	 * Registers new observer.
	 * 
	 * @param observer - observer to be registered
	 */
	public void addObserver(IntegerStorageObserver observer) {
		if(observers == null) {
			observers = new ArrayList<>();
		}
		
		observers = new ArrayList<>(observers);
		observers.add(observer);
	}

	/**
	 * Removes given observer if it is registered.
	 * 
	 * @param observer - observer to be removed.
	 */
	public void removeObserver(IntegerStorageObserver observer) {
		observers = new ArrayList<>(observers);
		observers.remove(observer);;
	}

	/**
	 * Removes all registered observers.
	 */
	public void clearObservers() {
		observers.clear();
	}

	/**
	 * Gets stored value.
	 * 
	 * @return store {@code int} value.
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets value to given argument.
	 * 
	 * @param value - new value to be stored.
	 */
	public void setValue(int value) {
		
		if (this.value != value) {
			this.value = value;
			
			if (observers != null) {
				for (IntegerStorageObserver observer : observers) {
					observer.valueChanged(this);
				}
			}
		}
	}
	
}