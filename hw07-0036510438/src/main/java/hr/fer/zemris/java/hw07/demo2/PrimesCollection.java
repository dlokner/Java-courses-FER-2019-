package hr.fer.zemris.java.hw07.demo2;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Collection of prime numbers.
 * 
 * @author Domagoj Lokner
 *
 */
public class PrimesCollection implements Iterable<Integer>{

	/**
	 * Number of primes.
	 */
	private int n;
	
	/**
	 * COnstructs collection of first {@code n} prime numbers.
	 * @param n
	 */
	public PrimesCollection(int n) {
		this.n = n;
	}

	@Override
	public Iterator<Integer> iterator() {
		return new primesIterator();
	}
	
	/**
	 * Implementation of {@code PrimesCollection} iterator.
	 * 
	 * @author Domagoj Lokner
	 *
	 */
	private class primesIterator implements Iterator<Integer>{
		
		/**
		 * Count number of iterated primes.
		 */
		private int counter;
		
		/**
		 * Number from which next prime will be searched.
		 */
		private int next;
		
		/**
		 * Constructs {@code PrimesCollection} iterator.
		 */
		public primesIterator() {
			next = 2;
		}
	
		@Override
		public boolean hasNext() {
			return counter < n;
		}

		/**
		 * Returns next prime number.
		 */
		@Override
		public Integer next() {
			
			if(!hasNext()) {
				throw new NoSuchElementException();
			}
			
			++counter;

			if(next == 2) {
				return next++;
			}
			
			while(!isPrime(next)) {
				next += 2;
			}
			
			next += 2;
			
			return next-2;
		}
		
		/**
		 * Checks if given number is prime.
		 * 
		 * @param num - number to be checked.
		 * @return {@code true} if given number is prime otherwise {@code false}.
		 */
		private boolean isPrime(int num) {
			if(num == 1) {
				return true;
			}
			
			if(num == 2) {
				return true;
			}
			
			for(int i = 2, n = (int)Math.sqrt(num); i <= n; ++i) {
				if(num % i == 0) {
					return false;
				}
			}
			return true;
		}	
	}

}
