package hr.fer.zemris.java.hw07.observer2;

/**
 * Implements objects doing specified actions when {@code IntegerStorage} change value.
 * 
 * @author Domagoj Lokner
 *
 */
public interface IntegerStorageObserver {
	
	/**
	 * Performs action after value of given {@code IntegerStorage} is changed.
	 * 
	 * @param istorage - storage which value have been changed.
	 */
	public void valueChanged(IntegerStorageChange istorageChange);
	
}