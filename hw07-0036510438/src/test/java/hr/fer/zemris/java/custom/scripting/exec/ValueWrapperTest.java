package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ValueWrapperTest {

	@Test
	void constructorTest() {
		ValueWrapper val1 = new ValueWrapper(5);
		ValueWrapper val2 = new ValueWrapper("Siniša");
		
		assertEquals(5, val1.getValue());
		assertEquals("Siniša", val2.getValue());
	}
	
	@Test
	void addTest() {
		ValueWrapper val1 = new ValueWrapper(5);
		ValueWrapper val2 = new ValueWrapper("Siniša");
		
		val1.add(3.4);
		
		assertEquals(8.4, val1.getValue());
		assertTrue(val1.getValue() instanceof Double);
		
		assertThrows(RuntimeException.class, () -> val1.add(val2.getValue()));
		assertThrows(RuntimeException.class, () -> val2.add(5));
	}
	
	@Test
	void subtractTest() {
		ValueWrapper val1 = new ValueWrapper(5);
		ValueWrapper val2 = new ValueWrapper("Siniša");
		
		val1.subtract(3);
		
		assertEquals(2, val1.getValue());
		assertTrue(val1.getValue() instanceof Integer);
		
		assertThrows(RuntimeException.class, () -> val1.subtract(val2.getValue()));
		assertThrows(RuntimeException.class, () -> val2.subtract(5));
	}
	
	@Test
	void multiplyTest() {
		ValueWrapper val1 = new ValueWrapper(5);
		ValueWrapper val2 = new ValueWrapper("Siniša");
		
		val1.multiply("0.3e1");
		
		assertEquals(15d, val1.getValue());
		assertTrue(val1.getValue() instanceof Double);
		
		assertThrows(RuntimeException.class, () -> val1.multiply(val2.getValue()));
		assertThrows(RuntimeException.class, () -> val2.multiply(5));
		assertThrows(RuntimeException.class, () -> val1.multiply("5.4z3"));
	}
	
	
	@Test
	void divideTest() {
		ValueWrapper val1 = new ValueWrapper(15);
		ValueWrapper val2 = new ValueWrapper("Siniša");
		
		val1.divide("3");
		
		assertEquals(5, val1.getValue());
		assertTrue(val1.getValue() instanceof Integer);
		
		assertThrows(RuntimeException.class, () -> val1.divide(val2.getValue()));
		assertThrows(RuntimeException.class, () -> val2.divide(5));
	}
	
	@Test
	void numCompareTest() {
		ValueWrapper val1 = new ValueWrapper(15);
		
		assertTrue(val1.numCompare(15.001) < 0);
		assertTrue(val1.numCompare(14.009) > 0);

	}
	
	

}
