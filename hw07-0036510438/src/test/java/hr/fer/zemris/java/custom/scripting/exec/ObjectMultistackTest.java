package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.jupiter.api.Assertions.*;

import java.util.EmptyStackException;

import org.junit.jupiter.api.Test;

class ObjectMultistackTest {

	@Test
	void pushTest() {
		ObjectMultistack multistack = new ObjectMultistack();
		
		multistack.push("year", new ValueWrapper(Integer.valueOf(1900)));
		
		assertEquals(1900, multistack.peek("year").getValue());
		
		multistack.push("year", new ValueWrapper(Integer.valueOf(1905)));
		multistack.push("year", new ValueWrapper(Integer.valueOf(1910)));
		
		multistack.push("name", new ValueWrapper("Marko"));
		multistack.push("name", new ValueWrapper("Siniša"));
		
		assertEquals(1910, multistack.peek("year").getValue());
		assertEquals("Siniša", multistack.peek("name").getValue());
	}
	
	@Test
	void popTest() {
		ObjectMultistack multistack = new ObjectMultistack();
		
		multistack.push("year", new ValueWrapper(Integer.valueOf(1900)));	
		multistack.push("year", new ValueWrapper(Integer.valueOf(1905)));
		multistack.push("year", new ValueWrapper(Integer.valueOf(1910)));
		
		multistack.push("name", new ValueWrapper("Marko"));
		multistack.push("name", new ValueWrapper("Siniša"));
		
		assertEquals(1910, multistack.peek("year").getValue());
		assertEquals("Siniša", multistack.peek("name").getValue());
		
		multistack.pop("year");
		multistack.pop("name");
		multistack.pop("name");
		
		assertEquals(1905, multistack.peek("year").getValue());
		assertThrows(EmptyStackException.class, () -> multistack.peek("name").getValue());
	}
	
	@Test
	void peekTest() {
		ObjectMultistack multistack = new ObjectMultistack();
		
		assertThrows(EmptyStackException.class, () -> multistack.peek("tadam").getValue());
		
		multistack.push("year", new ValueWrapper(Integer.valueOf(1900)));
		
		assertEquals(1900, multistack.peek("year").getValue());
		
		multistack.push("year", new ValueWrapper(Integer.valueOf(1905)));
		multistack.push("year", new ValueWrapper(Integer.valueOf(1910)));
		
		assertEquals(1910, multistack.peek("year").getValue());
		
		multistack.push("name", new ValueWrapper("Marko"));
		multistack.push("name", new ValueWrapper("Siniša"));
		
		assertEquals("Siniša", multistack.peek("name").getValue());
	}
	
	@Test
	void isEmptyTest() {
		ObjectMultistack multistack = new ObjectMultistack();
		
		assertTrue(multistack.isEmpty("year"));
		
		multistack.push("year", new ValueWrapper(Integer.valueOf(1900)));
		
		assertFalse(multistack.isEmpty("year"));
		
		multistack.push("year", new ValueWrapper(Integer.valueOf(1905)));
		multistack.push("year", new ValueWrapper(Integer.valueOf(1910)));
		
		assertFalse(multistack.isEmpty("year"));
		
		multistack.push("name", new ValueWrapper("Marko"));
		multistack.push("name", new ValueWrapper("Siniša"));
		
		assertFalse(multistack.isEmpty("name"));
		
		multistack.pop("year");
		multistack.pop("year");
		multistack.pop("year");
		
		assertTrue(multistack.isEmpty("year"));

	}

}
