package coloring.algorithms;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * {@code SubspaceExploreUtil} is class that offers static methods that implements searching algorithms.
 * 
 * @author Domagoj Lokner
 *
 */
public class SubspaceExploreUtil {
	
	/**
	 * Breadth-first search algorithm.
	 * 
	 * @param s0 - supplier for starting state.
	 * @param process - process to be done if state is accepted.
	 * @param succ - finds list of neighbor states.
	 * @param acceptable - checks if state is acceptable.
	 */
	public static <S> void bfs(
			 Supplier<S> s0,
			 Consumer<S> process,
			 Function<S,List<S>> succ,
			 Predicate<S> acceptable
			) {
		
		List<S> explore = new LinkedList<S>();
		
		explore.add(s0.get());
		
		while(!explore.isEmpty()) {
			S si = explore.remove(0);
			
			if(!acceptable.test(si)) {
				continue;
			}
			
			process.accept(si);
			
			explore.addAll(succ.apply(si));
		}
	}
	
	/**
	 * Depth-first search algorithm.
	 * 
	 * @param s0 - supplier for starting state.
	 * @param process - process to be done if state is accepted.
	 * @param succ - finds list of neighbor states.
	 * @param acceptable - checks if state is acceptable.
	 */
	public static <S> void dfs(
			 Supplier<S> s0,
			 Consumer<S> process,
			 Function<S,List<S>> succ,
			 Predicate<S> acceptable
			) {
		
		List<S> explore = new LinkedList<S>();
		
		explore.add(s0.get());
		
		while(!explore.isEmpty()) {
			S si = explore.remove(0);
			
			if(!acceptable.test(si)) {
				continue;
			}
			
			process.accept(si);
			
			explore.addAll(0, succ.apply(si));
		}
	}
	
	/**
	 * Optimized bfs algorithm.
	 * 
	 * @param s0 - supplier for starting state.
	 * @param process - process to be done if state is accepted.
	 * @param succ - finds list of neighbor states.
	 * @param acceptable - checks if state is acceptable.
	 */
	public static <S> void bfsv(
			 Supplier<S> s0,
			 Consumer<S> process,
			 Function<S,List<S>> succ,
			 Predicate<S> acceptable
			) {

		List<S> explore = new LinkedList<S>();
		Set<S> visited = new HashSet<>();
		
		explore.add(s0.get());
		
		while(!explore.isEmpty()) {
			S si = explore.remove(0);
			
			if(!acceptable.test(si)) {
				continue;
			}
			
			process.accept(si);
			
			explore.addAll(succ.apply(si).stream()
					.filter((c) -> visited.add(c))
					.collect(Collectors.toList()));
		}
	}
}
