package coloring.algorithms;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import marcupic.opjj.statespace.coloring.Picture;

/**
 * Objects that offers methods used to fill monochrome area of the painting.
 * 
 * @author Domagoj Lokner
 *
 */
public class Coloring implements Supplier<Pixel>, Consumer<Pixel>, Function<Pixel, List<Pixel>>, Predicate<Pixel> {

	/**
	 * Color of referent pixel.
	 */
	private Pixel reference;
	
	/**
	 * Object that represents picture.
	 */
	private Picture picture;
	
	/**
	 * Color in which area will be painted.
	 */
	private int fillColor;
	
	/**
	 * Color of referent pixel.
	 */
	private int refColor;
	
	
	/**
	 * Constructs {@code Coloring object}.
	 * 
	 * @param reference - referent pixel of area to be painted.
	 * @param picture - picture to be painted.
	 * @param fillColor - color in which area will be painted.
	 */
	public Coloring(Pixel reference, Picture picture, int fillColor) {
		this.reference = reference;
		this.picture = picture;
		this.fillColor = fillColor;
		this.refColor = getPixelColor(picture, reference);
	}

	@Override
	public boolean test(Pixel t) {
		return getPixelColor(picture, t) == refColor;
	}

	@Override
	public List<Pixel> apply(Pixel t) {
		List<Pixel> result = new LinkedList<>();
		
		int x = t.x;
		int y = t.y;
		
		addToList(result, new Pixel(x+1, y), picture);
		addToList(result, new Pixel(x-1, y), picture);
		
		addToList(result, new Pixel(x, y+1), picture);
		
		return addToList(result, new Pixel(x, y-1), picture);
	}

	@Override
	public void accept(Pixel t) {
		picture.setPixelColor(t.x, t.y, fillColor);
	}

	@Override
	public Pixel get() {
		return reference;
	}
	
	/**
	 * Gets pixel's color.
	 * 
	 * @param picture - picture.
	 * @param pixel - pixel on picture which color will be gotten.
	 * @return color of given pixel.
	 */
	private static int getPixelColor(Picture picture, Pixel pixel) {
		return picture.getPixelColor(pixel.x, pixel.y);
	}
	
	/**
	 * Checks if given picture contains given pixel.
	 * 
	 * @param picture - picture.
	 * @param pixel - pixel to be checked.
	 * @return {@code true} if picture contains given pixel otherwise {@code false}.
	 */
	private static boolean isInRange(Picture picture, Pixel pixel) {
		int x = pixel.x;
		int y = pixel.y;
		
		if(x < 0) {
			return false;
		}
		
		if(y < 0) {
			return false;
		}
		
		if(x > picture.getWidth()-1) {
			return false;
		}
		
		if(y > picture.getHeight()-1) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Adds pixel to list if picture contains given pixel.
	 * 
	 * @param list - collection to which pixel will be added.
	 * @param pixel - pixel.
	 * @param picture - picture.
	 * @return reference on given list.
	 */
	private List<Pixel> addToList(List<Pixel> list, Pixel pixel, Picture picture) {
		if(isInRange(picture, pixel)) {
			list.add(pixel);
		}	
		return list;
	}

}
