package coloring.demo;

import marcupic.opjj.statespace.coloring.FillApp;

/**
 * Program for filling monochrome area with selected color.
 * 
 * @author Domagoj Lokner
 *
 */
public class Bojanje1 {

	/**
	 * Starting method of the program
	 * 
	 * @param args - command line arguments.
	 */
	public static void main(String[] args) {
		FillApp.run(FillApp.ROSE, null); // ili FillApp.OWL
	}

}
