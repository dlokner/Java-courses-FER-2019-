package searching.algorithms;

/**
 * Object represents single node in search tree.
 * 
 * @author Domagoj Lokner
 *
 * @param <S> type of state object.
 */
public class Node<S> {

	/**
	 * Reference on parent node.
	 */
	private Node<S> parent;
	
	/**
	 * State of node.
	 */
	private S state;
	
	/**
	 * Cost of this node.
	 */
	private double cost;
	
	/**
	 * Constructs {@code Node}.
	 * 
	 * @param parent - parent node of new node.
	 * @param cost - cost of new node.
	 */
	public Node(Node<S> parent, S state, double cost) {
		this.parent = parent;
		this.state = state;
		this.cost = cost;
	}
	
	/**
	 * Gets state.
	 * 
	 * @return state.
	 */
	public S getState() {
		return state;
	}

	/**
	 * Gets parent node.
	 * 
	 * @return parent node.
	 */
	public Node<S> getParent() {
		return parent;
	}

	/**
	 * Gets cost of node.
	 * 
	 * @return cost.
	 */
	public double getCost() {
		return cost;
	}
}
