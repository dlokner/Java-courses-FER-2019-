package searching.algorithms;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * {@code SearchUtil} is class that offers static methods that implements searching algorithms.
 * 
 * @author Domagoj Lokner
 *
 */
public class SearchUtil {
	
	/**
	 * Breadth-first search algorithm.
	 * 
	 * @param s0 - supplier for starting state.
	 * @param succ - finds list of possible transitions.
	 * @param goal - checks if generated configuration is acceptable.
	 * @return node with accepted configuration.
	 */
	public static <S> Node<S> bfs(
			 Supplier<S> s0,
			 Function<S, List<Transition<S>>> succ,
			 Predicate<S> goal) {
		
		List<Node<S>> explore = new LinkedList<>();
		
		explore.add(new Node<S>(null, s0.get(), 0));
		
		while(!explore.isEmpty()) {
			Node<S> ni = explore.remove(0);
			
			if(goal.test(ni.getState())) {
				return ni;
			}
			
			for(Transition<S> transition : succ.apply(ni.getState())) {
				explore.add(new Node<>(ni, 
						transition.getState(), 
						ni.getCost() + transition.getCost()));
			}
		}
		
		return null;
	}
	
	/**
	 * Optimized breadth-first search algorithm.
	 * 
	 * @param s0 - supplier for starting state.
	 * @param succ - finds list of possible transitions.
	 * @param goal - checks if generated configuration is acceptable.
	 * @return node with accepted configuration.
	 */
	public static <S> Node<S> bsfv(
			 Supplier<S> s0,
			 Function<S, List<Transition<S>>> succ,
			 Predicate<S> goal) {
		
		List<Node<S>> explore = new LinkedList<>();
		Set<S> visited = new HashSet<>();
		
		explore.add(new Node<S>(null, s0.get(), 0));
		
		while(!explore.isEmpty()) {
			Node<S> ni = explore.remove(0);
			visited.add(ni.getState());
			
			if(goal.test(ni.getState())) {
				return ni;
			}
			
			explore.addAll(succ.apply(ni.getState()).stream()
					.filter((t) -> visited.add(t.getState()))
					.map((t) -> new Node<S>(ni, t.getState(), ni.getCost() + t.getCost()))
					.collect(Collectors.toList()));
		}
		
		return null;
	}
}
