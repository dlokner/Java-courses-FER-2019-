package searching.algorithms;

/**
 * Object represents state and cost ordered pair of searching node. 
 * 
 * @author Domagoj Lokner
 *
 * @param <S> type of state object.
 */
public class Transition<S> {

	/**
	 * State.
	 */
	private S state;
	
	/**
	 * Cost.
	 */
	private double cost;
	
	/**
	 * Constructs {@code Transition}.
	 * 
	 * @param state - state of transition.
	 * @param cost - cost of transition.
	 */
	public Transition(S state, double cost) {
		this.state = state;
		this.cost = cost;
	}

	/**
	 * Gets state.
	 * 
	 * @return state.
	 */
	public S getState() {
		return state;
	}

	/**
	 * Gets cost.
	 * 
	 * @return cost.
	 */
	public double getCost() {
		return cost;
	}
	 
	
}
