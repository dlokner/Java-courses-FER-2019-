package searching.slagalica;

import java.util.Arrays;

/**
 * Represents a single puzzle state.
 * 
 * @author Domagoj Lokner
 *
 */
public class KonfiguracijaSlagalice {
	
	private static final int CONFIGURATION_LENGTH = 9;

	/**
	 * Stores puzzle configuration.<br>
	 * 0 represents empty field.
	 */
	private int[] configuration;

	/**
	 * Constructs {@code KonfiguracijaSlagalice}.
	 * 
	 * @param configuration - {@code int} array with length of 9 representing puzzle configuration.
	 * @throws IllegalArgumentException if length of configuration array is different than 9.
	 */
	public KonfiguracijaSlagalice(int[] configuration) {
		if(configuration.length != CONFIGURATION_LENGTH) {
			throw new IllegalArgumentException();
		}
		
		this.configuration = configuration;
	}

	/**
	 * Gets puzzle configuration.
	 * 
	 * @return configuration.
	 */
	public int[] getPolje() {
		return Arrays.copyOf(configuration, CONFIGURATION_LENGTH);
	}
		
	/**
	 * Returns index of empty field in configuration.
	 * 
	 * @return index of empty field (index of element with value 0) or 
	 * 		   -1 if there is no empty field in puzzle.
	 */
	public int indexOfSpace() {
		for(int i = 0; i < CONFIGURATION_LENGTH; ++i) {
			if(0 == configuration[i]) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0; i < CONFIGURATION_LENGTH; ++i) {
			if(configuration[i] == 0) {
				sb.append('*');
			} else {
				sb.append(configuration[i]);	
			}
						
			if(i != 0 && (i+1)%3 == 0) {
				sb.append(String.format("%n"));
			} else {
				sb.append(" ");
			}
		}
		
		sb.deleteCharAt(sb.length()-1);
		
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(configuration);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof KonfiguracijaSlagalice)) {
			return false;
		}
		KonfiguracijaSlagalice other = (KonfiguracijaSlagalice) obj;
		if (!Arrays.equals(configuration, other.configuration)) {
			return false;
		}
		return true;
	}
	
	
}
