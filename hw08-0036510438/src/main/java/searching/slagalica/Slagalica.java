package searching.slagalica;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import searching.algorithms.Transition;

/**
 * Implementation of puzzle game.
 * 
 * @author Domagoj Lokner
 *
 */
public class Slagalica implements Supplier<KonfiguracijaSlagalice>, 
									Function<KonfiguracijaSlagalice, 
									List<Transition<KonfiguracijaSlagalice>>>, 
									Predicate<KonfiguracijaSlagalice> {
	
	private static final int[] GOAL = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 0};
	
	private KonfiguracijaSlagalice configuration;

	/**
	 * Constructs {@code Slagalica}.
	 * 
	 * @param configuration
	 */
	public Slagalica(KonfiguracijaSlagalice configuration) {
		this.configuration = configuration;
	}

	@Override
	public boolean test(KonfiguracijaSlagalice t) {
		return Arrays.equals(GOAL, t.getPolje());
	}

	@Override
	public List<Transition<KonfiguracijaSlagalice>> apply(KonfiguracijaSlagalice t) {
		List<Transition<KonfiguracijaSlagalice>> result = new LinkedList<>();
		
		List<int[]> configurations = getConfigurations(t.getPolje(), t.indexOfSpace());
		
		for(int[] configuration : configurations) {
			result.add(new Transition<KonfiguracijaSlagalice>(
					new KonfiguracijaSlagalice(configuration), 1)
					);
		}
		
		return result;
	}

	@Override
	public KonfiguracijaSlagalice get() {
		return configuration;
	}
	
	/**
	 * Returns list of children configurations.
	 * 
	 * @param array - current configuration.
	 * @param emptyFieldPosition - index of empty space.
	 * @return collection of children configurations.
	 */
	private static List<int[]> getConfigurations(int[] array, int emptyFieldPosition) {
		List<int[]> configurations = new LinkedList<>();
		
		Cell emptyField = new Cell(emptyFieldPosition/3, emptyFieldPosition%3);
		
		Cell[] neighbors = new Cell[] {
				new Cell(emptyField.x-1, emptyField.y),
				new Cell(emptyField.x+1, emptyField.y),
				
				new Cell(emptyField.x, emptyField.y+1),
				new Cell(emptyField.x, emptyField.y-1)
		};
		
		for(Cell neighbor : neighbors) {
			try {
				configurations.add(computeConfiguration(emptyFieldPosition, neighbor, array));
			} catch (IllegalArgumentException ex) {
				continue;
			}
		}
		
		return configurations;
	}
	
	/**
	 * Compute configuration we get if in given array {@code emptyField} 
	 * is switched with given cell.
	 * 
	 * @param emptyField - position of empty field in array.
	 * @param cell - cell to switched with empty cell.
	 * @param array - current configuration,
	 * @return generated configuration.
	 */
	private static int[] computeConfiguration(int emptyField, Cell cell, int[] array) {
		if(!cellInBounds(cell)) {
			throw new IllegalArgumentException();
		}
		
		int[] result = Arrays.copyOf(array, array.length);
		
		int cellIndex = cell.x*3 + cell.y;
		
		result[emptyField] = result[cellIndex];
		result[cellIndex] = 0;
		
		return result;
		
	}
	
	/**
	 * Checks if cell is 3x3 2D array.
	 * 
	 * @param cell - cell to be checked.
	 * @return {@code true} if 3x3 matrix contains given cell otherwise {@code false}.
	 */
	private static boolean cellInBounds(Cell cell) {
		if(cell.x < 0 || cell.x > 2) {
			return false;
		}
		
		if(cell.y < 0 || cell.y > 2) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Object represents single cell in 2D array.
	 * 
	 * @author Domagoj Lokner
	 *
	 */
	private static class Cell {
		
		/**
		 * x coordinate
		 */
		private int x; 
		
		/**
		 * y coordinate
		 */
		private int y;
		
		/**
		 * Constructs {@code Cell}.
		 * 
		 * @param x - x coordinate. 
		 * @param y - y coordinate,
		 */
		public Cell(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
	
}
