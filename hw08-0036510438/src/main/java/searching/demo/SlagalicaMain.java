package searching.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import searching.algorithms.Node;
import searching.algorithms.SearchUtil;
import searching.slagalica.KonfiguracijaSlagalice;
import searching.slagalica.Slagalica;
import searching.slagalica.gui.SlagalicaViewer;

/**
 * Demonstration program for {@code Slagalica} class and searching algorithms.<br>
 * <p>
 * Program expects single argument from command line, puzzle configuration given as array of numbers:<br>
 * 	<li>123705468
 * </p>
 * @author Domagoj Lokner
 *
 */
public class SlagalicaMain {

	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("Program expects single argument from command line!");
			System.exit(1);
		}
		
		if(args[0].length() != 9) {
			System.out.println("Invalid argument is given!");
			System.exit(1);
		}
		
		int[] arguments = new int[9];
		
		try {
			for(int i = 0; i < args[0].length(); ++i) {
				arguments[i] = Integer.parseInt(Character.toString(args[0].charAt(i)));
			}
		} catch (NumberFormatException e) {
			System.out.println("Argument can be parsed to int!");
			System.exit(1);
		}
		
		Slagalica slagalica = new Slagalica(new KonfiguracijaSlagalice(arguments));
		Node<KonfiguracijaSlagalice> rješenje = SearchUtil.bsfv(slagalica, slagalica, slagalica);
		if (rješenje == null) {
			System.out.println("Nisam uspio pronaći rješenje.");
		} else {
			System.out.println("Imam rješenje. Broj poteza je: " + rješenje.getCost());
			List<KonfiguracijaSlagalice> lista = new ArrayList<>();
			Node<KonfiguracijaSlagalice> trenutni = rješenje;
			while (trenutni != null) {
				lista.add(trenutni.getState());
				trenutni = trenutni.getParent();
			}
			Collections.reverse(lista);
			lista.stream().forEach(k -> {
				System.out.println(k);
				System.out.println();
			});
			SlagalicaViewer.display(rješenje);
		}
	}

}
