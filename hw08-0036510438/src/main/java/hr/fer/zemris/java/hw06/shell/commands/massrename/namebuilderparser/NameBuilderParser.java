package hr.fer.zemris.java.hw06.shell.commands.massrename.namebuilderparser;

import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.commands.massrename.NameBuilder;
import hr.fer.zemris.java.hw06.shell.commands.massrename.namebuildertoken.NameBuilderToken;
import hr.fer.zemris.java.hw06.shell.commands.massrename.namebuildertoken.NameBuilderTokenType;
import hr.fer.zemris.java.hw06.shell.commands.massrename.namebuilderlexer.NameBuilderLexer;
import hr.fer.zemris.java.hw06.shell.commands.massrename.namebuilderlexer.NameBuilderLexerException;
import hr.fer.zemris.java.hw06.shell.commands.massrename.namebuilderlexer.NameBuilderLexerState;

public class NameBuilderParser {
	
	private List<NameBuilder> list = new LinkedList<>();
	
	/**
	 * Constructs {@code NameBuilderParser}.
	 * 
	 * @param izraz - expression to be parsed.
	 */
	public NameBuilderParser(String izraz) {
		
		try {
			parse(new NameBuilderLexer(izraz));
		} catch(NameBuilderLexerException ex) {
			throw new NameBuilderParserException(ex.getMessage());
		}
	}

	/**
	 * Returns completed {@code NameBuilder}.
	 * 
	 * @return name builder.
	 */
	public NameBuilder getNameBuilder() {
		return (result, sb) -> {
			for(NameBuilder nb : list) {
				nb.execute(result, sb);
			}
		};
	}
	
	/**
	 * Parse tokens from given lexer.
	 * 
	 * @param lexer - lexer.
	 */
	private void parse(NameBuilderLexer lexer) {
		
		boolean inTag = false;
		
		while(lexer.getToken().getType() != NameBuilderTokenType.EOF) {
			 
			NameBuilderToken token = lexer.nextToken();
			 
			 if(token.getType() == NameBuilderTokenType.OPEN_TAG) {
				 lexer.setState(NameBuilderLexerState.TAG);
				 inTag = true;
				 continue;
			 }
			 
			 if(token.getType() == NameBuilderTokenType.CLOSED_TAG) {
				 lexer.setState(NameBuilderLexerState.TEXT);
				 inTag = false;
				 continue;
			 }
			 
			 if(token.getType() == NameBuilderTokenType.TEXT) {
				 list.add(text(token.getValue().toString()));
				 continue;
			 }
			 
			 
			 if(token.getType() == NameBuilderTokenType.NUMBER) {
				 int index = (Integer)token.getValue();
				 
				 token = lexer.nextToken();
				 
				 if(token.getType() == NameBuilderTokenType.CLOSED_TAG) {
					 list.add(group(index));
					 lexer.setState(NameBuilderLexerState.TEXT);
					 inTag = false;
					 continue;
				 } else {
					 char padding = token.getType() == NameBuilderTokenType.PADDING ? 
								(Character)token.getValue() : ' ';
								
					token = token.getType() == NameBuilderTokenType.PADDING ? 
							lexer.nextToken() : token;			
								
					if(token.getType() == NameBuilderTokenType.NUMBER) {
						int minWidth = (Integer)token.getValue();
						list.add(group(index, padding, minWidth));
						continue;
					}
					throw new NameBuilderParserException("Unvalid TAG expression");
				 }
			 } 
		}
		
		if(inTag) {
			throw new NameBuilderParserException("Unclosed tag");
		}
	}
	
	private static NameBuilder text(String t) { return (result, sb) -> sb.append(t); }
	
	private static NameBuilder group(int index) { return (result, sb) -> sb.append(result.group(index)); }
	
	private static NameBuilder group(int index, char padding, int minWidth) { return(result, sb) -> {
		String group = result.group(index);
		
		for(int i = group.length(); i < minWidth; ++i){
			sb.append(padding);
		}
		sb.append(group);
		
	}; }
}
