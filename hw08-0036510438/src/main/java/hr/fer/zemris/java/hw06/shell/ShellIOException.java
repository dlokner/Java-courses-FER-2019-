package hr.fer.zemris.java.hw06.shell;

/**
 * Thrown to indicate an error in shell.
 * 
 * @author Domagoj Lokner
 *
 */
public class ShellIOException extends RuntimeException {
	
	private static final long serialVersionUID = 5097516274279103215L;

	/**
	 * Constructs {@code ShellIOException}.
	 */
	public ShellIOException() {
	}
	
	/**
	 * Constructs {@code ShellIOException} with specified detail message.
	 * 
	 * @param message - given message.
	 */
	public ShellIOException(String message) {
		super(message);
	}
}
