package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Stack;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

public class ListdShellCommand implements ShellCommand {

	@SuppressWarnings("unchecked")
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(!Util.requireNonEmptyArg(arguments)) {
			env.writeln("popd command requires no arguments");
			return ShellStatus.CONTINUE;
		}
		
		ArrayList<Path> list = new ArrayList<>();
		
		list.addAll((Stack<Path>)env.getSharedData("cdstack"));
		
		if(list.isEmpty()) {
			System.out.println("Nema pohranjenih direktorija.");
		} else {
			ListIterator<Path> iterator = list.listIterator(list.size());
			
			while(iterator.hasPrevious()) {
				System.out.println(iterator.previous());
			}
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "listd";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command lists all directories pushed on stack.");
		description.add("Command expects no arguments.");
		
		return Collections.unmodifiableList(description);
	}
		
	
}
