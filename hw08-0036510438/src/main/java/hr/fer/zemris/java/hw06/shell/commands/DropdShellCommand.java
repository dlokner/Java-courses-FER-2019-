package hr.fer.zemris.java.hw06.shell.commands;

import java.util.Collections;
import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

public class DropdShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(!Util.requireNonEmptyArg(arguments)) {
			env.writeln("popd command requires no arguments");
			return ShellStatus.CONTINUE;
		}
		
		
		try {
			System.out.println(Util.popFromCdStack(env) + ": has been removed from stack.");
		} catch (EmptyStackException ex) {
			env.writeln("There is no stored directories");
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		// TODO Auto-generated method stub
		return "dropd";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command removes last directory pushed on stack.");
		description.add("Command expects no arguments.");
		
		return Collections.unmodifiableList(description);
	}

}
