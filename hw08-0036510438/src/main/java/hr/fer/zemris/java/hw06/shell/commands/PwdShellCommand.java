package hr.fer.zemris.java.hw06.shell.commands;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

public class PwdShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(Util.requireNonEmptyArg(arguments)) {
			env.writeln(env.getCurrentDirectory().toString());
		} else {
			env.writeln("pwd command requiers no arguments.");
		}
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "pwd";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command display absolute path to current directory.");
		description.add("Command expects no arguments.");
		
		return Collections.unmodifiableList(description);
	}

}
