package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

public class CdShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(Util.requireNonEmptyArg(arguments)) {
			env.writeln("cd command requires single argument");
			return ShellStatus.CONTINUE;
		}
		
		String[] paths = Util.extractArguments(arguments);
		
		if(paths.length != 1) {
			env.writeln("unvalid number of arguments");
			return ShellStatus.CONTINUE;
		}
		
		Path path = Util.apsolutToCurrentDir(Paths.get(paths[0]), env);
		
		try {
			env.setCurrentDirectory(path);
			env.writeln(env.getCurrentDirectory() + ": is current directory");
		} catch (IllegalArgumentException ex) {
			env.writeln(path + ": No such directory");
		}
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "cd";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command change current directory.");
		description.add("Command expects single argument:");
		description.add("	Absolute path or path relative to current directory path.");
		description.add("Current path will be set to path which is given as argument.");
		
		return Collections.unmodifiableList(description);
	}

}
