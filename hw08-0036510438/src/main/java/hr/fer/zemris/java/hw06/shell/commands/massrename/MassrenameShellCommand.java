package hr.fer.zemris.java.hw06.shell.commands.massrename;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Stream;


import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;
import hr.fer.zemris.java.hw06.shell.commands.massrename.namebuilderparser.NameBuilderParser;
import hr.fer.zemris.java.hw06.shell.commands.massrename.namebuilderparser.NameBuilderParserException;

/**
This command rename/move all files from directory based on regex expression.
<p>
Command expects 4 or 5 arguments:
<ul>
	<li>The first argument is source directory.
	<li>The second argument is destination directory.
	<li>The third argument is function name (filter, groups, show, execute).
	<li>The fourth argument is regex patern.
	<li>and fifth argument is rename expression.
<ul>
</p>
 * @author Domagoj Lokner
 *
 */
public class MassrenameShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(Util.requireNonEmptyArg(arguments)) {
			env.writeln("massername command requires arguments");
			return ShellStatus.CONTINUE;
		}
		
		String[] args = Util.extractArguments(arguments);
		
		if(args.length < 4) {
			env.writeln("too few arguments");
			return ShellStatus.CONTINUE;
		}
		
		Path dir1 = Util.apsolutToCurrentDir(Paths.get(args[0]), env);
		Path dir2 = Util.apsolutToCurrentDir(Paths.get(args[1]), env);
		
		switch (args.length) {
		case 4:
			switch (args[2]) {
			case ("filter"):
				filterCommand(env, dir1, dir2, args[3]);
				break;
			case ("groups"):
				groupsCommand(env, dir1, dir2, args[3]);
				break;
			default:
				env.writeln("Unsuported operation: " + args[2]);
			}
			break;
		case 5:
			switch (args[2]) {
			case ("show"):
				showCommand(env, dir1, dir2, args[3], args[4]);
				break;
			case ("execute"):
				executeCommand(env, dir1, dir2, args[3], args[4]);
				break;	
			default:
				env.writeln("Unsuported operation: " + args[2]);
			}
			break;
		default:
			env.writeln("unvalid number of arguments");
			return ShellStatus.CONTINUE;
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "massrename";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command rename/move all files from directory based on regex expression.");
		description.add("Command expects 4 or 5 arguments:");
		description.add("	The first argument is source directory.");
		description.add("	The second argument is destination directory.");
		description.add("	The third argument is function name (filter, groups, show, execute).");
		description.add("	The fourth argument is regex patern");
		description.add("	and fifth argument is rename expression.");
		
		return Collections.unmodifiableList(description);
	}
	
	/**
	 * Returns filtered list of results.
	 * 
	 * @param dir - directory.
	 * @param pattern - regex pattern.
	 * @return filtered file results chosen from files {@code dir} contains.
	 * 
	 * @throws IOException - if given {@code dir} is not existing directory.
	 * @throws IllegalArgumentException - if given {@code pattern} string 
	 * 									  can't be interpreted as regex pattern.
	 */
	private static List<FilterResult> filter(Path dir, String pattern) throws IOException {
		
		List<FilterResult> result = new LinkedList<>(); 
		
		Pattern reg;
		try {
			reg = Pattern.compile(pattern, Pattern.UNICODE_CASE);
		} catch (PatternSyntaxException ex) {
			throw new IllegalArgumentException("Unvalid regex pattern is given");
		}		
		
		try (Stream<Path> stream = Files.list(dir)) {
			
			stream.forEach((f) -> {
				if(Files.isRegularFile(f)){
					
					String name = f.getFileName().toString();
					if(name.matches(pattern)) {
						result.add(new FilterResult(name, reg));
					}
				}
			});
		}
		
		return result;
	}
	
	/**
	 * Executes "filter" command.
	 * 
	 * @param env - environment.
	 * @param dir1 - source directory.
	 * @param dir2 - destination directory.
	 * @param pattern - regex pattern.
	 */
	private void filterCommand(Environment env, Path dir1, Path dir2, String pattern) {
		
		List<FilterResult> results = getFilteredResults(env, dir1, pattern);
		
		if(Objects.isNull(results)) {
			return;
		}
		
		for(FilterResult file : results) {
			env.writeln(file.toString());
		}
	}

	/**
	 * Executes "groups" command.
	 * 
	 * @param env - environment.
	 * @param dir1 - source directory.
	 * @param dir2 - destination directory.
	 * @param pattern - regex pattern.
	 */
	private void groupsCommand(Environment env, Path dir1, Path dir2, String pattern) {
		
		List<FilterResult> results = getFilteredResults(env, dir1, pattern);
		
		if(Objects.isNull(results)) {
			return;
		}
		
		for(FilterResult file : results) {
			StringBuilder sb = new StringBuilder();
			
			sb.append(file.toString() + " ");

			for(int i = 0, n = file.numberOfGroups(); i <= n; ++i) {
				sb.append(String.format("%d: %s ", i, file.group(i)));
			}
			
			env.writeln(sb.toString().stripTrailing());
		}
	}

	/**
	 * Executes "show" command.
	 * 
	 * @param env - environment.
	 * @param dir1 - source directory.
	 * @param dir2 - destination directory.
	 * @param pattern - regex pattern.
	 * @param expression - rename expression.
	 */
	private void showCommand(Environment env, Path dir1, Path dir2, String pattern, String expression) {
		List<FilterResult> results = getFilteredResults(env, dir1, pattern);
		
		if(Objects.isNull(results)) {
			return;
		}
		
		NameBuilderParser parser;
		try {
			parser = new NameBuilderParser(expression);
		} catch (NameBuilderParserException ex) {
			env.writeln(ex.getMessage());
			return;
		}
			
		for(FilterResult file : results) {
			StringBuilder sb = new StringBuilder();
			
			sb.append(file.toString() + " => ");
			
			parser.getNameBuilder().execute(file, sb);
			
			env.writeln(sb.toString());
		}
		
	}
	
   /**
	 * Executes "execute" command.
	 * 
	 * @param env - environment.
	 * @param dir1 - source directory.
	 * @param dir2 - destination directory.
	 * @param pattern - regex pattern.
	 * @param expression - rename expression.
	 */
	private void executeCommand(Environment env, Path dir1, Path dir2, String pattern, String expression) {
		if(!Files.isDirectory(dir2)) {
			return;
		}
		
		List<FilterResult> results = getFilteredResults(env, dir1, pattern);
		
		if(Objects.isNull(results)) {
			return;
		}
		
		NameBuilderParser parser;
		try {
			parser = new NameBuilderParser(expression);
		} catch (NameBuilderParserException ex) {
			env.writeln(ex.getMessage());
			return;
		}
			
		for(FilterResult file : results) {
			StringBuilder sb = new StringBuilder();
			
			parser.getNameBuilder().execute(file, sb);
			
			Path source = dir1.resolve(Paths.get(file.toString()));
			Path destination = dir2.resolve(Paths.get(sb.toString()));
			
			try {
				Files.move(source, destination, StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				env.writeln("Invalid source or destination filename");
				break;
			}
			
			env.writeln(source.toString() + " => " + destination.toString());
		}
	}
	
	
	/**
	 * Returns list of filtered results. If error occured while reading directories, message will be
	 * displayed on console.
	 * 
	 * @param env - environment.
	 * @param dir - directory.
	 * @param pattern - regex pattern.
	 * @return filtered list.
	 */
	private static List<FilterResult> getFilteredResults(Environment env, Path dir, String pattern){
		List<FilterResult> results = null;
		
		try {
			results = filter(dir, pattern);
		} catch (IOException ex) {
			env.writeln(dir + ": No such directory");
			return null;
		}
		
		return results;
	}

}
