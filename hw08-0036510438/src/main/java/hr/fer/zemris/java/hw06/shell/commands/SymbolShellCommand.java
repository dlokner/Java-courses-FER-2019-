package hr.fer.zemris.java.hw06.shell.commands;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

/**
 * Implementation of {@code ShellCommand} that 
 * changes symbols for PROMPT, MULTILINE and MORELINES.
 * 
 * <p>
 * Command expects one or two arguments:
 * <ul>
 * 	<li>If command is entered only with one of keywords for symol (PROMPT/MULTILINE/MORELINES) symol will be displayed.
 * 	<li>If command symol is entered as argument including key word current used symbol will be replaced with given symbol.
 * </ul>
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class SymbolShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		if(Util.requireNonEmptyArg(arguments)) {
			env.writeln("command arguments missing");
			return ShellStatus.CONTINUE;
		}
		
		String[] argumentsSplited = Util.symbolArguments(arguments);
		
		if(argumentsSplited.length < 1 || argumentsSplited.length > 2) {
			env.writeln("unvalid number of arguments");
			return ShellStatus.CONTINUE;
		}
		
		if(argumentsSplited.length == 1) {
			switch(argumentsSplited[0]) {
			
			case("PROMPT"):
				env.writeln("Symbol for PROMPT is '" + env.getPromptSymbol().toString() + '\'');
				break;
			case("MORELINES"):
				env.writeln("Symbol for MORELINES is '" + env.getMorelinesSymbol().toString() + '\'');
				break;
			case("MULTILINE"):
				env.writeln("Symbol for MULTILINE is '" + env.getMultilineSymbol().toString() + '\'');
				break;
			default:
				env.writeln(argumentsSplited[0] + ": unvalid argument");
			}
		} else {
			
			int symbolPosition = argumentsSplited[0].length() == 1 ? 0 : 1;
			
			if(argumentsSplited[symbolPosition].length() != 1) {
				env.writeln("unvalid argument");
				return ShellStatus.CONTINUE;
			}
			
			char symbol = argumentsSplited[symbolPosition].charAt(0);
			String argument = argumentsSplited[1 - symbolPosition];
			
			switch(argument) {
			
			case("PROMPT"):
				env.writeln(String.format("Symbol for PROMPT changed from '%c' to '%c'", env.getPromptSymbol(), symbol));
				env.setPromptSymbol(symbol);
				break;
			case("MORELINES"):
				env.writeln(String.format("Symbol for MORELINES changed from '%c' to '%c'", env.getMorelinesSymbol(), symbol));
				env.setMorelinesSymbol(symbol);
				break;
			case("MULTILINE"):
				env.writeln(String.format("Symbol for MULTILINE changed from '%c' to '%c'", env.getMultilineSymbol(), symbol));
				env.setMultilineSymbol(symbol);
				break;
			default:
				env.writeln(argument + ": unvalid argument");
			}
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "symbol";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command changes symbols for PROMPT, MULTILINE and MORELINES.");
		description.add("If command is entered only with one of keywords for symol (PROMPT/MULTILINE/MORELINES) symol will be displayed.");
		description.add("If command symol is entered as argument including key word current used symbol will be replaced with given symbol");
		
		return Collections.unmodifiableList(description);
	}

}
