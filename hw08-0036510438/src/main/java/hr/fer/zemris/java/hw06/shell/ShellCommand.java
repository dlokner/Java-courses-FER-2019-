package hr.fer.zemris.java.hw06.shell;

import java.util.List;

/**
 * Implements single shell command.
 * 
 * @author Domagoj Lokner
 *
 */
public interface ShellCommand {

	/**
	 * Executes shell command through given environment.
	 * 
	 * @param env - environment.
	 * @param arguments - command arguments.
	 * @return Status of shell command.
	 */
	ShellStatus executeCommand(Environment env, String arguments);
	
	/**
	 * Returns command name.
	 * 
	 * @return command name.
	 */
	String getCommandName();
	
	/**
	 * Returns a list of strings that describe this command.
	 * 
	 * @return list of strings that describes the command.
	 */
	List<String> getCommandDescription();
	
}
