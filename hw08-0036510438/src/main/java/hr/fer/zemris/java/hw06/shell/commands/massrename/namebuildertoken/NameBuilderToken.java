package hr.fer.zemris.java.hw06.shell.commands.massrename.namebuildertoken;

/**
 * Represents a single token.
 * 
 * @author Domagoj Lokner
 *
 */
public class NameBuilderToken {

	private NameBuilderTokenType type;
	private Object value;
	
	/**
	 * Constructs token.
	 * 
	 * @param type - type of token.
	 * @param value - token value.
	 */
	public NameBuilderToken(NameBuilderTokenType type, Object value) {
		this.type = type;
		this.value = value;
	}
	
	/**
	 * Gets value of token.
	 * 
	 * @return token value.
	 */
	public Object getValue() {
		return value;
	}
	
	/**
	 * Gets type of token.
	 * 
	 * @return token type.
	 */
	public NameBuilderTokenType getType() {
		return type;
	}
	
}
