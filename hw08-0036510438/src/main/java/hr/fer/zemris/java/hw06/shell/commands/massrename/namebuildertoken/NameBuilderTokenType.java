package hr.fer.zemris.java.hw06.shell.commands.massrename.namebuildertoken;

public enum NameBuilderTokenType {
	TEXT,
	OPEN_TAG,
	CLOSED_TAG,
	PADDING,
	NUMBER,
	EOF
}
