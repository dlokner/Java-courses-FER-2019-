package hr.fer.zemris.java.hw06.shell.commands.massrename.namebuilderlexer;

import java.util.Objects;

import hr.fer.zemris.java.hw06.shell.commands.massrename.namebuildertoken.NameBuilderToken;
import hr.fer.zemris.java.hw06.shell.commands.massrename.namebuildertoken.NameBuilderTokenType;

/**
 * Convert given expression to tokens.
 * 
 * @author Domagoj Lokner
 *
 */
public class NameBuilderLexer {
	
	private char[] data;
	private NameBuilderToken token;
	private int currentIndex;
	private NameBuilderLexerState state;
	
	/**
	 * Constructs lexer with {@code text} to be tokenized.
	 * 
	 * @param text - string to be tokenize.
	 */
	public NameBuilderLexer(String text) {
		this.data = text.toCharArray();
		state = NameBuilderLexerState.TEXT;
		token = new NameBuilderToken(null, null);
	}

	/**
	 * Generate and returns next token.
	 * 
	 * @return next token.
	 * @throws NameBuilderLexerException if an error occurs.
	 */
	public NameBuilderToken nextToken() {
		if(state == NameBuilderLexerState.TEXT) {
			return token = nextTokenText();
		} else {
			return token = nextTokenTag();
		}
	}

	/**
	 * Returns last generated token.
	 * 
	 * @return last token.
	 */
	public NameBuilderToken getToken() {
		return token;
	}
	
	/**
	 * Sets working state of lexer.
	 * 
	 * @param state - lexer working state.
	 */
	public void setState(NameBuilderLexerState state) {
		Objects.requireNonNull(state);
		
		this.state = state;
	}
	
	/**
	 * Generate next token by TAG state rules.
	 * 
	 * @return next token.
	 */
	private NameBuilderToken nextTokenTag() {
		if(token.getType() == NameBuilderTokenType.EOF && token.getValue() == null) {
			throw new NameBuilderLexerException();
		}
		
		skipBlanksAndComma();
		
		if(data.length <= currentIndex) {
			return new NameBuilderToken(NameBuilderTokenType.EOF, null);
		}
		
		if(data[currentIndex] == '}') {
			++currentIndex;
			return new NameBuilderToken(NameBuilderTokenType.CLOSED_TAG, null);
		}
		
		if(token.getType() == NameBuilderTokenType.PADDING) {
			return new NameBuilderToken(NameBuilderTokenType.NUMBER, extractPositiveNumber());
		}
		
		if(Character.isDigit(data[currentIndex])) {
			if(data[currentIndex] == '0' && Character.isDigit(data[currentIndex+1])) {
				return new NameBuilderToken(NameBuilderTokenType.PADDING, '0');
			} else {
				return new NameBuilderToken(NameBuilderTokenType.NUMBER, extractPositiveNumber());
			}
		}
		
		throw new NameBuilderLexerException("Unvalid expression");
		
	}

	/**
	 * Generate next token by TEXT state rules.
	 * 
	 * @return next token.
	 */
	private NameBuilderToken nextTokenText() {
		if(token.getType() == NameBuilderTokenType.EOF && token.getValue() == null) {
			throw new NameBuilderLexerException();
		}
		
		if(data.length <= currentIndex) {
			return new NameBuilderToken(NameBuilderTokenType.EOF, null);
		}
		
		if(data[currentIndex] == '$' && data[currentIndex+1] == '{') {
			currentIndex += 2;
			return new NameBuilderToken(NameBuilderTokenType.OPEN_TAG, null);
		}	
		
		return new NameBuilderToken(NameBuilderTokenType.TEXT, extractText());
	}
	
	/**
	 * Extract text starting from {@code currentIndex}.
	 * 
	 * @return extracted text.
	 */
	private Object extractText() {
		StringBuilder sb = new StringBuilder();
		
		do {
			if(data[currentIndex] == '\\') {
				
				if(data[currentIndex+1] == '\\' || data[currentIndex+1] == '"') {
					sb.append(data[currentIndex+1]);
					currentIndex += 2; 
					continue;
				}
				
			} else if(data[currentIndex] == '$' && data[currentIndex+1] == '{') {
				break;
			}
			
			sb.append(data[currentIndex++]);
			
		} while(currentIndex < data.length);
		
		return sb.toString();	
	}
	
	/**
	 * Extract positive number starting from {@code currentIndex}.
	 * 
	 * @return extracted number.
	 */
	private Object extractPositiveNumber() {
		StringBuilder sb = new StringBuilder();
		
		do {
			if(Character.isDigit(data[currentIndex])){
				sb.append(data[currentIndex]);
				currentIndex++;
				continue;
			} else {
				break;
			}
		} while(currentIndex < data.length);
		
		try {
			return Integer.parseInt(sb.toString());
		}catch (NumberFormatException ex) {
			throw new NameBuilderLexerException("Number in tag can't be parsed to int");
		}
	}
	
	/**
	 * Set {@code currentIndex} on first position thats not whitespace.
	 */
	private void skipBlanksAndComma() {
		while(currentIndex < data.length && Character.isWhitespace(data[currentIndex]) || data[currentIndex] == ',') {
			++currentIndex;
		}
	}
}
