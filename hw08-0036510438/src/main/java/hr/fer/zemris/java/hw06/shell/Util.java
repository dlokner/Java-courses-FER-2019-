package hr.fer.zemris.java.hw06.shell;

import java.nio.file.Path;
import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Class contains static methods to support implementing shell commands.
 * 
 * @author Domagoj Lokner
 *
 */
public class Util {
	
	/**
	 * Extract command key word from given parameter.
	 * 
	 * @param line - parameter to be parsed.
	 * @return string representation of command.
	 */
	public static String extractCommand(String line) {
		line = line.strip();
		
		if(line.length() < 1) {
			throw new IllegalArgumentException("Unvalid command expression!");
		}
		
		String[] splitedLine = line.split("\\s+");
		
		return splitedLine[0];
		
	}
	
	/**
	 * Extract arguments from given parameter.
	 * 
	 * @param line - parameter to be parsed.
	 * @return arguments represented by string.
	 */
	public static String extractArgument(String line) {
		String command = extractCommand(line);
		
		line = line.strip();
		
		return line
				.substring(command.length(), line.length())
				.strip();
	}
	
	/**
	 * Returns array of arguments extracted from given argument, 
	 * method is customized to work specifically with arguments under quotes.<br>
	 * If argument is under quotes blanks are allowed and quotes can be escaped.
	 * 
	 * @param line - string to be parsed to paths.
	 * @return array of strings representing paths.
	 * @throws IllegalArgumentException if quotes i path definition are not closed.
	 */
	public static String[] extractArguments(String line) {
		
		line = line.strip();
		
		List<String> result = new LinkedList<>();
		
		int currentIndex = 0;
		
		char[] input = line.toCharArray();
		
		while(currentIndex < line.length()) {
			
			if(input[currentIndex] == '"') {
				currentIndex = extractString(input, ++currentIndex, result);
			} else {
				currentIndex = extractPath(input, currentIndex, result);
			}
			
			if(currentIndex < line.length()) {
				while(Character.isWhitespace(input[currentIndex])) {
					++currentIndex;
				}
			}
		}
		
		return result.toArray(new String[1]);
	}
	
	/**
	 * Returns array of arguments extracted from given argument, 
	 * method is customized that doesn't work speciffic .
	 * 
	 * @param line - string to be parsed to paths.
	 * @return array of strings representing paths.
	 * @throws IllegalArgumentException if quotes i path definition are not closed.
	 */
	public static String[] symbolArguments(String line) {
		line = line.strip();
		
		List<String> result = new LinkedList<>();
		
		int currentIndex = 0;
		
		char[] input = line.toCharArray();
		
		while(currentIndex < line.length()) {

			currentIndex = extractPath(input, currentIndex, result);

			
			if(currentIndex < line.length()) {
				while(Character.isWhitespace(input[currentIndex])) {
					++currentIndex;
				}
			}
		}
		
		return result.toArray(new String[1]);
	}
	
	/**
	 * Checks if given {@code arg} is not empty string.
	 *  
	 * @param arg - string to be checked.
	 * @return {@code true} if string is not empty, otherwise {@code false}.
	 */
	public static boolean requireNonEmptyArg(String arg) {
		return arg.length() < 1;
	}
	
	private static int extractPath(char[] input, int currentIndex, List<String> result) {
		StringBuilder sb = new StringBuilder();
		
		while(currentIndex < input.length) {
			if(Character.isWhitespace(input[currentIndex])) {
				break;
			}
			
			sb.append(input[currentIndex++]);
		}
		
		result.add(sb.toString());
		
		return currentIndex;
	}

	/**
	 * Returns absolute path relative to current directory path.
	 * 
	 * @param path - path to be resolved to absolute path.
	 * @param env - environment.
	 * @return {@code path} if path is already absolute otherwise resolved absolute path.
	 */
	public static Path apsolutToCurrentDir(Path path, Environment env) {
		if(!path.isAbsolute()) {
			path = env.getCurrentDirectory().resolve(path);
		}
		return path;
	}
	
	/**
	 * Pushes given path to stack.
	 *  
	 * @param path - path to be pushed.
	 * @param env - environment where stack is stored.
	 * @return pushed path.
	 */
	@SuppressWarnings("unchecked")
	public static Path pushToCdStack(Path path, Environment env) {
		if(env.getSharedData("cdstack") == null) {
			env.setSharedData("cdstack", new Stack<Path>());
		}
		
		return ((Stack<Path>)env.getSharedData("cdstack")).push(path);
	}
	
	/**
	 * Pops given path to stack.
	 *  
	 * @param path - path to be popped to stack.
	 * @param env - environment where stack is stored.
	 * @return popped path.
	 * @throws EmptyStackException if stack is empty.
	 */
	@SuppressWarnings("unchecked")
	public static Path popFromCdStack(Environment env) {
		if(env.getSharedData("cdstack") == null) {
			throw new EmptyStackException();
		}
		return ((Stack<Path>)env.getSharedData("cdstack")).pop();
	}
	
	private static int extractString(char[] input, int currentIndex, List<String> result) {
		StringBuilder sb = new StringBuilder();
		
		while(currentIndex < input.length) {
			
			if(input[currentIndex] == '\\') {
				try {
					if(input[currentIndex+1] == '"' || input[currentIndex+1] == '\\') {
						sb.append(input[currentIndex+1]);
						currentIndex += 2;
						continue;
					}
				} catch(IndexOutOfBoundsException ex) {
					throw new IllegalArgumentException("Unclosed quotes!");
				}
			}
			
			if(input[currentIndex] == '"') {
				currentIndex++;
				break;
			}
			
			sb.append(input[currentIndex++]);
		}
		
		if(currentIndex == input.length && input[currentIndex-1] != '"') {
			throw new IllegalArgumentException("Unclosed quotes!");
		}
		
		result.add(sb.toString());
		
		return currentIndex;
	}
}
