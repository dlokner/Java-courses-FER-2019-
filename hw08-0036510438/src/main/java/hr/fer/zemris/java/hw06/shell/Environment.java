package hr.fer.zemris.java.hw06.shell;

import java.nio.file.Path;
import java.util.SortedMap;

/**
 * Represents object through which all shell commands will be capable to communicate with shell. 
 * 
 * @author Domagoj Lokner
 *
 */
public interface Environment {
	
	/**
	 * Read single line from console.
	 * 
	 * @return read line.
	 * @throws ShellIOException if error occur during reading.
	 */
	String readLine() throws ShellIOException;
	
	/**
	 * Write given text on console.
	 * 
	 * @param text - text to be written.
	 * @throws ShellIOException if error occur during writing.
	 */
	void write(String text) throws ShellIOException;
	
	/**
	 * Write given text on console with adding line separator string on the end.
	 * 
	 * @param text - text to be written.
	 * @throws ShellIOException if error occur during writing.
	 */
	void writeln(String text) throws ShellIOException;
	
	/**
	 * Returns unmodifiable map of all commands this environment offers.
	 * 
	 * @return map of commands.
	 */
	SortedMap<String, ShellCommand> commands();
	
	/**
	 * Gets multiline symbol.
	 * 
	 * @return multiline symbol.
	 */
	Character getMultilineSymbol();
	
	/**
	 * Sets given symbol as multiline symbol.
	 * 
	 * @param symbol - symbol to be set.
	 */
	void setMultilineSymbol(Character symbol);
	
	/**
	 * Gets prompt symbol.
	 * 
	 * @return prompt symbol.
	 */
	Character getPromptSymbol();
	
	/**
	 * Sets given symbol as prompt symbol.
	 * 
	 * @param symbol - symbol to be set.
	 */
	void setPromptSymbol(Character symbol);
	
	/**
	 * Gets morelines symbol.
	 * 
	 * @return morelines symbol.
	 */
	Character getMorelinesSymbol();
	
	/**
	 * Sets given symbol as morelines symbol.
	 * 
	 * @param symbol - symbol to be set.
	 */
	void setMorelinesSymbol(Character symbol);
	
	/**
	 * Gets current directory of shell.<br>
	 * Returned path is absolute and normalized.
	 * 
	 * @return current directory.
	 */
	Path getCurrentDirectory();
	
	/**
	 * Sets current directory to given path.
	 * 
	 * @param path - path to directory to be set as current directory.
	 * @throws IllegalArgumentException if given path is not existing directory.
	 */
	void setCurrentDirectory(Path path);
	
	/**
	 * Gets data stored by given key.
	 * 
	 * @param key - key by which data is stored.
	 * @return value stored by the key or {@code null} 
	 * 		   if there is no data stored by given key.
	 */
	Object getSharedData(String key);
	
	/**
	 * Sets data with given value by given key.
	 * 
	 * @param key - key of data to be stored.
	 * @param value - data value.
	 */
	void setSharedData(String key, Object value);
}
