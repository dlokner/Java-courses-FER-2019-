package hr.fer.zemris.java.hw06.shell.commands;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

/**
 * Implementation of {@code ShellCommand} which opens 
 * given file and writes its content to console.
 * 
 * <p>
 * Command expects one or two arguments:
 * <ul>
 * 	<li>The first argument is path to some file and is mandatory.
 * 	<li>The second argument is charset name that should be used to interpret chars from bytes.
 * </ul>
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class CatShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(Util.requireNonEmptyArg(arguments)) {
			env.writeln("cat command requires single argument");
			return ShellStatus.CONTINUE;
		}
		
		String[] paths = Util.extractArguments(arguments);
		
		if(paths.length > 2 || paths.length < 1) {
			env.writeln("unvalid number of arguments");
			return ShellStatus.CONTINUE;
		}
		
		Path path = Util.apsolutToCurrentDir(Paths.get(paths[0]), env);
		
		Charset charset = paths.length == 1 ? 
				Charset.defaultCharset() : Charset.forName(paths[1]);
		
		try (BufferedReader input = new BufferedReader(
				new InputStreamReader(
						new BufferedInputStream(
								Files.newInputStream(path)), charset.name())))
		{
			char[] output = new char[1024];
			
			do {
				int r = input.read(output);
				
				if(r < 1) {
					break;
				}
				
				env.write(String.valueOf(output, 0, r));
			
			} while(true);
			
			env.writeln("");
			
		} catch (UnsupportedEncodingException e) {
			env.writeln(paths[1] + ": Encoding can't be reached!");
			return ShellStatus.CONTINUE;
		} catch (IOException e) {
			env.writeln(path.toString() + ": No such file");
			return ShellStatus.CONTINUE;
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "cat";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command opens given file and writes its content to console.");
		description.add("Command expects one or two arguments:");
		description.add("	The first argument is path to some file and is mandatory.");
		description.add("	The second argument is charset name that should be used to interpret chars from bytes.");
		description.add("If second argument is not provided, a default platform charset will be used.");
		
		return Collections.unmodifiableList(description);
	}

}
