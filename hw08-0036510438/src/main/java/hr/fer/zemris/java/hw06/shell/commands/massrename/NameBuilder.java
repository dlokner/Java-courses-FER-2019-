package hr.fer.zemris.java.hw06.shell.commands.massrename;

/**
 * Implements objects capable building file names.
 * 
 * @author Domagoj Lokner
 *
 */
@FunctionalInterface
public interface NameBuilder {

	/**
	 * Append builded name on given {@code sb}.
	 * 
	 * @param result - source file.
	 * @param sb - string builder.
	 */
	void execute(FilterResult result, StringBuilder sb);

}
