package hr.fer.zemris.java.hw06.shell.commands.massrename.namebuilderlexer;

/**
 * Define working states of {@code NameBuilderLexer}.
 * 
 * @author Domagoj Lokner
 *
 */
public enum NameBuilderLexerState {
	TEXT,
	TAG
}
