package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

/**
 * Implementation of {@code ShellCommand} that produces 
 * hex-output of the content of given file.
 * 
 * <p>
 * Command expects one argument, path to file to be displayed in hex form.
 * </p>
 * 
 * <p>
 * The output consists of 4 columns:
 * <ul>
 * 	<li>First column indicate index of first byte displayed in that row.
 * 	<li>Second two columns presents next 16 bytes of file in hex form.
 * 	<li>Fourth column is character representation od 16-bytes in that row.
 * </ul>
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class HexdumpShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(Util.requireNonEmptyArg(arguments)) {
			env.writeln("hexdump command requires one argument");
			return ShellStatus.CONTINUE;
		}
		
		String[] paths = Util.extractArguments(arguments);
		
		if(paths.length != 1) {
			env.writeln("unvalid number of arguments");
			return ShellStatus.CONTINUE;
		}
		
		Path path = Util.apsolutToCurrentDir(Paths.get(paths[0]), env);

		
		try (InputStream input = Files.newInputStream(path)) {
			
			boolean eof = false;
			int level = 0;
			
			while (!eof) {
				
				int i;
				
				String[] hex = new String[16];
				StringBuilder text = new StringBuilder();
				
				for(i = 0; i < hex.length; ++i) { 
					
					int r = input.read();
					
					if(r < 1) {
						eof = true;
						i--;
						break;
					}
					
					hex[i] = Integer.toHexString(r);
					
					text.append(r < 128  && r > 31 ? (char)r : '.');
				}
				if(text.length() != 0) {
					env.writeln(formatOutput(i, hex, text.toString(), level));
					++level;
				} else {
					break;
				}	
			}

		} catch (IOException ex) {
			env.writeln(path.toString() + ": No such file");
			return ShellStatus.CONTINUE;
		}
		
		
		return ShellStatus.CONTINUE;
	}

	private String formatOutput(int size, String[] hex, String string, int level) {
		StringBuilder result = new StringBuilder();
		
		result.append(String.format("%08X: ", level*16 & 0xFFFFFFFF));
		
		for(int i = 0; i < 16; ++i) {
			if(i < size) {
				if(hex[i].length() == 1) {
					result.append('0');
				}
				result.append(hex[i].toUpperCase() + " ");
			} else {
				result.append("   ");
			}
			
			if(i == 7) {
				result.delete(result.length()-1, result.length());
				result.append('|');
			}
		}
		
		result.append("| ");
		result.append(string.substring(0, size));
		
		return result.toString();
	}

	@Override
	public String getCommandName() {
		return "hexdump";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command produces hex-output of the content of given file.");
		description.add("Command expects one argument, path to file to be displayed in hex form.");
		description.add("The output consists of 4 columns:");
		description.add("	First column indicate index of first byte displayed in that row.");
		description.add("	Second two columns presents next 16 bytes of file in hex form.");
		description.add("	Fourth column is character representation od 16-bytes in that row.");
		
		return Collections.unmodifiableList(description);
	}

}
