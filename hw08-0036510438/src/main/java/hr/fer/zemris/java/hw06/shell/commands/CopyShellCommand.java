package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

/**
 * Implementation of {@code ShellCommand} which copies given file content.
 * 
 * <p>
 * Command expects one or two arguments:
 * <ul>
 * 	<li>The first argument is path to source file.
 * 	<li>The second argument is destination file or directory.
 * </ul>
 * If second argument is existing file user will be asked if he want to overwrite it.<br>
 * If directory is given as second argument copy of source file will be
 * placed in destination directory under original file name.
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class CopyShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		String[] paths = Util.extractArguments(arguments);

		if(paths.length != 2) {
			env.writeln("copy command requires two arguments");
			return ShellStatus.CONTINUE;
		}
		
		Path source = Util.apsolutToCurrentDir(Paths.get(paths[0]), env);
		Path destination = Util.apsolutToCurrentDir(Paths.get(paths[1]), env);
		
		if(!Files.isRegularFile(source)) {
			env.writeln(source + ": No such file");
			return ShellStatus.CONTINUE;
		} 
		
		/*if destination is existing file or dir*/
		if(Files.exists(destination)) {
			
			/*destination is file*/
			if(Files.isRegularFile(destination)) {
				do {
					env.write("Woud you like to overwrite " + destination + " file? [y/n] ");
					String ans = env.readLine();
					
					if(ans.equalsIgnoreCase("y")) {
						break;
					} else if(ans.equalsIgnoreCase("n")) {
						return ShellStatus.CONTINUE;
					}
					
					env.writeln("Please answer with 'y' or 'n'! ");
					
				} while(true);	
			} else {
				
				/*destination is dir*/
				destination = destination.resolve(source.getFileName());
			}
		}
		
		try (InputStream input = Files.newInputStream(source); OutputStream output = Files.newOutputStream(destination)) {
			
			byte[] buff = new byte[1024];
			
			while (true) {
				
				int r = input.read(buff);
				
				if (r < 1) {
					break;
				}				
				output.write(buff, 0, r);
			}

		} catch (IOException ex) {
			throw new IllegalArgumentException("File can't be reached!");
		}
		
		env.writeln(source + " is copied to " + destination);
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "copy";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command copies given file content");
		description.add("Command expects one or two arguments:");
		description.add("	The first argument is path to source file.");
		description.add("	The second argument is destination file or directory.");
		description.add("If second argument is existing file user will be asked if he want to overwrite it.");
		description.add("If directory is given as second argument copy of source file will be " +
						"placed in destination directory under original file name.");
		
		return Collections.unmodifiableList(description);
	}

}
