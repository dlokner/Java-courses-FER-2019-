package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.file.Path;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

public class PopdShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(!Util.requireNonEmptyArg(arguments)) {
			env.writeln("popd command requires no arguments");
			return ShellStatus.CONTINUE;
		}
		
		Path path;
		
		try {
			path = Util.popFromCdStack(env);
		} catch (EmptyStackException ex) {
			env.writeln("There is no stored directories");
			return ShellStatus.CONTINUE;
		}
		
		try {
			env.setCurrentDirectory(path);
			env.writeln(env.getCurrentDirectory() + ": is current directory");
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "popd";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command pop last directory stored on stack and set it as current directory.");
		description.add("Command expects no arguments.");
		
		return Collections.unmodifiableList(description);
	}	
}
