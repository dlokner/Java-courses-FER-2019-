package hr.fer.zemris.java.hw06.shell.commands;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

/**
 * Implementation of {@code ShellCommand} which 
 * list defined shell command description.
 * 
 * <p>
 * If command is called without arguments all shell commands will be listed.<br>
 * Command also can accept one argument, if argument represent command name,
 * it will be displayed including command description.
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class HelpShellCommand  implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		if(Util.requireNonEmptyArg(arguments)) {
			
			env.writeln("List of shell suported commands:");
			
			for(Map.Entry<String, ShellCommand> command : env.commands().entrySet()) {
				env.writeln("  " + command.getValue().getCommandName());
			}
			
			return ShellStatus.CONTINUE;
		}
		
		ShellCommand command = env.commands().get(arguments);
		
		if(Objects.isNull(command)) {
			env.writeln(arguments + ": command is not found");
		} else {
			
			env.writeln(command.getCommandName() + " command description:");
			
			for(String line : command.getCommandDescription()) {
				env.writeln("  " + line);
			}
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "help";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command list defined shell command description.");
		description.add("If command is called without arguments all shel commands will be listed.");
		description.add("Command also can accept one argument, if argument represent command name, "+
						"it will be displayed including command description.");
		
		return Collections.unmodifiableList(description);
	}

}
