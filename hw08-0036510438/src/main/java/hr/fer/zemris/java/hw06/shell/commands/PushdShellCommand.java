package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

public class PushdShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(Util.requireNonEmptyArg(arguments)) {
			env.writeln("pushd command requires single argument");
			return ShellStatus.CONTINUE;
		}
		
		Path path = Util.apsolutToCurrentDir(Paths.get(arguments), env);
		Path currentDir = env.getCurrentDirectory();
		
		try {
			env.setCurrentDirectory(path);
			env.writeln(env.getCurrentDirectory() + ": is current directory.");
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
			return ShellStatus.CONTINUE;
		}
		
		Util.pushToCdStack(currentDir, env);
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "pushd";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command push current directory on stac and sets given directory as current directory.");
		description.add("Command expects single argument, directory to be set as current directory.");
		
		return Collections.unmodifiableList(description);
	}
}
