package hr.fer.zemris.java.hw06.shell.commands.massrename;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Object represents single file result of massrename command.<br>
 * Stores all informations about file for massrename command.
 * 
 * @author Domagoj Lokner
 *
 */
public class FilterResult {

	/**
	 * Matcher for file name.
	 */
	private Matcher matcher;
	
	/**
	 * Name of the file.
	 */
	private String fileName;
	
	/**
	 * Constructs {@code FilterResult}.<br>
	 * Class expects that given pattern matches file name otherwise 
	 * methods will not work properly.
	 * 
	 * @param fileName - file name.
	 * @param pattern - regex pattern that match given filename.
	 */
	public FilterResult(String fileName, Pattern pattern) {
		this.matcher = pattern.matcher(fileName);
		matcher.matches();

		this.fileName = fileName;
	}
	
	@Override
	public String toString() {
		return fileName;
	}
	
	/**
	 * Returns number of groups that file name matches from given pattern.
	 * 
	 * @return number of groups.
	 */
	public int numberOfGroups() {
		return matcher.groupCount();
	}
	
	/**
	 * Returns group for given index. 
	 * Group zero denotes the entire pattern.
	 * 
	 * @param index - index of group.
	 * @return group for given index.
	 * @throws IndexOutOfBoundsException if there is no group with the given index.
	 */
	public String group(int index) {
		return matcher.group(index);
	}
}
