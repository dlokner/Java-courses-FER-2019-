package hr.fer.zemris.java.hw06.shell.commands.massrename.namebuilderparser;

/**
 * Thrown to indicate an error in {@code NameBuilderParser}.
 * 
 * @author Domagoj Lokner
 *
 */
public class NameBuilderParserException extends RuntimeException {

	private static final long serialVersionUID = 7003668174530223367L;

	/**
	 * Constructs {@code NameBuilderParserException}.
	 */
	public NameBuilderParserException() {
	}
	
	/**
	 * Constructs {@code NameBuilderParserException} with specified detail message.
	 * 
	 * @param message - given message.
	 */
	public NameBuilderParserException(String message) {
		super(message);
	}
}
