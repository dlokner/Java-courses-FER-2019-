package hr.fer.zemris.java.hw06.shell.commands.massrename.namebuilderlexer;

/**
 * Thrown to indicate an error in {@code NameBuilderLexer}.
 * 
 * @author Domagoj Lokner
 *
 */
public class NameBuilderLexerException extends RuntimeException {

	private static final long serialVersionUID = -7661801744810066435L;

	/**
	 * Constructs {@code NameBuilderLexerException}.
	 */
	public NameBuilderLexerException() {
	}
	
	/**
	 * Constructs {@code NameBuilderLexerException} with specified detail message.
	 * 
	 * @param message - given message.
	 */
	public NameBuilderLexerException(String message) {
		super(message);
	}
}
