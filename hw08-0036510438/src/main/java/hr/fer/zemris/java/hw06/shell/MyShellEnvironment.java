package hr.fer.zemris.java.hw06.shell;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

import hr.fer.zemris.java.hw06.shell.commands.*;
import hr.fer.zemris.java.hw06.shell.commands.massrename.MassrenameShellCommand;

/**
 * Concrete implementation of shell environment.<br>
 * This implementation reads from standard input and 
 * and writes to standard output
 * 
 * @author Domagoj Lokner
 *
 */
public class MyShellEnvironment implements Environment {
	
	private static final char DEFAULT_PROMPTSYMBOL = '>';
	private static final char DEFAULT_MORELINES = '\\';
	private static final char DEFAULT_MULTILINE = '|';
	
	/**
	 * Collection that contains all supported commands.
	 */
	SortedMap<String, ShellCommand> commands;
	
	/**
	 * Object used for reading from console.
	 */
	Scanner sc;
	
	/**
	 * Prompt symbol.
	 */
	private char promptsymbol;
	
	/**
	 * Symbol that indicate that more lines should be expected on input.
	 */
	private char morelines;
	
	/**
	 * Symbol that will be starting symbol of new line.
	 */
	private char multiline;
	
	/**
	 * Stores current directory path. 
	 */
	private Path currentDirectory;
	
	Map<String, Object> data;

	/**
	 * Constructs {@code MyShellEnvironment}.
	 * Sets symbols on default values, fill map with commands 
	 * and sets scanner to read from standard input.
	 */
	public MyShellEnvironment() {
				
		promptsymbol = DEFAULT_PROMPTSYMBOL;
		morelines = DEFAULT_MORELINES;
		multiline = DEFAULT_MULTILINE;
		
		setCurrentDirectory(Paths.get("."));
		
		data = new HashMap<>();
		
		commands = new TreeMap<>();
		initCommandsMap();
		
		
		
		sc = new Scanner(System.in);
		
		writeln("Welcome to MyShell v 1.0");

	}
	
	/**
	 * Fills map with all commands.
	 */
	private void initCommandsMap() {
		commands.put("exit", new ExitShellCommand());
		commands.put("cat", new CatShellCommand());
		commands.put("copy", new CopyShellCommand());
		commands.put("charsets", new CharsetsShellCommand());
		commands.put("hexdump", new HexdumpShellCommand());
		commands.put("ls", new LsShellCommand());
		commands.put("mkdir", new MkdirShellCommand());
		commands.put("symbol", new SymbolShellCommand());
		commands.put("tree", new TreeShellCommand());	
		commands.put("help", new HelpShellCommand());
		commands.put("cd", new CdShellCommand());	
		commands.put("pwd", new PwdShellCommand());
		commands.put("pushd", new PushdShellCommand());	
		commands.put("popd", new PopdShellCommand());
		commands.put("listd", new ListdShellCommand());
		commands.put("dropd", new DropdShellCommand());
		commands.put("massrename", new MassrenameShellCommand());
	}

	@Override
	public String readLine() throws ShellIOException {
		
		StringBuilder sb = new StringBuilder();
		
		write(getPromptSymbol() + " ");
		
		do {
			
			String line;
			try {
				line = sc.nextLine();
			} catch(NoSuchElementException ex) {
				throw new ShellIOException();
			}
			
			if(line.endsWith(getMorelinesSymbol().toString())) {
				sb.append(line.substring(0, line.length()-1));
				write(getMultilineSymbol() + " ");
				continue;
			}
			sb.append(line);
			break;
			
		} while(true);
		
		return sb.toString();
	}

	@Override
	public void write(String text) throws ShellIOException {
		System.out.print(text);
	}

	@Override
	public void writeln(String text) throws ShellIOException {
		System.out.println(text);
	}

	@Override
	public SortedMap<String, ShellCommand> commands() {
		return Collections.unmodifiableSortedMap(commands);
	}

	@Override
	public Character getMultilineSymbol() {
		return multiline;
	}

	@Override
	public void setMultilineSymbol(Character symbol) {
		multiline = symbol;
	}

	@Override
	public Character getPromptSymbol() {
		return promptsymbol;
	}

	@Override
	public void setPromptSymbol(Character symbol) {
		promptsymbol = symbol;
	}

	@Override
	public Character getMorelinesSymbol() {
		return morelines;
	}

	@Override
	public void setMorelinesSymbol(Character symbol) {
		morelines = symbol;	
	}

	@Override
	public Path getCurrentDirectory() {
		return currentDirectory.toAbsolutePath().normalize();
	}

	@Override
	public void setCurrentDirectory(Path path) {
		Objects.requireNonNull(path);
		
		if(!Files.isDirectory(path)) {
			throw new IllegalArgumentException(path + ": No such directory");
		}
		
		currentDirectory = path;
	}

	@Override
	public Object getSharedData(String key) {
		return data.get(key);
	}

	@Override
	public void setSharedData(String key, Object value) {
		Objects.requireNonNull(key, "Data key can not be null");
		
		data.put(key, value);
	}
}
