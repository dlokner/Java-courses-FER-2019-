package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

/**
 * Implementation of {@code ShellCommand} that writes dictionary listing.
 * 
 * <p>
 * Command expects one argument, path to directory to be listed.
 * </p>
 * 
 * <p>
 * The output consists of 4 columns:
 * <ul>
 * 	<li>First column indicates if current object is directory ( d ), 
 * 		readable ( r ), writable ( w ) and executable ( x ).
 * 	<li>Second column contains object size in bytes that is right 
 * 		aligned and occupies 10 characters.
 * 	<li>Third column contains file creation date/time.
 * 	<li>Fourth column is file name.
 * </ul>
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class LsShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		if(Util.requireNonEmptyArg(arguments)) {
			env.writeln("ls command requires single argument");
			return ShellStatus.CONTINUE;
		}
				
		Path path = Util.apsolutToCurrentDir(Paths.get(arguments), env);
		
		if(!Files.isDirectory(path)) {
			env.writeln("\"" + path + "\"" + ": No such directory");
			return ShellStatus.CONTINUE;
		}
		
		try (Stream<Path> stream = Files.list(path)) {
			
			stream.forEach((f) -> {
								
				BasicFileAttributeView faView = Files.getFileAttributeView(
						f, BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS
				);
				
				BasicFileAttributes attributes = null;
				try {
					attributes = faView.readAttributes();
				} catch (IOException e) {
					env.writeln(f + ": File can't be reached!");
				}
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				FileTime fileTime = attributes.creationTime();
				String formattedDateTime = sdf.format(new Date(fileTime.toMillis()));
				
				char[] permissions = new char[] {'-', '-', '-', '-'};
				
				if(Files.isDirectory(f)){
					permissions[0] = 'd';
				}
				if(Files.isReadable(f)) {
					permissions[1] = 'r';
				}
				if(Files.isWritable(f)) {
					permissions[2] = 'w';
				}
				if(Files.isExecutable(f)) {
					permissions[3] = 'x';
				}
				
				long size = attributes.size();
				
				
				
				env.writeln(String.format("%s%10d %s %s", 
						String.valueOf(permissions),
						size,
						formattedDateTime,
						f.getFileName()));
			});
		} catch (IOException e) {
			env.writeln(path.toString() + ": No such directory");
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "ls";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command writes dictionary listing.");
		description.add("Command expects one argument, path to directory to be listed.");
		description.add("The output consists of 4 columns:");
		description.add("	First column indicates if current object is directory ( d )," + 
						" readable ( r ), writable ( w ) and executable ( x ).");
		description.add("	Second column contains object size in bytes that is right aligned and occupies 10 characters.");
		description.add("	Third column contains file creation date/time.");
		description.add("	Fourth column is file name.");
		
		return Collections.unmodifiableList(description);
	}

}
