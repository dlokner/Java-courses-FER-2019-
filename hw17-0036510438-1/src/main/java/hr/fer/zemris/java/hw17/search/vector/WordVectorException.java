package hr.fer.zemris.java.hw17.search.vector;

/**
 * Indicate error in {@code WordVector} class.
 *
 * @author Domagoj Lokner
 */
public class WordVectorException extends RuntimeException {

	private static final long serialVersionUID = -5686683765171165113L;

	/**
     * Constructs {@code WordVectorException}.
     */
    public WordVectorException() {
    }

    /**
     * Constructs {@code WordVectorException} with specified detail message.
     *
     * @param message error message.
     */
    public WordVectorException(String message) {
        super(message);
    }
}
