package hr.fer.zemris.java.hw17.search.engine.commands;

import hr.fer.zemris.java.hw17.search.engine.SearchCommand;
import hr.fer.zemris.java.hw17.search.engine.SearchEngine;
import hr.fer.zemris.java.hw17.search.engine.SearchEngineStatus;

/**
 * Command that will terminate console.
 *
 * @author Domagoj Lokner
 */
public class ExitCommand implements SearchCommand {

    @Override
    public SearchEngineStatus executeCommand(SearchEngine eng, String arg) {
        return SearchEngineStatus.TERMINATE;
    }
}
