package hr.fer.zemris.java.hw17.search.documents;

import hr.fer.zemris.java.hw17.search.BagOfWordsParser;
import hr.fer.zemris.java.hw17.search.vector.WordVector;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * Object that represents single document.
 * Documents represented by this class are compatible with
 * {@link hr.fer.zemris.java.hw17.search.engine.SearchEngine} and
 * {@link SearchDocumentDB} objects.
 *
 * @author Domagoj Lokner
 *
 */
public class SearchDocument {

    /**
     * Path to document.
     */
    private Path filePath;

    /**
     * Bag of words.
     */
    private Map<String, Integer> bagOfWords;

    /**
     * Instance of vocabulary.
     */
    private Vocabulary vocabulary;

    /**
     * TF vector that represents number of words
     * from vocabulary that this document contains.
     */
    private WordVector tf;

    /**
     * Constructs {{@code SearchDocument}.
     *
     * @param filePath real path to this document.
     * @param vocabulary instance of vocabulary.
     * @throws IOException if error occurs during reading file.
     */
    public SearchDocument(Path filePath, Vocabulary vocabulary) throws IOException {

        try {
            String document = Files.readString(filePath);
            BagOfWordsParser parser = new BagOfWordsParser(document);
            bagOfWords = parser.getBagOfWords();
        } catch (IOException ex) {
            throw new IOException(filePath.toString() + ": file can't be reached.");
        }

        this.filePath = filePath;
        this.vocabulary = vocabulary;

        tf = creteTfVectro();
    }

    /**
     * Constructs {@code SearchDocument}.
     *
     * @param docContent document content.
     * @param vocabulary instance of vocabulary.
     */
    public SearchDocument(String docContent, Vocabulary vocabulary) {

        BagOfWordsParser parser = new BagOfWordsParser(docContent);
        bagOfWords = parser.getBagOfWords();

        this.vocabulary = vocabulary;

        tf = creteTfVectro();
    }

    /**
     * Creates TF vector.
     *
     * @return TF vector.
     */
    private WordVector creteTfVectro() {
        Set<String> vocabular =  vocabulary.getVocabular();
        double[] vector = new double[vocabular.size()];

        int i = 0;
        for (String word : vocabular) {
            if (bagOfWords.containsKey(word)) {
                vector[i++] = bagOfWords.get(word);
            } else {
                vector[i++] = 0;
            }
        }

        return new WordVector(vector);
    }

    /**
     * Gets TF vector.
     *
     * @return TF vector.
     */
    public WordVector getTf() {
        return tf;
    }

    /**
     * Gets path to document.
     *
     * @return path to document.
     */
    public Path getFilePath() {
        return filePath;
    }

    /**
     * Gets vocabulary.
     *
     * @return vocabulary.
     */
    public Vocabulary getVocabulary() {
        return vocabulary;
    }

    /**
     * Gets bag of words of this document.
     *
     * @return bag of words.
     */
    public Map<String, Integer> getBagOfWords() {
        return Collections.unmodifiableMap(bagOfWords);
    }
}
