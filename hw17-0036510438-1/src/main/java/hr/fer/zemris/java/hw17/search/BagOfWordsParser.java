package hr.fer.zemris.java.hw17.search;

import hr.fer.zemris.java.hw17.search.documents.Stopwords;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Implementation of parser which parse words from given string.
 * Parser receive string to be parsed through constructor.
 * All parsed words will be saved in <i>bagOfWords</i> map - word represents key and value
 * is equal to frequency of that word.
 *
 * @author Domagoj Lokner
 *
 */
public class BagOfWordsParser {

    /**
     * Original parsed document.
     */
    private char[] doc;

    /**
     * Variable used for storing index of position
     * in parsing.
     */
    private int currentIndex;

    /**
     * Map that stores all parsed words.
     * <ul>
     * <li><b>key</b> - word</li>
     * <li><b>value</b> - word frequency</li>
     * </ul>
     */
    private Map<String, Integer> bagOfWords;

    /**
     * Constructs {@code BagOfWordsParser}.
     *
     * @param document document to be parsed.
     */
    public BagOfWordsParser(String document) {
        Objects.requireNonNull(document);
        this.doc = document.toLowerCase().toCharArray();

        bagOfWords = new HashMap<>();

        parse();
    }

    /**
     * Parses words from {@code doc} property.
     */
    private void parse() {

        Stopwords stopwords;

        try {
            stopwords = Stopwords.getInstance();
        } catch (IOException ex) {
            stopwords = null;
        }

        while (currentIndex < doc.length) {
            skip();

            if (currentIndex < doc.length) {
                String word = extractWord();

                if (stopwords != null) {
                    if (stopwords.getStopwords().contains(word)) {
                        currentIndex++;
                        continue;
                    }
                }

                bagOfWords.merge(word, 1, Integer::sum);
            }

            ++currentIndex;
        }
    }

    /**
     * Extracts word from {@code doc} starting from {@code currentIndex}.
     *
     * @return extracted word.
     */
    private String extractWord() {
        StringBuilder sb = new StringBuilder();

        while (currentIndex < doc.length) {
            if (Character.isAlphabetic(doc[currentIndex])) {
                sb.append(doc[currentIndex]);
                ++currentIndex;
                continue;
            }
            break;
        }

        return sb.toString();
    }

    /**
     * Skips all non alphabetic signs.
     * {@code currentIndex} will be changed if char on
     * that index in {@code doc} is not alphabetic sign.
     */
    private void skip() {
        while (currentIndex < doc.length) {
            if (!Character.isAlphabetic(doc[currentIndex])) {
                ++currentIndex;
                continue;
            }
            break;
        }
    }

    /**
     * Gets map of all words.
     *
     * @return map of all words.
     */
    public Map<String, Integer> getBagOfWords() {
        return bagOfWords;
    }

}
