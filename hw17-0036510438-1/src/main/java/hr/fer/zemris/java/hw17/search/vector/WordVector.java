package hr.fer.zemris.java.hw17.search.vector;

import java.util.Arrays;

/**
 * Implementation of int vector used for search engine.
 * Every vector component represents single word from vocabulary.
 *
 * @author Domagoj Lokner
 */
public class WordVector {

    /**
     * Array that represents vector.
     */
    private double[] v;

    /**
     * Constructs {@code WordVector}.
     *
     * @param v vector components.
     */
    public WordVector(double ... v) {
        this.v = v;
    }

    /**
     * Gets component on given {@code index}.
     *
     * @param index index of component.
     * @return vector's component on given {@code index}.
     */
    public double getCommponent(int index) {
        if (index >= v.length) {
            throw new IndexOutOfBoundsException();
        }
        return v[index];
    }

    /**
     * Sets value of component on given {@code index} in vector.
     *
     * @param index index of component.
     * @param val value to be set.
     */
    public void setCommponent(int index, double val) {
        if (index >= v.length) {
            throw new IndexOutOfBoundsException();
        }
        v[index] = val;
    }

    /**
     * Dimension of this vector.
     *
     * @return vectors dimension.
     */
    public int dimension() {
        return v.length;
    }

    /**
     * Norm of vector.
     *
     * @return vector's norm.
     */
    public double norm() {
        double norm = 0d;

        for (double v : v) {
            norm += v*v;
        }

        return Math.sqrt(norm);
    }

    /**
     * Dot product of this vector and given vector {@code v2}.
     *
     * @param v2 multiplier vector.
     * @return dot product.
     */
    public double dot(WordVector v2) {
        if (v2.dimension() != dimension()) {
            throw new WordVectorException("Invalid vector length");
        }

        double dotProduct = 0;

        for (int i = 0; i < v.length; ++i) {
            dotProduct += this.getCommponent(i) * v2.getCommponent(i);
        }

        return dotProduct;
    }

    /**
     * Cosine of angle between this vector and given vector {@code v2}.
     *
     * @param v2 vector.
     * @return cosine.
     */
    public double cos(WordVector v2) {
        if (v2.dimension() != dimension()) {
            throw new WordVectorException("Invalid vector length");
        }

        double cos = this.dot(v2) / (this.norm() * v2.norm());

        return cos;
    }

    /**
     * Calculates IDF vector.
     *
     * @param d number of documents.
     * @param counter vector which components represents number of files
     *                from this database that contains word stored on
     *                that index in vocabulary.
     * @return IDF vector.
     */
    public static WordVector calcIdf(int d, WordVector counter) {
        double[] counterArray = Arrays.copyOf(counter.v, counter.dimension());

        for (int i = 0; i < counterArray.length; ++i) {
            counterArray[i] = Math.log(d / counterArray[i]);
        }

        return new WordVector(counterArray);
    }

    /**
     * Multiplies component of this vector with
     * component on same index od given vector {@code v2}.
     *
     * @param v2 multiplier vector.
     * @return this vector multiplied by given vector.
     */
    public WordVector mul(WordVector v2) {
        if (v2.dimension() != dimension()) {
            throw new WordVectorException("Invalid vector length");
        }

        for (int i = 0, n = v.length; i < n; ++i) {
            v[i] *= v2.getCommponent(i);
        }
        return this;
    }
}
