package hr.fer.zemris.java.hw17.search.engine.commands;

import hr.fer.zemris.java.hw17.search.documents.SearchDocument;
import hr.fer.zemris.java.hw17.search.documents.SearchDocumentDB;
import hr.fer.zemris.java.hw17.search.engine.SearchCommand;
import hr.fer.zemris.java.hw17.search.engine.SearchEngine;
import hr.fer.zemris.java.hw17.search.engine.SearchEngineStatus;

import java.util.*;

/**
 * Command that will print 10 documents which content have best match
 * to query argument.
 *
 * @author Domagoj Lokner
 */
public class QueryCommand implements SearchCommand {
    @Override
    public SearchEngineStatus executeCommand(SearchEngine eng, String arg) {

        SearchDocument query = new SearchDocument(arg, eng.getDocumentDB().getVocabulary());

        printQuery(eng, query);

        if (Double.compare(query.getTf().norm(), 0d) != 0) {
            findTop10(eng, query);
        }

        printTop10(eng);

        return SearchEngineStatus.CONTINUE;
    }

    /**
     * This method will write 10 best matching documents on engine.
     *
     * @param eng search engine through which result will be written.
     */
    private void printTop10(SearchEngine eng) {
        if (eng.lastResult().size() != 0) {
            eng.writeln("Najboljih 10 rezultata:");
            Util.printLastResult(eng);
        } else {
            eng.writeln("Nema rezultata");
        }
    }

    /**
     * Finds best 10 matching documents to given {@code query}.
     *
     * @param eng search engine.
     * @param query argument which words will be searched in documents.
     */
    private void findTop10(SearchEngine eng, SearchDocument query) {
        LinkedHashMap<SearchDocument, Double> matches = new LinkedHashMap<>();
        SearchDocumentDB db = eng.getDocumentDB();

        for (SearchDocument doc : db.getDocuments()) {

            double sim = db.calcDocSimilarity(doc, query);

            if (Double.compare(sim, 0d) == 0) {
                continue;
            }

            matches.put(doc, sim);
        }

        eng.lastResult().clear();

        int lastIndex = matches.size() < 10 ? matches.size() : 10;
        eng.lastResult().addAll(Util.sortedResults(matches).subList(0, lastIndex));
    }

    /**
     * Prints key words that will be searched.
     *
     * @param eng search engine.
     * @param query string from which key fords will be extracted.
     */
    private void printQuery(SearchEngine eng, SearchDocument query) {
        Map<String, Integer> queryWords = query.getBagOfWords();

        StringBuilder sb = new StringBuilder();

        sb.append("Query is: [");

        for (String word : queryWords.keySet()) {
            sb.append(word + ", ");
        }

        sb.delete(sb.length()-2, sb.length());

        sb.append("]");
        eng.writeln(sb.toString());
    }
}
