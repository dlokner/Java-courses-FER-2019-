package hr.fer.zemris.java.hw17.search.engine;

import hr.fer.zemris.java.hw17.search.documents.SearchDocument;
import hr.fer.zemris.java.hw17.search.documents.SearchDocumentDB;

import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * Model of document search engine.
 *
 * @author Domagoj Lokner
 */
public interface SearchEngine {
    /**
     * Read single line from console.
     *
     * @return read line.
     * @throws SearchIOException if error occur during reading.
     */
    String readLine() throws SearchIOException;

    /**
     * Write given text on console.
     *
     * @param text - text to be written.
     * @throws SearchIOException if error occur during writing.
     */
    void write(String text) throws SearchIOException;

    /**
     * Write given text on console with adding line separator string on the end.
     *
     * @param text - text to be written.
     * @throws SearchIOException if error occur during writing.
     */
    void writeln(String text) throws SearchIOException;

    /**
     * Returns unmodifiable map of all commands this environment offers.
     *
     * @return map of commands.
     */
    SortedMap<String, SearchCommand> commands();

    /**
     * Returns result of last query.
     *
     * @return list of documents which were result of last query.
     */
    List<Map.Entry<SearchDocument, Double>> lastResult();

    /**
     * Gets documents database.
     *
     * @return database.
     */
    SearchDocumentDB getDocumentDB();
}
