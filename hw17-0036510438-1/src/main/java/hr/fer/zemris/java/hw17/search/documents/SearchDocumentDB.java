package hr.fer.zemris.java.hw17.search.documents;

import hr.fer.zemris.java.hw17.search.vector.WordVector;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Database of documents.
 *
 * @author Domagoj Lokner
 */
public class SearchDocumentDB {

    /**
     * Path to directory from which documents
     * will be added to database.
     */
    private Path dir;

    /**
     * Instance of vocabulary.
     */
    private Vocabulary vocabulary;

    /**
     * List in which will be stored all documents from database.
     */
    private List<SearchDocument> documents;

    /**
     * Vector which components represents number of files
     * from this database that contains word stored on that index in
     * vocabulary.
     */
    private WordVector counterVector;

    /**
     * Constructs {@code SearchDocumentDB}.
     *
     * @param dir path to directory from which documents will be added to database.
     * @throws IOException if directory is not found or
     *                     if error occurs during reading files.
     */
    public SearchDocumentDB(Path dir) throws IOException {
        try {
            this.vocabulary = new Vocabulary(dir);
        } catch (IOException ex) {
            throw new IOException(ex);
        }
        this.dir = dir;
        constructDatabase();
    }

    /**
     * Constructs this database.
     *
     * @throws IOException if error occurs during reading files.
     */
    private void constructDatabase() throws IOException {
        DocumentsCreator creator = new DocumentsCreator();

        Files.walkFileTree(dir, creator);
        documents = creator.documents;

        this.counterVector = creator.counterVector;
    }

    /**
     * Calculates coeficient of similarity between documents.
     *
     * @param d1 first document.
     * @param d2 second document.
     * @return coeficient of similarity between documents.
     */
    public double calcDocSimilarity(SearchDocument d1, SearchDocument d2) {
        int d = documents.size();

        WordVector tfidf1 = WordVector.calcIdf(d, counterVector).mul(d1.getTf());
        WordVector tfidf2 = WordVector.calcIdf(d, counterVector).mul(d2.getTf());

        return tfidf1.cos(tfidf2);
    }

    /**
     * Gets path to directory.
     *
     * @return path to directory.
     */
    public Path getDir() {
        return dir;
    }

    /**
     * Gets vocabulary.
     *
     * @return vocabulary.
     */
    public Vocabulary getVocabulary() {
        return vocabulary;
    }

    /**
     * Gets list of all documents in this database.
     *
     * @return documents.
     */
    public List<SearchDocument> getDocuments() {
        return documents;
    }


    /**
     * {@code FileVisitor} that will create instances of {@code SearchDocument}
     * objects for all documents. Created documents will be stored into
     * {@code documents} list.
     *
     * @author Domagoj Lokner
     */
    private class DocumentsCreator extends SimpleFileVisitor<Path> {

        /**
         * Set of all words from vocabulary.
         */
        private Set<String> vocabular;

        /**
         * List of all documents
         */
        private List<SearchDocument> documents;

        /**
         * Vector which components represents number of files
         * from this database that contains word stored on that index in
         * vocabulary.
         */
        private WordVector counterVector;

        /**
         * Constructs {@code DocumentsCreator}.
         */
        public DocumentsCreator() {
            vocabular = vocabulary.getVocabular();
            documents = new LinkedList<>();

            double[] counter = new double[vocabular.size()];
            Arrays.fill(counter, 0);
            counterVector = new WordVector(counter);
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

            SearchDocument doc = new SearchDocument(file, vocabulary);
            documents.add(doc);

            WordVector tf = doc.getTf();
            for (int i = 0, n = tf.dimension(); i < n; ++i) {
                if (tf.getCommponent(i) != 0) {
                    counterVector.setCommponent(i, counterVector.getCommponent(i) + 1);
                }
            }

            return FileVisitResult.CONTINUE;
        }
    }
}
