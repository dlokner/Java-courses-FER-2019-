package hr.fer.zemris.java.hw17.search.engine;

/**
 * Indicate IO Exception in search engine and application.
 *
 * @author Domagoj Lokner
 */
public class SearchIOException extends RuntimeException {

	private static final long serialVersionUID = 7767871516182844055L;

	/**
     * Constructs {@code SearchIOException}.
     */
    public SearchIOException() {
    }

    /**
     * Constructs {@code SearchIOException} with specified detail message.
     *
     * @param message error message.
     */
    public SearchIOException(String message) {
        super(message);
    }
}
