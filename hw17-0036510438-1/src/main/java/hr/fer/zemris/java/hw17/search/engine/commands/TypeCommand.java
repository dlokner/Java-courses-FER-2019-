package hr.fer.zemris.java.hw17.search.engine.commands;

import hr.fer.zemris.java.hw17.search.documents.SearchDocument;
import hr.fer.zemris.java.hw17.search.engine.SearchCommand;
import hr.fer.zemris.java.hw17.search.engine.SearchEngine;
import hr.fer.zemris.java.hw17.search.engine.SearchEngineStatus;

import java.io.IOException;

/**
 * This command require single argument from command line. Argument must be an
 * integer number. If document with requested index is present in last <i>query</i>
 * results that document will be printed.
 *
 * @author Domagoj Lokner
 */
public class TypeCommand implements SearchCommand {

    @Override
    public SearchEngineStatus executeCommand(SearchEngine eng, String arg) {
        int index;

        try {
            index = Integer.parseInt(arg.strip());
        } catch (NumberFormatException ex) {
            eng.writeln("Argument for type command must be an integer number.");
            return SearchEngineStatus.CONTINUE;
        }

        SearchDocument doc = reachDocument(eng, index);

        if (doc != null) {
            try {
                Util.printDocument(eng, doc);
            } catch (IOException ex) {
                eng.writeln(ex.getMessage());
            }
        }

        return SearchEngineStatus.CONTINUE;
    }

    /**
     * Gets document form last results stored by given {@code index}.
     *
     * @param eng search engine.
     * @param index index of document.
     * @return document to be gotten.
     */
    private SearchDocument reachDocument(SearchEngine eng, int index) {
        try {
            return eng.lastResult().get(index).getKey();
        } catch (IndexOutOfBoundsException ex) {
            eng.writeln("Invalid document index is given.");
            return null;
        }
    }
}
