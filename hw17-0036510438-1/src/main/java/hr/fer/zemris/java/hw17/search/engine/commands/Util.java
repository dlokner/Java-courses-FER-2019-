package hr.fer.zemris.java.hw17.search.engine.commands;

import hr.fer.zemris.java.hw17.search.documents.SearchDocument;
import hr.fer.zemris.java.hw17.search.engine.SearchEngine;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Search engine commands util class.
 *
 * @author Domagoj Lokner
 */
public class Util {

    /**
     * Sorts results stored in map by value descending.
     *
     * @param results map of results.
     * @return list of sorted results.
     */
    public static List<Map.Entry<SearchDocument, Double>> sortedResults(Map<SearchDocument, Double> results) {
        Set<Map.Entry<SearchDocument, Double>> resultSet = results.entrySet();

        List<Map.Entry<SearchDocument, Double>> list = resultSet.stream()
                .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
                .collect(Collectors.toList());

        return list;
    }

    /**
     * Writes last results from search engine on search engine.
     *
     * @param eng search engine.
     */
    public static void printLastResult(SearchEngine eng) {
        List<Map.Entry<SearchDocument, Double>> printList = eng.lastResult();

        int counter = 0;
        for (Map.Entry<SearchDocument, Double> res : printList) {
            StringBuilder sb = new StringBuilder();

            sb.append("[");
            sb.append(String.format("%2d", counter));
            sb.append("]");

            sb.append(String.format("(%.4f) ", res.getValue()));

            sb.append(res.getKey().getFilePath().toString());

            eng.writeln(sb.toString());
            counter++;
        }
    }

    /**
     * Print document's content on search engine.
     *
     * @param eng search engine.
     * @param doc document to be written.
     * @throws IOException if error occurs during reading document.
     */
    public static void printDocument(SearchEngine eng, SearchDocument doc) throws IOException {

        String text;
        try {
            text = Files.readString(doc.getFilePath());
        } catch (IOException ex) {
            throw new IOException(doc.getFilePath().toString() + ": file can't be reached");
        }

        String title = "Dokument: " + doc.getFilePath().toString();

        eng.writeln("-".repeat(title.length()));
        eng.writeln(title);
        eng.writeln("-".repeat(title.length()));
        eng.writeln(text);
        eng.writeln("-".repeat(title.length()));
    }

    /**
     * Extract command name form given string.
     *
     * @param commandLine string from which command name will be extracted.
     * @return command name.
     */
    public static String extractCommand(String commandLine) {
        commandLine = commandLine.strip();

        String[] splitedCommand = commandLine.split("\\s+");

        return splitedCommand[0];
    }

    /**
     * Extracts command arguments.
     *
     * @param commandLine string from which arguments will be extracted.
     * @return arguments.
     */
    public static String extractArgument(String commandLine) {
        commandLine = commandLine.stripLeading();

        return commandLine.substring(extractCommand(commandLine).length());
    }
}
