package hr.fer.zemris.java.hw17.search.documents;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Object that stores stopwords defined in resource file "hrvatski_stoprijeci.txt".
 * This class is Singleton.
 *
 * @author Domagoj Lokner
 */
public final class Stopwords {

    /**
     * Instance of this class.
     */
    private static Stopwords instance;

    /**
     * Set of all stepwords,
     */
    private Set<String> stopwords;

    /**
     * Constructs {@code Stopwords}.
     *
     * @throws IOException if error occures during reading resource file.
     */
    private Stopwords() throws IOException {
        setStopwords();
    }

    /**
     * Returns instance of this class.
     *
     * @return instance of this class.
     * @throws IOException if error occurs during reading resource file.
     */
    public static Stopwords getInstance() throws IOException {

        if (instance == null) {
            try {
                instance = new Stopwords();
            } catch (IOException ex) {
                throw new IOException(ex);
            }
        }
        return instance;
    }

    /**
     * Parse all stopwords and stores them to {@code stopwords} property.
     *
     * @throws IOException if error occurs during reading resource file.
     */
    private void setStopwords() throws IOException {

        try (InputStream is = new BufferedInputStream(getClass().getResourceAsStream("hrvatski_stoprijeci.txt"))) {

            String stopwords = new String(is.readAllBytes(), StandardCharsets.UTF_8);

            this.stopwords = new HashSet<>();
            this.stopwords.addAll(
                    Arrays.asList(stopwords.split("\\r\\n"))
            );

        } catch (IOException ex) {
            throw new IOException("Stopwords resource is not found");
        }
    }

    /**
     * Returns unmodifiable set of all stepwords.
     *
     * @return set of stepwords.
     */
    public Set<String> getStopwords() {
        return Collections.unmodifiableSet(stopwords);
    }
}
