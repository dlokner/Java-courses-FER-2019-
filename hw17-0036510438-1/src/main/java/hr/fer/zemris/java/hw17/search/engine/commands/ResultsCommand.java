package hr.fer.zemris.java.hw17.search.engine.commands;

import hr.fer.zemris.java.hw17.search.engine.SearchCommand;
import hr.fer.zemris.java.hw17.search.engine.SearchEngine;
import hr.fer.zemris.java.hw17.search.engine.SearchEngineStatus;

/**
 * This command will write last results.
 *
 * @author Domagoj Lokner
 */
public class ResultsCommand implements SearchCommand {

    @Override
    public SearchEngineStatus executeCommand(SearchEngine eng, String arg) {

        if (!eng.lastResult().isEmpty()) {
            Util.printLastResult(eng);
        } else {
            eng.writeln("\"query\" command should be called before \"results\" command.");
        }

        return SearchEngineStatus.CONTINUE;
    }
}
