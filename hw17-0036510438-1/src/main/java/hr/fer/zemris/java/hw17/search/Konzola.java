package hr.fer.zemris.java.hw17.search;

import hr.fer.zemris.java.hw17.search.engine.SearchCommand;
import hr.fer.zemris.java.hw17.search.engine.SearchEngine;
import hr.fer.zemris.java.hw17.search.engine.SearchEngineImpl;
import hr.fer.zemris.java.hw17.search.engine.SearchEngineStatus;
import hr.fer.zemris.java.hw17.search.engine.commands.Util;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * Program for searching text files.
 * Program expects single command line argument,
 * path to directory in which are files to be searched.
 *
 * @author Domagoj Lokner
 */
public class Konzola {

    /**
     * Starting method of the program.
     *
     * @param args command line arguments.
     */
    public static void main(String[] args) {

        SearchEngine engine = null;

        try {
            engine = new SearchEngineImpl(Paths.get(args[0].strip()));
        } catch (IOException e1) {
            System.err.println("Invalid path is given");
            System.err.println(e1.getMessage());
            System.exit(1);
        } catch (IndexOutOfBoundsException e2) {
            System.err.println("Program expects single argument (path to directory)");
            System.exit(1);
        }

        startConsole(engine);
    }

    /**
     * Method that will start searching console.
     *
     * @param eng searching engine.
     */
    private static void startConsole(SearchEngine eng) {
        SearchEngineStatus status = SearchEngineStatus.CONTINUE;

        do {
            String commandLine = eng.readLine();

            String commandName = Util.extractCommand(commandLine);
            String argument = Util.extractArgument(commandLine);

            SearchCommand command = eng.commands().get(commandName);

            if (Objects.isNull(command)) {
                eng.writeln("Nepoznata naredba.");
                continue;
            }

            status = command.executeCommand(eng, argument);
            eng.writeln("");

        } while (status == SearchEngineStatus.CONTINUE);
    }
}
