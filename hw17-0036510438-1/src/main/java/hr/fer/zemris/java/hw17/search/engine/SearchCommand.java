package hr.fer.zemris.java.hw17.search.engine;

/**
 * Model of single search engine command.
 *
 * @author Domagoj Lokner
 */
public interface SearchCommand {

    /**
     * Executes search command in given search engine.
     *
     * @param eng search engine.
     * @param arg command arguments.
     */
    SearchEngineStatus executeCommand(SearchEngine eng, String arg);
}
