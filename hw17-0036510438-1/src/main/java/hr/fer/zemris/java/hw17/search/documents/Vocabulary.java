package hr.fer.zemris.java.hw17.search.documents;

import hr.fer.zemris.java.hw17.search.BagOfWordsParser;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Object that represents vocabulary of all
 * files from directory.
 *
 * @author Domagoj Lokner
 */
public class Vocabulary {

    /**
     * Path to directory from which all files will be used
     * for creating vocabulary.
     */
    private Path dir;

    /**
     * Collection of all words in vocabulary.
     */
    private Set<String> vocabulary;

    /**
     * Constructs {@code Vocabulary}.ž
     *
     * @param directory path to directory from which all files
     *                  will be used for creating vocabulary.
     * @throws IOException if file directory is not found or
     *                     if error occurs during reading files.
     */
    public Vocabulary(Path directory) throws IOException {
        Objects.requireNonNull(directory);

        if (!Files.isDirectory(directory)) {
            throw new IOException(directory.toString() + ": directory not found.");
        } else {
            dir = directory;
        }

        vocabulary = new LinkedHashSet<>();
        updateVocabulary();
    }

    /**
     * Updates vocabulary.
     *
     * @throws IOException if error occurs during reading files.
     */
    public void updateVocabulary() throws IOException {
        FilesContent filesVisitor = new FilesContent();
        Files.walkFileTree(dir, filesVisitor);

        String filesContent = filesVisitor.getFilesContent();

        BagOfWordsParser parser = new BagOfWordsParser(filesContent);

        vocabulary.addAll(parser.getBagOfWords().keySet());
    }


    /**
     * {@code FileVisitor} class that will store content of all files.
     */
    private class FilesContent extends SimpleFileVisitor<Path> {

        private StringBuilder sb;

        /**
         * Constructs {@code FilesContent}.
         */
        public FilesContent() {
            this.sb = new StringBuilder();
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            sb.append(Files.readString(file));
            sb.append(" ");
            return FileVisitResult.CONTINUE;
        }

        /**
         * Gets content of all files.
         *
         * @return content of all files.
         */
        private String getFilesContent() {
            return sb.toString();
        }
    }

    /**
     * Gets path to directory.
     *
     * @return directory.
     */
    public Path getDir() {
        return dir;
    }

    /**
     * Gets unmodifiable set of all words
     * form vocabulary.
     *
     * @return vocabulary.
     */
    public Set<String> getVocabular() {
        return Collections.unmodifiableSet(vocabulary);
    }

}
