package hr.fer.zemris.java.hw17.search.engine;

import hr.fer.zemris.java.hw17.search.documents.SearchDocument;
import hr.fer.zemris.java.hw17.search.documents.SearchDocumentDB;
import hr.fer.zemris.java.hw17.search.engine.commands.ExitCommand;
import hr.fer.zemris.java.hw17.search.engine.commands.QueryCommand;
import hr.fer.zemris.java.hw17.search.engine.commands.ResultsCommand;
import hr.fer.zemris.java.hw17.search.engine.commands.TypeCommand;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

/**
 * Concrete implementation of {@code SearchEngine} interface.
 * This implementation will writes on system output and commands will
 * be read from standard input.
 *
 * @author Domagoj Lokner
 */
public class SearchEngineImpl implements SearchEngine {

    /**
     * Map of registered commands.
     */
    SortedMap<String, SearchCommand> commands;

    /**
     * List in which last results will be stored.
     */
    List<Map.Entry<SearchDocument, Double>> lastResult;

    /**
     * Scanner for reading commands.
     */
    Scanner sc;

    /**
     * Documents database.
     */
    SearchDocumentDB database;

    /**
     * Path to directory which files will be searched.
     */
    Path dir;


    /**
     * Constructs {@code SearchEngineImpl}.
     *
     * @param dir path to directory which files will be searched.
     * @throws IOException
     */
    public SearchEngineImpl(Path dir) throws IOException {
        sc = new Scanner(System.in);

        database = new SearchDocumentDB(dir);


        this.commands = new TreeMap<>();
        this.lastResult = new LinkedList<>();
        this.dir = dir;

        initCommandMap();

        writeln("Veličina rječnika je " + database.getVocabulary().getVocabular().size() + " riječi.");
        writeln("");
    }

    /**
     * Initialize commands.
     */
    private void initCommandMap() {
        commands.put("query", new QueryCommand());
        commands.put("type", new TypeCommand());
        commands.put("results", new ResultsCommand());
        commands.put("exit", new ExitCommand());
    }

    @Override
    public String readLine() throws SearchIOException {
        System.out.print("Enter command > ");
        return sc.nextLine();
    }

    @Override
    public void write(String text) throws SearchIOException {
        System.out.print(text);
    }

    @Override
    public void writeln(String text) throws SearchIOException {
        System.out.println(text);
    }

    @Override
    public SortedMap<String, SearchCommand> commands() {
        return Collections.unmodifiableSortedMap(commands);
    }

    @Override
    public List<Map.Entry<SearchDocument, Double>> lastResult() {
        return lastResult;
    }

    @Override
    public SearchDocumentDB getDocumentDB() {
        return database;
    }

}
