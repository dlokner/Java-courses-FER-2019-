package hr.fer.zemris.java.hw17.search.engine;

/**
 * Constants that indicates statuses of search engine.
 *
 * @author Domagoj Lokner
 */
public enum SearchEngineStatus {
    CONTINUE,
    TERMINATE
}
