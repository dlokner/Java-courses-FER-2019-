package hr.fer.zemris.java.custom.scripting.parser;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;

class SmartScriptParserTest {

	@Test
	void numberOfChildrenTest() {
		SmartScriptParser parser = new SmartScriptParser(loader("document1.txt"));
		
		Node docNode = parser.getDocumentNode();
		
		assertEquals(4, docNode.numberOfChildren());
		assertEquals(3, docNode.getChild(1).numberOfChildren());
		assertEquals(5, docNode.getChild(3).numberOfChildren());
		assertEquals(0, docNode.getChild(2).numberOfChildren());
	}

	@Test
	void forLoopNodeTest() {
		SmartScriptParser parser = new SmartScriptParser(loader("document1.txt"));
		
		Node docNode = parser.getDocumentNode();
		
		assertEquals(new ForLoopNode(new ElementVariable("i"),
				new ElementConstantInteger(1), 
				new ElementConstantInteger(10), 
				new ElementConstantInteger(1)), docNode.getChild(1));
		
		assertEquals(new ForLoopNode(new ElementVariable("i"),
				new ElementConstantInteger(0), 
				new ElementConstantInteger(10), 
				new ElementConstantInteger(2)), docNode.getChild(3));
	}
	
	@Test
	void echoNodeTest() {
		SmartScriptParser parser = new SmartScriptParser(loader("document1.txt"));
		
		Node docNode = parser.getDocumentNode();
		
		assertEquals(new EchoNode(new Element[] {
				new ElementVariable("i"),
				new ElementVariable("i"), 
				new ElementOperator("*"), 
				new ElementFunction("sin"),
				new ElementString("0.000"),
				new ElementFunction("decfmt"),
					}), docNode.getChild(3).getChild(3));
	}
	
	@Test
	void numberOfChildrenTest2() {
		SmartScriptParser parser = new SmartScriptParser(loader("document2.txt"));
		
		Node docNode = parser.getDocumentNode();
		
		assertEquals(3, docNode.numberOfChildren());
		assertEquals(1, docNode.getChild(0).numberOfChildren());
	}
	
	@Test
	void forLoopNodeTest2() {
		SmartScriptParser parser = new SmartScriptParser(loader("document2.txt"));
		
		Node docNode = parser.getDocumentNode();
		
		assertEquals(new ForLoopNode(new ElementVariable("bbb"),
				new ElementString("3ki"), 
				new ElementConstantDouble(14.125), 
				new ElementString("Štefica")), docNode.getChild(0));
	}
	
	@Test
	void echoNodeTest2() {
		SmartScriptParser parser = new SmartScriptParser(loader("document2.txt"));
		
		Node docNode = parser.getDocumentNode();
		
		assertEquals(new EchoNode(new Element[] {
				new ElementOperator("+"),
				new ElementVariable("rg_23_f4"), 
				new ElementOperator("*"), 
				new ElementFunction("function"),
				new ElementString("Štefica \"Štefa\"")
					}), docNode.getChild(2));
	}

	
	@Test
	void uableToParseOpenTagOnTheEnd() {
		assertThrows(SmartScriptParserException.class, () -> {
			new SmartScriptParser(loader("document3.txt"));
		});
	}
	
	@Test
	void uableToParseEndMissing() {
		assertThrows(SmartScriptParserException.class, () -> {
			new SmartScriptParser(loader("document4.txt"));
		});
	}
	
	@Test
	void uableToParseWrongVarName() {
		assertThrows(SmartScriptParserException.class, () -> {
			new SmartScriptParser(loader("document5.txt"));
		});
	}
	
	@Test
	void uableToParseUnvalideEscape() {
		assertThrows(SmartScriptParserException.class, () -> {
			new SmartScriptParser(loader("document6.txt"));
		});
	}
	
	private String loader(String filename) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		try(InputStream is =
				this.getClass().getClassLoader().getResourceAsStream(filename)) {
			byte[] buffer = new byte[1024];
			while(true) {
				int read = is.read(buffer);
				if(read<1) break;
					bos.write(buffer, 0, read);
		}
			return new String(bos.toByteArray(), StandardCharsets.UTF_8);
		} catch(IOException ex) {
			return null;
		}
	}
}
