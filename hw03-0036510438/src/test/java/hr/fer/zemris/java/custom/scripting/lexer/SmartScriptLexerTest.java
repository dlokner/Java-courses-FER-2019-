package hr.fer.zemris.java.custom.scripting.lexer;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.custom.scripting.token.SmartScriptToken;
import hr.fer.zemris.java.custom.scripting.token.SmartScriptTokenType;

class SmartScriptLexerTest {

	@Test
	public void testMultipartInput() {

		SmartScriptLexer lexer = new SmartScriptLexer("This is \n{$ FOR i-1.35bbb\"1\" $} This is echo {$ =i_tadam$}-th time {$END$}");

		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, "This is \n"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.OPEN_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TAGS);
		
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG, "FOR"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.VARIABLE, "i"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.DOUBLE, -1.35));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.VARIABLE, "bbb"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.STRING, "1"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CLOSED_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TEXT);
			
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, " This is echo "));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.OPEN_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TAGS); 
		
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG, "="));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.VARIABLE, "i_tadam"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CLOSED_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TEXT);
		
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, "-th time "));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.OPEN_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TAGS); 
		
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG, "END"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CLOSED_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TEXT);

		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.EOF, null));
		assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());


	}
	
	@Test
	public void backslashOnTheEnd() {

		SmartScriptLexer lexer = new SmartScriptLexer("This is {$ FOR i-1.35bbb\"1\" $} text {$END$}");

		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, "This is "));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.OPEN_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TAGS);
		
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG, "FOR"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.VARIABLE, "i"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.DOUBLE, -1.35));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.VARIABLE, "bbb"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.STRING, "1"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CLOSED_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TEXT);
			
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, " text "));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.OPEN_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TAGS); 
		
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG, "END"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CLOSED_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TEXT);

		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.EOF, null));
		assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());


	}
	
	@Test
	public void escapeingTagInText() {

		SmartScriptLexer lexer = new SmartScriptLexer("This is \\{$= i $} \n{$ i $}");

		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, "This is {$= i $} \n"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.OPEN_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TAGS);
		
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG, "i"));

		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CLOSED_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TEXT);

		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.EOF, null));
		assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());


	}
	
	@Test
	public void escapeingString() {

		SmartScriptLexer lexer = new SmartScriptLexer("{$ FOR i-1.35bbb\"Štefica \\\"Štefa\\\" Štefanija\" $} {$END$}");

		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.OPEN_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TAGS);
		
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG, "FOR"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.VARIABLE, "i"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.DOUBLE, -1.35));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.VARIABLE, "bbb"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.STRING, "Štefica \"Štefa\" Štefanija"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CLOSED_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TEXT);
			
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, " "));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.OPEN_TAG, null));

		lexer.setState(SmartScriptLexerState.TAGS); 
		
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG, "END"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CLOSED_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TEXT);

		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.EOF, null));
		assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());
	}
	
	@Test
	public void unvalidTagName() {

		SmartScriptLexer lexer = new SmartScriptLexer("{$ 1tag i-1.35bbb $} {$END$}");

		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.OPEN_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TAGS);
		
		assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());

	}
	
	@Test
	public void escapeBackslash() {

		SmartScriptLexer lexer = new SmartScriptLexer("This \\\\is \\{$= i $} \n{$ i $}");

		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TEXT, "This \\is {$= i $} \n"));
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.OPEN_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TAGS);
		
		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.TAG, "i"));

		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.CLOSED_TAG, null));
		
		lexer.setState(SmartScriptLexerState.TEXT);

		checkToken(lexer.nextToken(), new SmartScriptToken(SmartScriptTokenType.EOF, null));
		assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());


	}
	
	private void checkToken(SmartScriptToken actual, SmartScriptToken expected) {
			String msg = "Token are not equal.";
			assertEquals(expected.getValue(), actual.getValue(), msg);
			assertEquals(expected.getType(), actual.getType(), msg);
			assertEquals(expected.getValue(), actual.getValue(), msg);
			
	}

}
