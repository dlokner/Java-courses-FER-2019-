package hr.fer.zemris.java.hw03.prob1;

/**
 * Define working states of lexer.
 * 
 * @author Domagoj Lokner
 *
 */
public enum LexerState {
	BASIC,
	EXTENDED
}
