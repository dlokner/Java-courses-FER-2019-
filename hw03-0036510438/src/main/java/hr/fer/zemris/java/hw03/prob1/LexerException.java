package hr.fer.zemris.java.hw03.prob1;

/**
 * Thrown to indicate an error in lexer.
 * 
 * @author Domagoj Lokner
 *
 */
public class LexerException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructs {@code LexerException}.
	 */
	public LexerException() {
		super();
	}
	
	/**
	 * COnstructs {@code LexerException} with specified detail message.
	 * 
	 * @param message - given message.
	 */
	public LexerException(String message) {
		super(message);
	}
	
}
