package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Represents function expression.
 * 
 * @author Domagoj Lokner
 *
 */
public class ElementFunction extends Element {
	private String name;
	
	/**
	 * Constructs {@code ElementFunction} with {@code name}.
	 * 
	 * @param name - name of function.
	 */
	public ElementFunction(String name) {
		this.name = name;
	}
	
	/**
	 * Returns {@code name} of function.
	 * 
	 * @return {@code name}.
	 */
	@Override
	public String asText() {
		return "@" + name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ElementFunction)) {
			return false;
		}
		ElementFunction other = (ElementFunction) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
	
	
}
