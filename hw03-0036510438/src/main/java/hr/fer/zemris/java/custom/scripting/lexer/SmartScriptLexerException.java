package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Thrown to indicate an error in {@code SmartScriptLexerException}.
 * 
 * @author Domagoj Lokner
 *
 */
public class SmartScriptLexerException extends RuntimeException {
		
	private static final long serialVersionUID = 5936656131853783560L;

	/**
	 * Constructs {@code SmartScriptLexerException}.
	 */
	public SmartScriptLexerException() {
		super();
	}
	
	/**
	 * COnstructs {@code SmartScriptLexerException} with specified detail message.
	 * 
	 * @param message - given message.
	 */
	public SmartScriptLexerException(String message) {
		super(message);
	}
}
