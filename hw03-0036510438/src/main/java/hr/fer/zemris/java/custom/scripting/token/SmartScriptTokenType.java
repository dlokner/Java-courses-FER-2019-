package hr.fer.zemris.java.custom.scripting.token;

/**
 * Define types of tokens used by {@code SmartScriptLexer}.
 * 
 * @author Domagoj Lokner
 *
 */
public enum SmartScriptTokenType {
	TEXT, //standard text
	OPEN_TAG, // "{$"
	CLOSED_TAG, //  "$}"
	STRING, // "string"
	FUNCTION, //  @function
	VARIABLE, //i, gh_13, a_3ha
	DOUBLE, //0.234234
	INTEGER, //1595452
	OPERATOR, //*, +, -, /
	TAG, // FOR, =
	EOF
}
