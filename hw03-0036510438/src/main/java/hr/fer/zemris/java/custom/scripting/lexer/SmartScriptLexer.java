package hr.fer.zemris.java.custom.scripting.lexer;

import java.util.Objects;

import hr.fer.zemris.java.custom.scripting.token.SmartScriptToken;
import hr.fer.zemris.java.custom.scripting.token.SmartScriptTokenType;

/**
 * Converts string given to constructor in tokens.
 * 
 * <p>
 * Lexer can work in two different states - TEXT and TAGS.<br>
 * 
 * <ul>
 * <li>In TEXT state lexer will interpret input as string until i reaches unescaped "{$" set od characters.
 * Input be sent as one TEXT type token.<br>
 * 
 * <li>In TAGS state lexer will be working by new rules
 * <ul>
 * 	<li> numbers will interpret as integer or double if is floating point number.
 * 	<li> set of characters between two ' " ' signs will be interpret as string. 
 * 		In string escaping is allowed for backslash and quoting marks.
 * 	<li> all sets of characters starting with letter and containing only letters, numbers and underscores
 * 		will bew interpret as variable.
 * 	<li> variable name which foolows '@' signe will be interpreat as function name.
 * 	<li> variable name or '=' sign after "{$" will be interpret as tag.
 * </ul>
 * </ul>
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class SmartScriptLexer {
	
	private char[] data;
	private SmartScriptToken token;
	private int currentIndex;
	private SmartScriptLexerState state;
	
	/**
	 * Constructs lexer with {@code text} to be tokenized.
	 * 
	 * @param text - string to be tokenize.
	 */
	public SmartScriptLexer(String text) {
		this.data = text.toCharArray();
		state = SmartScriptLexerState.TEXT;
		token = new SmartScriptToken(null, null);
	}

	/**
	 * Generate and returns next token.
	 * 
	 * @return next token.
	 * @throws SmartScriptLexerException if an error occurs.
	 */
	public SmartScriptToken nextToken() {
		if(state == SmartScriptLexerState.TEXT) {
			return token = nextTokenText();
		} else {
			return token = nextTokenTag();
		}
	}
	
	/**
	 * Returns last generated token.
	 * 
	 * @return last token.
	 */
	public SmartScriptToken getToken() {
		return token;
	}
	
	/**
	 * Sets working state of lexer.
	 * 
	 * @param state - lexer working state.
	 */
	public void setState(SmartScriptLexerState state) {
		Objects.requireNonNull(state);
		this.state = state;
	}
	
	/**
	 * Generate next token by TEXT state rules.
	 * 
	 * @return next token.
	 */
	private SmartScriptToken nextTokenText() {
		
		if(token.getType() == SmartScriptTokenType.EOF && token.getValue() == null) {
			throw new SmartScriptLexerException();
		}
		
		if(data.length <= currentIndex) {
			return new SmartScriptToken(SmartScriptTokenType.EOF, null);
		}
		
		if(data[currentIndex] == '{' && data[currentIndex+1] == '$') {
			currentIndex += 2;
			return new SmartScriptToken(SmartScriptTokenType.OPEN_TAG, null);
		}
		
		if(data[currentIndex] == '\\'){
			++currentIndex;
			
			if(currentIndex >= data.length) {
				throw new SmartScriptLexerException();
			} else if(data[currentIndex] != '"') {
				if(data[currentIndex] != '{' || data[currentIndex] != '$') {
					throw new SmartScriptLexerException();
				}
			}
		}	
		
		return new SmartScriptToken(SmartScriptTokenType.TEXT, extractText());
	}
	
	
	/**
	 * Generate next token by TAGS state rules.
	 * 
	 * @return next token.
	 */
	private SmartScriptToken nextTokenTag() {
		
		if(token.getType() == SmartScriptTokenType.EOF && token.getValue() == null) {
			throw new SmartScriptLexerException();
		}
		
		skipBlanks();
		
		if(data.length <= currentIndex) {
			return new SmartScriptToken(SmartScriptTokenType.EOF, null);
		}
		
		if(data[currentIndex] == '$' && data[currentIndex+1] == '}') {
			currentIndex += 2;
			return new SmartScriptToken(SmartScriptTokenType.CLOSED_TAG, null);
		}
		
		if(token.getType() == SmartScriptTokenType.OPEN_TAG) {
			if(data[currentIndex] == '=') {
				++currentIndex;
				return new SmartScriptToken(SmartScriptTokenType.TAG, "=");
			} else if(Character.isLetter(data[currentIndex])) {
				return new SmartScriptToken(SmartScriptTokenType.TAG, extractVariable());
			} else {
				throw new SmartScriptLexerException();
			}
		}
		
		if(Character.isLetter(data[currentIndex])) {
			return new SmartScriptToken(SmartScriptTokenType.VARIABLE, extractVariable());
		}
		
		if(data[currentIndex] == '"') {
			return new SmartScriptToken(SmartScriptTokenType.STRING, extractString());
		}
		
		if(Character.isDigit(data[currentIndex]) || Character.isDigit(data[currentIndex+1]) && data[currentIndex] == '-') {
			String number = extractNumber();
			
				try {
					return new SmartScriptToken(SmartScriptTokenType.INTEGER, Integer.parseInt(number));
				} catch(NumberFormatException ex1) {
					try {
						return new SmartScriptToken(SmartScriptTokenType.DOUBLE, Double.parseDouble(number));
					} catch(NumberFormatException ex2) {
						throw new SmartScriptLexerException();
					}
				}
		}

		if(data[currentIndex] == '@') {
			if(Character.isLetter(data[currentIndex+1])) {
				return new SmartScriptToken(SmartScriptTokenType.FUNCTION, extractFunction());
			} else {
				throw new SmartScriptLexerException();
			}
		}
		
		if(isOperator(data[currentIndex])) {
			return new SmartScriptToken(SmartScriptTokenType.OPERATOR, Character.toString(data[currentIndex++]));
		}
		
		throw new SmartScriptLexerException();
	}
	
	/**
	 * Extract text starting with current index.
	 * 
	 * @return text.
	 */
	private String extractText() {
		StringBuilder sb = new StringBuilder();
		
		do {
			if(data[currentIndex] == '\\') {
				
				if(data[currentIndex+1] == '\\') {
					sb.append('\\');
					currentIndex += 2; 
					continue;
				}
				
				if(data[currentIndex+1] == '{' && data[currentIndex+2] == '$') {
					sb.append("{$");
					currentIndex += 3;
					continue;
				}
				
//				This code is used if "{" should be escaped same as "{$"
//				
//				if(data[currentIndex+1] == '{') {
//					sb.append("{");
//					currentIndex += 2;
//					continue;
//				}
				
				throw new SmartScriptLexerException();
				
			} else if(data[currentIndex] == '{' && data[currentIndex+1] == '$') {
				break;
			}
			
			sb.append(data[currentIndex++]);
			
		} while(currentIndex < data.length);
		
		return sb.toString();	
	}
	
	
	/**
	 * Extract value of string type token.
	 * 
	 * @return string.
	 */
	private String extractString() {
		StringBuilder sb = new StringBuilder();
		
		currentIndex++;
		
		do {
			
			/* if string is not closed ("...) */
			if(currentIndex >= data.length) {
				throw new SmartScriptLexerException();
			}
			
			if(data[currentIndex] == '\\') {
				if(data[currentIndex+1] == '\\') {
					sb.append('\\');
					currentIndex += 2; 
					continue;
				}
				
				if(data[currentIndex+1] == '"') {
					sb.append('"');
					currentIndex += 2;
					continue;
				}
				
				if(data[currentIndex+1] == 'n') {
					sb.append('\n');
					currentIndex += 2;
					continue;
				}
				
				if(data[currentIndex+1] == 'r') {
					sb.append('\r');
					currentIndex += 2;
					continue;
				}
				
				if(data[currentIndex+1] == 't') {
					sb.append('\t');
					currentIndex += 2;
					continue;
				}
				
				throw new SmartScriptLexerException();
				
			} else if(data[currentIndex] == '{' && data[currentIndex+1] == '$') {
				break;
			}
			
			sb.append(data[currentIndex++]);
			
		} while(data[currentIndex] != '"');
		
		currentIndex++;

		return sb.toString();	
	}
	
	/**
	 * Variable name starting character on {@code currentIndex}.
	 * 
	 * @return variable name.
	 */
	private String extractVariable() {
		StringBuilder sb = new StringBuilder();
		
		do {
			if(Character.isLetter(data[currentIndex]) || Character.isDigit(data[currentIndex])) {
				sb.append(data[currentIndex++]);
				continue;
			} 
			
			if(data[currentIndex] == '_') {
				sb.append('_');
				currentIndex++;
				continue;
			}
			
			break;
			
		} while(currentIndex < data.length);
		
		return sb.toString();
	}
	
	/**
	 * String representation of number starting from {@code currentIndex}. 
	 * 
	 * @return number.
	 */
	private String extractNumber() {
		StringBuilder sb = new StringBuilder();
		boolean floatingPoint = false;
		
		if(data[currentIndex] == '-') {
			sb.append('-');
			currentIndex++;
		}
		
		do {
			if(Character.isDigit(data[currentIndex])){
				sb.append(data[currentIndex]);
				currentIndex++;
				continue;
			}
			if(data[currentIndex] == '.' && !floatingPoint){
				sb.append(data[currentIndex]);
				currentIndex++;
				continue;
			}
			break;
		} while(currentIndex < data.length);
		
		return sb.toString();
	}
	
	/**
	 * Checks is given sign one of allowed operators.
	 * 
	 * @param signe -sign to be checked.
	 * @return {@code true} if sign is operator otherwise {@code false}.
	 */
	private boolean isOperator(char signe) {
		
		if(signe == '+') {
			return true;
		}
		
		if(signe == '-') {
			return true;
		}
		
		if(signe == '*') {
			return true;
		}
		
		if(signe == '/') {
			return true;
		}
		
		if(signe == '^') {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Extracts function name.
	 * 
	 * @return function name.
	 */
	private String extractFunction() {
		currentIndex++;
		return extractVariable();
	}
	
	/**
	 * Set {@code currentIndex} on first position thats not whitespace.
	 */
	private void skipBlanks() {
		while(currentIndex < data.length && Character.isWhitespace(data[currentIndex])) {
			++currentIndex;
		}
	}
}
