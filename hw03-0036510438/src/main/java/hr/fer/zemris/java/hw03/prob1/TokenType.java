package hr.fer.zemris.java.hw03.prob1;

/**
 * Define token types.
 * 
 * @author Domagoj Lokner
 *
 */
public enum TokenType {
	EOF, 
	WORD, 
	NUMBER, 
	SYMBOL
}
