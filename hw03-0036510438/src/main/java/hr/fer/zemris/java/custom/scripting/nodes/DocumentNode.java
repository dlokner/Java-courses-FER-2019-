package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * {@code Node} representing an entire document.
 * 
 * @author Domagoj Lokner
 *
 */
public class DocumentNode extends Node {

}
