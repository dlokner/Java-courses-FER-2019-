package hr.fer.zemris.java.custom.collections;

/**
 * Thrown to indicate that there is no elements stored on stack.
 * 
 * @author Domagoj Lokner
 *
 */
public class EmptyStackException extends RuntimeException{
	
	private static final long serialVersionUID = -6807996960060013272L;
	
	/**
	 * Constructs an {@code EmptyStackException} without arguments.
	 */
	public EmptyStackException() {
        super();
    }
    
	/**
	 * Constructs an {@code EmptyStackException} with given message.
	 * 
	 * @param message - specified detail message.
	 */
	public EmptyStackException(String message) {
        super(message);
    }
}
