package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Objects of this class represents expressions of the language.
 * 
 * @author Domagoj Lokner
 *
 */
public class Element {
	
	/**
	 * Returns an empty string.
	 * 
	 * @return empty string.
	 */
	public String asText() {
		return "";
	}
	
	
}
