package hr.fer.zemris.java.hw03;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

import java.nio.file.Files;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

/**
 * Main program for testing {@code SmartScriptParser}.<br>
 * Program expect single command line argument 
 * that represent full path to document to be parsed.<br>
 * If parsing was successfully finished program will print reconstructed input document.
 * 
 * @author Domagoj Lokner
 *
 */
public class SmartScriptTester {

	/**
	 * Starting method of program.
	 * 
	 * @param args - command line arguments.
	 */
	public static void main(String[] args) {
		String docBody = "";
		try {
			docBody = new String(Files.readAllBytes(Paths.get(args[0])), StandardCharsets.UTF_8);
		} catch (IndexOutOfBoundsException e1) {
			System.out.println("Program expects single argument from command line!");
			System.exit(1);
		} catch (IOException e2) {
			System.out.println("File can't be reached!");
			System.exit(1);
		}
		
		SmartScriptParser parser = null;
		
		try {
			parser = new SmartScriptParser(docBody);
		} catch (SmartScriptParserException e) {
			System.out.println("Unable to parse document!");
			System.exit(-1);
		} catch (Exception e) {
			System.out.println("If this line ever executes, you have failed this class!");
			System.exit(-1);
		}
		
		DocumentNode document = parser.getDocumentNode();
		String originalDocumentBody = createOriginalDocumentBody(document);
		
		System.out.println(originalDocumentBody); // should write something like original
													// content of docBody
	}

	/**
	 * Reconstructs document body based on given {@code document} node.
	 * 
	 * @param document - node representing document to be reconstructed.
	 * @return reconstruction of document as string.
	 */
	private static String createOriginalDocumentBody(Node document) {
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0; i < document.numberOfChildren(); ++i) {		
			sb.append(document.getChild(i).toString());
		}
		
		return sb.toString();
	}
}
