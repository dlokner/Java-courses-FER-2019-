package hr.fer.zemris.java.hw03.prob1;

import java.util.Objects;

/**
 * Converts string given to constructor in tokens.
 * 
 * <p>
 * Lexer offers two working states - BASIC and EXTENDED.<br>
 * 
 * <li>In BASIC state lexer returns tokens with words, numbers or signes, escaped numbers will be read as words.
 * Whitespaces will be ignored. Backslash sines must be escaped to be read as backslash.<br>
 * 
 * <li>In EXTENDED state lexer returns only word type tokens. 
 * All groups of signes, letters or numbers will be interpreted as word.
 * </p>
 * 
 * <p>
 * Lexer states are defined in {@code LexerState} enum.
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class Lexer {
	private char[] data; 
	private Token token; 
	private int currentIndex; 
	private LexerState state;
	
	/**
	 * Constructs lexer with {@code text} to be tokenized.
	 * 
	 * @param text - string to be tokenized.
	 */
	public Lexer(String text) {
		data = text.toCharArray();
		state = LexerState.BASIC;
		token = new Token(null, null);
	}
	
	/**
	 * Generate and returns next token.
	 * 
	 * @return next token.
	 * @throws LexerException if an error occurs.
	 */
	public Token nextToken() {		
		if(state == LexerState.BASIC) {
			return token = nextTokenBasic();
		} else {
			return token = nextTokenExtended();
		}
	}
	
	/**
	 * Generates next token in BASIC state.
	 * 
	 * @return next token.
	 */
	private Token nextTokenBasic() {
		
		if(token.getType() == TokenType.EOF && token.getValue() == null) {
			throw new LexerException();
		}
		
		skipBlanks();
		
		if(data.length <= currentIndex) {
			return new Token(TokenType.EOF, null);
		}
		
		if(data[currentIndex] == '\\'){
			++currentIndex;
			
			if(currentIndex >= data.length) {
				throw new LexerException();
			} else if(!Character.isDigit(data[currentIndex])) {
				if(data[currentIndex] != '\\') {
					throw new LexerException();
				}
			}
		}	
		
		if(Character.isLetter(data[currentIndex]) || data[currentIndex-1] == '\\') {
			return new Token(TokenType.WORD, extractWordBasic());
		}
		
		if(Character.isDigit(data[currentIndex])) {
			return new Token(TokenType.NUMBER, extractNumber());
		}
		
		return new Token(TokenType.SYMBOL, Character.valueOf(data[currentIndex++]));
	}
	
	/**
	 * Generate next token in EXTENDED state.
	 * 
	 * @return next token.
	 */
	private Token nextTokenExtended() {
		
		if(token.getType() == TokenType.EOF && token.getValue() == null) {
			throw new LexerException();
		}
		
		skipBlanks();
		
		if(data.length <= currentIndex) {
			return new Token(TokenType.EOF, null);
		}
		
		if(data[currentIndex] == '#'){
			return new Token(TokenType.SYMBOL, data[currentIndex++]);
		}
			
		return new Token(TokenType.WORD, extractWordExtended());
	}
	
	/**
	 * Returns last generated token.
	 * 
	 * @return last token.
	 */
	public Token getToken() {
		return token;
	}
	
	/**
	 * Set {@code currentIndex} on first position thats not whitespace.
	 */
	private void skipBlanks() {
		while(currentIndex < data.length && Character.isWhitespace(data[currentIndex])) {
			++currentIndex;
		}
	}
	
	/**
	 * Sets working state of lexer.
	 * 
	 * @param state - lexer working state.
	 */
	public void setState(LexerState state) {
		Objects.requireNonNull(state);
		this.state = state;
	}
	
	/**
	 * Extract word by BASC state rules.
	 * 
	 * @return word.
	 */
	private String extractWordBasic() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(data[currentIndex++]);
		
		do {
			if(data[currentIndex] == '\\' && (Character.isDigit(data[currentIndex+1]) || data[currentIndex+1] == '\\')) {
				sb.append(data[currentIndex+1]);
				currentIndex += 2;
				continue;
			} else if(Character.isLetter(data[currentIndex])) {
				sb.append(data[currentIndex]);
				++currentIndex;
				continue;
			}
			break;
			
		} while(currentIndex < data.length);
		
		return sb.toString();
	}
	
	/**
	 * Extract word by EXTENDED state rules.
	 * 
	 * @return word.
	 */
	private String extractWordExtended() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(data[currentIndex++]);
		
		do {
			if(data[currentIndex] == '#' || Character.isWhitespace(data[currentIndex])) {
				break;
			}
			sb.append(data[currentIndex++]);
		} while(currentIndex < data.length);
		
		return sb.toString();
	}
	
	/**
	 * Extract number.
	 * 
	 * @return number.
	 */
	private Long extractNumber() {
		StringBuilder sb = new StringBuilder();
		
		while(currentIndex < data.length && Character.isDigit(data[currentIndex])) {
			
			sb.append(data[currentIndex++]);
			
		}
		try {
			return Long.parseLong(sb.toString());
		} catch(NumberFormatException ex) {
			throw new LexerException();
		}
	}
}