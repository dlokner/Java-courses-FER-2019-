package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Represents string expression element.
 * 
 * @author Domagoj Lokner
 *
 */
public class ElementString extends Element {
	private String value;
	
	/**
	 * Constructs {@code ElementString} with {@code value}.
	 * 
	 * @param value - string value.
	 */
	public ElementString(String value) {
		this.value = value;
	}
	
	/**
	 * Returns {@code value}.
	 * 
	 * @return {@code value}.
	 */
	@Override
	public String asText() {
		StringBuilder sb = new StringBuilder();
		
		sb.append('\"');
		
		
		char[] array = value.toCharArray();
		
		for(int i = 0; i < array.length; ++i) {
			
			if(array[i] == '"'){
				sb.append("\\\"");
				continue;
			}
			
			sb.append(array[i]);
		}
		
		sb.append('\"');
		
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ElementString)) {
			return false;
		}
		ElementString other = (ElementString) obj;
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		return true;
	}
	
	
}
