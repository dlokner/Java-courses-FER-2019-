package hr.fer.zemris.java.hw03.prob1;

/**
 * Representation of one token.
 * 
 * @author Domagoj Lokner
 *
 */
public class Token {
	
	private TokenType type;
	private Object value;
	
	/**
	 * Constructs token.
	 * 
	 * @param type - type of token.
	 * @param value - value of token.
	 */
	public Token(TokenType type, Object value) {
		this.type = type;
		this.value = value;
	}
	
	/**
	 * Gets value of token.
	 * 
	 * @return token value.
	 */
	public Object getValue() {
		return value;
	}
	
	/**
	 * Gets type of token.
	 * 
	 * @return token type.
	 */
	public TokenType getType() {
		return type;
	}
}
