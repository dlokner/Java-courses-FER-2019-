package hr.fer.zemris.java.custom.scripting.token;

/**
 * Represents a single token.
 * 
 * @author Domagoj Lokner
 *
 */
public class SmartScriptToken {
	
	private SmartScriptTokenType type;
	private Object value;
	
	/**
	 * Constructs token.
	 * 
	 * @param type - type of token.
	 * @param value - token value.
	 */
	public SmartScriptToken(SmartScriptTokenType type, Object value) {
		this.type = type;
		this.value = value;
	}
	
	/**
	 * Gets value of token.
	 * 
	 * @return token value.
	 */
	public Object getValue() {
		return value;
	}
	
	/**
	 * Gets type of token.
	 * 
	 * @return token type.
	 */
	public SmartScriptTokenType getType() {
		return type;
	}
	
	
}
