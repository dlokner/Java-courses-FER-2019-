package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Represents double value expression.
 * 
 * @author Domagoj Lokner
 *
 */
public class ElementConstantDouble extends Element {
	private double value;
	
	/**
	 * Constructs {@code ElementConstantDouble} with {@code value}.
	 * 
	 * @param value - double value.
	 */
	public ElementConstantDouble(double value) {
		this.value = value;
	}
	
	/**
	 * Returns {@code String} representation of double value.
	 * 
	 * @return double value as {@code String}.
	 */
	@Override
	public String asText() {
		return Double.valueOf(value).toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ElementConstantDouble)) {
			return false;
		}
		ElementConstantDouble other = (ElementConstantDouble) obj;
		if (Double.doubleToLongBits(value) != Double.doubleToLongBits(other.value)) {
			return false;
		}
		return true;
	}
	
	
	
}
