package hr.fer.zemris.java.hw01;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class FactorialTest {
	
	@Test 
	public void factArg0() {
		long n = Factorial.fact(0);
		assertEquals(0, n);
	}
	
	@Test
	public void factArg3() {
		long n = Factorial.fact(3);
		assertEquals(6, n);
	}
	
	@Test
	public void factArg19() {
		long n = Factorial.fact(19);
		assertEquals(121645100408832000l, n);
	}
	
	@Test
	public void factNegativeNumber() {
		assertThrows(IllegalArgumentException.class, () -> Factorial.fact(-5));
	}
	
	@Test
	public void factArgNumOver20() {
		assertThrows(IllegalArgumentException.class, () -> Factorial.fact(21));
	}

}
