package hr.fer.zemris.java.hw01;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.hw01.UniqueNumbers.TreeNode;

class UniqueNumbersTest {

	static TreeNode root = new TreeNode();
	
	@BeforeAll
	public static void initTree() {
		root = null;
		
		root = UniqueNumbers.addNode(root, 5);
		UniqueNumbers.addNode(root, 3);
		UniqueNumbers.addNode(root, 4);
		UniqueNumbers.addNode(root, 0);
		UniqueNumbers.addNode(root, 7);
		UniqueNumbers.addNode(root, 10);
		UniqueNumbers.addNode(root, 22);
		UniqueNumbers.addNode(root, 6);
		UniqueNumbers.addNode(root, 2551);
		UniqueNumbers.addNode(root, -100);
		UniqueNumbers.addNode(root, 4);
		UniqueNumbers.addNode(root, 22);
	}
	
	@Test
	public void containsValue6() {
		assertTrue(UniqueNumbers.containsValue(root, 6));
	}
	
	@Test
	public void containsValue3negative() {
		assertFalse(UniqueNumbers.containsValue(root, -3));
	}
	
	@Test
	public void containsValue100negative() {
		assertTrue(UniqueNumbers.containsValue(root, -100));
	}
	
	@Test
	public void containsValue0() {
		assertTrue(UniqueNumbers.containsValue(root, 0));
	}
	
	@Test
	public void containsValue2551() {
		assertTrue(UniqueNumbers.containsValue(root, 2551));
	}
	
	@Test
	public void treeSize() {
		assertEquals(10, UniqueNumbers.treeSize(root));
	}
	
	@Test
	public void containsValueRootNull() {
		assertFalse(UniqueNumbers.containsValue(null, 5));
	}
	
	@Test
	public void addValueRootNull() {
		assertNotNull(UniqueNumbers.addNode(null, 5));
	}
	
	@Test
	public void treeSizeNull() {
		assertEquals(0, UniqueNumbers.treeSize(null));
	}
	
	@Test
	public void addNodeAlreadyAdded() {
		
		int size = UniqueNumbers.treeSize(root);
		
		UniqueNumbers.addNode(root, 6);
		UniqueNumbers.addNode(root, 22);
		
		assertEquals(size, UniqueNumbers.treeSize(root));
	}

}
