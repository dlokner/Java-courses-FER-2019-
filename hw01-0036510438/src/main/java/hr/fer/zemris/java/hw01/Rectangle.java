package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * Program that calculate perimeter and area of rectangle.
 * 
 * Arguments are received from command line (width and height) 
 * or their will be requested in the main method if program is started
 * with no arguments in command line.
 * 
 * @author Domagoj Lokner
 */
public class Rectangle {
	
	/**
	 * Starting method of the program.
	 * 
	 * @param args - Arguments from command line.
	 */
	public static void main(String[] args) {
		
		if(args.length != 0 && args.length != 2) {
			System.out.println("Neispravan broj argumenata!");
			System.exit(1);
		}
		
		double width = 0d, height = 0d;
		
		if(args.length == 2) {
			
			try {
				width = Double.parseDouble(args[0]);
				height = Double.parseDouble(args[1]);
			} catch(NumberFormatException ex) {
				System.out.println("Neispravni argumenti!");
				System.exit(1);
			}
		
		} else {
		
			Scanner sc = new Scanner(System.in);
			
			width = scan(sc, -1);
			height = scan(sc, 1);
			
			sc.close();
		}
		
		double perimeter = 0d, area = 0d;
		
		/*if arguments from comand line are negative numbers,
		 * perimeter and area functions will throw IllegalArgumentException
		 * and program will be terminated*/
		try {
			perimeter = perimeter(width, height);
			area = area(width, height);
		} catch(IllegalArgumentException ex) {
			System.out.println("Argumenti ne smiju biti negativni!");
			System.exit(1);
		}
			
		System.out.format("Pravokutnik širine %.1f i visine %.1f ima površinu %.1f te opseg %.1f.", width, height, area, perimeter);
	}
	
	/**
	 * Compute peremeter of rectangle.
	 * 
	 * @param w - Width.
	 * @param h - Height.
	 * @return Double value of perimeter.
	 */
	public static double perimeter(double w, double h) {
		checkArgs(w, h);
		return 2*w + 2*h;
	}
	
	/**
	 * Compute area of rectangle.
	 * 
	 * @param w - Width.
	 * @param h - Height.
	 * @return Double value of area.
	 */
	public static double area(double w, double h) {
		checkArgs(w, h);
		return w*h;
	}
	
	public static void checkArgs(double a, double b) {
		if(a < 0 || b < 0) {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	  * Read arguments for height and width and parse them to double. 
	  * 
	  * @param sc - Scanner object.
	  * @param param - Prints message for height entering if param is negative 
	  * or for width if param is positive number.
	  * @return Double value of readed argument.
	  */
	private static double scan(Scanner sc, int param) {
		
		do {
			
			double result;
			
			if(param < 0) {
				System.out.print("Unesite širinu > ");
			} else {
				System.out.print("Unesite visinu > ");
			}
			String input = sc.next();
			
			try {
				result = Double.parseDouble(input);
			} catch(NumberFormatException ex) {
				System.out.format("'%s' se ne može protumačiti kao broj.%n", input);
				continue;
			}
			
			if(result < 0) {
				System.out.println("Unijeli ste negativanu vrijednost.");
				continue;
			}
			
			return result;
			
		} while(true); 	
		
	}
	
}
