package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * Program for computing factorials of numbers in range [3-20]
 * 
 * @author Domagoj Lokner
 */
public class Factorial {

	/**
	 * Starting method of the program.
	 * 
	 * @param args - Arguments from  command line.
	 */
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		do {
			
			System.out.print("Unesite broj > ");
			String inputStr = sc.nextLine();
					
			if(inputStr.equals("kraj")) {
				break;
			}
			
			int input;
			
			try {
				input = Integer.parseInt(inputStr);
			 } catch(NumberFormatException ex) {
				 System.out.format("'%s' nije cijeli broj.%n", inputStr);
				 continue;
			 }
			
			if(input < 3 || input > 20) {
				System.out.format("'%d' nije broj u dozvoljenom rasponu.%n", input);
				continue;
			}
			
			System.out.format("%d! = %d%n", input, fact(input));
			
		} while(true);
		
		sc.close();
		
		System.out.println("Doviđenja.");
	}
	
	/**
	 * Returns the long value of positive number factorial
	 * (argument n must be less than or equal to 20 because of the long type value range)
	 * 
	 * @param n - a value
	 * (argument n must be less than or equal to 20 because of the {@code}long value range).
	 * @exception IllegalArgumentException if argument n is negative number or number greater than 20.
	 * @return The factorial of n. 
	 */
	public static long fact(int n) {
		
		if(n < 0 || n > 20) {
			throw new IllegalArgumentException();
		}
		
		if(n == 0) {
			return 0;
		}
		
		long result = 1;
		
		for(int i = 2; i <= n; ++i) {
			result *= i;
		}
		
		return result;
	}

}
