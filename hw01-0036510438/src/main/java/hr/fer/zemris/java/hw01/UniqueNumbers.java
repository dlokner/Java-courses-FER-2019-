package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * Program that works with sorted binary tree.
 * Requires elements of the tree form system input (elements must be {@code int} type).
 * Program ends when argument "kraj" is entered.
 * On the end, program print elements of the tree ordered ascending and descending.
 * 
 * @author Domagoj Lokner
 *
 */
public class UniqueNumbers {

	/**
	 * Starting method of the program.
	 * 
	 * @param args - Arguments from command line.
	 */
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		TreeNode root = null;
		
		do {
			System.out.print("Unesite broj > ");
			String input = sc.next();
			
			if(input.equals("kraj")) {
				break;
			}
			
			int element;
			
			try {
				element = Integer.parseInt(input);
				System.out.println("Broj: " + element);
			} catch(NumberFormatException ex) {
				System.out.format("'%s' nije cijeli broj.%n", input);
				continue;
			}
			
			if(containsValue(root, element)) {
				System.out.println("Broj već postoji. Preskačem.");
			} else {
				root = addNode(root, element);
				System.out.println("Dodano.");
			}
			
		} while(true);
		
		System.out.print("Ispis od najmanjeg: " + elemAsc(root));
		System.out.print("\nIspis od najvećeg: " + elemDesc(root));
		
		sc.close();
		
	}
	
	/**
	 * Public structure that represent one node 
	 * of a binary tree with {@code int} type value.
	 */
	public static class TreeNode {
		public TreeNode left;
		public TreeNode right;
		public int value;
	}
	
	/**
	 * Adds the {@code int} element to sorted binary tree if it is not already present.
	 * 
	 * @param root - root node of the tree.
	 * @param element - element to be added to binary tree.
	 * @return root node of the tree.
	 */
	public static TreeNode addNode(TreeNode root, int element) {
				
		if(containsValue(root, element)) {
			return root;
		}
		
		if(root == null) {
			root = new TreeNode();
			root.value = element;
			return root;
		}
		
		if(root.value > element) {
			root.left = addNode(root.left, element);
		} else {
			root.right = addNode(root.right, element);
		}
		return root;
	}
	
	/**
	 * Return true if sorted binary tree contains specified {@code int} element.
	 * 
	 * @param root - Root node of the tree.
	 * @param element - Element to be checked for containment.
	 * @return True if binary tree contains the specified element.
	 */
	public static boolean containsValue(TreeNode root, int element) {
		
		if(root == null) {
			return false;
		}
		
		if(root.value == element) {
			return true;
		}
		
		if(element <  root.value) {
			return containsValue(root.left, element);
		} else {
			return containsValue(root.right, element);
		}
		
	}
	
	/**
	 * Count number of elements in the binary tree.
	 * 
	 * @param root - Root node of binary tree.
	 * @return Number of elements in the tree.
	 */
	public static int treeSize(TreeNode root) {
		
		int sum = 0;
		
		if(root == null) {
			return 0;
		}
		
		sum += 1 + treeSize(root.left) + treeSize(root.right);
		
		return sum;
	}
	
	/**
	 * Forms a new {@code String} that contains elements 
	 * of sorted binary tree ordered ascending and separated by whitespace.
	 * 
	 * @param root - Root node of the tree.
	 * @return The new string.
	 */
	public static String elemAsc(TreeNode root) {
		String result = "";
		
		if(root == null)
			return "";
		
		result += elemAsc(root.left);
		result += root.value + " ";
		result += elemAsc(root.right);
		
		return result;
	}
	
	/**
	 * Forms a new {@code String} that contains elements 
	 * of sorted binary tree ordered descending and separated by whitespace.
	 * 
	 * @param root - Root node of the tree.
	 * @return The new string.
	 */
	public static String elemDesc(TreeNode root) {
		String result = "";
		
		if(root == null)
			return "";
		
		result = elemDesc(root.right);
		result += root.value + " ";
		result += elemDesc(root.left);
		
		return result;
	}
}
