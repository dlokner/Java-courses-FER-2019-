package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * JUnit tests for {@code ArrayIndexedCollection} class.
 * 
 * @author Domagoj Lokner
 *
 */
class ArraIndexCollectionTest {
	
	private ArrayIndexedCollection array = new ArrayIndexedCollection(4);
	
	@BeforeEach
	void initArray() {
		array.clear();
		
		array.add(20);
		array.add("Marko");
		array.add(2.134);
		array.add("Ivan");
	}
	
	@Test
	void DefaultConstructorTest() {
		ArrayIndexedCollection array1 = new ArrayIndexedCollection();		
		assertTrue(array1.getCapacity() == 16);
	}
	
	@Test
	void initialCapacityConstructorTest() {		
		assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection(0));
		
		ArrayIndexedCollection array1 = new ArrayIndexedCollection(3);
		
		assertEquals(3, array1.getCapacity());
	}
	
	@Test
	void collectionConstructorTest() {
		ArrayIndexedCollection array1 = new ArrayIndexedCollection(array);
		
		assertEquals(array1.getCapacity(), array.size());
		assertEquals(array1.get(0), 20);
		assertEquals(array1.get(3), "Ivan");
		assertThrows(IndexOutOfBoundsException.class, () -> array1.get(5));
	}
	
	@Test
	void collectionAndInitialCapacityConstructorTest() {
		ArrayIndexedCollection array1 = new ArrayIndexedCollection(array, 5);
		
		assertEquals(array1.getCapacity(), 5);
		assertEquals(array1.get(0), 20);
		assertEquals(array1.get(3), "Ivan");
		assertThrows(IndexOutOfBoundsException.class, () -> array1.get(5));
	}

	@Test
	void containsTest() {
		assertTrue(array.contains(20));
		assertTrue(array.contains(2.134));
		assertTrue(array.contains("Ivan"));
		assertFalse(array.contains("Ivana"));
	}
	
	@Test
	void removeIndex() {
		assertTrue(array.contains("Marko"));
		
		array.remove(1);
		
		assertFalse(array.contains("Marko"));
		assertEquals(2.134, array.get(1));
	}
	
	@Test
	void removeLastIndex() {
		assertTrue(array.contains("Ivan"));
		
		array.remove(3);
		
		assertFalse(array.contains("Ivan"));
		assertThrows(IndexOutOfBoundsException.class, () -> array.get(3));
	}
	
	@Test
	void toArrayTest() {
		Object[] array1 = {20, "Marko", 2.134, "Ivan"};
		assertArrayEquals(array.toArray(), array1);
	}
	
	@Test
	void forEachTest() {
		ArrayIndexedCollection array1 = new ArrayIndexedCollection();
		
		array1.add("Štefica");
		array1.add(-3.14);
		array1.add(15);
		
		class ToArray extends Processor {
			
			private Object[] resultArray = new Object[array1.size()];
			private int i;
			
			@Override
			public void process(Object element) {
				resultArray[i] = element;
				++i;
			}
		}
		
		ToArray newArray = new ToArray();
		
		array1.forEach(newArray);
		
		assertArrayEquals(array1.toArray(), newArray.resultArray);
		
	}
	
	@Test
	void addTest() {
		array.add(-3.14);
		array.add("Siniša");
		
		assertEquals(-3.14, array.get(4));
		assertEquals("Siniša", array.get(5));
	}
	
	@Test
	void sizeTest() {
		assertEquals(4, array.size());
		
		array.add("Štefica");
		
		assertEquals(5, array.size());
	}
	
	@Test
	void isEmptyTest() {
		assertFalse(array.isEmpty());
		
		array.clear();
		
		assertTrue(array.isEmpty());
	}
	
	@Test
	void getTest() {
		assertEquals(20, array.get(0));
		assertEquals("Marko", array.get(1));
		assertEquals("Ivan", array.get(3));
		
		assertThrows(IndexOutOfBoundsException.class, () -> array.get(4));
		assertThrows(IndexOutOfBoundsException.class, () -> array.get(-1));
	}
	
	@Test
	void clearTest() {
		array.clear();
		
		assertEquals(0, array.size());
		assertTrue(array.isEmpty());
	}
	
	@Test 
	void insertTest() {
		array.insert(0, 0);
		array.insert("Štefica", 5);
		
		assertEquals(0, array.get(0));
		assertEquals("Štefica", array.get(5));
		
		assertThrows(IndexOutOfBoundsException.class, () -> array.insert("Zagreb", 7));
		assertThrows(IndexOutOfBoundsException.class, () -> array.insert("Zagreb", -1));
	}
	
	@Test
	void indexOfTest() {
		assertEquals(1, array.indexOf("Marko"));
		assertEquals(3, array.indexOf("Ivan"));
		assertEquals(-1, array.indexOf("Štefica"));
	}
	
	@Test
	void removeValue() {
		array.remove("Ivan");
		array.remove(Integer.valueOf(20));
				
		assertEquals("Marko", array.get(0));
		assertEquals(2.134, array.get(1));
		
		assertThrows(IndexOutOfBoundsException.class, () -> array.get(2));
	}
	
	@Test
	void addAllTest() {
		ArrayIndexedCollection array1 = new ArrayIndexedCollection();
		
		array1.add("Štefica");
		array1.add(-3.14);
		array1.add(15);
		
		array.addAll(array1);
		
		assertEquals(20, array.get(0));
		assertEquals("Štefica", array.get(4));
		assertEquals(15, array.get(6));
		
		assertThrows(IndexOutOfBoundsException.class, () -> array.get(7));
		assertThrows(IndexOutOfBoundsException.class, () -> array.get(-1));
	}
}
