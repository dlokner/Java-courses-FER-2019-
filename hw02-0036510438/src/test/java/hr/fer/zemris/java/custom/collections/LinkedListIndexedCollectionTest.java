package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * JUnit tests for {@code LinkedListIndexedCollection} class.
 * 
 * @author Domagoj Lokner
 *
 */
class LinkedListIndexedCollectionTest {

	private LinkedListIndexedCollection list = new LinkedListIndexedCollection();
	
	@BeforeEach
	void initList() {
		list.clear();
		
		list.add(20);
		list.add("Marko");
		list.add(2.134);
		list.add("Ivan");
	}
	
	@Test
	void DefaultConstructorTest() {
		LinkedListIndexedCollection list1 = new LinkedListIndexedCollection();		
		assertTrue(list1.size() == 0);
	}
	
	@Test
	void collectionConstructorTest() {
		LinkedListIndexedCollection list1 = new LinkedListIndexedCollection(list);
		
		assertEquals(list1.get(0), 20);
		assertEquals(list1.get(3), "Ivan");
		assertThrows(IndexOutOfBoundsException.class, () -> list1.get(5));
	}

	@Test
	void containsTest() {
		assertTrue(list.contains(20));
		assertTrue(list.contains(2.134));
		assertTrue(list.contains("Ivan"));
		assertFalse(list.contains("Ivana"));
	}
	
	@Test
	void removeIndex() {
		assertTrue(list.contains("Marko"));
		
		list.remove(1);
		
		assertFalse(list.contains("Marko"));
		assertEquals(2.134, list.get(1));
	}
	
	@Test
	void removeLastIndex() {
		assertTrue(list.contains("Ivan"));
		
		list.remove(3);
		
		assertFalse(list.contains("Ivan"));
		assertThrows(IndexOutOfBoundsException.class, () -> list.get(3));
	}
	
	@Test
	void toArrayTest() {
		Object[] list1 = {20, "Marko", 2.134, "Ivan"};
		assertArrayEquals(list.toArray(), list1);
	}
	
	@Test
	void forEachTest() {
		LinkedListIndexedCollection list1 = new LinkedListIndexedCollection();
		
		list1.add("Štefica");
		list1.add(-3.14);
		list1.add(15);
		
		class ToArray extends Processor {
			
			private Object[] resultArray = new Object[list1.size()];
			private int i;
			
			@Override
			public void process(Object element) {
				resultArray[i] = element;
				++i;
			}
		}
		
		ToArray array = new ToArray();
		
		list1.forEach(array);
		
		assertArrayEquals(list1.toArray(), array.resultArray);
		
	}
	
	@Test
	void addTest() {
		list.add(-3.14);
		list.add("Siniša");
		
		assertEquals(-3.14, list.get(4));
		assertEquals("Siniša", list.get(5));
	}
	
	@Test
	void sizeTest() {
		assertEquals(4, list.size());
		
		list.add("Štefica");
		
		assertEquals(5, list.size());
	}
	
	@Test
	void isEmptyTest() {
		assertFalse(list.isEmpty());
		
		list.clear();
		
		assertTrue(list.isEmpty());
	}
	
	@Test
	void getTest() {
		assertEquals(20, list.get(0));
		assertEquals("Marko", list.get(1));
		assertEquals("Ivan", list.get(3));
		
		assertThrows(IndexOutOfBoundsException.class, () -> list.get(4));
		assertThrows(IndexOutOfBoundsException.class, () -> list.get(-1));
	}
	
	@Test
	void clearTest() {
		list.clear();
		
		assertEquals(0, list.size());
		assertTrue(list.isEmpty());
	}
	
	@Test 
	void insertTest() {
		list.insert(0, 0);
		list.insert("Štefica", 5);
		
		assertEquals(0, list.get(0));
		assertEquals("Štefica", list.get(5));
		
		assertThrows(IndexOutOfBoundsException.class, () -> list.insert("Zagreb", 7));
		assertThrows(IndexOutOfBoundsException.class, () -> list.insert("Zagreb", -1));
	}
	
	@Test
	void indexOfTest() {
		assertEquals(1, list.indexOf("Marko"));
		assertEquals(3, list.indexOf("Ivan"));
		assertEquals(-1, list.indexOf("Štefica"));
	}
	
	@Test
	void removeValue() {
		list.remove("Ivan");
		list.remove(Integer.valueOf(20));
		
		assertEquals("Marko", list.get(0));
		assertEquals(2.134, list.get(1));
		
		assertThrows(IndexOutOfBoundsException.class, () -> list.get(2));
	}
	
	@Test
	void addAllTest() {
		LinkedListIndexedCollection list1 = new LinkedListIndexedCollection();
		
		list1.add("Štefica");
		list1.add(-3.14);
		list1.add(15);
		
		list.addAll(list1);
		
		assertEquals(20, list.get(0));
		assertEquals("Štefica", list.get(4));
		assertEquals(15, list.get(6));
		
		assertThrows(IndexOutOfBoundsException.class, () -> list.get(7));
		assertThrows(IndexOutOfBoundsException.class, () -> list.get(-1));
	}
}
