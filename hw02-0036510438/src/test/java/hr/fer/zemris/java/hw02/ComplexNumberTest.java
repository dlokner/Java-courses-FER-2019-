package hr.fer.zemris.java.hw02;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * JUnit tests for {@code ComplexNumber} class.
 * 
 * @author Domagoj Lokner
 *
 */
class ComplexNumberTest {

	/**
	 * Compare given {@code first} and {@code second} double with precision on 8 decimals.
	 * 
	 * 
	 * @param first - first complex number to be compared.
	 * @param second - second complex number to be compared.
	 * @return true if numbers are equals otherwise false.
	 */
	public static boolean doubleEquals(double first, double second) {
		if(Math.abs(first - second) <= 0.00000001) {
			return true;
		}
		return false;
	}
	
	@Test
	void calculateAngleTest1() {
		ComplexNumber num = new ComplexNumber(1.0, 1.0);
		assertTrue(doubleEquals(num.getAngle(), 0.785398163));
	}
	
	@Test
	void calculateAngleTest2() {
		ComplexNumber num = new ComplexNumber(-1.0, 0d);
		assertTrue(doubleEquals(num.getAngle(), Math.PI));
	}
	
	@Test
	void calculateAngleTest3() {
		ComplexNumber num = new ComplexNumber(-5.12, -17.36);
		assertTrue(doubleEquals(num.getAngle(), 4.425589223));
	}
	
	@Test
	void fromRealTestNumber() {
		ComplexNumber num = new ComplexNumber(51.1345, 0d);
		assertEquals(ComplexNumber.fromReal(51.1345), num);
	}
	
	@Test
	void fromRealTestZero() {
		ComplexNumber num = new ComplexNumber(0d, 0d);
		assertEquals(ComplexNumber.fromReal(0d), num);
	}
	
	@Test
	void fromImaginaryZero() {
		ComplexNumber num = new ComplexNumber(0d, 0d);
		assertEquals(ComplexNumber.fromImaginary(0d), num);
	}
	
	@Test
	void fromMagnitudeAndAngleZero1() {
		ComplexNumber num = new ComplexNumber(0d, 0d);
		assertEquals(ComplexNumber.fromMagnitudeAndAngle(0d, 7.34), num);
	}
	
	@Test
	void fromMagnitudeAndAngleZero2() {
		ComplexNumber num = new ComplexNumber(0d, 0d);
		assertEquals(ComplexNumber.fromMagnitudeAndAngle(-47.2342, 0d), num);
	}
	
	@Test
	void fromMagnitudeAndAngleNumber() {
		ComplexNumber num = new ComplexNumber(-5.043579486, 17.383104037);
		assertEquals(ComplexNumber.fromMagnitudeAndAngle(18.10, 4.43), num);
	}
	
	@Test
	void parseTestRealAndImaginaryNegative() {
		ComplexNumber num = new ComplexNumber(-7.43568739115, -88.3758496009);
		assertEquals(ComplexNumber.parse("-7.43568739115-88.3758496009i"), num);
	}
	
	@Test
	void parseTestPlusBeforeRealPart() {
		ComplexNumber num = new ComplexNumber(87.456738765323, 45.567823423342);
		assertEquals(ComplexNumber.parse("+87.456738765323+45.567823423342i"), num);
	}
	
	@Test
	void parseTestOnlyi() {
		ComplexNumber num = new ComplexNumber(0d, 1d);
		assertEquals(ComplexNumber.parse("i"), num);
	}
	
	@Test
	void parseTestNegativei() {
		ComplexNumber num = new ComplexNumber(0d, -1d);
		assertEquals(ComplexNumber.parse("-i"), num);
	}
	
	@Test
	void parseTestOnlyRealNegative() {
		ComplexNumber num = ComplexNumber.fromReal(-159.543876947857);
		assertEquals(ComplexNumber.parse("-159.543876947857"), num);
	}
	
	@Test
	void parseTestOnlyRealPluseBefore() {
		ComplexNumber num = ComplexNumber.fromReal(75.1567863554);
		assertEquals(ComplexNumber.parse("+75.1567863554"), num);
	}
	
	@Test
	void parseTestEndsWithSigne1() {
		assertThrows(IllegalArgumentException.class, 
				() -> ComplexNumber.parse("7.43568739115-88.3758496009i-"));
	}
	
	@Test
	void parseTestEndsWithSigne2() {
		assertThrows(IllegalArgumentException.class, 
				() -> ComplexNumber.parse("-34-"));
	}
	
	@Test
	void parseTestTwoSignes1() {
		assertThrows(IllegalArgumentException.class, 
				() -> ComplexNumber.parse("--7.43568739115-88.3758496009i"));
	}
	
	@Test
	void parseTestTwoSignes2() {
		assertThrows(IllegalArgumentException.class, 
				() -> ComplexNumber.parse("-+7.43568739115-88.3758496009i"));
	}
	
	@Test
	void parseTestTwoSignes3() {
		assertThrows(IllegalArgumentException.class, 
				() -> ComplexNumber.parse("-7.43568739115--88.3758496009i"));
	}
	
	@Test
	void iOnBeginning() {
		assertThrows(IllegalArgumentException.class, 
				() -> ComplexNumber.parse("i567"));
	}
	
	@Test
	void iBeforeNumber() {
		assertThrows(IllegalArgumentException.class, 
				() -> ComplexNumber.parse("875.654345-i582.7885232"));
	}
	
	@Test
	void addTest() {
		ComplexNumber num1 = new ComplexNumber(-57.678762332344, -4565.567823478234);
		ComplexNumber num2 = new ComplexNumber(-7554.723746234, 815.78272627348237);
		assertTrue(num1.add(num2).equals(
				new ComplexNumber(num1.getReal()+num2.getReal(), num1.getImaginary()+num2.getImaginary())));
	}
	
	@Test
	void subTest() {
		ComplexNumber num1 = new ComplexNumber(-57.678762332344, -4565.567823478234);
		ComplexNumber num2 = new ComplexNumber(-7554.723746234, 815.78272627348237);
		assertTrue(num1.sub(num2).equals(
				new ComplexNumber(num1.getReal()-num2.getReal(), num1.getImaginary()-num2.getImaginary())));
	}
	
	@Test
	void mulTest() {
		ComplexNumber num1 = new ComplexNumber(-35.752148645, 76.678526523);
		ComplexNumber num2 = new ComplexNumber(-12.456789258, -15.78272627);
		assertTrue(num1.mul(num2).equals(new ComplexNumber(1655.553176090899,-390.90186988258809)));
	}
	
	@Test
	void divTest() {
		ComplexNumber num1 = new ComplexNumber(-35.752148645, 76.678526523);
		ComplexNumber num2 = new ComplexNumber(-12.456789258, -15.78272627);
		assertTrue(num1.div(num2).equals(new ComplexNumber(-1.89192047943197045,-3.75850168946625084)));
	}
	
	@Test
	void powerTest() {
		ComplexNumber num1 = new ComplexNumber(-14.758745625845, 6.674588526523);
		assertEquals(num1.power(3), new ComplexNumber(-1242.24622355558468, 4064.2342812227104034));
	}
	
	@Test
	void rootTest() {
		ComplexNumber num1 = new ComplexNumber(-147.547535752845, 126.55876243354153);
		ComplexNumber[] roots = {
				new ComplexNumber(3.06446675422597, 2.13340523412663),
				new ComplexNumber(-2.13340523412663, 3.06446675422597),
				new ComplexNumber(-3.06446675422597, -2.13340523412663),
				new ComplexNumber(2.13340523412663, -3.06446675422597)
		};
		
		assertArrayEquals(roots, num1.root(4));
	}
	
	@Test
	void equalsTestTrue() {
		ComplexNumber num1 = new ComplexNumber(15.764485241525515, -154.754551452555471);
		ComplexNumber num2 = new ComplexNumber(15.764485241525515, -154.754551452555471);
		
		assertEquals(num1, num2);
	}
	
	@Test
	void equalsTestFase() {
		ComplexNumber num1 = new ComplexNumber(15.764485241525515, -154.754551452555471);
		ComplexNumber num2 = new ComplexNumber(15.764485291525515, -154.754551412555471);
		
		assertNotEquals(num1, num2);
	}
	
	@Test
	void toStringTest1() {
		ComplexNumber num = new ComplexNumber(15.764485241525, -154.754551452555);		
		assertEquals("15.764485241525-154.754551452555i", num.toString());
	}
	
	@Test
	void toStringTest2() {
		ComplexNumber num = new ComplexNumber(15.764485241525, 154.754551452555);		
		assertEquals("15.764485241525+154.754551452555i", num.toString());
	}
	
	
	@Test
	void toStringTestOnlyImaginary() {
		ComplexNumber num = new ComplexNumber(0d, -154.754551452555);	
		assertEquals("-154.754551452555i", num.toString());
	}
	
	@Test
	void toStringTestOnlyReal() {
		ComplexNumber num = new ComplexNumber(15.764485241525, 0d);	
		assertEquals("15.764485241525", num.toString());
	}
	
	@Test
	void toStringTestOnlyi() {
		ComplexNumber num = new ComplexNumber(0d, 1.0);		
		assertEquals("1.0i", num.toString());
	}
	
	@Test
	void getRealTest() {
		ComplexNumber num = new ComplexNumber(13.53627365542, -7.462713846723);	
		assertEquals(13.53627365542, num.getReal());
	}
	
	@Test
	void getImaginaryTest() {
		ComplexNumber num = new ComplexNumber(13.53627365542, -7.462713846723);	
		assertEquals(-7.462713846723, num.getImaginary());
	}
	
	@Test
	void getAngleTest() {
		ComplexNumber num = new ComplexNumber(13.53627365542, -7.462713846723);	
		assertEquals(5.779335148482829, num.getAngle());
	}
	
	@Test
	void getMagnitudeTest() {
		ComplexNumber num = new ComplexNumber(13.53627365542, -7.462713846723);	
		assertEquals(15.4571278843286, num.getMagnitude());
	}
}
