package hr.fer.zemris.java.hw02.demo;

import hr.fer.zemris.java.hw02.ComplexNumber;

/**
 * Program for testing {@code ComplexNumber} class.
 * 
 * <p>
 * Program do not expect any arguments from command line, all given arguments will be ignored.
 * </p>
 */
public class ComplexDemo {

	/**
	 * Starting method of the program.
	 * 
	 * @param args - arguments from command line.
	 */
	public static void main(String[] args) {
		
		ComplexNumber c1 = new ComplexNumber(2, 3);
		ComplexNumber c2 = ComplexNumber.parse("2.5-3i");
		ComplexNumber c4 = ComplexNumber.parse("2+i");
		ComplexNumber c5 = ComplexNumber.parse("-2+i");
		ComplexNumber c6 = ComplexNumber.parse("-2-i");
		ComplexNumber c7 = ComplexNumber.parse("2-i");
		
		System.out.println(c4.getAngle());
		System.out.println(c5.getAngle());
		System.out.println(c6.getAngle());
		System.out.println(c7.getAngle());
		
		ComplexNumber c3 = c1.add(ComplexNumber.fromMagnitudeAndAngle(2, 1.57))
		.div(c2).power(3).root(2)[1];
		System.out.println(c3);
		
	}

}
