package hr.fer.zemris.java.hw02;

public class ComplexNumber {
	private double re;
	private double im;
	private double angle;
	private double magnitude;

	/**
	 * Constructs complex number with real part {@code re} and imaginary part {@code im}.
	 * 
	 * @param re - real part of complex number.
	 * @param im - imaginary part of complex number. 
	 */
	public ComplexNumber(double re, double im) {
		this.re = re;
		this.im = im;

		this.angle = calculateAngle(re, im);
		this.magnitude = Math.sqrt(re * re + im * im);
	}

	/*
	 * Private method for calculating angle of complex number.
	 * 
	 * Returns angle in range [0, 2*PI].
	 */
	private double calculateAngle(double re, double im) {
		return im < 0 ? Math.atan2(im, re) + Math.PI * 2 : Math.atan2(im, re);
	}

	/**
	 * Returns new {@code CompexNumber} with real part equals to given {@code real}
	 * and imaginary part equals to 0.
	 * 
	 * @param real - real part of complex number.
	 * @return new {@code ComplexNumber}.
	 */
	public static ComplexNumber fromReal(double real) {
		return new ComplexNumber(real, 0);
	}

	/**
	 * Returns new {@code CompexNumber} object with imaginary part equals to given
	 * {@code imaginary} and real part equals to 0.
	 * 
	 * @param imaginary - imaginary part of complex number.
	 * @return new {@code ComplexNumber}.
	 */
	public static ComplexNumber fromImaginary(double imaginary) {
		return new ComplexNumber(0, imaginary);
	}

	/**
	 * Returns new {@code ComplexNumber} object of given {@code magnitude} and
	 * {@code angle}.
	 * 
	 * @param magnitude - magnitude of complex number.
	 * @param angle     - angle of complex number.
	 * @return new {@code ComplexNumber}
	 */
	public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
		return new ComplexNumber(magnitude * Math.cos(angle), magnitude * Math.sin(angle));
	}

	/**
	 * Returns new {@code ComplexNumber} intialized to the values represented
	 * by {@code String s}.
	 * 
	 * @param s - string to be parsed.
	 * @return {@code ComplexNumber} object represented by the string argument.
	 * @throws NumberFormatException if string argument can not be parsed to {@code ComplexNumber}.
	 */
	public static ComplexNumber parse(String s) {
		String reS = "", imS = "";
		double re, im;

		String[] numbers = s.split("[+-]+");
		String[] signes = s.split("[^+-]+");
		
		for(int i = 0; i < numbers.length; ++i) {
			numbers[i] = numbers[i].strip();
		}

		int numLen = numbers.length - 1;
		int sigLen = signes.length - 1;
		
		/*throw exception if there are two signes (+,-) together*/
		for(String signe : signes) {
			if(signe.length() > 1) {
				throw new NumberFormatException();
			}
		}
		
		/*exception is thrown if strig endsWith '+' or '-'*/
		if(s.endsWith("-") || s.endsWith("+")) {
			throw new NumberFormatException();
		}
		
		try {
			if (s.contains("i")) {
				
				imS = numbers[numLen].substring(0, numbers[numLen].length() - 1);
				imS = imS.isEmpty() ? "1" : imS;
				
				if(sigLen != -1) {
					imS = signes[sigLen].contains("-") ? signes[sigLen] + imS : imS;
				}
				
				im = Double.parseDouble(imS);
				
				if(numLen > 0 && !numbers[numLen-1].isEmpty()) {
					reS = signes[sigLen-1].contains("-") ? signes[sigLen-1] + numbers[numLen-1] : numbers[numLen-1];
					re = Double.parseDouble(reS);
				} else {
					re = 0d;
				}
			} else {
				im = 0d;
				
				reS = numbers[numLen];
				
				if(sigLen != -1) {
					reS = signes[sigLen].contains("-") ? signes[sigLen] + reS : reS;
				}
				
				re = Double.parseDouble(reS);
			}
			
			return new ComplexNumber(re, im);
		} catch (IndexOutOfBoundsException | NumberFormatException ex2) {
			throw new NumberFormatException();
		}
	}

	/**
	 * Addition by {@code ComplexNumber c}.
	 * 
	 * @param c - {@code ComplexNumber} to be added.
	 * @return new {@code ComplexNumber} initialized on sum of this number and given argument.
	 */
	public ComplexNumber add(ComplexNumber c) {
		return new ComplexNumber(this.re + c.re, this.im + c.im);
	}
	
	/**
	 * Subtraction by {@code ComplexNumber c}.
	 * 
	 * @param c - subtrahend.
	 * @return new {@code ComplexNumber} initialized on subtraction of this number and given argument.
	 */
	public ComplexNumber sub(ComplexNumber c) {
		return new ComplexNumber(this.re - c.re, this.im - c.im);
	}
	
	/**
	 * Multiplication by {@code ComplexNumber c}.
	 * 
	 * @param c - multiplicator. 
	 * @return new {@code ComplexNumber} initialized on multiplication of this number and given argument.
	 */
	public ComplexNumber mul(ComplexNumber c) {
		return new ComplexNumber(re*c.re - im*c.im, re*c.im + c.re*im);
	}
	
	/**
	 * Divides by {@code ComplexNumber c}.
	 * 
	 * @param c - divisor.
	 * @return new {@code ComplexNumber} initialized on result of division.
	 */
	public ComplexNumber div(ComplexNumber c) {
		double real = (magnitude/c.magnitude)*Math.cos(angle-c.angle);
		double imaginary = (magnitude/c.magnitude)*Math.sin(angle-c.angle);
		return new ComplexNumber(real, imaginary);
	}
	
	/**
	 * Powers by given {@code n}.
	 * 
	 * @param n - exponent.
	 * @return new {@code ComplexNumber} powered by {@code n}.
	 * @throws IllegalArgumentException if n is 
	 */
	public ComplexNumber power(int n) {
		if(n < 0) {
			throw new IllegalArgumentException();
		}
		
		double real = Math.pow(magnitude, n)*Math.cos(n*angle);
		double imaginary = Math.pow(magnitude, n)*Math.sin(n*angle);
		
		return new ComplexNumber(real, imaginary);
	}
	
	/**
	 * Return array of {@code n} th roots of this complex number.
	 * 
	 * @param n - degree.
	 * @return Array of references on {@code ComplexNumber}.
	 */
	public ComplexNumber[] root(int n) {
		if(n <= 0) {
			throw new IllegalArgumentException();
		}
		
		ComplexNumber[] result = new ComplexNumber[n];
		
		for(int i = 0; i < n; ++i) {
			double real = Math.pow(magnitude, 1d/n)*Math.cos((angle + 2*i*Math.PI)/n);
			double imaginary = Math.pow(magnitude, 1d/n)*Math.sin((angle + 2*i*Math.PI)/n);
			
			result[i] = new ComplexNumber(real, imaginary);
		}
		
		return result;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(im);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(re);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ComplexNumber other = (ComplexNumber) obj;
		
		if(Math.abs(re-other.re) <= 0.00000001) {
			return true;
		}
		if(Math.abs(im-other.im) <= 0.00000001) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		
		if(re == 0d) {
			return im  + "i";
		}
		if(im == 0d) {
			return re + "";
		}
		return im < 0 ? re + "-" + Math.abs(im) + "i" : re + "+" + Math.abs(im) + "i";
	}
	
	/**
	 * Getter of real part of complex number.
	 * 
	 * @return real part of complex number.
	 */
	public double getReal() {
		return re;
	}
	
	/**
	 * Getter of imaginary part of complex number.
	 * 
	 * @return imaginary part of complex number.
	 */
	public double getImaginary() {
		return im;
	}

	/**
	 * Getter of angle of complex number.
	 * 
	 * @return angle of complex number.
	 */
	public double getAngle() {
		return angle;
	}

	/**
	 * Getter of magnitude of complex number.
	 * 
	 * @return magnitude of complex number.
	 */
	public double getMagnitude() {
		return magnitude;
	}	
}
