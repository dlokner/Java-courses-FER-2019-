package hr.fer.zemris.java.custom.collections;

/**
 * Class that represents some general collection of objects.
 * 
 * @author Domagoj Lokner
 *
 */
public class Collection {
	
	/**
	 * Default constructor.
	 */
	protected Collection(){
	}
	
	/**
	 * Checks if collection contains no objects.
	 * 
	 * @return {@code true} if collection is empty and {@code false} otherwise.
	 */
	public boolean isEmpty() {
		return size() == 0;
	}
	
	/**
	 * Returns the number of currently stored objects in collection.
	 * 
	 * @return size of the collection.
	 */
	public int size() {
		return 0;
	}
	
	/**
	 * Adds given {@code value} into the collection.
	 * 
	 * @param value - element to be added.
	 */
	public void add(Object value) {
	}
	
	/**
	 * Checks for containment of given {@code value} in collection.
	 * 
	 * @param value - object to be checked for containment.
	 * @return {@code true} if collection contains given {@code value} otherwise {@code false}.
	 */
	boolean contains(Object value) {
		return false;
	}
	
	/**
	 * Remove given {@code value} from collection.
	 * 
	 * @param value - element to be removed.
	 * @return {@code true} if element is removed otherwise {@code false}.
	 */
	public boolean remove(Object value) {
		return false;
	}
	
	/**
	 * Allocates new array with size equals to the size of this collections, 
	 * fills it with collection content.
	 * 
	 * @return Newly allocated array.
	 */
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Calls {@code processor.process} for each element this collection contains. 
	 * 
	 * @param processor - {@code Processor} object.
	 */
	public void forEach(Processor processor) {
	}
	
	/**
	 * Adds all elements from given {@code other} collection into this collection.
	 * 
	 * @param other - collection to be added into current collection.
	 */
	public void addAll(Collection other) {
		
		class Add extends Processor {
			
			@Override
			public void process(Object element) {
				add(element);
			}
		}
		
		other.forEach(new Add());
	}
	
	/**
	 * Remove all elements from this collection.
	 */
	void clear() {
	}
}
