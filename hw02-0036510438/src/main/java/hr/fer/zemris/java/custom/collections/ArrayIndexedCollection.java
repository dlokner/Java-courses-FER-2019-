package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;
import java.util.Objects;

/**
 * Class that implements array backed collection. 
 * 
 * Storage of {@code null} value elements is not allowed in this implementation.
 * 
 * <p>
 * Average complexity of:
 * <ul>
 * 	<li>{@code void add(Object value)} - O(1)</li>
 * 	<li>{@code Object get(int index)} - O(1)</li>
 * 	<li>{@code void insert(Object value, int position)} - O(size/2)</li>
 * 	<li>{@code int indexOf(Object value)} - O(size/2)</li>
 * </ul>
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class ArrayIndexedCollection extends Collection {

	private int size;
	private Object[] elements;
	private int capacity;

	/**
	 * Default constructor. 
	 * Set an instance of the collection with capactiy set to 16.
	 */
	public ArrayIndexedCollection() {
		this(16);
	}

	/**
	 * Constructor which set instance of the collecion with capacity of received
	 * argument.
	 * 
	 * @param initialCapacity - {@code int} value of the collection capacity.
	 */
	public ArrayIndexedCollection(int initialCapacity) {
		if (initialCapacity < 1) {
			throw new IllegalArgumentException();
		}
		this.capacity = initialCapacity;
		this.elements = new Object[this.capacity];
	}

	/**
	 * Constructor which receives collection as argument and copy it into
	 * this collection. 
	 * 
	 * @param other - collection to be copied into new collection.
	 * @throws NullPointerException if null is sent as {@code other} parameter.
	 */
	public ArrayIndexedCollection(Collection other) {
		this(other, other.size());
	}
	
	/**
	 * Constructor which receives two arguments, collection that will be copied in new
	 * collection and initial capacity of the collection.
	 * If size of the received collection is grater than initial capacity, new collection will
	 * have capacity equals to size of that collection.
	 * 
	 * @param other - collection to be copied into new collection.
	 * @param initialCapacity - initial capactity of the collection.
	 * @throws NullPointerException if null is sent as {@code other} parameter.
	 */
	public ArrayIndexedCollection(Collection other, int initialCapacity) {
		Objects.requireNonNull(other, "Collection parameter can't be null");
		
		this.capacity = other.size() > initialCapacity ? other.size() : initialCapacity;
		
		this.elements = new Object[this.capacity];
		
		this.addAll(other);
	}
	
	@Override
	public boolean contains(Object value) {
		return indexOf(value) == -1 ? false : true;
	}
	
	@Override
	public boolean remove(Object value) {
		
		int index = indexOf(value);
		
		if(index != -1) {
			remove(index);
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public  Object[] toArray() {
		
		Object[] array = new Object[size];
		
		for(int i = 0; i < size; ++i) {
			array[i] = elements[i];
		}
		
		return array;
	}
	
	@Override
	public void forEach(Processor processor) {
		
		for(int i = 0; i < size; ++i) {
			processor.process(elements[i]);
		}
	}
	
	/**
	 * {@inheritDoc} 
	 * 
	 * @throws NullPointerException if given value is null.
	 */
	@Override
	public void add(Object value) {
		Objects.requireNonNull(value, "Null can't be added into the collection!");
		
		checkAndAllocate();
		
		elements[size] = value;
		++size;
	}
	
	@Override
	public int size() {
		return size;
	}
	
	@Override
	public boolean isEmpty() {
		return size == 0;
	}
	
	/**
	 * Gets the element stored on the received index in this array.
	 * 
	 * @param index - index of the element we want get.
	 * @return element stored on the index {@code index}.
	 * @throws IndexOutOfBoundsException if index is not value in range [0, array size - 1].
	 */
	public Object get(int index) {
		
		isLegalIndex(0, size-1, index);
		
		return elements[index];
	}
	
	@Override
	public void clear() {
		
		for(int i = 0; i < size; ++i) {
			elements[i] = null;
		}
		
		size = 0;
	}
	
	/**
	 * Inserts the given {@code value} at the given {@code position} in array.
	 * 
	 * @param value - value to be inserted into array.
	 * @param position - index on which value will be stored
	 * @throws IndexOutOfBoundsException if position is not in range [0, array size].
	 */
	public void insert(Object value, int position) {
		Objects.requireNonNull(value, "Null can't be added into the collection!");
		
		isLegalIndex(0, size, position);
		
		checkAndAllocate();
		
		for(int i = size-1; i >= position; --i) {
			elements[i + 1] = elements[i];
		}
		
		elements[position] = value;
		++size;
	}
	
	/**
	 * Searches the collection and returns the index of the first occurrence of the given {@code value}.
	 * 
	 * @param value - value that is searched in array.
	 * @return Index of the first occurrence of the given {@code value} or -1 if it is not found.
	 */
	public int indexOf(Object value) {
		
		if(value == null) {
			return -1;
		}
		
		for(int i = 0;  i < size; ++i) {
			if(elements[i].equals(value)) {
				return i;
			}
		}
		
		return -1;
	}
	
	/**
	 * Removes element at specified {@code index} from array.
	 * 
	 * @param index - Index of the element to be removed.
	 * @throws IndexOutOfBoundsException if {@code index} is not between 0 and size-1.
	 */
	public void remove(int index) {
		
		isLegalIndex(0, size-1, index);
		
		for(int i = index; i < size-1; ++i) {
			elements[i] = elements[i + 1];
		}
		
		elements[size-1] = null;
		--size;
	}

	
	/**
	 * Private method that allocate the array with doubled capacity.
	 * All elements from original array are copied into the new array
	 */
	private void checkAndAllocate() {
		if(size == capacity) {
			capacity = 2*capacity;
			elements = Arrays.copyOf(elements, capacity);
			
		}
	}
	
	/**
	 * Private method that throws IndexOutOfBound exception if index is not value 
	 * in range [{@code lower}, {@code upper}].
	 * 
	 * @param lower - lower bound.
	 * @param upper - upper bound.
	 * @param index - index to checked.
	 */
	private void isLegalIndex(int lower, int upper, int index) {
		if(index < lower || index > upper) {
			throw new IndexOutOfBoundsException();
		}
	}
	
	/**
	 * Getter for current capacity of the array.
	 * This method is written and used only for JUnit tests.
	 * 
	 * @return capacity of array.
	 */
	public int getCapacity() {
		return this.capacity;
	}
	
}