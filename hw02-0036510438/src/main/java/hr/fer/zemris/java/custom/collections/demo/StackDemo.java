package hr.fer.zemris.java.custom.collections.demo;

import hr.fer.zemris.java.custom.collections.EmptyStackException;
import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * Program for evaluating postfix expressions.
 * 
 * <p>
 * Program accepts single argument from command line (e.g. "8 -2 4 + /"). <br>
 * Argument must be valid expression in postfix notation or program will be terminated. 
 * In expression operands and operants should be separated by whitespaces.<br>
 * If expression is valid program will print value of solved expression.
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class StackDemo {

	/**
	 * Starting method of the program.
	 * 
	 * @param args - arguments from command line.
	 */
	public static void main(String[] args) {
		
		ObjectStack stack = new ObjectStack();
		
		if(args.length != 1) {
			System.out.println("Program expects single command line argument!");
			System.exit(1);
		}
		
		String[] input = args[0].trim().split("\\s+");
		
		for(int i = 0; i < input.length; ++i) {
			
			try {
				
				stack.push(Integer.parseInt(input[i]));
			
			} catch(NumberFormatException ex1) {
				
				int first, second;
				
				try {
					second = (int)stack.pop();
					first = (int)stack.pop();
				} catch(EmptyStackException ex2) {
					break;
				}
				
				switch(input[i]) {
					case "+": 	stack.push(first + second);
								break;
								
					case "-": 	stack.push(first - second);
								break;
								
					case "/":	checkForDivByZero(second);
								stack.push(first / second);
								break;
								
					case "*": 	stack.push(first * second);
								break;
								
					case "%": 	checkForDivByZero(second);
								stack.push(Math.floorMod(first, second));
								break;
				}
			} 
		}
		
		if(stack.size() != 1) {
			System.out.println("Invalid expression is given!");
			System.exit(1);
		} 
			
		System.out.format("Expression evaluates to %d.", stack.pop());
	}
	
	
	/*
	 * If zero is given to this method program will be terminated and error message
	 * for dividing by zero will be printed on standard output.
	 */
	private static void checkForDivByZero(int number) {
		if(number == 0) {
			System.out.println("Dividing by zero!");
			System.exit(1);
		}
	}

}
