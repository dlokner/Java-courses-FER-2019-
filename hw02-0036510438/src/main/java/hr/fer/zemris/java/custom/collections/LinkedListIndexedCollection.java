package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * Class that implements linked list backed collection.
 * 
 * Storage of element value null is not allowed in this implementation.
 * Duplicate elements are allowed.
 * 
 * <p>
 * <b>Average complexity</b> of:
 * <ul>
 * 	<li>{@code void add(Object value)} - O(1)</li>
 * 	<li>{@code Object get(int index)} - the greatest complexity (size/2+1)</li>
 * 	<li>{@code void insert(Object value, int position)} - O((size/2+1)/2)</li>
 * 	<li>{@code int indexOf(Object value)} - O(size/2)</li>
 * </ul>
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class LinkedListIndexedCollection extends Collection {

	private int size;
	private ListNode first;
	private ListNode last;

	/*
	 * Private class that represents one node of the linked list.
	 * 
	 * Contains pointers on previous and next node in the list and reference for
	 * value storage.
	 * 
	 * Class also have default constructor and two more constructors for
	 * initializing attributes.
	 */
	private static class ListNode {
		private ListNode previous;
		private ListNode next;
		private Object value;

		private ListNode() {
		}

		private ListNode(Object value) {
			this.value = value;
		}

		private ListNode(Object value, ListNode previous, ListNode next) {
			this.value = value;
			this.previous = previous;
			this.next = next;
		}
	}

	/**
	 * Default constructor. Attributes {@code first} and {@code last} stay on
	 * {@code null} value. Size of the collection is 0.
	 */
	public LinkedListIndexedCollection() {
	}

	/**
	 * Constructor that copy elements stored in {@code other} collection to new
	 * {@code LinkedListIndexedCollection}.
	 * 
	 * @param other - collection to be copied into the list.
	 * @throws NullPointerException if null is sent as {@code other} parameter.
	 */
	public LinkedListIndexedCollection(Collection other) {
		Objects.requireNonNull(other, "Collection parameter can't be null");

		this.addAll(other);
	}
	
	/**
	 * {@inheritDoc} 
	 * 
	 * @throws NullPointerException if given value is null.
	 */
	@Override
	public void add(Object value) {
		Objects.requireNonNull(value);

		ListNode newNode = new ListNode(value, last, null);
		++size;
		
		if (first == null) {
			first = newNode;
		} else {
			last.next = newNode;
		}
		last = newNode;
	}
	
	@Override
	public boolean isEmpty() {
		return size == 0;
	}
	
	@Override
	public int size() {
		return size;
	}
	
	@Override
	public boolean contains(Object value) {
		return indexOf(value) == -1 ? false : true;
	}
	
	@Override
	public boolean remove(Object value) {
		
		int index = indexOf(value);
		
		if(index == -1) {
			return false;
		} 
		
		remove(index);
		return true;
	}
	
	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		
		ListNode tmp = first;
		
		for(int i = 0; i < size; ++i) {
			array[i] = tmp.value;
			tmp = tmp.next;
		}
		
		return array;
	}
	
	@Override
	public void forEach(Processor processor) {
		ListNode element = first;
		
		while(element != null) {
			processor.process(element.value);
			element = element.next;
		}
		
	}
	
	/**
	 * Return object that is stored in linked list at position {@code index}.
	 * 
	 * @param index - position of element in linked list.
	 * @return element stored on {@code index} position.
	 * @throws IndexOutOfBoundsException if {@code index} is not in range [0, size-1].
	 */
	public Object get(int index) {
		
		isLegalIndex(0, size-1, index);
		
		return getNodeOnIndex(index).value;
		
	}
	
	@Override
	public void clear() {
		first = null;
		last = null;
		
		size = 0;
	}
	
	/**
	 * Inserts the given element at given {@code position} into linked list.
	 * 
	 * @param value - element to be inserted.
	 * @param position - index of inserting in linked list.
	 * @throws IndexOutOfBoundsException if {@code} is not in range [0, size].
	 */
	public void insert(Object value, int position) {
		
		Objects.requireNonNull(value);
		
		isLegalIndex(0, size, position);
		
		if(position == size) {
			add(value);
			return;
		}
		
		ListNode newNode = new ListNode(value);
		ListNode current = getNodeOnIndex(position);
	
		if(current.previous == null) {
			current.previous = newNode;
			first = newNode;
		} else {
			current.previous.next = newNode;
		}
		
		
		newNode.next = current;
		
		++size;
	}
	
	/**
	 * Search the collection and returns index of the first occurrence of the given {@code value}.
	 * 
	 * @param value - value which is searched in the collection.
	 * @return index of the first occurrence of {@code value} or -1 if {@code value} is not found.
	 */
	public int indexOf(Object value) {
		
		ListNode tmp = first;
		
		int index = 0;
		while(tmp != null) {
			if(tmp.value.equals(value)) {
				return index;
			}
			
			tmp = tmp.next;
			++index;
		}
		
		return -1;
	}

	/**
	 * Removes element at specified {@code index} from linked list.
	 * 
	 * @param index - index of element to be removed.
	 * @throws IndexOutOfBoundsException if {@code index} is not in range [0, size-1].
	 */
	public void remove(int index) {
		isLegalIndex(0, size-1, index);
		
		ListNode node = getNodeOnIndex(index);
		
		if(node.previous == null) {
			node.next.previous = null;
			first = node.next;
		} else {
			node.previous.next = node.next;
			if(node.next == null) {
				last = node.previous;
			}
		}
		
		--size;
	}

	/**
	 * Private method that throws IndexOutOfBound exception if index is not value 
	 * in range [{@code lower}, {@code upper}].
	 * 
	 * @param lower - lower bound.
	 * @param upper - upper bound.
	 * @param index - index to checked.
	 */
	private void isLegalIndex(int lower, int upper, int index) {
		if(index < lower || index > upper) {
			throw new IndexOutOfBoundsException();
		}
	}
	
	/*
	 * Private method that returns ListNode at given index in linked list.
	 */
	
	/**
	 * Private method that gets node at given index in linked list.
	 * 
	 * @param index - index of node to be gotten.
	 * @return {@code ListNode} on {@code index}.
	 */
	private ListNode getNodeOnIndex(int index) {
		
		int median = size/2;
		
		ListNode tmp;
		if(index <= median) {
			tmp = first;
			
			for(int i = 0; i < index; ++i) {
				tmp = first.next;
			}
			
		} else {
			tmp = last;
			
			for(int i = size-1; i > index; --i) {
				tmp = tmp.previous;
			}
		}
		
		return tmp;
	}
	
}
