package hr.fer.zemris.java.servlets;

import javax.inject.Singleton;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.*;

/**
 * Object that offers descriptions of images
 * stored in <i>opisnik.txt</i>.
 *
 * @author Domagoj Lokner
 */
@Singleton
public class Descriptor {

    /**
     * List of descriptions.
     */
    private List<Description> descriptions;

    /**
     * Instance of {@code Descriptor} object.
     */
    private static Descriptor instance;

    /**
     * Path to descriptor.
     */
    private static Path descriptorPath;

    /**
     * Indicate if {@code Descriptor} object is set.
     */
    private static boolean isSet = false;

    /**
     * Constructs {@code Descriptor} object.
     */
    private Descriptor(Path descriptorPath) {
        List<String> lines;

        try {
            lines = Files.readAllLines(descriptorPath);
        } catch (IOException ex) {
            throw new InvalidPathException(descriptorPath.toString(), "File can't be reached");
        }
        
        Descriptor.descriptorPath = descriptorPath;

        descriptions = createListOfDescriptions(lines);
        isSet = true;
    }

    /**
     * Gets all descriptions.
     *
     * @return descriptions.
     */
    public List<Description> getDescriptions() {
        return descriptions;
    }

    /**
     * Gets description stored on {@code index}.
     *
     * @param index index of description.
     * @return description on {@code index}.
     */
    public Description getDescription(int index) {
        return descriptions.get(index);
    }

    /**
     * Gets all descriptions with given {@code tag}.
     *
     * @param tag requested {@code tag}.
     * @return descriptions with given {@code tag}.
     */
    public List<Description> getDescriptions(String tag) {
        Objects.requireNonNull(tag);

        List<Description> result = new ArrayList<>();

        for (Description d : descriptions) {
            if (d.containsTag(tag)) {
                result.add(d);
            }
        }

        return result;
    }

    /**
     * Gets instance of {@code Descriptor}.
     *
     * @return instance of {@code Descriptor}.
     */
    public static Descriptor getInstance() {
        if (!isSet) {
            throw new UnsupportedOperationException("Descriptor must be set. Call ");
        }

        if (instance == null) {
            instance = new Descriptor(descriptorPath);
        }
        return instance;
    }

    /**
     * Returns list of all tags.
     *
     * @return list of all tags.
     */
    public List<String> getTags() {
        LinkedHashSet<String> tags = new LinkedHashSet<>();

        for (Description d : descriptions) {
            for (String tag : d.tags) {
                tags.add(tag);
            }
        }

        return new LinkedList<>(tags);
    }

    /**
     * Private methods that parses all lines form <i>opisnik.txt</i> to
     * {@code Description} objects.
     *
     * @param lines all lines from <i>opisnik.txt</i>.
     * @return list of descriptions.
     * @throws IllegalArgumentException if <i>opisnik.txt</i> format is invalid.
     */
    private List<Description> createListOfDescriptions(List<String> lines) {
        Objects.requireNonNull(lines);

        if (lines.size() % 3 != 0) {
            throw new IllegalArgumentException("Invalid format of images description file");
        }

        String[] l = lines.toArray(new String[lines.size()]);

        List<Description> descriptions = new ArrayList<>();

        for (int i = 0; i < l.length; i += 3) {
            Description d = new Description();

            d.fileName = l[i];
            d.title = l[i+1];
            d.setTags(l[i+2]);

            descriptions.add(d);
        }

        return descriptions;
    }

    /**
     * Sets {@code Descriptor} object.
     *
     * @param path path to descriptor file.
     */
    public static void update(Path path) {
        try {
            instance = new Descriptor(path);
        } catch (InvalidPathException ex) {
            throw ex;
        }
    }


    /**
     * Structure that represents single description.
     *
     * @author Domagoj Lokner
     */
    public static class Description {

        /**
         * File name.
         */
        public String fileName;

        /**
         * File title.
         */
        public String title;

        /**
         * Tags.
         */
        public String[] tags;

        /**
         * Sets tags defined with given string {@code line}.
         * Expected description record is format <i>"tag1, tag2 , tag3 ,tag4"</i>.
         *
         * @param line string to be parsed to tags.
         */
        public void setTags(String line) {
            tags = line.strip().split("\\s*,\\s*");
        }

        /**
         * Returns true if description contains given {@code tag}.
         *
         * @param tag tag to be checked.
         * @return {@code true} if tag is present otherwise {@code false}.
         */
        public boolean containsTag(String tag) {
            tag = tag.strip();

            for (String t : tags) {
                if (t.equals(tag)) {
                    return true;
                }
            }
            return false;
        }
    }

}
