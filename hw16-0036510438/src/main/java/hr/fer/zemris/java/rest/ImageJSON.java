package hr.fer.zemris.java.rest;

import com.google.gson.Gson;
import hr.fer.zemris.java.servlets.Descriptor;
import hr.fer.zemris.java.servlets.GalleryImg;
import hr.fer.zemris.java.servlets.ImagesDB;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import hr.fer.zemris.java.servlets.ImagesDB.*;
import java.util.List;

/**
 * Implementation of REST API that converts Images used in Gallery
 * webapp in JSON format.
 *
 * @author Domagoj Lokner
 */
@Path("/imgj")
public class ImageJSON {

    /**
     * Returns all tags as JSON file.
     *
     * @return JSON file as response.
     */
    @GET
    @Path("tags")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTags() {
        List<String> tags = Descriptor.getInstance().getTags();

        String jsonText = getJson(tags);

        return Response.status(Response.Status.OK).entity(jsonText).build();
    }

    /**
     * Returns thumbnail images that contains requested tag.
     *
     * @param tag tag with which the image is presented.
     * @return JSON file as response.
     */
    @GET
    @Path("thumbnails/{tag}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getThumbnails(@PathParam("tag") String tag) {
        ImagesDB db = ImagesDB.getInstance();
        List<JsonGalleryImage> thumbnails = db.galleryToJson(db.getThumbnails(tag));

        String jsonText = getJson(thumbnails);

        return Response.status(Response.Status.OK).entity(jsonText).build();
    }

    /**
     * Returns image with requested name.
     *
     * @param imgName name of image.
     * @return response as JSON file.
     */
    @GET
    @Path("{imgName}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getImage(@PathParam("imgName") String imgName) {
        ImagesDB db = ImagesDB.getInstance();
        GalleryImg img = db.getImage(imgName);
        JsonGalleryImage jsonImage = new JsonGalleryImage();

        if (img != null) {
            jsonImage.fill(img);
        } else {
            jsonImage = null;
        }

        String jsonText = getJson(jsonImage);

        return Response.status(Response.Status.OK).entity(jsonText).build();
    }

    /**
     * Returns {@code String} that represents JSON file for given {@code Object o}-
     * If {@code null} is given empty string will be returned.
     *
     * @param o object to be presented with JSON.
     * @return JSON text.
     */
    private String getJson(Object o) {

        if (o == null) {
            return "";
        } else {
            Gson gson = new Gson();
            return gson.toJson(o);
        }
    }

}
