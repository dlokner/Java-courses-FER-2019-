package hr.fer.zemris.java.servlets;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.nio.file.Paths;

/**
 * This servlet listener will set paths for {@code ImagesDB} and {@code Descriptor} 
 * when application is started.
 * 
 * @author Domagoj Lokner
 *
 */
@WebListener
public class Initialization implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        Descriptor.update(Paths.get(sce.getServletContext().getRealPath("WEB-INF/opisnik.txt")));
        ImagesDB.update(Paths.get(sce.getServletContext().getRealPath("/WEB-INF/slike")));
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
