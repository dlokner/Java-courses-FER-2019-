package hr.fer.zemris.java.servlets;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.servlets.Descriptor.*;

import javax.imageio.ImageIO;
import javax.inject.Singleton;

import static java.awt.Image.SCALE_DEFAULT;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;

/**
 * Database of all images described in <i>opisnik.txt</i>.
 *
 * @author Domagoj Lokner
 */
@Singleton
public class ImagesDB {

    public static Path imagesPath;
    public static Path thumbnailsPath;

    /**
     * Instance of {@code ImagesDB}.
     */
    private static ImagesDB instance;

    /**
     * List in which all images are stored.
     */
    private List<GalleryImg> imgs;

    /**
     * Indicate if {@code Descriptor} object is set.
     */
    private static boolean isSet = false;

    /**
     * Constructs {@code ImagesDB}.
     *
     * @throws InvalidPathException if directory with images can't be reached.
     * @throws RuntimeException if error occurs during creating directory in which thumbnails
     *                          will be stored.
     */
    private ImagesDB() {
        if (!Files.isDirectory(imagesPath)) {
            throw new InvalidPathException(imagesPath.toString(), "Directory not found.");
        }

        if (!Files.exists(thumbnailsPath)) {
            try {
                Files.createDirectory(thumbnailsPath);
            } catch (IOException ex) {
                throw new RuntimeException("Error during creation of thumbnails dir");
            }

        }

        imgs = createListOfImages();
        isSet = true;
    }

    /**
     * Gets instance of {@code ImagesDB}.
     *
     * @return instance of this object.
     */
    public static ImagesDB getInstance() {
        if (!isSet) {
            throw new UnsupportedOperationException("Descriptor must be set. Call ");
        }

        if (Objects.isNull(instance)) {
            instance = new ImagesDB();
        }
        return instance;
    }

    /**
     * Creates {@code GalleryImg} object described with {@code d} object.
     *
     * @param d object that describes image to be constructed.
     * @return constructed object.
     * @throws RuntimeException if error occurs during reading
     *                          image file.
     */
    private GalleryImg createImage(Description d) {
        GalleryImg img = new GalleryImg();

        img.setFileName(d.fileName);
        img.setTags(d.tags);
        img.setTitle(d.title);

        BufferedImage buffImg;

        try {
            buffImg = ImageIO.read(imagesPath.resolve(d.fileName).toFile());
        } catch (IOException ex) {
            throw new RuntimeException("Error occurs during reading images");
        }

        img.setImage(buffImg);

        return img;
    }

    /**
     * Crete list of all images which descriptions are stored in
     * {@code Descriptor} object.
     *
     * @return list of images.
     */
    private List<GalleryImg> createListOfImages() {
        List<Description> descripts = Descriptor.getInstance().getDescriptions();

        List<GalleryImg> list = new LinkedList<>();

        for (Description d : descripts) {
            GalleryImg img = createImage(d);
            list.add(img);
        }

        return list;
    }

    /**
     * Gets thumbnail image (150px * 150px) of given {@code img}.
     *
     * @param img image which thumbnail will be gotten.
     * @return thumbnail.
     */
    private GalleryImg getThumbnail(GalleryImg img) {
        Objects.requireNonNull(img);

        Path tnPath = thumbnailsPath.resolve(img.getFileName());

        GalleryImg thumbnail = new GalleryImg();

        try {

            if (Files.exists(tnPath)) {
                thumbnail.setImage(ImageIO.read(tnPath.toFile()));
            } else {
                BufferedImage t = convertToThumbnail(img.getImage());
                ImageIO.write(t, "jpg", tnPath.toFile());
                thumbnail.setImage(t);
            }
        } catch (IOException ex) {
            return null;
        }

        thumbnail.setTitle(img.getTitle());
        thumbnail.setTags(img.getTags());
        thumbnail.setFileName(img.getFileName());

        return thumbnail;
    }

    /**
     * Converts image to thumbnail (150px * 150px).
     *
     * @param img image to be converted.
     * @return thumbnail image.
     */
    private BufferedImage convertToThumbnail(BufferedImage img) {

        Image scaled = img.getScaledInstance(150, 150, SCALE_DEFAULT);
        BufferedImage thumbnail = new BufferedImage(
                scaled.getWidth(null),
                scaled.getHeight(null),
                TYPE_INT_RGB);
        Graphics2D g = thumbnail.createGraphics();
        g.drawImage(scaled, 0, 0, null);
        g.dispose();

        return thumbnail;
    }

    /**
     * Gets images with given tag.
     *
     * @param tag tag
     * @return list of images with given tag.
     */
    public List<GalleryImg> getImages(String tag) {
        Objects.requireNonNull(tag);

        List<GalleryImg> result = new LinkedList<>();

        for (GalleryImg img : imgs) {
            if (img.containsTag(tag)) {
                result.add(img);
            }
        }
        return result;
    }

    /**
     * Gets image with given file name.
     *
     * @param imgName name of image file.
     * @return image.
     */
    public GalleryImg getImage(String imgName) {
        Objects.requireNonNull(imgName);

        for (GalleryImg img : imgs) {
            if (img.getFileName().equals(imgName)) {
                return img;
            }
        }

        return null;
    }

    /**
     * Gets list of thumbnail images with given tag.
     *
     * @param tag tag.
     * @return list of thumbnails.
     */
    public List<GalleryImg> getThumbnails(String tag) {
        Objects.requireNonNull(tag);

        List<GalleryImg> thumbnails = new LinkedList<>();

        for (GalleryImg img : getImages(tag)) {
            thumbnails.add(getThumbnail(img));
        }

        return thumbnails;
    }

    /**
     * Sets {@code Descriptor} object.
     *
     * @param path path to descriptor file.
     */
    public static void update(Path path) {
        imagesPath = path;
        thumbnailsPath = path.resolve("..").resolve("thumbnails");

        try {
            instance = new ImagesDB();
        } catch (InvalidPathException ex) {
            throw ex;
        }
    }

    /**
     * Gets list of all images.
     *
     * @return images.
     */
    public List<GalleryImg> getImages() {
        return imgs;
    }

    /**
     * Objects that represents {@code GalleryImg} adapted for converting
     * to JSON format using {@code Gson} object.
     *
     * @author Domagoj Lokner
     */
    public static class JsonGalleryImage {

        /**
         * Image title.
         */
        @SuppressWarnings("unused")
		private String title;

        /**
         * Name of image file.
         */
        @SuppressWarnings("unused")
		private String fileName;

        /**
         * Image in base64 format.
         */
        @SuppressWarnings("unused")
		private String image;

        /**
         * Image tags.
         */
        @SuppressWarnings("unused")
		private String[] tags;

        /**
         * Fills this object with informations specified by
         * given object {@code img}.
         *
         * @param img image with which this object will be filled.
         * @return reference on this object.
         */
        public JsonGalleryImage fill(GalleryImg img) {
            title = img.getTitle();
            fileName = img.getFileName();
            image = img.getImageBase64();
            tags = img.getTags();

            return this;
        }
    }

    /**
     * Converts list of {@link GalleryImg} to list of {@link JsonGalleryImage}.
     *
     * @param imgs list of images to be converted.
     * @return converted list of images or {@code null} if
     *         {@code null} is given as argument.
     */
    public List<JsonGalleryImage> galleryToJson(List<GalleryImg> imgs) {
        if (Objects.isNull(imgs)) {
            return null;
        }

        List<JsonGalleryImage> result = new LinkedList<>();

        for (GalleryImg img : imgs) {
            result.add(new JsonGalleryImage().fill(img));
        }

        return result;
    }
}
