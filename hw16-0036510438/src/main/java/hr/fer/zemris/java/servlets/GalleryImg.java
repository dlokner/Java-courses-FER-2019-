package hr.fer.zemris.java.servlets;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Objects;

/**
 * Object that represents single image used in <i>gallery</i> webapp.
 *
 * @author Domagoj Lokner
 */
public class GalleryImg {

    /**
     * Image title.
     */
    private String title;

    /**
     * Name of image file.
     */
    private String fileName;

    /**
     * Image.
     */
    private BufferedImage img;

    /**
     * String that represents image encoded by <i>base64</i> encoder.
     */
    private String base64;

    /**
     * Tags that represents this image.
     */
    private String[] tags;

    /**
     * Checks if image contains given {@code tag}.
     *
     * @param tag tag to be checked.
     * @return {@code true} if image contains tag otherwise {@code false}.
     */
    public boolean containsTag(String tag) {
        Objects.requireNonNull(tag);

        tag = tag.strip();

        for (String t : tags) {
            if (t.equals(tag)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets title.
     *
     * @return title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets image title.
     *
     * @param title title to be set.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets image file name.
     *
     * @return file name.
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets image file name.
     *
     * @param fileName name to be set.
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Gets image.
     *
     * @return image.
     */
    public BufferedImage getImage() {
        return img;
    }

    /**
     * Sets image.
     *
     * @param img image to be set.
     */
    public void setImage(BufferedImage img) {
        this.img = img;
        this.base64 = toBase64();
    }

    /**
     * Gets array of tags.
     *
     * @return tags.
     */
    public String[] getTags() {
        return tags;
    }

    /**
     * Sets array of tags.
     *
     * @param tags tags to be set.
     */
    public void setTags(String[] tags) {
        this.tags = tags;
    }

    /**
     * Coverts this image to <i>base64</i> format.
     *
     * @return this image in base64 encode.
     */
    private String toBase64() {
        try (ByteArrayOutputStream bytes = new ByteArrayOutputStream()) {
            ImageIO.write(img, "jpg", bytes);
            return Base64.getEncoder().encodeToString(bytes.toByteArray());
        } catch (IOException ex) {
            return null;
        }
    }

    /**
     * Gets image in base64 form.
     *
     * @return image in base64 form.
     */
    public String getImageBase64() {
        return base64;
    }
}
