
$(document).ready(
    getTags()
);


function getTags() {
    $.ajax(
        {
            url: "rest/imgj/tags",
            dataType: "json",

            success: function (data) {
                let tags = data;
                let html = "";

                if (tags.length == 0) {
                    html = "Tags not found...";
                } else {
                    for (let i = 0; i < tags.length; i++) {
                        html += createTagButton(tags[i]);
                    }
                }
                $("#tags").html(html);
            }
        }
    );
}

function showThumbnails(tag) {
    $("#thumbnails").hide();
    $.ajax(
        {
            url: "rest/imgj/thumbnails/" + tag,
            dataType: "json",

            success: function (data) {
                let thumbs = data;
                let html = "";

                if (thumbs.length == 0) {
                    html = "There is no pictures for tag \"" + tag + "\"...";
                } else {
                    for (let i = 0; i < thumbs.length; ++i) {
                        let fileName = thumbs[i].fileName;
                        html += "<img class='thumbnail' src='data:image/jpg;base64, " + thumbs[i].image + "' alt='"+ fileName +"' onclick='displayImg(this.alt)'>";
                    }
                }
                $("#thumbnails").html(html);
                $("#thumbnails").slideDown(750);
            }
        }
    )
}

function displayImg(imageName) {

    $("#thumbnails").slideUp(1000);

    $.ajax(
        {
            url: "rest/imgj/" + imageName,
            dataType: "json",

            success: function (data) {
                let image = data;
                let img = "";
                let title = "";
                let tags = "";

                if (image.length == 0) {
                    img = "Image can't be reached";
                } else {
                    img += "<img class='image' src='data:image/jpg;base64, " + image.image + "'>";
                    title += image.title;

                    for (let i = 0; i < image.tags.length; ++i) {
                        tags += createTagButton(image.tags[i]);
                    }

                }
                $("#imageName").html(imageName);
                $("#imageTitle").html(title);
                $("#image").html(img);
                $("#imageTags").html(tags);
            }
        }
    )
}

function createTagButton(tag) {
    return "<input class='button' type='button' value='" + tag + "' onclick='showThumbnails(this.value)'>";
}

