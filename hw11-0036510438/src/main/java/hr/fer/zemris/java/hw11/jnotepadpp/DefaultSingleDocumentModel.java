package hr.fer.zemris.java.hw11.jnotepadpp;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of {@code SingleDocumentModel}.
 *
 * @author Domagoj Lokner
 */
public class DefaultSingleDocumentModel implements SingleDocumentModel {

    /**
     * Path from which document was loaded.
     * Can be {@code null} for new document.
     */
    private Path filePath;

    /**
     * List of all registered listeners.
     */
    private List<SingleDocumentListener> listeners;

    /**
     * Component which is used for editing document.
     */
    private JTextArea textComponent;

    /**
     * Document modification flag.
     */
    private boolean modified;

    /**
     * Constructs new {@code DefaultSingleDocumentModel}.
     *
     * @param filePath - path from which document was loaded.
     * @param text - text content.
     */
    public DefaultSingleDocumentModel(Path filePath, String text) {

        textComponent = new JTextArea();
        textComponent.setText(text);

        this.filePath = filePath;

        //sets modified on true if signs are inserted or removed from file
        textComponent.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                setModified(true);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                setModified(true);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {

            }
        });

        //if file is new file
        if (Objects.isNull(filePath)) {
            setModified(true);
        }
    }

    @Override
    public JTextArea getTextComponent() {
        return textComponent;
    }

    @Override
    public Path getFilePath() {
        return filePath;
    }

    @Override
    public void setFilePath(Path path) {
        if(Objects.isNull(path)) {
            throw new NullPointerException("Path can't be null");
        }

        filePath = path;

        notifyPathModification();
    }

    @Override
    public boolean isModified() {
        return modified;
    }

    @Override
    public void setModified(boolean modified) {
        this.modified = modified;
        notifyDocModification();
    }

    @Override
    public void addSingleDocumentListener(SingleDocumentListener l) {
        if (Objects.isNull(listeners)) {
            listeners = new LinkedList<>();
        }
        listeners.add(l);
    }

    @Override
    public void removeSingleDocumentListener(SingleDocumentListener l) {
        if (!Objects.isNull(listeners)) {
            listeners.remove(l);
        }
    }

    /**
     * Notify all listeners that document have been modified.
     */
    private void notifyDocModification() {
        if (!Objects.isNull(listeners)) {
            listeners.forEach(l -> l.documentModifyStatusUpdated(this));
        }
    }

    /**
     * Notify all listeners that document path have been modified.
     */
    private void notifyPathModification() {
        if (!Objects.isNull(listeners)) {
            listeners.forEach(l -> l.documentFilePathUpdated(this));
        }
    }
}
