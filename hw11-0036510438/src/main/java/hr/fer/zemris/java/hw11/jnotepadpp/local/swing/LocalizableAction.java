package hr.fer.zemris.java.hw11.jnotepadpp.local.swing;

import javax.swing.AbstractAction;

import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * {@code AbstractAction} object that can be localized.
 * 
 * @author Domagoj Lokner
 *
 */
public abstract class LocalizableAction extends AbstractAction {
	
	private static final long serialVersionUID = -8519250552679413425L;

	/**
	 * Key that represents value name.
	 */
	private String key;
	
	/**
	 * Object that provides localized texts.
	 */
	private ILocalizationProvider provider;

	/**
	 * Constructs {@code LocalizableAction}. 
	 * 
	 * @param key string that represents value name.
	 * @param provider object that provides localized texts.
	 */
	public LocalizableAction(String key, ILocalizationProvider provider) {
		this.key = key;
		this.provider = provider;
		
		setAction();
		
		provider.addLocalizationListener(() -> {
			setAction();
		}); 
	}
	
	/**
	 * Sets action parameters.
	 */
	private void setAction() {
		setActionName();
		setShortDescription();
	}
	
	/**
	 * Sets action name.
	 */
	private void setActionName() {
		putValue(NAME, provider.getString(key));
	}
	
	/**
	 * Sets short description.
	 */
	private void setShortDescription() {
		putValue(SHORT_DESCRIPTION, provider.getString(key + "_descript"));
	}
	
}

