package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * Implements objects that represents bridge between registered listeners and 
 * localization provider.
 * 
 * @author Domagoj Lokner
 *
 */
public class LocalizationProviderBridge extends AbstractLocalizationProvider {
	
	/**
	 * Flag that indicate if bridge is connected to localization provider.
	 */
	private boolean connected;
	
	/**
	 * Localization provider.
	 */
	private ILocalizationProvider provider; 
	
	/**
	 * This variable store last language of provider before bridge was disconnected.
	 */
	private String lastLanguage;
	
	/**
	 * Bridge listener.
	 */
	private final ILocalizationListener bridgeListener = () -> {
		if(connected) {
			fire();
		}
	};
	
	/**
	 * Constructs {@code LocalizationProviderBridge}.
	 * @param provider
	 */
	public LocalizationProviderBridge(ILocalizationProvider provider) {
		this.provider = provider;
		connect();
	}
	
	/**
	 * Disconnect registered listeners and localization provider.
	 */
	public void disconnect() {
		if(!connected) {
			return;
		}
		
		lastLanguage = provider.getCurrentLanguage();
		provider.removeLocalizationListener(bridgeListener);
		connected = false;
	}
	
	/**
	 * Connect registered listeners to localization provider.
	 */
	public void connect() {
		if(connected) {
			return;
		}
		
		provider.addLocalizationListener(bridgeListener);
		connected = true;
		
		if(!provider.getCurrentLanguage().equals(lastLanguage)) {
			fire();
		}	
	}

	@Override
	public String getString(String key) {
		return provider.getString(key);
	}

	@Override
	public String getCurrentLanguage() {
		return provider.getCurrentLanguage();
	}
	
}
