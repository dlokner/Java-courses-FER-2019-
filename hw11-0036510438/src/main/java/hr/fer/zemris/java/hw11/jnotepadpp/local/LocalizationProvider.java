package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.*;

/**
 * Implementation of {@code ILocalizationProvider}.
 * Singleton class.
 *
 * @author Domagoj Lokner
 */
public class LocalizationProvider extends AbstractLocalizationProvider {

    /**
     * Default language.
     */
    private static final String DEFAULT_LANGUAGE = "en";

    /**
     * Instance of this class.
     */
    private static LocalizationProvider instance;


    /**
     * Current language.
     */
    private String language;

    /**
     * Bundle for getting resource texts.
     */
    private ResourceBundle bundle;


    /**
     * Constructs new {@code LocalizationProvider}.
     */
    private LocalizationProvider() {
        setLanguage(DEFAULT_LANGUAGE);
    }

    /**
     * Gets instance of this class.
     *
     * @return instance of {@code LocalizationProvider}.
     */
    public static LocalizationProvider getInstance() {
        if (Objects.isNull(instance)) {
            instance = new LocalizationProvider();
        }
        return instance;
    }

    /**
     * Sets current language.
     *
     * @param language language to be set.
     */
    public void setLanguage(String language) {
        this.language = language;
        bundle = ResourceBundle.getBundle(
        		"hr.fer.zemris.java.hw11.jnotpadpp.local.translations",
                Locale.forLanguageTag(language)
        );
        fire();
    }

    @Override
    public String getString(String key) {
        return bundle.getString(key);
    }

    @Override
    public String getCurrentLanguage() {
        return language;
    }

}
