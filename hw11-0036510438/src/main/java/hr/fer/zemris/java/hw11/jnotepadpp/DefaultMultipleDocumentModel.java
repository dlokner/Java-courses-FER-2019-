package hr.fer.zemris.java.hw11.jnotepadpp;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.List;

public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel, Iterable<SingleDocumentModel> {

	private static final long serialVersionUID = 834900743448879279L;

	/**
     * New file text.
     */
    private static final String NEW_DOC_TEXT = "(unnamed)";

    /**
     * Collection of all documents in this model.
     */
    private List<SingleDocumentModel> documents;

    /**
     * Reference to current document.
     */
    private SingleDocumentModel current;

    /**
     * Collection that stores all registered listeners.
     */
    private List<MultipleDocumentListener> listeners;

    /**
     * Collection that stores listeners to all documents in model.
     */
    private List<SingleDocumentListener> documentListeners;

    /**
     * Tab icon for saved file.
     */
    private ImageIcon saved;

    /**
     * Tab icon for unsaved file.
     */
    private ImageIcon modified;

    /**
     * Constructs {@code DefaultMultipleDocumentModel}.
     */
    public DefaultMultipleDocumentModel() {
        documents = new ArrayList<>();

        //if selceted index changes change current document
        addChangeListener(e -> {
            if (getSelectedIndex() != -1) {
                setCurrentDocument(documents.get(getSelectedIndex()));
            } else {
                setCurrentDocument(null);
            }
        });

        saved = getIcon("/hr/fer/zemris/java/hw11/jnotpadpp/icons/saved.png", "Unmodified");
        modified = getIcon("/hr/fer/zemris/java/hw11/jnotpadpp/icons/modified.png", "Modified");
    }

    @Override
    public SingleDocumentModel createNewDocument() {
        DefaultSingleDocumentModel newDocument = new DefaultSingleDocumentModel(null, "");
        return addDocument(newDocument);
    }

    @Override
    public SingleDocumentModel getCurrentDocument() {
        return current;
    }

    /**
     *{@inheritDoc}
     *
     * @throws IllegalArgumentException if file can't be reached.
     */
    @Override
    public SingleDocumentModel loadDocument(Path path) {

        if (Objects.isNull(path)) {
            throw new NullPointerException("Path can't be null");
        }

        int index = documentIndex(path);

        if (index != -1) {
            SingleDocumentModel doc = documents.get(index);
            setCurrentDocument(doc);
            return doc;
        }

        String text;
        try {
            text = Files.readString(path);
        } catch (IOException e) {
            throw new IllegalArgumentException("File can't be reached!");
        }

        return addDocument(new DefaultSingleDocumentModel(path, text));
    }

    /**
     * {@inheritDoc}
     *
     * @throws IllegalArgumentException if invalid file path is given 
     *         or if file with given path is already opened in this model.
     */
    @Override
    public void saveDocument(SingleDocumentModel model, Path newPath) {

        String text = model.getTextComponent().getText();
        
        int indexOfNewPathDoc = documentIndex(newPath);
        
        if(indexOfNewPathDoc != -1) {
        	if(documents.indexOf(model) != indexOfNewPathDoc) {
        		throw new IllegalArgumentException("Opened document cannot be overwritten.");
        	}
        }

        try {
            Files.writeString(newPath, text);
        } catch (IOException e) {
            throw new IllegalArgumentException("Invalid file path");
        }

        model.setFilePath(newPath);
        model.setModified(false);
    }

    @Override
    public void closeDocument(SingleDocumentModel model) {
        int index;

        try {
            index = checkDocument(model);
        } catch (IllegalArgumentException ex) {
            //document is not present
            return;
        }

        removeTabAt(index);
        
        documents.remove(index)
        .removeSingleDocumentListener(documentListeners.remove(index));
       
        notifyRemovedDocument(model);
        
//        if(getSelectedIndex() == -1) {
//        	setCurrentDocument(null);
//        }
    }

    @Override
    public void addMultipleDocumentListener(MultipleDocumentListener l) {
        if (Objects.isNull(listeners)) {
            listeners = new LinkedList<>();
        }

        listeners.add(l);
    }

    @Override
    public void removeMultipleDocumentListener(MultipleDocumentListener l) {
        if (!Objects.isNull(listeners)) {
            listeners.remove(l);
        }
    }

    @Override
    public int getNumberOfDocuments() {
        return documents.size();
    }

    @Override
    public SingleDocumentModel getDocument(int index) {
        return documents.get(index);
    }

    @Override
    public Iterator<SingleDocumentModel> iterator() {
        return documents.iterator();
    }

    /**
     * Adds document to model.
     *
     * @param document - document to be added.
     * @return added document.
     */
    private SingleDocumentModel addDocument(SingleDocumentModel document) {

        documents.add(document);

        add(createPanel(document.getTextComponent()));

        configureTab(document);
        addDocumentListener(document);

        notifyAddedDocument(document);

        return document;
    }

    /**
     * Sets tab icon and title.
     *
     * @param document - document which tab will be configured.
     */
    private void configureTab(SingleDocumentModel document) {
        setTitle(document);
        setIcon(document);
    }

    /**
     * Notify all registered listeners that current document was changed.
     *
     * @param previous - previous document.
     */
    private void notifyCurrentChanged(SingleDocumentModel previous, SingleDocumentModel current) {
        if (!Objects.isNull(listeners)) {
            listeners.forEach(l -> l.currentDocumentChanged(previous, current));
        }
    }

    /**
     * Notify all registered listeners that new document is addedn into model.
     *
     * @param added - added document.
     */
    private void notifyAddedDocument(SingleDocumentModel added) {
        if (!Objects.isNull(listeners)) {
            listeners.forEach(l -> l.documentAdded(added));
        }
    }

    /**
     * Notify all registered listeners that document have been removed from model.
     *
     * @param removed - removed document.
     */
    private void notifyRemovedDocument(SingleDocumentModel removed) {
        if (!Objects.isNull(listeners)) {
            listeners.forEach(l -> l.documentRemoved(removed));
        }
    }

    /**
     * Sets title to tab of given document.
     *
     * @param model - document model which tab title will be set.
     * @throw IllegalArgumentException if this model don't contain given document.
     */
    private void setTitle(SingleDocumentModel model) {
        int index = checkDocument(model);

        String title;

        if(Objects.isNull(model.getFilePath())) {
            title = NEW_DOC_TEXT;
        } else {
            title = model.getFilePath().getFileName().toString();
        }

        setTitleAt(index, title);
    }

    /**
     * Sets icon to tab of given document.
     *
     * @param model - document model which tab icon will be set.
     * @throw IllegalArgumentException if this model don't contain given document.
     */
    private void setIcon(SingleDocumentModel model) {
        int index = checkDocument(model);

        setIconAt(index, model.isModified() ? modified : saved);
    }

    /**
     * Gets icon from resources.
     *
     * @param path - path to icon image.
     * @param description - icon description.
     * @return icon or {@code null} if invalid path is given.
     */
    private ImageIcon getIcon(String path, String description) {

        byte[] bytesIcon;

        try(InputStream is = getClass().getResourceAsStream(path)) {

            if (Objects.isNull(is)) {
                return null;
            }

            bytesIcon = is.readAllBytes();

        } catch (IOException e) {
            return null;
        }

        Image img = new ImageIcon(bytesIcon)
                .getImage().getScaledInstance(25, 25, Image.SCALE_DEFAULT);

        return new ImageIcon(img, description);
    }

    /**
     * Sets given document as current.
     * If {@code null} is given as parameter method will set current to null
     * and to {@link JTabbedPane#setSelectedIndex(int)} will be sent -1.
     *
     * @see JTabbedPane#setSelectedIndex(int)
     * @param model - document to be set as current.
     * @return set document.
     * @throw IllegalArgumentException if this model don't contain given document.
     */
    private SingleDocumentModel setCurrentDocument(SingleDocumentModel model) {

        if(!Objects.isNull(model)) {
	        int index = checkDocument(model);
	        setSelectedIndex(index);
        }
        
        SingleDocumentModel previous = current;
        current = model;
        
        notifyCurrentChanged(previous, current);
        
        return model;
    }

    /**
     * Checks if this model contains given document.
     *
     * @param model - document to be checked.
     * @return index of document.
     * @throws IllegalArgumentException if this model don't contain given document.
     */
    private int checkDocument(SingleDocumentModel model) {
        int index = documents.indexOf(model);

        if(index == -1) {
            throw new IllegalArgumentException("Document is not added to model");
        }

        return index;
    }

    /**
     * Creates {@code JPanel} with set {@code BorderLayout} and
     * places given component into center of layout.
     *
     * @param component - component to be added to layout.
     * @return created panel.
     */
    private static JPanel createPanel(JComponent component) {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        panel.add(new JScrollPane(component), BorderLayout.CENTER);

        return panel;
    }

    /**
     * Creates document listener that will perform actions of changing tab title and tab icon.
     *
     * @param model - document for which listener is created.
     * @return created listener.
     */
    private SingleDocumentListener createDocumentListener(SingleDocumentModel model) {
        return new SingleDocumentListener() {

            @Override
            public void documentModifyStatusUpdated(SingleDocumentModel model) {
                DefaultMultipleDocumentModel.this.setIcon(model);
            }

            @Override
            public void documentFilePathUpdated(SingleDocumentModel model) {
                DefaultMultipleDocumentModel.this.setTitle(model);
            }
        };
    }

    /**
     * Adds document listener for given document.
     *
     * @param document - document for which listener will be added.
     */
    private boolean addDocumentListener(SingleDocumentModel document) {
        if(Objects.isNull(documentListeners)) {
            documentListeners = new ArrayList<>();
        }

        SingleDocumentListener listener = createDocumentListener(document);

        document.addSingleDocumentListener(listener);
        return documentListeners.add(listener);
    }

    /**
     * Returns index of document with given path.
     *
     * @param path - document path.
     * @return index if such document is present otherwise -1.
     */
    private int documentIndex(Path path) {
        for (int i = 0; i < documents.size(); ++i) {
            if (path.equals(documents.get(i).getFilePath())) {
                return i;
            }
        }
        return -1;
    }
}
