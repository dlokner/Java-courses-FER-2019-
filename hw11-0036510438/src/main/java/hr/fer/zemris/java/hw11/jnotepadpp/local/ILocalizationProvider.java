package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * Implements object that provides localized texts.
 */
public interface ILocalizationProvider {

    /**
     * Adds localization listener.
     *
     * @param l listener to be added.
     */
    void addLocalizationListener(ILocalizationListener l);

    /**
     * Removes registered localization listener.
     *
     * @param l listener to be removed.
     */
    void removeLocalizationListener(ILocalizationListener l);

    /**
     * Gets localized string represented by given key.
     *
     * @param key key under which wanted value is stored.
     * @return localized value.
     */
    String getString(String key);

    /**
     * Gets current language.
     *
     * @return current language.
     */
    String getCurrentLanguage();
}
