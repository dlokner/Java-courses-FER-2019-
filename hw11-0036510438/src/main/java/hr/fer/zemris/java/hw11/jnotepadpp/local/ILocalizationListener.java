package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * Implements localisation observers.s
 */
@FunctionalInterface
public interface ILocalizationListener {

    /**
     * Signals that localization have been changed.
     */
    void localizationChanged();
}
