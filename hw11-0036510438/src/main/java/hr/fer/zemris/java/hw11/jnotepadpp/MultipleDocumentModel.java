package hr.fer.zemris.java.hw11.jnotepadpp;

import java.nio.file.Path;

/**
 * Model capable of holding zer, one or more documents.
 * Have concept of <i>current document</i> - the one which is currently shown to user.
 *
 * @author Domagoj Lokner
 */
public interface MultipleDocumentModel extends Iterable<SingleDocumentModel> {

    /**
     * Creates new document in this model.
     *
     * @return created document.
     */
    SingleDocumentModel createNewDocument();

    /**
     * Returns current document.
     *
     * @return current document.
     */
    SingleDocumentModel getCurrentDocument();

    /**
     * Loads document from given file path.
     *
     * @param path - path to document.
     * @return loaded document.
     * @throws NullPointerException if given path is {@code null}.
     */
    SingleDocumentModel loadDocument(Path path); //path must not be null

    /**
     * Saves document on disk.
     *
     * @param model - document to be saved to disk.
     * @param newPath - path on which document will be saved.
     */
    void saveDocument(SingleDocumentModel model, Path newPath);

    /**
     * Closes document.
     *
     * @param model - document to be closed.
     */
    void closeDocument(SingleDocumentModel model); //removes specified document (does not check modification status or ask any questions)

    /**
     * Registers new {@code addMultipleDocumentListener}.
     *
     * @param l - listener to be registered.
     */
    void addMultipleDocumentListener(MultipleDocumentListener l);

    /**
     * Removes given listener from registered listsener if it is present.
     *
     * @param l - listener to be removed.
     */
    void removeMultipleDocumentListener(MultipleDocumentListener l);

    /**
     * Returns number of documents in this model.
     *
     * @return number of documents.
     */
    int getNumberOfDocuments();

    /**
     * Gets document stored on given index.
     *
     * @param index - index of document.
     * @return document on given index.
     * @throws IndexOutOfBoundsException if there is no document on given index.
     */
    SingleDocumentModel getDocument(int index);
}