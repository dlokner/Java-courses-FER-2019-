package hr.fer.zemris.java.hw11.jnotepadpp;

import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.swing.FormLocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.swing.LocalizableAction;

import javax.swing.*;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultEditorKit;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Collator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.List;

import static javax.swing.JOptionPane.ERROR_MESSAGE;

/**
 * Simple text editor program.
 *
 * @author Domagoj Lokner
 */
public class JNotepadPP extends JFrame {

	private static final long serialVersionUID = 1348543319435136984L;

	/**
     * Editor component.
     */
    private DefaultMultipleDocumentModel editor;
    
    /**
     * JNotpadPP localization provider.
     */
    private ILocalizationProvider localProvider = new FormLocalizationProvider(
    		LocalizationProvider.getInstance(), 
    		this
    );

    /**
     * Constructs {@code JNotepadPP}.
     */
    public JNotepadPP() {
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setLocation(20, 20);
        setSize(700, 400);
        setTitle("JNotpad++");

        initGUI();
    }

    /**
     * Initialize GUI.
     */
    private void initGUI() {
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        JPanel editorPanel = new JPanel();
        editorPanel.setLayout(new BorderLayout());
        cp.add(editorPanel, BorderLayout.CENTER);

        editor = new DefaultMultipleDocumentModel();
        editorPanel.add(editor, BorderLayout.CENTER);

        StatusBarLabel statusBar = new StatusBarLabel(editor, localProvider);
        editorPanel.add(statusBar, BorderLayout.PAGE_END);

        //title and caret listener
        editor.addMultipleDocumentListener(new MultipleDocumentListener() {
            private final CaretListener caretListener = c -> {
            	statusBar.update();
            	
            	boolean enabled = false;
            	
            	if(c.getDot() == c.getMark() && !enabled) {
            			setEnableTools(false);
            			setEnableSort(false);
            			uniqueAction.setEnabled(false);
            			enabled = false;
            	} else {
        			setEnableTools(true);
        			setEnableSort(true);
        			uniqueAction.setEnabled(true);
        			enabled = true;
            	}
            	
            };

            @Override
            public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
            	statusBar.update();
            	updateTitle();
            	
            	if(!Objects.isNull(currentModel)) {
            		currentModel.getTextComponent().addCaretListener(caretListener);
            	}
            	
            	if(!Objects.isNull(previousModel)) {
            		previousModel.getTextComponent().removeCaretListener(caretListener);
            	}  	
            }

            @Override
            public void documentAdded(SingleDocumentModel model) {
            	updateTitle();
            }

            @Override
            public void documentRemoved(SingleDocumentModel model) {
            	updateTitle();
            }
        });

        //quits program
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                quitProgram();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                statusBar.stopClock();
            }
        });

        createActions();
        createMenus();

        cp.add(createToolBar(), BorderLayout.PAGE_START);

    }
    
    /**
     * Starting method of the program.
     *
     * @param args - command line arguments.
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new JNotepadPP().setVisible(true));
    }

    /**
     * Creates tool bar.
     *
     * @return created tool bar.
     */
    private JToolBar createToolBar() {
        JToolBar toolBar = new JToolBar();

        toolBar.setFloatable(true);

        toolBar.add(new JButton(createDocument));
        toolBar.add(new JButton(openDocument));
        toolBar.add(new JButton(saveDocument));
        toolBar.add(new JButton(saveDocumentAs));
        toolBar.addSeparator();
        toolBar.add(new JButton(copySelected));
        toolBar.add(new JButton(pasteSelected));
        toolBar.add(new JButton(cutSelected));
        toolBar.addSeparator();
        toolBar.add(statisticalInfo);
        toolBar.addSeparator();
        toolBar.addSeparator();
        toolBar.add(new JButton(closeDocument));
        toolBar.add(new JButton(exitProgram));

        return toolBar;
    }

    /**
     * Creates menu bar.
     *
     * @return created menu bar.
     */
    private JMenuBar createMenus() {
        JMenuBar mb = new JMenuBar();

        JMenu file = new JMenu(localProvider.getString("file"));
        mb.add(file);

        file.add(createDocument);
        file.add(openDocument);
        file.addSeparator();
        file.add(saveDocument);
        file.add(saveDocumentAs);
        file.addSeparator();
        file.add(statisticalInfo);
        file.addSeparator();
        file.add(closeDocument);
        file.add(exitProgram);

        JMenu edit = new JMenu(localProvider.getString("edit"));
        mb.add(edit);

        edit.add(copySelected);
        edit.add(pasteSelected);
        edit.add(cutSelected);
        
        JMenu languages = new JMenu(localProvider.getString("languages"));
        mb.add(languages);
        
        languages.add(setEnglish);
        languages.add(setCroatian);
        languages.add(setGerman);
        
        JMenu tools = new JMenu(localProvider.getString("tools"));
        mb.add(tools);
        
        tools.add(tooUpper);
        tools.add(tooLower);
        tools.add(invertCase);
        setEnableTools(false);
        
        JMenu sort = new JMenu(localProvider.getString("sort"));
        mb.add(sort);
        
        sort.add(sortAscending);
        sort.add(sortDescending);
        setEnableSort(false);

        JMenuItem unique = new JMenuItem(uniqueAction);
        uniqueAction.setEnabled(false);
        
        mb.add(unique);
        
        setJMenuBar(mb);
        
        localProvider.addLocalizationListener(() -> {
        	file.setText(localProvider.getString("file"));
        	edit.setText(localProvider.getString("edit"));
        	languages.setText(localProvider.getString("languages"));
        	unique.setText(localProvider.getString("unique"));
        });

        return mb;
    }

    /**
     * Helper method for adding descriptions, accelerator keys and mnemonic to action properties.
     */
    private void createActions() {       
        createDocument.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control N"));
        createDocument.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);

        openDocument.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
        openDocument.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);

        saveDocument.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
        saveDocument.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);

        saveDocumentAs.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control shift O"));
        saveDocumentAs.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);

        statisticalInfo.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control I"));
        statisticalInfo.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_I);

        closeDocument.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control W"));
        closeDocument.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_W);

        exitProgram.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control Q"));
        exitProgram.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_Q);

        copySelected.putValue(Action.NAME, localProvider.getString("copy"));
        copySelected.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
        copySelected.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
        copySelected.putValue(Action.SHORT_DESCRIPTION, localProvider.getString("copy_descript"));

        pasteSelected.putValue(Action.NAME, localProvider.getString("paste"));
        pasteSelected.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
        pasteSelected.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_V);
        pasteSelected.putValue(Action.SHORT_DESCRIPTION, localProvider.getString("paste_descript"));

        cutSelected.putValue(Action.NAME, localProvider.getString("cut"));
        cutSelected.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
        cutSelected.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
        cutSelected.putValue(Action.SHORT_DESCRIPTION, localProvider.getString("cut_descript"));
        
        setCroatian.putValue(Action.NAME, "Croatian");
        setCroatian.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
        setCroatian.putValue(Action.SHORT_DESCRIPTION, "Sets language to Croatian");
        
        setEnglish.putValue(Action.NAME, "English");
        setEnglish.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_E);
        setEnglish.putValue(Action.SHORT_DESCRIPTION, "Sets language to English");
        
        setGerman.putValue(Action.NAME, "German");
        setGerman.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_D);
        setGerman.putValue(Action.SHORT_DESCRIPTION, "Sets language to German");
        
        //localization for default actions
        localProvider.addLocalizationListener(() -> {
            copySelected.putValue(Action.NAME, localProvider.getString("copy"));
            pasteSelected.putValue(Action.NAME, localProvider.getString("paste"));
            cutSelected.putValue(Action.NAME, localProvider.getString("cut"));
        });
    }

    /**
     * Enable or disable tool actions.
     *  
     * @param b {@code true} to enable actions, {@code false} to disable.
     */
    private void setEnableTools(boolean b) {
    	tooLower.setEnabled(b);
    	tooUpper.setEnabled(b);
    	invertCase.setEnabled(b);
    }
    
    /**
     * Enable or disable sort actions.
     *  
     * @param b {@code true} to enable actions, {@code false} to disable.
     */
    private void setEnableSort(boolean b) {
    	sortAscending.setEnabled(b);
    	sortDescending.setEnabled(b);
    }
    
    
    /**
     * Update notepad title.
     */
    private void updateTitle() {
        int index = editor.getSelectedIndex();

        if (index != -1) {
            setTitle(editor.getTitleAt(index) + " - JNotepad++");
        } else {
            this.setTitle("JNotepad++");
        }
    }
    

    //
    //
    // Actions definition
    //
    //
    
    /**
     * Sets language to Croatian
     */
    private final Action setCroatian = new AbstractAction() {

		private static final long serialVersionUID = -3660225573128456868L;

		@Override
		public void actionPerformed(ActionEvent e) {
			LocalizationProvider.getInstance().setLanguage("hr");
		}
	};
    
	/**
     * Sets language to English
     */
    private final Action setEnglish = new AbstractAction() {

		private static final long serialVersionUID = -2796634880921047137L;

		@Override
		public void actionPerformed(ActionEvent e) {
			LocalizationProvider.getInstance().setLanguage("en");
		}
	};
	
	/**
     * Sets language to German
     */
    private final Action setGerman = new AbstractAction() {

		private static final long serialVersionUID = -3330931000709851504L;

		@Override
		public void actionPerformed(ActionEvent e) {
			LocalizationProvider.getInstance().setLanguage("de");
		}
	};
    
    

    /**
     * Action object which create new document.
     */
    private final Action createDocument = new LocalizableAction("create", localProvider) {
		
		private static final long serialVersionUID = -6821252397037245630L;

		@Override
        public void actionPerformed(ActionEvent e) {
            editor.createNewDocument();
        }
	};

    /**
     * Action object which opens file.
     */
    private final Action openDocument = new LocalizableAction("open", localProvider) {

		private static final long serialVersionUID = -1395661784327032415L;

		@Override
        public void actionPerformed(ActionEvent e) {
            open();
        }
    };

    /**
     * Action object which saves file.
     */
    private final Action saveDocument = new LocalizableAction("save", localProvider) {

		private static final long serialVersionUID = -9208466851547720119L;

		@Override
        public void actionPerformed(ActionEvent e) {
            if (isTabSelected()) {
                save(editor.getCurrentDocument());
            }
        }
    };

    /**
     * Save as action.
     */
    private final Action saveDocumentAs = new LocalizableAction("saveAs", localProvider) {
    	
		private static final long serialVersionUID = 3657858675264125773L;

		@Override
        public void actionPerformed(ActionEvent e) {
            if (isTabSelected()) {
                saveAs(editor.getCurrentDocument());
            }
        }
    };

    /**
     * Closes document. If all documents are closed quit program.
     */
    private final Action closeDocument = new LocalizableAction("close", localProvider) {

		private static final long serialVersionUID = 1623178307979843751L;

		@Override
        public void actionPerformed(ActionEvent e) {
            if(isTabSelected()) {
                close(editor.getCurrentDocument(), editor.getTitleAt(editor.getSelectedIndex()), true);
            }
        }
    };

    /**
     * Exit action
     */
    private final Action exitProgram = new LocalizableAction("quit", localProvider) {

		private static final long serialVersionUID = -8921259651835416559L;

		@Override
        public void actionPerformed(ActionEvent e) {
            quitProgram();
        }
    };

    /**
     * Copy action.
     */
    private final Action copySelected = new DefaultEditorKit.CopyAction();

    /**
     * Paste action.
     */
    private final Action pasteSelected = new DefaultEditorKit.PasteAction();

    /**
     * Cut action
     */
    private final Action cutSelected = new DefaultEditorKit.CutAction();


    /**
     * Displays statistical info about current document.
     */
    private final Action statisticalInfo = new LocalizableAction("statisticalInfo", localProvider) {

		private static final long serialVersionUID = -3115141778975041284L;

		@Override
        public void actionPerformed(ActionEvent e) {
            if (isTabSelected()) {
                displayDocumentInfo(editor.getCurrentDocument());
            }
        }
    };
    
    /**
     * Action that sets selected text to lower case.
     */
    private final Action tooUpper = new LocalizableAction("toUpper", localProvider) {

		private static final long serialVersionUID = -6278190031704100605L;
	
		@Override
		public void actionPerformed(ActionEvent e) {
			performOnSelected((s) -> s.toUpperCase());
		}	
	};
	
	/**
	 * Action that sets selected text to upper case.
	 */
    private final Action tooLower = new LocalizableAction("toLower", localProvider) {

		private static final long serialVersionUID = -2192575453755819752L;

		@Override
		public void actionPerformed(ActionEvent e) {
			performOnSelected((s) -> s.toLowerCase());
		}	
	};
	
	/**
	 * Action that invert case of selected text.
	 */
    private final Action invertCase = new LocalizableAction("invertCase", localProvider) {

		private static final long serialVersionUID = -8632949666956604123L;

		@Override
		public void actionPerformed(ActionEvent e) {
			performOnSelected((s) -> invertCase(s));
		}	
	};

	/**
	 * Sort selected lines ascending.
	 */
	private final Action sortAscending = new LocalizableAction("ascending", localProvider) {

		private static final long serialVersionUID = 1039196291708262397L;

		@Override
		public void actionPerformed(ActionEvent e) {
			sortLines(Collator.getInstance(new Locale(localProvider.getCurrentLanguage())), true);
		}
	};
	
	/**
	 * Sort selected lines descending.
	 */
	private final Action sortDescending = new LocalizableAction("descending", localProvider) {

		private static final long serialVersionUID = -2320654903477302660L;

		@Override
		public void actionPerformed(ActionEvent e) {
			sortLines(Collator.getInstance(new Locale(localProvider.getCurrentLanguage())), false);
		}
	};
	
	/**
	 * Removes duplicated lines
	 */
	private final Action uniqueAction = new LocalizableAction("unique", localProvider) {

		private static final long serialVersionUID = 1444795640905414262L;

		@Override
		public void actionPerformed(ActionEvent e) {
			removeDuplicatedLines();
		}
	};
	

    //
    //
    // Methods used for defining actions
    //
    //
	
	
	/**
	 * Sorts selected lines.
	 * 
	 * @param collator
	 */
	private void sortLines(Collator collator, boolean flag) {	
		JTextArea doc = editor.getCurrentDocument().getTextComponent();
		
		int startigLine = 0;
		int endingLine = 0;
		
		try {
			startigLine = doc.getLineOfOffset(startOfSelction());
			endingLine = doc.getLineOfOffset(startOfSelction() + selectionLength());
		} catch (BadLocationException ignorable) {
		}
		
		List<String> lines = getSelctedLines();
		
		if(flag) {
			lines.sort(collator);
		} else {
			lines.sort(collator.reversed());
		}
		
		StringBuilder sb = new StringBuilder();
		
		for(String line : lines) {
			sb.append(line + String.format("%n"));
		}
		
		try {
			doc.replaceRange(sb.toString(), 
					doc.getLineStartOffset(startigLine), 
					doc.getLineEndOffset(endingLine)
			);
		} catch (BadLocationException ignorable) {
		}
	}
	
	/**
	 * Gets list of all selected lines.
	 * 
	 * @return all selected lines.
	 */
	private List<String> getSelctedLines() {
		
		JTextArea doc = editor.getCurrentDocument().getTextComponent();
		List<String> lines = new LinkedList<>();
		
		try {
			int startLine = doc.getLineOfOffset(startOfSelction());
			int endLine = doc.getLineOfOffset(startOfSelction() + selectionLength());
						
			for(int i = startLine; i <= endLine; ++i) {
				lines.add(doc.getText(
						doc.getLineStartOffset(i), 
						doc.getLineEndOffset(i) - doc.getLineStartOffset(i)).trim()
				);
			}
			
		} catch (BadLocationException ignorable) {
		}
		
		return lines;
	}
	
	/**
	 * Gets list of all selected lines.
	 * 
	 * @return all selected lines.
	 */
	private void removeDuplicatedLines() {
		JTextArea doc = editor.getCurrentDocument().getTextComponent();
		
		int startigLine = 0;
		int endingLine = 0;
		
		try {
			startigLine = doc.getLineOfOffset(startOfSelction());
			endingLine = doc.getLineOfOffset(startOfSelction() + selectionLength());
		} catch (BadLocationException ignorable) {
		}
		
		List<String> lines = getSelctedLines();
		Set<String> linesSet = new LinkedHashSet<>(lines);
		
		StringBuilder sb = new StringBuilder();
		
		for(String line : linesSet) {
			sb.append(line + String.format("%n"));
		}
		
		try {
			if(!doc.getText(0, doc.getLineEndOffset(endingLine)).endsWith(String.format("%n"))) {
				sb.deleteCharAt(sb.length()-1);
			}

			doc.replaceRange(sb.toString(), 
					doc.getLineStartOffset(startigLine), 
					doc.getLineEndOffset(endingLine)
			);
		} catch (BadLocationException ignorable) {
		}
	}
	
	/**
	 * Convert all letters from selected text to upper case.
	 * 
	 * @param flag change case to upper if {@code flag} is {@code true} otherwise
	 * 		  case text will be set to lowerCase.
	 */
	private void performOnSelected(Function<String, String> function) {
		JTextArea doc = editor.getCurrentDocument()
				.getTextComponent();
		
		int start = startOfSelction();
		int len = selectionLength();
		
		String text = doc.getSelectedText();
		doc.replaceSelection(function.apply(text));
		
		doc.select(start, start + len);		
	}
	
	/**
	 * Returns caret position in document.
	 * 
	 * @return caret position
	 */
	private int caretPosition() {
		return editor.getCurrentDocument()
				.getTextComponent()
				.getCaret()
				.getDot();
	}
	
	/**
	 * Returns position of end of selection determined by 
	 * caret position.
	 * 
	 * @return mark position.
	 */
	private int markPosition() {
		return editor.getCurrentDocument()
				.getTextComponent()
				.getCaret()
				.getMark();
	}
	
	/**
	 * Invert case of given text.
	 * 
	 * @param text which case is going to be inverted.
	 * @return string with inverted case.
	 */
	private String invertCase(String text) {
		char[] chars = text.toCharArray();

		for (int i = 0; i < chars.length; ++i) {
			if (Character.isUpperCase(chars[i])) {
				chars[i] = Character.toLowerCase(chars[i]);
			} else if (Character.isLowerCase(chars[i])) {
				chars[i] = Character.toUpperCase(chars[i]);
			}
		}

		return new String(chars);
	}
	
	/**
	 * Gets starting point of selection.
	 * 
	 * @return start of selection.
	 */
	private int startOfSelction() {
		return Math.min(caretPosition(), markPosition());
	}
	
	/**
	 * Gets length of selected text.
	 * 
	 * @return length of selected text.
	 */
	private int selectionLength() {
		return Math.abs(caretPosition() - markPosition());
	}
	
	
    /**
     * Opens new document in editor.
     */
    private void open() {
        JFileChooser jfc = new JFileChooser();
        jfc.setDialogTitle(localProvider.getString("openFile"));

        if (jfc.showOpenDialog(JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
            return;
        }

        Path filePath = jfc.getSelectedFile().toPath();

        if (!Files.isReadable(filePath)) {
            JOptionPane.showMessageDialog(
                    JNotepadPP.this,
                    filePath + " " +
                    localProvider.getString("fileReachingError") + "!",
                    localProvider.getString("error"),
                    ERROR_MESSAGE);
            return;
        }

        editor.loadDocument(filePath);
    }

    /**
     * Saves current document.
     */
    private void save(SingleDocumentModel document) {
        Objects.requireNonNull(document);

        Path path = document.getFilePath();

        if (Objects.isNull(path)) {
            saveAs(document);
            return;
        }

        saveToPath(path, document);
    }

    /**
     * Editors <i>Save as</i> saving option.
     */
    private void saveAs(SingleDocumentModel document) {
        Objects.requireNonNull(document);

        JFileChooser jfc = new JFileChooser();
        jfc.setDialogTitle(localProvider.getString("saveAs")  + "...");

        if (jfc.showOpenDialog(this) != JFileChooser.APPROVE_OPTION) {
            JOptionPane.showMessageDialog(this,
                    localProvider.getString("savingCanceled"),
                    localProvider.getString("information"),
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        saveToPath(jfc.getSelectedFile().toPath(), document);

    }

    /**
     * Helper method for saving document.
     * Saves current document and print display messages.
     *
     * @param path - path to file where document should be saved.
     * @param document - document to be saved.
     */
    private void saveToPath(Path path, SingleDocumentModel document) {

        try {
            editor.saveDocument(document, path);
        } catch (IllegalArgumentException ex) {
            JOptionPane.showMessageDialog(this,
                    localProvider.getString("savingError") + ". " + ex.getMessage(),
                    localProvider.getString("error"),
                    ERROR_MESSAGE);
            return;
        }

        updateTitle();

        JOptionPane.showMessageDialog(this,
                localProvider.getString("savingSucc"),
                localProvider.getString("information"),
                JOptionPane.INFORMATION_MESSAGE);
    }


    /**
     * Checks if document is ready for closing and close it if flag is {@code true}.
     *
     * @param document - document to be closed.
     * @param title - document title.
     * @param flag - flag that indicates that document should not be closed.
     * @return {@code true} if document can be removed otherwise {@code false}.
     */
    private boolean close(SingleDocumentModel document, String title, boolean flag) {

        if(document.isModified()) {
            int option = JOptionPane.showOptionDialog(
                    this,
                    localProvider.getString("unsaved1") + 
                    " " + title + " " +
                    localProvider.getString("unsaved2") + " " +
                    localProvider.getString("savingQuestion"),
                    localProvider.getString("warning"),
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.WARNING_MESSAGE,
                    null,
                    new String[]{
	                    localProvider.getString("savingApprove"),
	                    localProvider.getString("savingRefuse"),
	                    localProvider.getString("cancel")},
                    null
            );

            if (option == JOptionPane.YES_OPTION) {
                save(document);
            } else if (option == JOptionPane.CANCEL_OPTION) {
                return false;
            }
        }

        if(flag) {
            editor.closeDocument(document);
        }

        return true;
    }

    /**
     * Checks if all documents are saved. User will be asked for saving all unsaved windows.
     * If user don't cancel operation all documents will be closed on the end.
     *
     * @return {@code true} if user want to close program otherwise {@code false}.
     */
    private boolean checkAndClose() {
        for(int i = 0; i < editor.getNumberOfDocuments(); ++i) {
            if (!close(editor.getDocument(i), editor.getTitleAt(i), false)) {
                return false;
            }
        }

        closeAll();
        return true;
    }

    /**
     * Closes all documents.
     */
    private void closeAll() {
        for (int i = 0; i < editor.getNumberOfDocuments(); ++i) {
            editor.closeDocument(editor.getDocument(i));
        }
    }

    /**
     * Exits program.
     */
    private void quitProgram() {
        if (checkAndClose()) {
            dispose();
        }
    }

    /**
     * Displays informations about document.
     *
     * @param documentModel - document which informations will be displayed.
     */
    private void displayDocumentInfo(SingleDocumentModel documentModel) {
        JOptionPane.showOptionDialog(this,
                localProvider.getString("docInfo1") + " " +
                numberOfCharacters(documentModel) + " " + 
                localProvider.getString("docInfo2") + " " +
                numberOfNonBlanks(documentModel) + " " +
                localProvider.getString("docInfo3") + " " +
                numberOfLines(documentModel) + " " +
                localProvider.getString("docInfo4"),
                localProvider.getString("statisticalInfo"),
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.INFORMATION_MESSAGE,
                null,
                null,
                null);
    }

    /**
     * Checks if any tab is selected in {@code editor}.
     *
     * @return {@code true} if there is selected tab otherwise {@code false}.
     */
    private boolean isTabSelected() {
        return editor.getSelectedIndex() != -1;
    }

    /**
     * Number of characters given document contains.
     *
     * @param document - document which characters will be counted.
     * @return number of characters, or -1 if null is given.
     */
    private static int numberOfCharacters(SingleDocumentModel document) {
        if (Objects.isNull(document)) {
            return -1;
        }
        return document.getTextComponent().getText().length();
    }

    /**
     * Number of non-blank characters given document contains.
     *
     * @param document - document which characters will be counted.
     * @return number of non-blank characters, or -1 if null is given.
     */
    private static int numberOfNonBlanks(SingleDocumentModel document) {
        if (Objects.isNull(document)) {
            return -1;
        }

        int result = 0;
        String text = document.getTextComponent().getText();

        for(char c : text.toCharArray()) {
            if(Character.isWhitespace(c)) {
                continue;
            }
            ++result;
        }

        return result;
    }

    /**
     * Number of lines given document contains.
     *
     * @param document - document which characters will be counted.
     * @return number of lines, or -1 if null is given.
     */
    private static int numberOfLines(SingleDocumentModel document) {
        if (Objects.isNull(document)) {
            return -1;
        }

        return  document.getTextComponent().getLineCount();
    }
}