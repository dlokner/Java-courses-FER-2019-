package hr.fer.zemris.java.hw11.jnotepadpp;

/**
 * Document listener.
 *
 * @author Domagoj Lokner
 */
public interface SingleDocumentListener {

    /**
     * Performs if document have been modified.
     *
     * @param model - modified document.
     */
    void documentModifyStatusUpdated(SingleDocumentModel model);

    /**
     * Performs if document path have been updated.
     *
     * @param model - document which path have been modified.
     */
    void documentFilePathUpdated(SingleDocumentModel model);
}
