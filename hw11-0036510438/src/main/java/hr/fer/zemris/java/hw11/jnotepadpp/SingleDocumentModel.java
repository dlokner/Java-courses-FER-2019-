package hr.fer.zemris.java.hw11.jnotepadpp;

import javax.swing.*;
import java.nio.file.Path;

/**
 * Model of a single document.
 * <ul>
 *     This model stores informations about document:
 * <li>path from which document was loaded</li>
 * <li>modification status</li>
 * <li>reference to component which is used for editing</li>
 * </ul>
 *
 */
public interface SingleDocumentModel {

    /**
     * Returns component used for editing document.
     *
     * @return text component used for editing this document.
     */
    JTextArea getTextComponent();

    /**
     * Gets file path from which this document was loaded.
     *
     * @return file path.
     */
    Path getFilePath();

    /**
     * Sets file path to given {@code path}.
     *
     * @param path - path to be set as this document file path.
     * @throws NullPointerException if given path is {@code null}.
     */
    void setFilePath(Path path); //file path can not be null

    /**
     * Checks if file is modified.
     *
     * @return {@code true} if file is modified otherwise {@code false}.
     */
    boolean isModified();

    /**
     * Sets file modification flag to given value.
     *
     * @param modified - value to which modification flag will be set.
     */
    void setModified(boolean modified);

    /**
     * Register new {@code addSingleDocumentListener}.
     *
     * @param l - {@code addSingleDocumentListener} to be registered as listener.
     */
    void addSingleDocumentListener(SingleDocumentListener l);

    /**
     * Removes given listener if it ise present in currently registered listeners.
     *
     * @param l - listener to be removed.
     */
    void removeSingleDocumentListener(SingleDocumentListener l);
}