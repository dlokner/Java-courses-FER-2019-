package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.LinkedList;
import java.util.List;

/**
 * Abstract localization provider class implements implements
 * registering listeners on localization provider.
 *
 * @author Domagoj Lokner
 */
public abstract class AbstractLocalizationProvider implements ILocalizationProvider {

    /**
     * Collection that stores all registered listeners.
     */
    private List<ILocalizationListener> listeners;

    /**
     * Constructs {@code AbstractLocalizationProvider}.
     */
    public AbstractLocalizationProvider() {
        listeners = new LinkedList<>();
    }

    @Override
    public void addLocalizationListener(ILocalizationListener l) {
        listeners.add(l);
    }

    @Override
    public void removeLocalizationListener(ILocalizationListener l) {
        listeners.remove(l);
    }

    /**
     * Inform all listeners that localization was changed.
     */
    public void fire() {
        listeners.forEach(l -> l.localizationChanged());
    }
}
