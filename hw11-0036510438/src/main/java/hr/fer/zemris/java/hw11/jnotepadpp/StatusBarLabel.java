package hr.fer.zemris.java.hw11.jnotepadpp;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;

import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

import java.awt.*;
import java.util.Objects;

/**
 * Panel implements status bar object for {@code editor} property.
 * Status bar displays length of currently opened document, carter position in line
 * and column, and selected text length
 *
 * @author Domagoj Lokner
 */
public class StatusBarLabel extends JPanel {

	private static final long serialVersionUID = -2948596881882670830L;

	/**
     * Model for which bar will display informations.
     */
    private DefaultMultipleDocumentModel editor;
    
    /**
     * Provides localized text for status bar.
     */
    private ILocalizationProvider localProvider;

    /**
     * Label that displays length.
     */
    private JLabel length;

    /**
     * Label which displays informations about caret,
     */
    private JLabel caretInfo;

    /**
     * Displays current date and time
     */
    private DateLabel dateAndTime;

    /**
     * Constructs new {@code StatusBar}.
     * 
     * @param editor model for which status bar will be used.
     * @param localProvider object that provides localized text for status bar.
     */
    public StatusBarLabel(DefaultMultipleDocumentModel editor, ILocalizationProvider localProvider) {
        this.editor = editor;
        this.localProvider = localProvider;

        setLayout(new GridLayout(1, 0));

        length = new JLabel();
        length.setHorizontalAlignment(JLabel.LEFT);

        caretInfo = new JLabel();
        caretInfo.setHorizontalAlignment(JLabel.LEFT);

        dateAndTime = new DateLabel("yyyy/MM/dd HH:mm:ss");
        dateAndTime.setHorizontalAlignment(JLabel.RIGHT);

        add(length);
        add(caretInfo);
        add(dateAndTime);
        
        localProvider.addLocalizationListener(() -> update());
        
        update();
    }

    /**
     * Updates status bar arguments.
     */
    public void update() {
        SingleDocumentModel document = editor.getCurrentDocument();

        if (Objects.isNull(document)) {
            setEmpty();
            return;
        } 

        length.setText(localProvider.getString("length") + ":" + getLength(document));
        
        caretInfo.setText(
                localProvider.getString("ln") + ":" +  getLine(document.getTextComponent()) + " " +
                localProvider.getString("col") + ":" + getColumn(document.getTextComponent()) + " " +
                localProvider.getString("sel") + ":" + getSelectedLength(document.getTextComponent().getCaret())

        );
    }

    /**
     * Sets status bar arguments to empty strings.
     */
    public void setEmpty() {
    	length.setText("");
    	caretInfo.setText("");
    }
    
    
    /**
     * Stops displayed clock.
     */
    public void stopClock() {
        dateAndTime.stop();
    }

    /**
     * Gets length of selected area.
     *
     * @param caret - document caret.
     * @return length of selected text.
     */
    private int getSelectedLength(Caret caret) {
        return Math.abs(caret.getDot()-caret.getMark());
    }

    /**
     * Compute caret positions in columns.
     *
     * @param document - text component which caret position will be computed.
     * @return caret position in columns.
     */
    private int getColumn(JTextArea document) {
        int line = getLine(document) - 1;
        int position = document.getCaret().getDot();

        int result = -1;

        try {
            result = position - document.getLineStartOffset(line);
        } catch (BadLocationException ignorable) {
        }

        return result + 1;
    }

    /**
     * Compute caret positions in lines.
     *
     * @param document - text component caret position will be computed.
     * @return caret position in lines.
     */
    private int getLine(JTextArea document) {
        int result = -1;

        int dot = document.getCaret().getDot();

        try {
            result = document.getLineOfOffset(dot);
        } catch (BadLocationException ignorable) {
        }

        return result + 1;
    }

    /**
     * Gets length of document.
     *
     * @param document - document which length will be computed.
     * @return length.
     */
    private int getLength(SingleDocumentModel document) {
        return document.getTextComponent().getText().length();
    }
    
//    private final MultipleDocumentListener editorListener = new MultipleDocumentListener() {
//        private final CaretListener caretListener = c -> update();
//    	
//		@Override
//		public void documentRemoved(SingleDocumentModel model) {
//		}
//		
//		@Override
//		public void documentAdded(SingleDocumentModel model) {
//			update();			
//		}
//		
//		@Override
//		public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
//            if (!Objects.isNull(previousModel)) {
//                previousModel.getTextComponent().removeCaretListener(caretListener);
//            }
//            currentModel.getTextComponent().addCaretListener(caretListener);		
//		}
//	};
}
