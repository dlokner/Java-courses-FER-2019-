package hr.fer.zemris.java.hw11.jnotepadpp;

import javax.swing.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Label which displays current date and time.<br>
 * Label updates time every 500ms and displays it in format:
 * <i>"yyyy/MM/dd HH:mm:ss"</i>
 *
 * @author Domagoj Lokner
 */
public class DateLabel extends JLabel {

	private static final long serialVersionUID = 1865840306739317127L;

	/**
     * Date format
     */
    private DateFormat dateFormat;

    /**
     * Signals that date shouldn't be updated anymore.
     */
    volatile boolean stop;

    /**
     * Constructs new {@code DateLabel}.
     *
     * @throws NullPointerException if {@code null} is given as argument.
     * @throws IllegalArgumentException if invalid date format is given.
     */
    public DateLabel(String format) {
        dateFormat = new SimpleDateFormat(format);

        updateTime();

        Thread t = new Thread(() -> {
            while(true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignorable) {
                }

                if(stop) {
                    break;
                }

                SwingUtilities.invokeLater(() -> updateTime());
            }
        });

        t.setDaemon(true);
        t.start();
    }

    /**
     * Stops date and time update.
     */
    public void stop() {
        stop = true;
    }

    /**
     * Updates informations about current date and time.
     */
    private void updateTime() {
        Date date = new Date();

        setText(dateFormat.format(date));
    }
}