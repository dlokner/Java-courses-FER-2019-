package hr.fer.zemris.java.hw11.jnotepadpp;

/**
 * Multiple document listener.
 *
 * @author Domagoj Lokner
 */
public interface MultipleDocumentListener {

    /**
     * Performs if current document have been changed.
     *
     * @param previousModel - previous document.
     * @param currentModel - new current document.
     */
    void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel);

    /**
     * Performs if document is added.
     *
     * @param model - added document.
     */
    void documentAdded(SingleDocumentModel model);

    /**
     * Perform if document is removed.
     *
     * @param model - removed document.
     */
    void documentRemoved(SingleDocumentModel model);
}

//previousModel and currentModel can be null but not both