package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DictionaryTest {

	private Dictionary<String, Integer> dictionary = new Dictionary<>();
	
	@BeforeEach
	void initDictionary() {
		dictionary.clear();
		
		dictionary.put("Štefica", 5);
		dictionary.put("Siniša", -1);
		dictionary.put("Milka", 3);
		dictionary.put("Dragan", -57);
	}
	
	@Test
	void putTestValue(){
		dictionary.put("Ranko", 122);
		dictionary.put("Dragan", 0);
		
		assertEquals(122, dictionary.get("Ranko"));
		assertEquals(0, dictionary.get("Dragan"));
	}
	
	@Test
	void putTestNull() {
		assertNull(dictionary.get("Jasna"));
		assertNull(dictionary.get(2.134));
		assertNull(dictionary.get('5'));
	}
	
	@Test
	void get(){
		assertEquals(5, dictionary.get("Štefica"));
		assertEquals(-57, dictionary.get("Dragan"));
	}
	
	@Test
	void isEmpty() {
		assertFalse(dictionary.isEmpty());
		
		dictionary.clear();
		
		assertTrue(dictionary.isEmpty());
	}
	
	@Test
	void sizeTest() {
		assertEquals(4, dictionary.size());
		
		dictionary.put("Ranko", 122);
		dictionary.put("Dragan", 0);
		
		assertEquals(5, dictionary.size());
	}

}
