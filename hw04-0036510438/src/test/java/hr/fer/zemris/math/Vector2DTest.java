package hr.fer.zemris.math;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector2DTest {

	@Test
	void constructorTest() {
		Vector2D v1 = new Vector2D(154.5678, -98.56712);
		
		assertEquals(154.5678, v1.getX(), 0.0001);
		assertEquals(-98.56712, v1.getY(), 0.0001);
	}
	
	@Test
	void translateTest() {
		Vector2D v1 = new Vector2D(154.5678, -98.56712);
		Vector2D v2 = new Vector2D(2.134, 6.5432);
		
		v1.translate(v2);
		
		checkVector(new Vector2D(156.7018, -92.0239), v1);
		
	}
	
	@Test
	void translatedTest() {
		Vector2D v1 = new Vector2D(154.5678, -98.56712);
		Vector2D v2 = new Vector2D(2.134, 6.5432);
		
		checkVector(new Vector2D(156.7018, -92.0239), v1.translated(v2));
		
	}
	
	@Test
	void rotateTest() {
		Vector2D v1 = new Vector2D(1d, 1d);
		v1.rotate(3.50);
		
		checkVector(v1, new Vector2D(-0.59, -1.29));
	}
	
	@Test
	void rotateTest2() {
		Vector2D v1 = new Vector2D(3.6567, -12.456);
		v1.rotate(1.121);
		
		checkVector(v1, new Vector2D(12.806, -2.122));
	}
	
	@Test
	void rotatedTest() {
		checkVector(new Vector2D(-0.59, -1.29), new Vector2D(1d, 1d).rotated(3.5));
	}
	
	@Test
	void rotatedTest2() {
		checkVector(new Vector2D(12.806, -2.122), new Vector2D(3.6567, -12.456).rotated(1.121));
	}
	
	@Test
	void scaleTest() {
		Vector2D v1 = new Vector2D(154.5678, -98.56712);
		double scaler = -7.125;
		
		v1.scale(scaler);
		
		checkVector(new Vector2D(-1101.295575, 702.29073), v1);
	}
	
	@Test
	void scaledTest() {
		Vector2D v1 = new Vector2D(154.5678, -98.56712);
		double scaler = -7.125;
		
		checkVector(new Vector2D(-1101.295575, 702.29073), v1.scaled(scaler));
	}
	
	@Test
	void copyTest() {
		Vector2D v1 = new Vector2D(123.09876543, -65.45789);
		Vector2D v2 = new Vector2D(-678.437468705, 0.0001);
		
		checkVector(v1, v1.copy());
		checkVector(v2, v2.copy());
	}
	
	void checkVector(Vector2D v1, Vector2D v2) {
		assertEquals(v1.getX(), v2.getX(), 0.01);
		assertEquals(v1.getY(), v2.getY(), 0.01);
	}
}
