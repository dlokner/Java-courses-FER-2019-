package hr.fer.zemris.java.custom.collections;

/**
 * Implements object that gets elements from collection.
 * 
 * @author Domagoj Lokner
 *
 * @param <E> - type of elements stored into collection.
 */
public interface ElementsGetter<E> {

	/**
	 * Returns {@code true} if there is more elements to be gotten.
	 * 
	 * @return {@code true} if there is more elements otherwise {@code false}.
	 */
	boolean hasNextElement();

	/**
	 * Gets value of next element in collection.
	 * 
	 * @return value of next element.
	 */
	E getNextElement();
 
	/**
	 * Call {@code p.process} method for all remaining elements of collection.
	 * 
	 * @param p - {@code Processor}
	 */
	default void processRemaining(Processor<? super E> p) {
		while(hasNextElement()) {
			p.process(getNextElement());
		}
	}
}
