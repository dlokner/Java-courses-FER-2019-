package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * This class implements stack that storage {@code Object} type elements.
 * 
 * @author Domagoj Lokner
 *
 * @param <T> - type of objects that this object stores.
 */
public class ObjectStack<T> {
	private ArrayIndexedCollection<T> array;
	
	/**
	 * Defaukt constructor. Allocate space for new collection.
	 */
	public ObjectStack() {
		array = new ArrayIndexedCollection<T>();
	}
	
	/**
	 * Checks if stack contains no objects.
	 * 
	 * @return {@code true} if stack is empty otherwise {@code false}.
	 */
	public boolean isEmpty() {
		return array.isEmpty();
	}
	
	/**
	 * Returns the number of currently stored elements on the stack.
	 * 
	 * @return number of elements on the stack.
	 */
	public int size() {
		return array.size();
	}
	
	/**
	 * Pushes given {@code variable} on the stack.
	 * 
	 * @param value - value to be pushed.
	 */
	public void push(T value) {
		Objects.requireNonNull(value);
		
		array.add(value);
	}
	
	/**
	 * Removes last value pushed on the stack.
	 * 
	 * @return last value pushed on the stack.
	 * @throws EmptyStackException if stack is empty.
	 */
	public T pop() {
		if(isEmpty()) {
			throw new EmptyStackException();
		}
		
		T result = array.get(size() - 1);
		array.remove(size() - 1);
		return result;
	}
	
	 /**
	  * Returns last element placed on the stack 
	  * but does note remove it from stack.
	  * 
	  * @return last element placed on the stack.
	  */
	public T peek() {
		return array.get(size() - 1);
	}
	
	/**
	 * Removes all elements from stack.
	 */
	public void clear() {
		array.clear();
	}
	
}
