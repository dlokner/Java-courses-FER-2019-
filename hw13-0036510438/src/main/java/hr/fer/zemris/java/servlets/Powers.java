package hr.fer.zemris.java.servlets;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet that sends as result xls file.
 * This servlet expects three parameters {@code a, b, n}.
 *
 * Resulting xls file will contain {@code n} pages and on every page will be
 * two columns. In first column are numbers from {@code a} to {@code b} and
 * in second column are numbers form first powered by number of page.
 *
 * @author Domagoj Lokner
 */
@WebServlet(name = "powers", urlPatterns = "/powers")
public class Powers extends HttpServlet {

	private static final long serialVersionUID = 6818610196986553103L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String paramA = req.getParameter("a");
        String paramB = req.getParameter("b");
        String paramN = req.getParameter("n");

        int a, b, n;

        try {
            a = intParameter(paramA, -100, 100);
            b = intParameter(paramB, -100, 100);
            n = intParameter(paramN, 1, 5);
        } catch (NullPointerException ex) {
            req.getRequestDispatcher(
                    "/error?message=File can't be generated, invalid arguments are given.")
                    .forward(req, resp);
            return;
        }

        HSSFWorkbook hwb = new HSSFWorkbook();
        
        constructExcelFile(hwb, n, a, b);

        byte[] xlsFile = hwb.getBytes();
        
        resp.setContentType("application/vnd.ms-excel");
        resp.setContentLength(xlsFile.length);
        resp.setHeader("Content-Disposition", "attachment; filename=\"tablica.xls\"");
        resp.getOutputStream().write(xlsFile);
        resp.getOutputStream().flush();

        req.getRequestDispatcher("index.jsp");
    }

    /**
     * Constructs excel file.
     *
     * @param hwb excel file that will be constructed.
     * @param n number of pages.
     * @param a lowest number in list.
     * @param b greatest number in list.
     */
    private void constructExcelFile(HSSFWorkbook hwb, int n, int a, int b) {
        for (int i = 1; i <= n; ++i) {
            constructPage(hwb, i, a, b);
        }
    }

    /**
     * Constructs single page of excel file.
     *
     * @param hwb excel file that will be constructed.
     * @param num page number.
     * @param a lowest number in list.
     * @param b greatest number in list.
     */
    private void constructPage(HSSFWorkbook hwb, int num, int a, int b) {
        HSSFSheet sheet = hwb.createSheet("Page " + num);

        HSSFRow rowhead = sheet.createRow(0);
        rowhead.createCell(0).setCellValue("x");
        rowhead.createCell(1).setCellValue("x^" + num);

        for (int i = 0; i <= b - a; ++i) {
            HSSFRow row = sheet.createRow(i+1);

            row.createCell(0).setCellValue(a + i);
            row.createCell(1).setCellValue((int)Math.pow(a+i, num));
        }

    }

    /**
     * Extracts int parameter from given string.
     *
     * @param param string from which int parameter will be parsed.
     * @param lbound lower bound of parameter value (included).
     * @param ubound upper bound of parameter value (included).
     * @return integer parameter or null if parameter is out of bounds or
     *         if given string can't be parsed to int.
     */
    private Integer intParameter(String param, int lbound, int ubound) {

        Integer result;
        try {
            result = Integer.parseInt(param);
        } catch (NumberFormatException ex) {
            return null;
        }

        if (result < lbound || result > ubound) {
            return null;
        }

        return result;
    }
}
