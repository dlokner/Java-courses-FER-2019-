package hr.fer.zemris.java.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Servlet that calculates sine and cosine for all number whole numbers between
 * values determined with parameters <i>a</i> and <i>b</i>.
 * If parameters are not defined default values will be used
 * (0 for <i>a</i> and 360 for <i>b</i>).
 *
 * @author Domagoj Lokner
 */
@WebServlet(name = "trigonometric", urlPatterns = "/trigonometric")
public class Trigonometric extends HttpServlet {

	private static final long serialVersionUID = -2701419815633804783L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int a = parseValue(req.getParameter("a"), 0);
        int b = parseValue(req.getParameter("b"), 360);

        if (a > b) {
            int tmp = a;
            a = b;
            b = tmp;
        }

        if (b > a + 720) {
            b = a + 720;
        }

        List<TrigonometricResult> results = new LinkedList<>();

        for (int i = a; i <= b; ++i) {
            results.add(new TrigonometricResult(
               i,
               String.format("%.10f", Math.sin(Math.toRadians(i))),
               String.format("%.10f", Math.cos(Math.toRadians(i)))
            ));
        }

        req.getSession().setAttribute("trigResults", results);

        req.getRequestDispatcher("/WEB-INF/pages/trigonometric.jsp").forward(req, resp);
    }

    /**
     * Parse given string to int value.
     *
     * @param value string to be parsed.
     * @param defValue default value which will be returned if
     *                 given string can't be interpreted as int.
     * @return int value.
     */
    private int parseValue(String value, int defValue) {
        int val;

        try {
            val = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            val = defValue;
        }

        return val;
    }

    /**
     * Java-bean object that represents result of trigonometric operations
     * for single value.
     *
     * @author Domagoj Lokner
     */
    public class TrigonometricResult {

        private int x;

        /**
         * Sine of {@code x}.
         */
        private String sin;

        /**
         * Cosine of {@code x}.
         */
        private String cos;


        /**
         * Constructs new {@code TrigonometricResult} object.
         *
         * @param x x
         * @param sin sin(x)
         * @param cos cos(x)
         */
        public TrigonometricResult(int x, String sin, String cos) {
            this.x = x;
            this.sin = sin;
            this.cos = cos;
        }

        /**
         * Gets x.
         *
         * @return x.
         */
        public int getX() {
            return x;
        }

        /**
         * Gets sine of x.
         *
         * @return sin(x).
         */
        public String getSin() {
            return sin;
        }

        /**
         * Gets cosine of x
         * @return cos(x).
         */
        public String getCos() {
            return cos;
        }
    }

}
