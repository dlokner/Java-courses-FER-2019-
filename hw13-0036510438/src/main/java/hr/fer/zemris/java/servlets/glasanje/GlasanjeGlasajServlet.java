package hr.fer.zemris.java.servlets.glasanje;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.Objects;

/**
 * Servlet that updates database that stores voting results.
 * Vote which id is sent as <i>"id"</i> argument will be incremented or
 * created with value 1 if it is not present.
 *
 * @author DomagojLokner
 */
@WebServlet(name = "glasanjeGlasaj", urlPatterns = "/glasanje-glasaj")
public class GlasanjeGlasajServlet extends HttpServlet {

	private static final long serialVersionUID = -8866108361878134703L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");

        if (Objects.isNull(id)) {
            req.getRequestDispatcher("/error?message=Invalid band ID.").forward(req, resp);
            return;
        }

        String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");

        Path filePath = GlasanjeUtil.getFile(fileName);

        List<String> votes = GlasanjeUtil.updateVote(id, Files.readAllLines(filePath));
        Files.write(filePath, votes, StandardOpenOption.TRUNCATE_EXISTING);

        resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
    }
}
