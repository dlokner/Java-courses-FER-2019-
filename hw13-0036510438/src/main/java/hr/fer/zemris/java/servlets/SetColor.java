package hr.fer.zemris.java.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * Servlets that receive hex number represents rgb color as parameter.<br>
 *
 * If <i>bgcolor</i> parameter is received that color will be seted as
 * background color for all pages on this server otherwise <b>white</b>
 * will be used as default color.
 *
 * @author Domagoj Lokner
 */
@WebServlet(name = "setcolor", urlPatterns = "/setcolor")
public class SetColor extends HttpServlet {

	private static final long serialVersionUID = -4846892109829500123L;
	
	/**
     * Default background color.
     */
    private final String DEFAULT_BGCOLOR = "FFFFFF";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bgcolor = req.getParameter("bgcolor");

        if (Objects.isNull(bgcolor)) {
            bgcolor = DEFAULT_BGCOLOR;
        }

        req.getSession().setAttribute("pickedBgCol", bgcolor);

        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }
}
