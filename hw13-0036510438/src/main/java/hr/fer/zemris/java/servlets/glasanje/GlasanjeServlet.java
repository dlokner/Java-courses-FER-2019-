package hr.fer.zemris.java.servlets.glasanje;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Prepares list of all bands for which user can vote and sends
 * request to jsp page that will construct voting page.
 *
 * @author Domagoj Lokner
 */
@WebServlet(name = "glasanje", urlPatterns = "/glasanje")
public class GlasanjeServlet extends HttpServlet {

	private static final long serialVersionUID = 4694141553261047774L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");

        Path filePath = Paths.get(fileName);

        List<String> lines = Files.readAllLines(filePath);
        Collection<Band> bands = GlasanjeUtil.getBandsMap(lines).values();

        req.setAttribute("bands", bands);
        req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
    }


}
