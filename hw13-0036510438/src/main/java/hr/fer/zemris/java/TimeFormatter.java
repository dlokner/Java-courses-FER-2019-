package hr.fer.zemris.java;

import java.util.concurrent.TimeUnit;

/**
 * Object that offer methods to get formatted string that represents
 * time interval that is passed from constructing this object.
 *
 * @author Domagoj Lokner
 */
public class TimeFormatter {

    /**
     * Time of constructing the object.
     */
    private long start;

    /**
     * Constructs {@code TimeFormatter}.
     */
    public TimeFormatter() {
        this.start = System.currentTimeMillis();
    }

    /**
     * Returns formatted time that is passed
     * @return
     */
    public String format() {
        long interval = System.currentTimeMillis() - start;

        long days = TimeUnit.MILLISECONDS.toDays(interval);

        interval -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(interval);

        interval -= TimeUnit.HOURS.toMillis(hours);
        long min = TimeUnit.MILLISECONDS.toMinutes(interval);

        interval -= TimeUnit.MINUTES.toMillis(min);
        long sec = interval / 1000;

        long milli = interval % 1000;

        return formString(days, hours, min, sec, milli);
    }

    /**
     * Forms time result.
     *
     * @param days number of days
     * @param hours number of hours
     * @param min number of minutes
     * @param sec number of seconds
     * @param milli number of milliseconds
     * @return formatted time interval.
     */
    private String formString(long days, long hours, long min, long sec, long milli) {
        StringBuilder sb = new StringBuilder();

        if (days != 0) {
            sb.append(days + " days ");
        }

        if (hours != 0) {
            sb.append(hours + " hours ");
        }

        if (min != 0) {
            sb.append(min + " minutes ");
        }

        sb.append(sec + " seconds and " + milli + "milliseconds");

        return sb.toString();
    }

}
