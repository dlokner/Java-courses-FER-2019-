package hr.fer.zemris.java.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet that sends error message it received as parameter to jsp that
 * constructs error page.
 *
 * @author Domagoj Lokner
 */
@WebServlet(name = "error", urlPatterns = "/error")
public class ErrorServlet extends HttpServlet {

	private static final long serialVersionUID = 5142909281422644090L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String message = req.getParameter("message");
        req.setAttribute("errMessage", message);
        req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
    }
}
