package hr.fer.zemris.java.servlets;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet that constructs and sends HTTP response to request for image of chart.
 * Chart will show percents of certain usage operating systems.
 *
 * @author Domagoj Lokner
 */
@WebServlet(name = "reportImg", urlPatterns = "/reportImage")
public class ReportImage extends HttpServlet {

	private static final long serialVersionUID = 5553167215148403831L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JFreeChart chart = createChart(createDataset(), "OS usage");

        byte[] img = ChartUtils.encodeAsPNG(chart.createBufferedImage(500, 300));

        resp.setContentType("image/png");
        resp.setContentLength(img.length);

        resp.getOutputStream().write(img);
    }

    /**
     * Creates a OS usage dataset.
     */
    private PieDataset createDataset() {
        DefaultPieDataset result = new DefaultPieDataset();
        result.setValue("Linux", 29);
        result.setValue("Mac", 20);
        result.setValue("Windows", 51);
        return result;
    }

    /**
     * Creates a chart.
     */
    private JFreeChart createChart(PieDataset dataset, String title) {

        JFreeChart chart = ChartFactory.createPieChart3D(
                title,                  // chart title
                dataset,                // data
                true,                   // include legend
                true,
                false
        );

        PiePlot3D plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
        return chart;

    }

}
