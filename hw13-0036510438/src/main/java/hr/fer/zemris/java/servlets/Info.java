package hr.fer.zemris.java.servlets;

import hr.fer.zemris.java.TimeFormatter;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Web listener object that stores time of starting server in
 * servlet context attributes.
 *
 * @author Domagoj Lokner
 */
@WebListener
public class Info implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute(
                "timeFormat",
                new TimeFormatter()
        );
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
