package hr.fer.zemris.java.servlets.glasanje;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet that prepares informations about results of voting
 * and dispatches tham to jsp to produce page with results.
 *
 * @author Domagoj Lokner
 */
@WebServlet(name = "glasanjeRezultati", urlPatterns = "/glasanje-rezultati")
public class GlasanjeRezultatiServlet extends HttpServlet {

	private static final long serialVersionUID = 3250094496095229122L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bandsFileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
        String votesFileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");

        List<Vote> votes = GlasanjeUtil.getSortedVotes(bandsFileName, votesFileName);

        req.setAttribute("votes", votes);
        req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
    }


}
