package hr.fer.zemris.java.servlets.glasanje;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet that send xls file with with table that shows voting results for all
 * bands as HTTP response.
 *
 * @author Domagoj Lokner
 */
@WebServlet(name = "glsanjeXls", urlPatterns = "/glasanje-xls")
public class GlasanjeXlsServlet extends HttpServlet {

	private static final long serialVersionUID = 8360887296127326002L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bandsFileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
        String votesFileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");

        List<Vote> votes = GlasanjeUtil.getSortedVotes(bandsFileName, votesFileName);

        HSSFWorkbook hwb = new HSSFWorkbook();
        constructPage(hwb, votes);

        byte[] xlsFile = hwb.getBytes();

        resp.setContentType("application/vnd.ms-excel");
        resp.setContentLength(xlsFile.length);
        resp.setHeader("Content-Disposition", "attachment; filename=\"glasanje.xls\"");
        resp.getOutputStream().write(xlsFile);
    }

    /**
     * Constructs page of xls file.
     *
     * @param hwb workbook in which page will be created.
     * @param votes list of votes for all bands which results should be
     *              added to file.
     */
    private void constructPage(HSSFWorkbook hwb, List<Vote> votes) {
        HSSFSheet sheet = hwb.createSheet("Rezultati glasanja");

        HSSFRow rowhead = sheet.createRow(0);
        rowhead.createCell(1).setCellValue("Bend");
        rowhead.createCell(2).setCellValue("Broj glasova");

        int i = 1;
        for (Vote v : votes) {
            HSSFRow row = sheet.createRow(i);

            row.createCell(0).setCellValue(i);
            row.createCell(1).setCellValue(v.getBand().getName());
            row.createCell(2).setCellValue(v.getNumberOfVotes());
            ++i;
        }
    }
}
