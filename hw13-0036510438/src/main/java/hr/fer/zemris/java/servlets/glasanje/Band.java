package hr.fer.zemris.java.servlets.glasanje;

/**
 * Java-bean object that represents a band in band voting system of
 * webapp2.
 *
 * @author Domagoj Lokner
 */
public class Band {

    /**
     * Band identificator.
     */
    String id;

    /**
     * Name of band.
     */
    String name;

    /**
     * Link to band's song.
     */
    String link;


    /**
     * Constructs new band object.
     *
     * @param id band identificator.
     * @param name name of the band.
     * @param link link to band's song.
     */
    public Band(String id, String name, String link) {
        this.id = id;
        this.name = name;
        this.link = link;
    }

    /**
     * Gets band ID.
     *
     * @return band ID.
     */
    public String getId() {
        return id;
    }

    /**
     * Gets name of the band.
     *
     * @return band name.
     */
    public String getName() {
        return name;
    }

    /**
     * Link to single band's song.
     *
     * @return link to song.
     */
    public String getLink() {
        return link;
    }
}
