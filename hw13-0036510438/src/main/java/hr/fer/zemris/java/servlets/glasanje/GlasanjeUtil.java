package hr.fer.zemris.java.servlets.glasanje;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class GlasanjeUtil {

    /**
     * Extracts band from {@code String} object that
     * represents single band record.<br>
     *
     * Expected format of given string is:
     * <i>"<b>band_ID</b>\t<b>band_name</b>\t<b>link_to_song</b>"</i>
     *
     *
     * @param line single band record.
     * @return newly constructed {@code Band} object.
     * @throws IllegalArgumentException if invalid band format is given.
     */
    private static Band extractBand(String line) {
        Objects.requireNonNull(line);

        String[] elems = line.split("\\t");

        if (elems.length != 3) {
            throw new IllegalArgumentException("Invalid band record");
        }

        return new Band(elems[0], elems[1], elems[2]);
    }


    /**
     * Increments single voting result record.
     *
     * @param v voting result record.
     *          Expected format of record is:
     *          <i>"<b>band_ID</b>\t<b>number_of_votes</b>"</i>
     * @return record with incremented result.
     */
    protected static String incrementVote(String v) {
        String[] vote = v.split("\\t");

        int numOfVotes = Integer.parseInt(vote[1]);
        numOfVotes++;

        return String.format("%s\t%d", vote[0], numOfVotes);
    }

    /**
     * Gets file with given name.
     * If file with that name is not present it will be constructed.
     *
     * @param fileName name of file to be gotten.
     * @return path to file.
     * @throws IOException if error occurs during file opening.
     */
    protected static Path getFile(String fileName) throws IOException {
        Path filePath = Paths.get(fileName);

        if (Files.isReadable(filePath)) {
            return filePath;
        }

        return Files.createFile(filePath);
    }

    /**
     * Update list of voting results.
     * Result of record with given {@code id} will be incremented.
     *
     * @param id identificator of record which result should be incremented.
     * @param votes list that contains all results of voting.
     * @return list with all results of voting.
     */
    protected static List<String> updateVote(String id, List<String> votes) {

        Iterator<String> it = votes.iterator();
        String updatedVote = null;

        while (it.hasNext()) {
            String vote = it.next();

            if (vote.startsWith(id)) {
                updatedVote = GlasanjeUtil.incrementVote(vote);
                it.remove();
                break;
            }
        }

        //if line that should be updated is not present
        if (Objects.isNull(updatedVote)) {
            updatedVote = String.format("%s\t1", id);
        }

        votes.add(updatedVote);
        return votes;
    }

    /**
     * Returns map of all bands which records are present in given list.
     * For map key is used band ID.<br>
     * Bands will be stored in same order in map as
     * they are in given list.
     *
     * @param records list of all band records.
     * @return map of bands.
     */
    public static Map<String, Band> getBandsMap(List<String> records) {
        Map<String, Band> bands = new LinkedHashMap<>();

        for (String record : records) {
            Band band = extractBand(record.strip());
            bands.put(band.getId(), band);
        }

        return bands;
    }

    /**
     * Returns number of votes for given band ID.
     * If band with given ID is not present in list of records 0
     * will be returned.
     *
     * @param id band identificator.
     * @param votes list of all voting results.
     * @return number of votes that earned band with given ID.
     */
    public static int numberOfVotes(String id, List<String> votes) {
        int num;

        for (String v : votes) {
            String vote = v.strip();

            if (vote.startsWith(id)) {
                num = extractNumberOfVotes(vote);
                return num;
            }
        }

        return 0;
    }

    /**
     * Extracts number of votes in given voting result record.
     *
     * @param vote record of voting results for single band-
     * @return number of votes.
     */
    private static int extractNumberOfVotes(String vote) {
        String[] v = vote.split("\\t");

        if (v.length != 2) {
            throw new IllegalArgumentException("Invalid result record");
        }

        return Integer.parseInt(v[1]);
    }

    /**
     * Constructs list of votes for all bands from given map.
     *
     * @param bands map with bands. Band ID is expected for map key.
     * @param results list of voting results.
     * @return list of votes.
     */
    public static List<Vote> getVotes(Map<String, Band> bands, List<String> results) {
        List<Vote> votes = new LinkedList<>();

        for (Map.Entry<String, Band> entry : bands.entrySet()) {
            votes.add(
                    new Vote(entry.getValue(),
                    numberOfVotes(entry.getValue().getId(), results))
            );
        }
        return votes;
    }

    /**
     * Constructs list of votes for all bands defined in given file.
     *
     * @param bandsFileName name of file with band definitions-
     * @param votesFileName name of file with results of voting.
     * @return list of votes.
     * @throws IOException if error occurs during opening files.
     */
    public static List<Vote> getVotes(String bandsFileName, String votesFileName) throws IOException {
        Path bandsPath = getFile(bandsFileName);
        Path votesPath = getFile(votesFileName);

        Map<String, Band> bands = getBandsMap(Files.readAllLines(bandsPath));
        List<Vote> votes = getVotes(bands, Files.readAllLines(votesPath));

        return votes;
    }

    /**
     * Constructs list of votes for all bands defined in given file.
     * List will be sorted by number of votes each band earned
     * (band with highest number of votes will be first).
     *
     * @param bandsFileName name of file with band definitions-
     * @param votesFileName name of file with results of voting.
     * @return list of votes.
     * @throws IOException if error occurs during opening files.
     */
    public static List<Vote> getSortedVotes(String bandsFileName, String votesFileName) throws IOException {
        List<Vote> votes = getVotes(bandsFileName, votesFileName);
        votes.sort((v1, v2) -> Integer.compare(v2.getNumberOfVotes(), v1.getNumberOfVotes()));
        return votes;
    }
}
