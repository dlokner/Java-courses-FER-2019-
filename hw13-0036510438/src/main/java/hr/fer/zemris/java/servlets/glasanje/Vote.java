package hr.fer.zemris.java.servlets.glasanje;

/**
 * Java-beans object that represents single voting record.
 *
 * @author Domagoj Lokner
 */
public class Vote {

    /**
     * Band for which votes are stored in this object.
     */
    private Band band;

    /**
     * Number of votes that {@code band} earned.
     */
    private int numberOfVotes;

    /**
     * Creates new {@code Vote} object.
     *
     * @param band band for which votes are stored in this object.
     * @param numberOfVotes number of votes that band earned.
     */
    public Vote(Band band, int numberOfVotes) {
        this.band = band;
        this.numberOfVotes = numberOfVotes;
    }

    /**
     * Gets band.
     *
     * @return band.
     */
    public Band getBand() {
        return band;
    }

    /**
     * Returns number of votes band earned.
     *
     * @returnnumber of votes
     */
    public int getNumberOfVotes() {
        return numberOfVotes;
    }
}
