<%@ page import="java.util.Random" %><%--
  Created by IntelliJ IDEA.
  User: domagoj
  Date: 02.06.19.
  Time: 16:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<html>
<head>
    <title>Funny</title>
    <%@include file="/WEB-INF/style/background.jsp" %>
</head>
<body>
<%
    String color = "";
    char[] nums = "0123456789ABCDEF".toCharArray();

    for (int i = 0; i < 6; ++i) {
        Random r = new Random();
        color += nums[r.nextInt(nums.length)];
    }
%>

<span style="color: #<%= color%>; ">
    <h1>Funny story</h1>
    <p>
        <b>Why do Java Programmers wear glasses?</b><br>
        Because they don't C#.
    </p>
</span>

</body>
</html>
