<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <title>Colors</title>
    <%@include file="/WEB-INF/style/background.jsp" %>
</head>
<body>

<a href="setcolor?bgcolor=FFFFFF">WHITE</a><br>
<a href="setcolor?bgcolor=FF6666">RED</a><br>
<a href="setcolor?bgcolor=66FF66">GREEN</a><br>
<a href="setcolor?bgcolor=66FFFF">CYAN</a><br>

</body>
</html>
