<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Glasanje</title>
    <%@include file="/WEB-INF/style/background.jsp" %>
</head>
<body>
    <body>
    <h1>Glasanje za omiljeni bend:</h1>

    <p>
        Od sljedećih bendova, koji Vam je bend najdraži?
        Kliknite na link kako bi ste glasali!
    </p>

    <ol>
        <c:forEach var="band" items="${bands}">
            <li><a href="glasanje-glasaj?id=${band.id}">${band.name}</a></li>
        </c:forEach>
    </ol>

    </body>

</body>
</html>
