<%@ page import="hr.fer.zemris.java.servlets.glasanje.Vote" %>
<%@ page import="hr.fer.zemris.java.servlets.glasanje.Band" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.LinkedList" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <title>Rezultati</title>
    <%@include file="/WEB-INF/style/background.jsp" %>
    <style type="text/css">
        table.rez td {text-align: center;}
    </style>
</head>
<body>

    <h1>Rezultati glasanja</h1>
    <p>Ovo su rezultati glasanja.</p>

    <table border="1" cellspacing="0" class="rez">
        <thead><tr><th>Bend</th><th>Broj glasova</th></tr></thead>
        <tbody>
        <c:forEach var="v" items="${votes}">
            <tr><td>${v.band.name}</td><td>${v.numberOfVotes}</td></tr>
        </c:forEach>
        </tbody>
    </table>

    <h2>Grafički prikaz rezultata</h2>
    <img alt="Pie-chart" src="glasanje-grafika" width="500" height="300" />

    <h2>Rezultati u XLS formatu</h2>
    <p>Rezultati u XLS formatu dostupni su <a href="glasanje-xls">ovdje</a></p>

        <%	
            List<Vote> votes = (List<Vote>)request.getAttribute("votes");
            List<Band> winners = new LinkedList<>();
            int highestResult = votes.get(0).getNumberOfVotes();

            if (highestResult != 0) {
                for (Vote v : votes) {
                    if (v.getNumberOfVotes() == highestResult) {
                        winners.add(v.getBand());
                        continue;
                    }
                    break;
                }
            }

            if(!winners.isEmpty()){%>
                <h2>Razno</h2>
                <p>Primjeri pjesama pobjedničkih bendova:</p><ul>
        <%}

            for (Band w : winners) {
                System.out.println(w.getName());
                out.print("<li><a href=\""+ w.getLink()+" target=\"_blank\">"+w.getName()+"</a></li>");
            }
        %>
    </ul>

</body>
</html>
