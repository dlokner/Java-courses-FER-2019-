<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <title>Error</title>
    <%@include file="/WEB-INF/style/background.jsp" %>
</head>
<body>

<h1>Error occurred</h1>
<p>${errMessage}</p>

</body>
</html>
