<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <title>Trigonometric</title>
    <%@include file="/WEB-INF/style/background.jsp" %>
</head>
<body>

<h1>Table of trigonometric functions</h1>

<table border="1">
    <tr>
        <th>x</th>
        <th>sin(x)</th>
        <th>cos(x)</th>
    </tr>
    <c:forEach var="res" items="${trigResults}">
        <tr>
            <th>${res.x}</th>
            <td>${res.sin}</td>
            <td>${res.cos}</td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
