<%@ page contentType="text/html; charset=UTF-8"%>
<html>

<head>
    <title>Index</title>
    <%@include file="/WEB-INF/style/background.jsp" %>
</head>
<body>
<h1>Home page</h1>

<hr>

<a href="colors.jsp" title="change background color">Background color chooser</a><br>

<hr>

<a href="trigonometric?b=90" title="sin(x) and cos(x) for x in [0-90]">Trigonometric</a><br><br>

<form action="trigonometric" method="GET">
    Početni kut:<br><input type="number" name="a" min="0" max="360" step="1" value="0"><br>
    Završni kut:<br><input type="number" name="b" min="0" max="360" step="1" value="360"><br><br>
    <input type="submit" value="Tabeliraj">
    <input type="reset" value="Reset">
</form>

<hr>

<a href="stories/funny.jsp">Funny story</a><br>

<hr>

<a href="report.jsp" title="Chart with OS usage statistics">OS statistics</a>

<hr>

<a href="powers?a=1&b=100&n=3" title="Generates xls file">XLS file</a>

<hr>

<a href="appinfo.jsp" title="Display time passed from starting application">Application info</a>

<hr>

<a href="glasanje" title="Vote for your favourite band">Glasanje</a>

</body>
</html>
