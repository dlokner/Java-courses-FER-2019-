<%@ page import="hr.fer.zemris.java.TimeFormatter" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <title>Info</title>
    <%@include file="/WEB-INF/style/background.jsp" %>
</head>
<body>
<h1>Application info</h1>

<%
    String time = ((TimeFormatter)application.getAttribute("timeFormat")).format();
%>

<p> <b>Application have been running for:</b> <%=time%></p>

</body>
</html>
