package hr.fer.zemris.java.hw06.shell;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class UtilTest {

	@Test
	void extractCommand() {
		assertEquals("cat", Util.extractCommand("cat /home/domagoj/fer/OPJJ/recenzije5.txt"));
	}
	
	@Test
	void extractArguments() {
		assertEquals("/home/domagoj/fer/OPJJ/recenzije5.txt", Util.extractArguments("cat /home/domagoj/fer/OPJJ/recenzije5.txt"));
	}
	
	@Test
	void extractPaths1() {
		String[] paths = Util.extractArguments("/home/domagoj/fer/OPJJ/recenzije5.txt");
		
		assertEquals(1, paths.length);
	}

}
