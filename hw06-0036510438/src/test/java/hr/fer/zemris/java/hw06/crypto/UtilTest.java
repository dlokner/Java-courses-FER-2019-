package hr.fer.zemris.java.hw06.crypto;

import static hr.fer.zemris.java.hw06.crypto.Util.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class UtilTest {

	@Test
	void hextobyteTest1() {
		assertArrayEquals(new byte[] {0, 15, 26, 17}, hextobyte("000F1a11"));
	}
	
	@Test
	void hextobyteTest2() {
		assertArrayEquals(new byte[] {1, 1, 18, -85}, hextobyte("010112aB"));
	}
	
	@Test
	void hextobyteTestExceptionOddNumbLength() {
		assertThrows(IllegalArgumentException.class, () -> hextobyte("1000F1a11"));
	}
	
	@Test
	void hextobyteTestExceptionIsNotHex() {
		assertThrows(IllegalArgumentException.class, () -> hextobyte("000H1a11"));
	}
	
	@Test
	void bytetohexTest1() {
		assertEquals("000f1a11", bytetohex(new byte[] {0, 15, 26, 17}));
	}
	
	@Test
	void bytetohexTest2() {
		assertEquals("010112ab", bytetohex(new byte[] {1, 1, 18, -85}));
	}

}
