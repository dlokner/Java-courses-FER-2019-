package hr.fer.zemris.java.hw06.shell;

import java.util.Objects;

/**
 * Simple shell program that requires user to enter commands on 
 * standard input and prints results on the standard output.
 * 
 * <p>
 * 	<li><i>"help"</i> command without any additional arguments will print 
 * 		all commands this shell know to work with.<br>
 * 	<li>Command "help + <i>command</i>" will write out description of that command.
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class MyShell {

	/**
	 * Starting method of the program.
	 * 
	 * @param args - command line arguments.
	 */
	public static void main(String[] args) {
		
		Environment env = new MyShellEnvironment();
		
		ShellStatus status = ShellStatus.CONTINUE;
		
		do {
			
			String stringCommand = env.readLine();
			
			String commandName = Util.extractCommand(stringCommand);
			String arguments = Util.extractArgument(stringCommand);
			
			ShellCommand command = env.commands().get(commandName);
			
			if(Objects.isNull(command)) {
				env.writeln(commandName + ": command not found");
				continue;
			}
			
			status = command.executeCommand(env, arguments);
			
		} while(status != ShellStatus.TERMINATE); 
		
	}
}
