package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

/**
 * Implementation of {@code ShellCommand} that creates directory.
 * 
 * <p>
 * All non existing parent directories will be created.
 * Command expects single argument, file path representing 
 * directory structure to be created.
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class MkdirShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		String[] paths = Util.extractArguments(arguments);
		
		if(Util.requireNonEmptyArg(arguments) || paths.length != 1) {
			env.writeln("mkdir command requires single argument");
			return ShellStatus.CONTINUE;
		}
		
		Path path = Paths.get(paths[0]);
		
		if(Files.isDirectory(path)) {
			env.writeln(path + " directory already exists.");
			return ShellStatus.CONTINUE;
		}
		
		try {
			Files.createDirectories(path);
		} catch (IOException e) {
			env.writeln("Unvalid path is given!");
			return ShellStatus.CONTINUE;
		}
		
		env.writeln(path + ": directory structure is successfully created.");
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "mkdir";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command creates directory.");
		description.add("All non existing parent directories will be created.");
		description.add("Command expects single argument, file path represesnting directory structure to be created.");
		
		return Collections.unmodifiableList(description);
	}

}
