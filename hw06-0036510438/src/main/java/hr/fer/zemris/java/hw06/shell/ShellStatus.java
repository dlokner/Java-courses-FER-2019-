package hr.fer.zemris.java.hw06.shell;

/**
 * Define shell status.
 * 
 * @author Domagoj Lokner
 *
 */
public enum ShellStatus {
	
	/**
	 * Shell can continue to work normally.
	 */
	CONTINUE,
	
	/**
	 * Shell should be terminated.
	 */
	TERMINATE
}
