package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.Util;

/**
 * Implementation of {@code ShellCommand} that lists 
 * contents of directories in a tree-like format.
 * 
 * <p>
 * Command expects single argument, path to directory to be listed.<br>
 * Output will be shifted two characters to the right for each directory level.
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class TreeShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		if(Util.requireNonEmptyArg(arguments)) {
			env.writeln("tree command requires single argument");
			return ShellStatus.CONTINUE;
		}
		
		Path path = Paths.get(arguments);
				
		if(!Files.isDirectory(path)) {
			env.writeln("\"" + path + "\"" + ": No such directory");
			return ShellStatus.CONTINUE;
		}
		
		try {
			Files.walkFileTree(path, new FileVisitor<Path>() {
				int level;

				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					env.writeln(" ".repeat(level*2) + dir.getFileName());
					level++;
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					env.writeln(" ".repeat(level*2) + file.getFileName());
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
					env.writeln("Reading \"" + file + "\" failed.");
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					--level;
					return FileVisitResult.CONTINUE;
				}
				
			});
		} catch (IOException e) {
			env.writeln(path.toString() + ": No such directory");
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "tree";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command list contents of directories in a tree-like format.");
		description.add("Command expects single argument, path to directory to be listed.");
		description.add("Output will be shifted two characters to the right for each directory level.");
		
		return Collections.unmodifiableList(description);
	}

}
