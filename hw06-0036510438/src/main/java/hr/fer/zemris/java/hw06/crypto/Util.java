package hr.fer.zemris.java.hw06.crypto;

/**
 * Class that offers static methods to work with hex numbers and byte arrays.
 * 
 * @author Domagoj Lokner
 *
 */
public class Util {

	
	/**
	 * Converts given hex number to byte array.
	 * 
	 * @param keyText - hex number.
	 * @return byte array that represents given hex number.
	 */
	public static byte[] hextobyte(String keyText) {
		
		byte[] result = new byte[keyText.length()/2];
		
		if(keyText.length() % 2 != 0) {
			throw new IllegalArgumentException("Odd hex number length!");
		}
				
		for(int i = 0, j = 0, n = keyText.length(); i < n; i +=2, ++j) {
			
			try {
				result[j] = (byte) (
						Integer.parseInt(keyText, i, i+2, 16) 
				);
				
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Given expression can not be inpreated as hex number!");
			}
		}
		
		return result;
	}
	
	/**
	 * Converts given byte array to hex number.
	 * 
	 * @param bytearray - byte array to be converted to hex number.
	 * @return string that represents hex number.
	 */
	public static String bytetohex(byte[] bytearray) {
		StringBuilder result = new StringBuilder();
		
		for(int i = 0, n = bytearray.length; i < n; ++i) {
			
			String digit = Integer.toString(Byte.toUnsignedInt(bytearray[i]), 16);
			
			if(digit.length() == 1) {
				result.append('0');
			}
			
			result.append(digit);
		}
		
		return result.toString();
	}
}
