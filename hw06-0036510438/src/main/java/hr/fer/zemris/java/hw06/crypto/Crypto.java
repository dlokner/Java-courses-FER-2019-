package hr.fer.zemris.java.hw06.crypto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Program that will allow the user to encrypt/decrypt 
 * file using the AES crypto-algorithm and the 128-bit 
 * encryption key or calculate and check the SHA-256 file digest.
 *  
 * <p>
 * Program expects two or three arguments to be given from command line.
 * <ul>
 * 	<li>if first argument is <i>"checksha"</i> argument that follows will be 
 * 		interpreted as file which SHA-256 digest we want check. <br>
 * 		After that user will be requested to enter expected SHA key for given file.
 * 	<li>if first argument is "encrypt" or "decrypt" following two arguments
 * 		will be interpreted as original file and file that represents<br>
 * 		encrypted/decrypted original file.		  
 * <ul>
 * </p>
 * @author Domagoj Lokner
 *
 */
public class Crypto {

	/**
	 * InputStream buffer size.
	 */
	private static final int BUFFER_SIZE = 1024;
	
	/**
	 * Starting method of the program.
	 * 
	 * @param args - command line arguments.
	 */
	public static void main(String[] args) {

		if (args.length < 2 || args.length > 3 ) {
			System.err.println("Unvalid number of arguments!");
			System.exit(1);
		}

		Scanner sc = new Scanner(System.in);

		boolean encrypt = false;
		
		try {
			switch (args[0]) {
				case ("checksha"):
					System.out.printf("Please provide expected sha-256 digest for hw06test.bin:%n> ");
					checkSha(args[1], sc.nextLine());
					break;
				case("encrypt"):
					encrypt = true;
				case("decrypt"):
					System.out.printf("Please provide password as hex-encoded text (16 bytes, i.e. 32 hex-digits):%n> ");
					String keyText = sc.nextLine();
					System.out.printf("Please provide initialization vector as hex-encoded text (32 hex-digits):%n> ");
					String ivText = sc.nextLine();
					crypt(args[1], args[2], keyText, ivText, encrypt);
					break;
				default:
					System.out.println("Neispravan argument");
			}
		} catch(IllegalArgumentException ex) {
			System.out.println(ex.getMessage());
			System.exit(1);
		}

		sc.close();

	}

	/**
	 * Encrypts or decrypts inout file to output file (if output file is not existing 
	 * it will be created otherwise file content will be overwritten).
	 * 
	 * @param inputFilePath - input file.
	 * @param outputFilePath - output file
	 * @param keyText - Encryption/decryption password in hex form.
	 * @param ivText - Initialization vector in hex form.
	 * @param encrypt - if parameter is {@code true} method will work in encryt mod 
	 * 					otherwise program will work in decryt mod.
	 */
	private static void crypt(String inputFilePath, String outputFilePath, String keyText, String ivText, boolean encrypt) {
		
		SecretKeySpec keySpec = new SecretKeySpec(Util.hextobyte(keyText), "AES");
		AlgorithmParameterSpec paramSpec = new IvParameterSpec(Util.hextobyte(ivText));
		Cipher cipher;
		
		try {
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalArgumentException("Given algorithm is not recognized!");
		} catch (NoSuchPaddingException e) {
			throw new IllegalArgumentException("Given padding is not recognized!");
		}
		
		try {
			cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, paramSpec);
		} catch (InvalidKeyException e) {
			throw new IllegalArgumentException("Unavlid key is given!");
		} catch (InvalidAlgorithmParameterException e) {
			throw new IllegalArgumentException("Uvalid initialization vector is given!");
		}
		
		Path inputPath = Paths.get(inputFilePath);
		Path outputPath = Paths.get(outputFilePath);
		
		try (InputStream input = Files.newInputStream(inputPath); OutputStream output = Files.newOutputStream(outputPath)) {
			
			byte[] buff = new byte[BUFFER_SIZE];
			
			while (true) {
				
				int r = input.read(buff);
				
				if (r < 1) {
					try {
						output.write(cipher.doFinal());
					} catch (IllegalBlockSizeException e) {
						throw new IllegalArgumentException("Illegal block size!");
					} catch (BadPaddingException e) {
						throw new IllegalArgumentException("Decrypted data is not bounded by the appropriate padding bytes!");
					}
					break;
				}				
				output.write(cipher.update(buff, 0, r));
			}

		} catch (IOException ex) {
			throw new IllegalArgumentException("File can't be reached!");
		}
		
		System.out.format("%s completed. Generated file %s based on file %s.",
				encrypt ? "Encryption" : "Decryption",
						outputFilePath,
						inputFilePath
		);
		
	}

	/**
	 * Checks if {@code expectedSHA} is same as SHA digest of given file.
	 * 
	 * @param filePath - file to be checked.
	 * @param expextedSHA - expected SHA for given file.
	 */
	private static void checkSha(String filePath, String expextedSHA) {

		Path path = Paths.get(filePath);
		
		MessageDigest md;
		
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException ex) {
			throw new IllegalArgumentException("Given algorithm is not recognized!");
		}
		
		try (InputStream input = Files.newInputStream(path)) {
			
			byte[] buff = new byte[BUFFER_SIZE];
			
			while (true) {
				int r = input.read(buff);
				
				if (r < 1) {
					break;
				}
				
				md.update(buff, 0, r);
			}
		} catch (IOException ex) {
			throw new IllegalArgumentException("File can't be reached!");
		} 
		
		byte[] fileSHA = md.digest();
		
		System.out.print("Digesting completed. ");
		
		if(Arrays.equals(fileSHA, Util.hextobyte(expextedSHA))) {
			System.out.format("Digest of %s matches expected digest.", filePath);
		} else {
			System.out.format("Digest of %s does not match the expected digest. Digest was: %s", 
					filePath, Util.bytetohex(fileSHA));
		}
	}
}
