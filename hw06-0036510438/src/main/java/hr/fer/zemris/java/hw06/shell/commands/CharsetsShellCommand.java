package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * Implementation of {@code ShellCommand} which lists 
 * names of supported charsets for your Java platform.
 * 
 * <p>
 * Command takes no arguments and writes 
 * single charset per line on console.
 * </p>
 * 
 * @author Domagoj Lokner
 *
 */
public class CharsetsShellCommand implements ShellCommand {

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		Charset.availableCharsets().forEach((k, v) -> env.writeln(v.toString()));
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "charsets";
	}

	@Override
	public List<String> getCommandDescription() {
		List<String> description = new LinkedList<>();
		
		description.add("This command lists names of supported charsets for your Java platform.");
		description.add("Command takes no arguments.");
		description.add("A single charset is writen per line.");
		
		return Collections.unmodifiableList(description);
	}

}
