package hr.fer.zemris.java.hw06.shell;

import java.util.SortedMap;

/**
 * Represents object through which all shell commands will be capable to communicate with shell. 
 * 
 * @author Domagoj Lokner
 *
 */
public interface Environment {
	
	/**
	 * Read single line from console.
	 * 
	 * @return read line.
	 * @throws ShellIOException if error occur during reading.
	 */
	String readLine() throws ShellIOException;
	
	/**
	 * Write given text on console.
	 * 
	 * @param text - text to be written.
	 * @throws ShellIOException if error occur during writing.
	 */
	void write(String text) throws ShellIOException;
	
	/**
	 * Write given text on console with adding line separator string on the end.
	 * 
	 * @param text - text to be written.
	 * @throws ShellIOException if error occur during writing.
	 */
	void writeln(String text) throws ShellIOException;
	
	/**
	 * Returns unmodifiable map of all commands this environment offers.
	 * 
	 * @return map of commands.
	 */
	SortedMap<String, ShellCommand> commands();
	
	/**
	 * Gets multiline symbol.
	 * 
	 * @return multiline symbol.
	 */
	Character getMultilineSymbol();
	
	/**
	 * Sets given symbol as multiline symbol.
	 * 
	 * @param symbol - symbol to be set.
	 */
	void setMultilineSymbol(Character symbol);
	
	/**
	 * Gets prompt symbol.
	 * 
	 * @return prompt symbol.
	 */
	Character getPromptSymbol();
	
	/**
	 * Sets given symbol as prompt symbol.
	 * 
	 * @param symbol - symbol to be set.
	 */
	void setPromptSymbol(Character symbol);
	
	/**
	 * Gets morelines symbol.
	 * 
	 * @return morelines symbol.
	 */
	Character getMorelinesSymbol();
	
	/**
	 * Sets given symbol as morelines symbol.
	 * 
	 * @param symbol - symbol to be set.
	 */
	void setMorelinesSymbol(Character symbol);
}
