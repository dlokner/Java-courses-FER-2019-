package hr.fer.zemris.java.gui.prim;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PrimListModelTest {

	private PrimListModel prim;
	
	@BeforeEach
	void init() {
		prim = new PrimListModel();
	}
	
	@Test
	void initSize() {
		assertEquals(1, prim.getSize());
	}
	
	@Test
	void next() {
		prim.next();
		
		long result = prim.getPrimes().get(prim.getPrimes().size()-1);
		
		assertEquals(2, result);
		
		prim.next();
		prim.next();
		prim.next();
		prim.next();
		prim.next();
		
		result = prim.getPrimes().get(prim.getPrimes().size()-1);
		
		assertEquals(13, result);
		
		prim.next();
		prim.next();
		prim.next();
		
		result = prim.getPrimes().get(prim.getPrimes().size()-1);
		
		assertEquals(23, result);
	}

}
