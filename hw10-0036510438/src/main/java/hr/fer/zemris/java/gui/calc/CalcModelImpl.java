package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalcValueListener;
import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.DoubleBinaryOperator;

/**
 * Implementation of calculator model.<br>
 * This implementation do not adherence to arithmetic operation priorities.
 *
 * @author Domagoj Lokner
 */
public class CalcModelImpl implements CalcModel {

    /**
     * Editable flag. Number can be added if flag is set on {@code true}.
     */
    private boolean editable;

    /**
     * {@code Negative} flag is set on {@code true} if current value is negative number.
     */
    private boolean negative;

    /**
     * String representation of current value.
     */
    private String currentValueString;

    /**
     * Current value.
     */
    private double currentValue;

    /**
     * Value of active operand.
     */
    private Double activeOperand;

    /**
     * List of registered listeners.
     */
    private List<CalcValueListener> listeners;

    /**
     * Pending binary operation.
     */
    private DoubleBinaryOperator operation;


    /**
     * Constructs new {@code CalcModelImpl}.
     */
    public CalcModelImpl() {
        editable = true;
        currentValueString = "";
        currentValue = 0d;
    }

    @Override
    public void addCalcValueListener(CalcValueListener l) {
        Objects.requireNonNull(l);

        if(Objects.isNull(listeners)){
            listeners = new LinkedList<>();
        }

        listeners.add(l);
    }

    @Override
    public void removeCalcValueListener(CalcValueListener l) {
        listeners.remove(l);
    }

    @Override
    public double getValue() {
        return negative ? currentValue * -1d : currentValue;
    }

    @Override
    public void setValue(double value) {
        if(value < 0d){
            negative = true;
        }
        currentValue = Math.abs(value);
        currentValueString = Double.toString(currentValue);
        editable = false;

        notifyListeners();
    }

    @Override
    public boolean isEditable() {
        return editable;
    }

    @Override
    public void clear() {
        clearCurrentValue();
        editable = true;

        notifyListeners();
    }

    @Override
    public void clearAll() {
        clearCurrentValue();
        clearActiveOperand();
        editable = true;

        notifyListeners();
    }

    @Override
    public void swapSign() throws CalculatorInputException {
        if(!editable){
            throw new CalculatorInputException("Value can't be changed");
        }

        negative = negative ? false : true;

        notifyListeners();
    }

    @Override
    public void insertDecimalPoint() throws CalculatorInputException {
        if(!editable) {
            throw new CalculatorInputException("Value can't be edited");
        }
        if(currentValueString.isEmpty()){
            throw new CalculatorInputException("There is no number entered before decimal point");
        }
        if(currentValueString.contains(".")){
            throw new CalculatorInputException("Number can't contain two decimal points");
        }

        try {
            appendToCurrent('.');
        } catch (NumberFormatException ex){
            throw new CalculatorInputException();
        }

        notifyListeners();
    }

    @Override
    public void insertDigit(int digit) throws CalculatorInputException, IllegalArgumentException {
        if(!editable) {
            throw new CalculatorInputException("Value can't be edited");
        }

        if(digit < 0 || digit > 9){
            throw new IllegalArgumentException("Digit must be number in range [0-9]");
        }

        try {
            appendToCurrent(Character.forDigit(digit, 10));
        } catch (IllegalArgumentException ex){
            throw new CalculatorInputException("Absolute value of number is bigger than value double type can store");
        }

        notifyListeners();

    }

    @Override
    public boolean isActiveOperandSet() {
        return !Objects.isNull(activeOperand);
    }

    @Override
    public double getActiveOperand() throws IllegalStateException {
        if(Objects.isNull(activeOperand)){
            throw new IllegalStateException("Active operand is not set");
        }
        return activeOperand.doubleValue();
    }

    @Override
    public void setActiveOperand(double activeOperand) {
        this.activeOperand = Double.valueOf(activeOperand);
    }

    @Override
    public void clearActiveOperand() {
        activeOperand = null;
    }

    @Override
    public DoubleBinaryOperator getPendingBinaryOperation() {
        return operation;
    }

    @Override
    public void setPendingBinaryOperation(DoubleBinaryOperator op) {

        if(isActiveOperandSet() && !Objects.isNull(getPendingBinaryOperation())){
            setValue(getPendingBinaryOperation().applyAsDouble(getActiveOperand(), currentValue));
            activeOperand = null;
            notifyListeners();
        }

        operation = op;

        if(Objects.isNull(operation)){
            return;
        }

        setActiveOperand(signedCurrent());
        clearCurrentValue();
    }

	@Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(negative ? '-' : "");

        if(currentValueString.isEmpty()){
            sb.append('0');
        } else {
            if(Double.isInfinite(currentValue)){
                sb.append("Infinity");
            } else {
                sb.append(currentValueString);
            }
        }

        return sb.toString();
    }

    /**
     * Set {@code currentValue} on 0 and {@code currentValueString} on empty string.
     */
    private void clearCurrentValue(){
        currentValue = 0d;
        currentValueString = "";
        negative = false;
        editable = true;
    }

    /**
     * Append given number or decimal point as character to current value.
     *
     * @param c - number or decimal point to be appended.
     * @throws NumberFormatException if given sign added to current value
     *                                  can't be parsed to double type.
     */
    private void appendToCurrent(Character c){

        //if current string is "0" and '0' is going to be added
        if(currentValueString.length() == 1) {
            if(currentValueString.charAt(0) == '0' && !c.equals('.')){
                currentValueString = "";
            }
        }

        currentValueString += c;

        if(c.equals('.')){
            return;
        }

        double newValue;
        newValue = Double.parseDouble(currentValueString);

        if(Double.isInfinite(newValue)){
            throw new IllegalArgumentException();
        }

        currentValue = newValue;
    }

    /**
     * Notify all registered listeners that value have been changed.
     */
    private void notifyListeners(){
        if(Objects.isNull(listeners)){
            return;
        }

        for(CalcValueListener listener : listeners){
            listener.valueChanged(this);
        }
    }
    
    private double signedCurrent() {
		return currentValue * (negative ? -1 : 1); 
	}

}
