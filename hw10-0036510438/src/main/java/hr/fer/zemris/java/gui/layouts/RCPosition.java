package hr.fer.zemris.java.gui.layouts;

import java.util.Objects;

/**
 * Immutable object represents elements position in {@code CalcLayout} layout.
 */
public class RCPosition {

    /**
     * Number of row.
     */
    private int row;

    /**
     * Number of column.
     */
    private int column;

    /**
     * Constructs {@code RCPosition}.
     *
     * @param row - number of row in {@code CalcLayout}.
     * @param column - number of column in {@code CalcLayout}.
     */
    public RCPosition(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * Gets row.
     *
     * @return - number of row.
     */
    public int getRow() {
        return row;
    }

    /**
     * Gets column.
     *
     * @return - number of column.
     */
    public int getColumn() {
        return column;
    }

    /**
     * Parse given string to {@code RCPosition} object.
     * String must be in format "<i>number_of_rows</i>,<i>number_of_columns</i>" to be parse successfully.
     *
     * @param position - string to be parsed.
     * @return {@code RCPosition} represented with given string.
     */
    public static RCPosition parse(String position) {
        Objects.requireNonNull(position);

        String[] components = position.split(",");

        if(components.length != 2){
            throw new IllegalArgumentException();
        }

        try{

            return new RCPosition(
                    Integer.parseInt(components[0]),
                    Integer.parseInt(components[1])
            );
        } catch (NumberFormatException e){
            throw new IllegalArgumentException();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RCPosition)) return false;
        RCPosition that = (RCPosition) o;
        return row == that.row &&
                column == that.column;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, column);
    }
}
