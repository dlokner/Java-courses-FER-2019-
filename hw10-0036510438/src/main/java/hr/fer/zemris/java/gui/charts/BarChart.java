package hr.fer.zemris.java.gui.charts;

import java.util.ArrayList;
import java.util.List;

/**
 * Object that represent bar chart.
 *
 * @author Domagoj Lokner
 */
public class BarChart {

    /**
     * Diagram values.<br>
     * x - position of the bar on x-axes.<br>
     * y - bar's height.
     */
    private List<XYValue>  values;

    /**
     * Description of x-axes.
     */
    private String xDescription;

    /**
     * Description of y-axes.
     */
    private String yDescription;

    /**
     * Minimum y in chart.
     */
    private int yMin;

    /**
     * Maximum y in chart.
     */
    private int yMax;

    /**
     * Value of step to next y value in chart.
     */
    private int yStep;

    /**
     * Constructs new {@code BarChart}.
     *
     * @param values - values displayed on diagram.
     * @param xDescription - description of x-axes.
     * @param yDescription - description of y-axes.
     * @param yMin - minimum y value.
     * @param yMax - maximum y value.
     * @param yStep - value of step to next y value in chart.
     * @throws IllegalArgumentException if y value of any argument is lower than {@code yMin}.
     */
    public BarChart(List<XYValue> values, String xDescription, String yDescription, int yMin, int yMax, int yStep) {

        values.forEach(v -> {
            if(v.getY() < yMin){
                throw new IllegalArgumentException("Y value of argument must not be lower than minimal y value.");
            }
        });

        this.values = new ArrayList<>(values);
        this.xDescription = xDescription;
        this.yDescription = yDescription;
        this.yMin = yMin;
        this.yMax = yMax;
        this.yStep = yStep;
    }

    /**
     * Gets chart values.
     *
     * @return values.
     */
    public List<XYValue> getValues() {
        return values;
    }

    /**
     * Gets x-axis description.
     *
     * @return description of x-axis.
     */
    public String getxDescription() {
        return xDescription;
    }

    /**
     * Gets y-axis description.
     *
     * @return description of y-axis.
     */
    public String getyDescription() {
        return yDescription;
    }

    /**
     * Gets minimal y value.
     *
     * @return minimal value of y.
     */
    public int getYMin() {
        return yMin;
    }

    /**
     * Gets maximum y value.
     *
     * @return maximum value of y.
     */
    public int getYMax() {
        return yMax;
    }

    /**
     * Gets value of step to next y value in chart.
     *
     * @return step.
     */
    public int getYStep() {
        return yStep;
    }
}
