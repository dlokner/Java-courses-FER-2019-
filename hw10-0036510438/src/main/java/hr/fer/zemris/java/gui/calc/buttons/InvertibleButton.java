package hr.fer.zemris.java.gui.calc.buttons;

/**
 * Define invertible calculator buttons.
 *
 * @author Domagoj Lokner
 */
public interface InvertibleButton {

    /**
     * Sets button in verse function mode.
     */
    void setInverse();

    /**
     * Returns button state to standard function mode.
     */
    void setFunction();

}
