package hr.fer.zemris.java.gui.prim;

import javax.swing.*;
import java.awt.*;

/**
 * Program that generate prime numbers.
 */
public class PrimDemo extends JFrame {

	private static final long serialVersionUID = 3615989674838267677L;

	/**
     * Constructs {@code PrimDemo}.
     *
     * @param primList - list which this model will use to display primes.
     */
    public PrimDemo(PrimListModel primList){
        setLocation(20, 50);
        setSize(300, 300);
        setTitle("Prime v.1.0");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        intGUI(primList);
    }

    /**
     * Initialise GUI.
     *
     * @param primList - list which this model will use to display primes.
     */
    private void intGUI(PrimListModel primList) {
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        JList<Long> list1 = new JList<>(primList);
        JList<Long> list2 = new JList<>(primList);

        JButton next = new JButton("next");

        next.addActionListener(e -> {
            primList.next();
        });

        JPanel lists = new JPanel();
        lists.setLayout(new GridLayout(0,2));
        lists.add(new JScrollPane(list1));
        lists.add(new JScrollPane(list2));

        cp.add(lists, BorderLayout.CENTER);
        cp.add(next, BorderLayout.PAGE_END);
    }

    /**
     * Starting method of the program.
     *
     * @param args - command line arguments.
     */
    public static void main(String[] args) {
        PrimListModel prim = new PrimListModel();

        SwingUtilities.invokeLater(() -> {
            JFrame frame = new PrimDemo(prim);
            frame.setVisible(true);
        });
    }

}
