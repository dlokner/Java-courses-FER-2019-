package hr.fer.zemris.java.gui.calc.buttons;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

import javax.swing.*;

/**
 * Button that will insert decimal point to current number on calculator.
 *
 * @author Domagoj Lokner
 */
public class DecimalPointButton extends JButton {

	private static final long serialVersionUID = -5940500572112685053L;
	
	/**
     * Calculator model.
     */
    CalcModel calc;

    /**
     * Constructs {@code DecimalPointButton}.
     *
     * @param text - button text.
     * @param calc - calculator implementation.
     */
    public DecimalPointButton(String text, CalcModel calc) {
        super(text);
        this.calc = calc;
        addActionListener(e -> calc.insertDecimalPoint());
    }
}
