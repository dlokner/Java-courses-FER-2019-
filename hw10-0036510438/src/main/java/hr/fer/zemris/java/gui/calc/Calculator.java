package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.buttons.*;
import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Calculator program. This program will display calculator GUI.
 *
 * @author Domagoj Lokner
 */
public class Calculator extends JFrame {

	private static final long serialVersionUID = 5016122500695877672L;

	/**
     * Constructs {@code Calculator} - Java Calculator v1.0.
     */
    public Calculator() {
        setLocation(20, 50);
        setSize(700, 375);
        setTitle("Java Calculator v1.0");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        CalcModel calc = new CalcModelImpl();

        initGUI(calc);
    }

    /**
     * Starting method of the program.
     *
     * @param args - command line arguments.
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new Calculator();
            frame.setVisible(true);
        });
    }

    /**
     * Initializes GUI fot calculator.
     *
     * @param calcModel - concrete implementation of {@code CalcModel} interface.
     */
    private void initGUI(CalcModel calcModel) {
        Container cp = getContentPane();
        cp.setLayout(new CalcLayout(5));

        List<InvertibleButton> invertible = setInvertible(calcModel, cp);

        JLabel label = new JLabel();
        calcModel.addCalcValueListener((model) -> label.setText(model.toString()));
        label.setHorizontalAlignment(SwingConstants.RIGHT);
        label.setBackground(new Color(255, 212, 28));
        label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        label.setFont(label.getFont().deriveFont(30f));
        label.setOpaque(true);
        cp.add(label, new RCPosition(1, 1));

        JCheckBox inv = new JCheckBox("Inv");
        cp.add(inv, new RCPosition(5, 7));
        inv.addActionListener(e -> {
            for(InvertibleButton button : invertible){
                if(inv.isSelected()){
                    button.setInverse();
                } else {
                    button.setFunction();
                }
            }
        });

        List<JButton> numbers = setNumbers(calcModel, cp);

        setOperators(calcModel,cp);

        stackButtons(calcModel, cp);

        cp.add(new FunctionButton("1/x", d -> 1/d, calcModel), new RCPosition(2, 1));

        cp.add(new SwapSignButton("+/-", calcModel), new RCPosition(5, 4));
        cp.add(new DecimalPointButton(".", calcModel), new RCPosition(5, 5));

        cp.add(new ClearButton("clr", calcModel), new RCPosition(1, 7));
        cp.add(new ClearButton("res", calcModel), new RCPosition(2, 7));

        List<Component> elements = new LinkedList<>(Arrays.asList(cp.getComponents()));
        elements.remove(label);
        elements.remove(inv);

        Color blue = new Color(144, 160, 208);
        elements.stream().forEach(e -> {
            e.setBackground(blue);
            e.setFont(e.getFont().deriveFont(14f));
        });

        numbers.forEach((n) -> n.setFont(n.getFont().deriveFont(30f)));

    }

    /**
     * Sets all invertible buttons.
     *
     * @param calcModel - calculator to which buttons will be set.
     * @param c - container in which buttons will be set.
     * @return list of all invertible buttons which have been set.
     */
    private static List<InvertibleButton> setInvertible(CalcModel calcModel, Container c){
        List<InvertibleButton> buttons = new LinkedList<>();

        InvertibleUnaryOperation sin = new InvertibleUnaryOperation("sin", "arcsin",
                Math::sin, Math::asin, calcModel);
        InvertibleUnaryOperation cos = new InvertibleUnaryOperation("cos", "arccos",
                Math::cos, Math::acos, calcModel);
        InvertibleUnaryOperation tan = new InvertibleUnaryOperation("tan", "arctan",
                Math::tan, Math::atan, calcModel);
        InvertibleUnaryOperation ctg = new InvertibleUnaryOperation("ctg", "arcctg",
                d -> 1/Math.tan(d), d -> Math.PI/2 - Math.atan(d), calcModel);

        InvertibleUnaryOperation log = new InvertibleUnaryOperation("log", "10^x",
                Math::log10, d -> Math.pow(10d, d), calcModel);
        InvertibleUnaryOperation ln = new InvertibleUnaryOperation("ln", "e^x",
                Math::log, Math::exp, calcModel);

        InvertibleBinaryOperation power = new InvertibleBinaryOperation("x^n", "x^(1/n)",
                Math::pow, (d1, d2) -> Math.pow(d1, 1/d2), calcModel);

        c.add(sin, new RCPosition(2, 2));
        c.add(cos, new RCPosition(3, 2));
        c.add(tan, new RCPosition(4, 2));
        c.add(ctg, new RCPosition(5, 2));

        c.add(log, new RCPosition(3, 1));
        c.add(ln, new RCPosition(4, 1));
        c.add(power, new RCPosition(5, 1));

        buttons.addAll(Arrays.asList(sin, cos, tan, ctg, log, ln, power));

        return buttons;
    }

    /**
     * Sets all number buttons (digits from 0 to 9).
     *
     * @param calcModel - calculator to which buttons will be set.
     * @param c - container in which buttons will be set.
     * @return list of all number buttons which have been set.
     */
    private static List<JButton> setNumbers(CalcModel calcModel, Container c){
        JButton zero = new NumberButton("0", 0, calcModel);
        JButton one = new NumberButton("1", 1, calcModel);
        JButton two = new NumberButton("2", 2, calcModel);
        JButton three = new NumberButton("3", 3, calcModel);
        JButton four = new NumberButton("4", 4, calcModel);
        JButton five = new NumberButton("5", 5, calcModel);
        JButton six = new NumberButton("6", 6, calcModel);
        JButton seven = new NumberButton("7", 7, calcModel);
        JButton eight = new NumberButton("8", 8, calcModel);
        JButton nine = new NumberButton("9", 9, calcModel);

        c.add(zero, new RCPosition(5, 3));
        c.add(one, new RCPosition(4, 3));
        c.add(two, new RCPosition(4, 4));
        c.add(three, new RCPosition(4, 5));
        c.add(four, new RCPosition(3, 3));
        c.add(five, new RCPosition(3, 4));
        c.add(six, new RCPosition(3, 5));
        c.add(seven, new RCPosition(2, 3));
        c.add(eight, new RCPosition(2, 4));
        c.add(nine, new RCPosition(2, 5));

        return Arrays.asList(zero, one, two, three, four, five, six, seven, eight, nine);
    }

    /**
     * Sets all operator buttons.
     *
     * @param calcModel - calculator to which buttons will be set.
     * @param c - container in which buttons will be set.
     * @return list of all operator buttons which have been set.
     */
    private static List<JButton> setOperators(CalcModel calcModel, Container c){
        JButton equal = new BinaryOperatorButton("=", null, calcModel);
        JButton div = new BinaryOperatorButton("/", (d1, d2) -> d1/d2, calcModel);
        JButton mul = new BinaryOperatorButton("*", (d1, d2) -> d1*d2, calcModel);
        JButton sub = new BinaryOperatorButton("-", (d1, d2) -> d1-d2, calcModel);
        JButton add = new BinaryOperatorButton("+", (d1, d2) -> d1+d2, calcModel);

        c.add(equal, new RCPosition(1, 6));
        c.add(div, new RCPosition(2, 6));
        c.add(mul, new RCPosition(3, 6));
        c.add(sub, new RCPosition(4, 6));
        c.add(add, new RCPosition(5, 6));

        return Arrays.asList(equal, div, mul, sub, add);
    }

    /**
     * Sets stack buttons (push and pop).<br>
     * This buttons will push currently entered value on stack or pop last pushed value.
     *
     * @param calcModel - calculator to which buttons will be set.
     * @param c - container in which buttons will be set.
     * @return list of stack buttons buttons.
     */
    private static List<JButton> stackButtons(CalcModel calcModel, Container c){
        Stack<Double> stack = new Stack<>();

        JButton pop = new JButton("pop");
        pop.addActionListener(e -> {
            calcModel.setValue(stack.pop());
        });

        JButton push = new JButton("push");
        push.addActionListener(e -> {
            stack.push(calcModel.getValue());
        });

        c.add(push, new RCPosition(3, 7));
        c.add(pop, new RCPosition(4, 7));

        return Arrays.asList(pop, push);
    }
}
