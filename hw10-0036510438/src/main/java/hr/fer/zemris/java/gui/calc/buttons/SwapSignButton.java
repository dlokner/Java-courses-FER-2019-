package hr.fer.zemris.java.gui.calc.buttons;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

import javax.swing.*;

/**
 * Calculator button that swap sign of current number when is pressed.
 *
 * @author Domagoj Lokner
 */
public class SwapSignButton extends JButton {

	private static final long serialVersionUID = -744255491082562442L;

	/**
     * Calculator model.
     */
    CalcModel calc;

    /**
     * Constructs {@code SwapSignButton}.
     *
     * @param text - button text.
     * @param calc - calculator implementation.
     */
    public SwapSignButton(String text, CalcModel calc) {
        super(text);
        this.calc = calc;
        addActionListener(e -> calc.swapSign());
    }
}
