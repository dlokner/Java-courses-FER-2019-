package hr.fer.zemris.java.gui.layouts;

import java.awt.*;
import java.util.*;
import java.util.function.Function;

/**
 * {@code CalcLayout} lays out a container, arranging and resizing its components to fit in grid contains 5 rows and 7 columns.<br>
 * Every element in grid is placed in same size square area expect first element that takes five columns in the first row.
 *
 * @author Domagoj Lokner
 */
public class CalcLayout implements LayoutManager2 {

    private static final int NUM_OF_ROWS = 5;
    private static final int NUM_OF_COLUMNS = 7;

    /**
     * Default interspace value.
     */
    private static final int DEFAULT_INTERSPACE = 0;

    /**
     * Space between rows and columns in pixels.
     */
    int interspace;

    /**
     * Matrix that stores components of this layout.
     */
    Component[][] components = new Component[NUM_OF_ROWS][NUM_OF_COLUMNS];

    /**
     * Constructs {@code CalcLayout} with space between elements set on default value.
     */
    public CalcLayout() {
        this(DEFAULT_INTERSPACE);
    }

    /**
     * Constructs {@code CalcLayout} with space between elements set on given value.
     *
     * @param interspace - distance between elements in greed in pixels.
     */
    public CalcLayout(int interspace){
        this.interspace = interspace;
    }

    @Override
    public float getLayoutAlignmentX(Container target) {
        return 0f;
    }

    @Override
    public float getLayoutAlignmentY(Container target) {
        return 0f;
    }

    @Override
    public void layoutContainer(Container parent) {
        synchronized (parent.getTreeLock()){

            Rectangle r = getComponentsSpace(parent);
            Rectangle[][] positions = computePositions(r);

            for(int i = 0; i < NUM_OF_ROWS; ++i){
                for(int j = 0; j < NUM_OF_COLUMNS; ++j){
                    if (!checkComponent(components[i][j], i, j)) {
                        continue;
                    }
                    components[i][j].setBounds(positions[i][j]);
                }
            }
        }
    }


    @Override
    public void invalidateLayout(Container target) {
    }

    @Override
    public void addLayoutComponent(Component comp, Object constraints) {

        RCPosition position;

        if(constraints instanceof RCPosition){
            position = (RCPosition)constraints;
        } else if(constraints instanceof String){
            try{
                position = RCPosition.parse((String)constraints);
            } catch (NumberFormatException ex){
                throw new CalcLayoutException("Constraints can't be interpreted as position.");
            }
        } else {
            throw new CalcLayoutException("Constraint is not RCPosition type object.");
        }

        synchronized (comp.getTreeLock()){
            storeIntoMatrix(comp, position);
        }
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {
        throw  new UnsupportedOperationException();
    }

    @Override
    public void removeLayoutComponent(Component comp) {

        RCPosition position;
        synchronized (comp.getTreeLock()) {
            position = getPosition(comp);
        }

        if(Objects.isNull(position)){
            throw new CalcLayoutException("Layout is already filled");
        }

        components[position.getRow()][position.getColumn()] = null;
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {

        int maxPreferedWidth = getMax(c -> c.getPreferredSize().width, true);
        int maxPreferedHeight = getMax(c -> c.getPreferredSize().height, false);

        return computeDimension(parent, maxPreferedWidth, maxPreferedHeight);
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        int maxMinimumWidth = getMax(c -> c.getMinimumSize().width, true);
        int maxMinimumHeight = getMax(c -> c.getMinimumSize().height, false);

        return computeDimension(parent, maxMinimumWidth, maxMinimumHeight);
    }

    @Override
    public Dimension maximumLayoutSize(Container target) {
        synchronized (target.getTreeLock()) {
            int maxMaximumWidth = getMax(c -> c.getMaximumSize().width, true);
            int maxMaximumHeight = getMax(c -> c.getMaximumSize().height, false);

            return computeDimension(target, maxMaximumWidth, maxMaximumHeight);
        }
    }

    /**
     * Returns max value that given function will return applied on every component in layout.
     *
     * @param function - function to be applied.
     * @param flag - if flag is {@code true} width value will be computed otherwise method will compute height value.
     * @return max value that function returns.
     */
    private int getMax(Function<Component, Integer> function, boolean flag){

        int max = 0;

        synchronized (this) {
            for (int i = 0; i < NUM_OF_ROWS; ++i) {
                for (int j = 0; j < NUM_OF_COLUMNS; ++j) {

                    if (!checkComponent(components[i][j], i, j)) {
                        continue;
                    }

                    int size = function.apply(components[i][j]);

                    if (flag && i == 0 && j == 0) {
                        size = (int) Math.ceil((size - 4 * interspace) / 5d);
                    }

                    if (size > max) {
                        max = size;
                    }
                }
            }
        }

        return max;
    }

    /**
     * Returns dimensions depending on given parameters.
     *
     * @param parent - container which dimensions will be computed.
     * @param minElemWidth - min width of every element.
     * @param minElemHeight - min height of every element.
     * @return dimensions of container.
     */
    private Dimension computeDimension(Container parent, int minElemWidth, int minElemHeight){

        Insets border;
        int parentWidth;
        int parentHeight;

        synchronized (parent.getTreeLock()) {
            border = parent.getInsets();
            parentWidth = parent.getWidth();
            parentHeight = parent.getHeight();
        }

        int elemsWidth = border.left
                + minElemWidth*NUM_OF_COLUMNS
                + interspace*(NUM_OF_COLUMNS-1)
                + border.right;
        int elemsHeight = border.top
                + minElemHeight*NUM_OF_ROWS
                + interspace*(NUM_OF_ROWS-1)
                + border.bottom;


        int width = parentWidth > elemsWidth ? parentWidth : elemsWidth;
        int height = parentHeight > elemsHeight ? parentHeight : elemsHeight;

        return new Dimension(width, height);
    }

    /**
     * Gets position of given object in layout.
     *
     * @param comp - object which position will be returned.
     * @return position ob given object or {@code null} if object is not found.
     */
    private RCPosition getPosition(Object comp){
        synchronized (comp) {
            for (int i = 0; i < NUM_OF_ROWS; ++i) {
                for (int j = 0; j < NUM_OF_COLUMNS; ++j) {
                    if (i == 0) {
                        if (j > 0 || j < 5) {
                            continue;
                        }
                    }

                    if (components[i][j] == comp) {
                        return new RCPosition(i + 1, j + 1);
                    }
                }
            }
        }
        return null;
    }

    /**
     * Adds given component into matrix.
     *
     * @param comp - component to be added.
     * @param position - position on which component will be added.
     * @throws IndexOutOfBoundsException if given position is not legal position.
     * @throws CalcLayoutException if position is out of layout bounds
     *                             or if there is already stored component on given position.
     */
    private void storeIntoMatrix(Component comp, RCPosition position) {
        if(!checkPosition(position)) {
            throw new CalcLayoutException("Invalid position is given.");
        }

        int x = position.getRow() - 1;
        int y = position.getColumn() - 1;

        synchronized (comp.getTreeLock()) {
            if (!Objects.isNull(components[x][y])) {
                throw new CalcLayoutException("Component is already placed on given position.");
            }

            components[x][y] = comp;
        }
    }

    /**
     * Checks if position is in bounds.
     *
     * @param position - position to nbe checked.
     * @return {@code true} if position is in bounds otherwise {@code false}.
     */
    private static boolean checkPosition(RCPosition position) {
        int row = position.getRow();
        int column = position.getColumn();

        if(row < 1 || row > 5){
            return false;
        }

        if(column < 1 || column > 7){
            return false;
        }

        if(row == 1){
            if(column > 1 && column < 6){
                return false;
            }
        }
        return true;
    }

    /**
     * Checks component position in matrix.
     *
     * @param c - component to be checked.
     * @param i - row in matrix in which component is stored.
     * @param j - column in matrix in which component is stored.
     * @return {@code true} if component is on valid position and is not {@code null} otherwise {@code false}.
     */
    private static boolean checkComponent(Component c, int i, int j) {
        if(Objects.isNull(c)){
            return false;
        }

        if (i == 0) {
            if (j > 0 && j < 5) {
                return false;
            }
        }

        return true;
    }

    /**
     * Compute bounds for every element in layout.
     *
     * @param r - rectangle in which all elements should be placed.
     * @return matrix of bounds for every element.
     */
    private Rectangle[][] computePositions(Rectangle r) {
        int size = (r.width - interspace*(NUM_OF_COLUMNS-1)) / NUM_OF_COLUMNS;
        int remainder = r.width - size*NUM_OF_COLUMNS - (NUM_OF_COLUMNS-1)*interspace;

        int[] widths = uniformedArray(size, NUM_OF_COLUMNS, remainder);

        size = (r.height - interspace*(NUM_OF_ROWS-1)) / NUM_OF_ROWS;
        remainder = r.height - size*NUM_OF_ROWS - (NUM_OF_ROWS-1)*interspace;

        int[] heights = uniformedArray(size, NUM_OF_ROWS, remainder);

        Rectangle[][] result = new Rectangle[NUM_OF_ROWS][NUM_OF_COLUMNS];

        int y = r.y;

        for(int i = 0; i < NUM_OF_ROWS; ++i){

            int x = r.x;

            for(int j = 0; j < NUM_OF_COLUMNS; x += widths[j]+interspace, ++j){

                if(i == 0 && j == 5) {
                    result[0][0].width = x - interspace;
                }

                if(i == 0 && j > 0 && j < 5){
                    continue;
                }

                result[i][j] = new Rectangle(x, y, widths[j], heights[i]);
            }

            y += heights[i] + interspace;
        }

        return result;
    }

    /**
     * Returns array with {@code length} number of elements.
     * Method guarantees that sum of all elements will be {@code size + remainder} andd that
     * elements in array wil be
     *
     * @param size - minimal value of every element in the array.
     * @param length - length of result array (expects odd number).
     * @param remainder - remainder that should be added to specified elements.
     * @return uniformed array.
     */
    private int[] uniformedArray(int size, int length, int remainder) {
        int[] result = new int[length];

        for(int i = 0; i < result.length; ++i){
            result[i] = size;
        }

        if(remainder == 0){
            return result;
        }

        int median = (length-1) / 2;

        if(remainder % 2 != 0){
            result[median]++;
            remainder--;
        } else {
            result[median-1]++;
            result[median+1]++;
            remainder -= 2;
        }

        if(remainder == 0){
            return result;
        }

        int offset = (length-1) / remainder < 0 ? remainder*(-1) : remainder;

        for(int i = 0, j = length-1; remainder > 0; i += offset, j -= offset, remainder -= 2) {
            result[i]++;
            result[j]++;
        }

        return result;
    }


    /**
     * Returns rectangle representing space for displaying elements in given container.
     *
     * @param c - container.
     * @return rectangle.
     */
    private static Rectangle getComponentsSpace(Container c) {
        Insets boreder = c.getInsets();
        Dimension dim = c.getSize();

        return new Rectangle(
                boreder.left,
                boreder.right,
                dim.width - boreder.left - boreder.right,
                dim.height - boreder.top - boreder.bottom
        );
    }
}
