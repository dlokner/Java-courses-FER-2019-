package hr.fer.zemris.java.gui.calc.buttons;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

import javax.swing.*;

/**
 * Button which will perform {@code clearAll()} method on given {@code CalcModel}.
 * @see hr.fer.zemris.java.gui.calc.model.CalcModel
 *
 * @author Domagoj Lokner
 */
public class ClearAllButton extends JButton {

	private static final long serialVersionUID = -7527340228170304204L;
	
	/**
     * Calculator model.
     */
    CalcModel calc;

    /**
     * Constructs {@code ClearAllButton}.
     *
     * @param text - button text.
     * @param calc - calculator implementation.
     */
    public ClearAllButton(String text, CalcModel calc) {
        super(text);
        this.calc = calc;
        addActionListener(e -> calc.clearAll());
    }
}
