package hr.fer.zemris.java.gui.charts;

/**
 * Object stores two integers, x and y value.
 *
 * @author Domagoj Lokner
 */
public class XYValue {

    /**
     * x value.
     */
    private int x;

    /**
     * y value;
     */
    private int y;

    /**
     * Constructs {@code XYValue}.
     *
     * @param x - x value.
     * @param y - y value.
     */
    public XYValue(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Gets x value.
     *
     * @return x value.
     */
    public int getX() {
        return x;
    }

    /**
     * Gets y value.
     *
     * @return y value.
     */
    public int getY() {
        return y;
    }
}
