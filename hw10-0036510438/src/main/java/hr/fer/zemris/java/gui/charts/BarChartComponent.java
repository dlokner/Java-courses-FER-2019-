package hr.fer.zemris.java.gui.charts;


import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@code JComponent}.
 * This component displays bar chart.
 *
 * @author Domagoj Lokner
 */
public class BarChartComponent extends JComponent {

	private static final long serialVersionUID = 4279979206217933183L;

	private static final int DISTANCE = 8;

    private static final int ARROW_SPACE = 16;

    private static final int AXES_MARK = 8;

    /**
     * Reference to bar chart this component is able to display.
     */
    private BarChart chart;

    /**
     * Constructs {@code BarChartComponent}.
     *
     * @param chart - reference to bar chart this component is able to display.
     */
    public BarChartComponent(BarChart chart) {
        this.chart = chart;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        //left upper corner
        Point start = new Point(
                getInsets().left,
                getInsets().top
        );

        Dimension size = getSize();

        Graphics2D g2d = (Graphics2D)g;

        g2d.setFont(new Font("Numbers", Font.BOLD, 17));

        AxesResult axes = drawAxes(g2d, start, size);

        drawArrows(g2d, axes);
        drawGrid(g2d, axes);

        drawBars(g2d, axes);

        xAxesValues(g2d, axes);
        numerizeYAxes(g2d, axes);

        g2d.setFont(new Font("Descriptions", Font.PLAIN, 15));

        drawXAxesDescription(g2d, axes);
        drawYAxesDescription(g2d, axes);

    }

    /**
     * Draw x-axes descriptions.
     *
     * @param g - graphics object that draw values.
     * @param axes - axes of graph.
     */
    private void drawXAxesDescription(Graphics2D g, AxesResult axes) {
        FontMetrics fm = g.getFontMetrics();

        int median = (axes.xStart.x + axes.xLength) / 2;

        int startX = median - fm.stringWidth(chart.getxDescription())/2;
        int startY = getHeight() - getInsets().bottom - fm.getDescent();

        g.drawString(
                chart.getxDescription(),
                startX,
                startY
        );
    }

    /**
     * Draw y-axes descriptions.
     *
     * @param g - graphics object that draw values.
     * @param axes - axes of graph.
     */
    private void drawYAxesDescription(Graphics2D g, AxesResult axes) {
        FontMetrics fm = g.getFontMetrics();

        int median = axes.yStart.y - axes.yLength / 2;

        int startX = median + fm.stringWidth(chart.getyDescription())/2;
        int startY = fm.getAscent();

        AffineTransform at = new AffineTransform();
        at.rotate(- Math.PI / 2);
        g.setTransform(at);

        g.drawString(
                chart.getyDescription(),
                -startX,
                startY
        );
    }

    /**
     * Numerates y axes.
     *
     * @param g - graphics object that draw values.
     * @param axes - axes of graph.
     */
    private void numerizeYAxes(Graphics2D g, AxesResult axes) {
        FontMetrics fm = g.getFontMetrics();

        g.setColor(Color.BLACK);

        for(int i = 0; i < axes.yPoints.size(); ++i){
            Point p1 = axes.yPoints.get(i);

            int value = (i) * chart.getYStep();
            String valueStr = Integer.toString(value);

            int start = p1.x - (DISTANCE + AXES_MARK+2 + fm.stringWidth(valueStr));

            g.drawString(
                    valueStr,
                    start,
                    p1.y + fm.getAscent()/2
            );
        }
    }

    /**
     * Draws values under x axes.
     *
     * @param g - graphics object that draw values.
     * @param axes - axes of graph.
     */
    private void xAxesValues(Graphics2D g, AxesResult axes) {
        FontMetrics fm = g.getFontMetrics();

        g.setColor(Color.BLACK);

        for(int i = 1; i < axes.xPoints.size(); ++i){
            Point p1 = axes.xPoints.get(i);

            XYValue value = chart.getValues().get(i-1);

            int median = p1.x - axes.xStep/2;
            int start = median - fm.stringWidth(Integer.toString(value.getX()))/2;

            g.drawString(
                    Integer.toString(value.getX()),
                    start,
                    p1.y + DISTANCE + fm.getAscent()
            );
        }
    }

    /**
     * Draws bars graph bars on given graph.
     *
     * @param g - graphics object that draw bars.
     * @param axes - axes of graph in which bars will be drawn.
     */
    private void drawBars(Graphics2D g, AxesResult axes) {
        int gridMaxY = chart.getYMax() + chart.getYMax() % chart.getYStep();

        List<XYValue> values = chart.getValues();

        for(int i = 1; i < axes.xPoints.size(); ++i){
            int barHeight = Math.round(
                    axes.yLength * (values.get(i-1).getY()/(float)gridMaxY)
            );

            Point p1 = axes.xPoints.get(i-1);
            Point p2 = axes.xPoints.get(i);

            g.setColor(new Color(245, 120, 71));
            g.fillRect(
                    p1.x + 3,
                    p2.y - barHeight,
                    p2.x - p1.x - 3,
                    barHeight - 3
            );
        }
    }

    /**
     * Draws grid for given axes.
     *
     * @param g - graphics object that draw axes.
     * @param axes - axes for which grid will be drawn.
     */
    private void drawGrid(Graphics2D g, AxesResult axes) {

        g.setStroke(new BasicStroke(3f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        g.setColor(new Color(239, 221, 189));

        for(Point p : axes.xPoints.subList(1, axes.xPoints.size())){
            g.drawLine(p.x, p.y, p.x, p.y - (axes.yLength + ARROW_SPACE));
        }

        for(Point p : axes.yPoints.subList(1, axes.yPoints.size())){
            g.drawLine(p.x, p.y, p.x + axes.xLength + ARROW_SPACE, p.y);
        }

    }

    /**
     * Draw exes on {@code g}.
     *
     * @param g - graphics object that draw axes.
     * @param start - starting point of drawing area.
     * @param d - dimensions of drawing area.
     * @return {@code AxesResult} with all information about drawn axes.
     */
    private AxesResult drawAxes(Graphics2D g, Point start, Dimension d){
        FontMetrics fm = g.getFontMetrics();

        Point xStart = new Point(
                start.x + fm.getHeight() + AXES_MARK + DISTANCE*2 + fm.stringWidth(Integer.toString(chart.getYMax())),
                start.y + d.height - (fm.getHeight() + DISTANCE*2 + fm.getAscent() + AXES_MARK)
        );

        Point xEnd = new Point(
                start.x + d.width - xStart.x,
                xStart.y
        );

        Point yStart = xStart;

        Point yEnd = new Point(
                xStart.x,
                start.y
        );

        AxesResult result = new AxesResult();

        result.xStart = xStart;
        result.xEnd = xEnd;
        result.yStart = yStart;
        result.yEnd = yEnd;

        g.setStroke(new BasicStroke(3f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        g.setColor(new Color(188, 186, 186));

        g.drawLine(xStart.x, xStart.y, xEnd.x, xEnd.y);
        g.drawLine(yStart.x, yStart.y, yEnd.x, yEnd.y);

        int xLength = xEnd.x -  xStart.x - ARROW_SPACE;
        int yLength = yStart.y - yEnd.y - ARROW_SPACE;

        result.xLength = xLength;
        result.yLength = yLength;

        int xMarkStep = xLength / chart.getValues().size();
        int yMarkStep = yLength / (chart.getYMax() + chart.getYMax() % chart.getYStep());

        result.xStep = xMarkStep;
        result.yStep = yMarkStep;

        for(int i = 0; i <= chart.getValues().size(); i ++){
            Point p = new Point(
                    xStart.x + i*xMarkStep, xStart.y
            );

            g.drawLine(p.x, p.y, p.x, p.y + AXES_MARK);

            result.xPoints.add(p);
        }

        for(int i = 0; i*yMarkStep <= yLength; i += chart.getYStep()){
            Point p = new Point(
                    yStart.x, yStart.y - i*yMarkStep
            );

            g.drawLine(p.x, p.y, p.x - AXES_MARK, p.y);

            result.yPoints.add(p);
        }

        return result;
    }

    /**
     * Draws arrows on axes end.
     *
     * @param g - graphics object that draw arrows.
     * @param axes - axes for which arrows will be drawn.
     */
    private void drawArrows(Graphics2D g, AxesResult axes) {
        //y axes arrow
        g.setColor(new Color(188, 186, 186));
        g.fillPolygon(new Polygon(
                new int[] {
                        axes.yEnd.x,
                        axes.yEnd.x - ARROW_SPACE/2,
                        axes.yEnd.x + ARROW_SPACE/2
                },
                new int[] {
                        axes.yEnd.y,
                        axes.yEnd.y + (ARROW_SPACE - 2),
                        axes.yEnd.y + (ARROW_SPACE - 2)
                },
                3
        ));

        //x axes arrow
        g.fillPolygon(new Polygon(
                new int[] {
                        axes.xEnd.x,
                        axes.xEnd.x - (ARROW_SPACE - 3),
                        axes.xEnd.x - (ARROW_SPACE - 3)
                },
                new int[] {
                        axes.xEnd.y,
                        axes.xEnd.y - ARROW_SPACE/2,
                        axes.xEnd.y + ARROW_SPACE/2
                },
                3
        ));
    }


    /**
     * Private class that represents storage for
     * all information about x and y axes.
     *
     * @author Domagoj Lokner
     */
    private static class AxesResult{

        Point xStart;
        Point yStart;

        Point xEnd;
        Point yEnd;

        int xLength;
        int yLength;

        List<Point> xPoints;
        List<Point> yPoints;

        int xStep;
        
        @SuppressWarnings("unused")
		int yStep;

        /**
         * Constructs new {@code AxesResult} object.
         */
        public AxesResult() {
            xPoints = new ArrayList<>();
            yPoints = new ArrayList<>();
        }
    }
}
