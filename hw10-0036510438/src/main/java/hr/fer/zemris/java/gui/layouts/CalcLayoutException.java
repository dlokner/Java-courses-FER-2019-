package hr.fer.zemris.java.gui.layouts;

/**
 * Indicate error in {@code CalcLayout}.
 */
public class CalcLayoutException extends RuntimeException {

	private static final long serialVersionUID = 8465025981115533808L;

	/**
     * Constructs {@code CalcLayoutException}.
     */
    public CalcLayoutException() {
    }

    /**
     * Constructs {@code CalcLayoutException} with specific detail message about occurred exception.
     *
     * @param message - detail message.
     */
    public CalcLayoutException(String message) {
        super(message);
    }

}
