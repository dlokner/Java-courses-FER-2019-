package hr.fer.zemris.java.gui.charts;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Program displays bar chart.
 * Graph will be created using file with all needed informations and path to
 * file is expected as command line argument.
 *
 *
 * <ul>
 *     File should contain following informations:
 *     <li>x-axes description</li>
 *     <li>y-axes description</li>
 *     <li>list of xy values separated with blanks</li>
 *     <li>minimum y value/li>
 *     <li>maximum y value</li>
 *     <li>value of step to next y value in chart</li>
 * </ul>
 *
 * @author Domagoj Lokner
 */
public class BarChartDemo extends JFrame {

	private static final long serialVersionUID = 7407960336585811460L;

	/**
     * Constructs new {@code BarChartDemo} program.
     *
     * @param chart - bar chart to be added to GUI.
     * @param path - path to file which name will be displayed in top of the frame.
     */
    public BarChartDemo(BarChart chart, Path path) {
        setLocation(20, 50);
        setSize(700, 700);
        setTitle("Bar Chart v1.0");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        initGUI(chart, path);
    }

    /**
     * Starting method of the program.
     *
     * @param args - command line arguments.
     */
    public static void main(String[] args) {
        if(args.length < 1){
            System.out.println("Program expects single command line argument");
            System.exit(1);
        }

        Path path = Paths.get(args[0].strip());
        List<String> lines = null;

        try {
            lines = Files.readAllLines(path);
        } catch (IOException ex){
            System.out.println("File can't be reached!");
            System.exit(1);
        }

        if(lines.size() < 6){
            System.out.println("Invalid file format.");
            System.exit(1);
        }

        BarChart chart = createBarChart(lines.subList(0, 6));

        SwingUtilities.invokeLater(() -> {
            JFrame frame = new BarChartDemo(chart, path);
            frame.setVisible(true);
        });
    }

    /**
     * Initialize GUI.
     *
     * @param chart - bar chart to be added to GUI.
     * @param path - path to file which name will be displayed in top of the frame.
     */
    private void initGUI(BarChart chart, Path path){
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        BarChartComponent graph = new BarChartComponent(chart);
        cp.add(graph, BorderLayout.CENTER);

        JLabel label = new JLabel(path.toAbsolutePath().toString());
        label.setHorizontalAlignment(SwingConstants.CENTER);
        cp.add(label, BorderLayout.PAGE_START);
    }

    /**
     * Constructs new {@code BarChart} depending of first six string frog given
     * list. Arguments in list are expected in following order:
     *  x-axes description, y-axes description, values, yMin, yMax, yStep.
     *
     * @param arguments - list of arguments.
     * @return new {@code BarChart} object.
     * @throws IllegalArgumentException if format of file is not valid.
     * @throws IndexOutOfBoundsException if list have less than 6 arguments.
     */
    private static BarChart createBarChart(List<String> arguments) {
        String x = arguments.get(0);
        String y = arguments.get(1);

        String[] valuesStr = arguments.get(2).split("\\s+");

        List<XYValue> values = new LinkedList<>();
        int yMin, yMax, yStep;

        try {
            Arrays.stream(valuesStr)
                    .forEach(v -> {
                        if (!v.isEmpty()) {
                            String[] value = v.split(",");

                            if (value.length == 2) {
                                values.add(new XYValue(
                                        Integer.parseInt(value[0]),
                                        Integer.parseInt(value[1])
                                ));
                            } else {
                                throw new IllegalArgumentException();
                            }
                        }
                    });

            yMin = Integer.parseInt(arguments.get(3));
            yMax = Integer.parseInt(arguments.get(4));
            yStep = Integer.parseInt(arguments.get(5));

        }catch (IllegalArgumentException ex){
            throw new IllegalArgumentException("Invalid file format.");
        }

        return new BarChart(values, x, y, yMin, yMax, yStep);
    }
}
