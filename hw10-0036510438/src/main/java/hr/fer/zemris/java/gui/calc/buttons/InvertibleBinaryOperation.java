package hr.fer.zemris.java.gui.calc.buttons;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

import javax.swing.*;
import java.util.function.DoubleBinaryOperator;

/**
 * Implements single calculator button that represents invertible binary operation.
 *
 * <p>
 *     Constructor given binary operation will be set as operator in given {@code calcModel}
 *     every time when button is pressed.
 * </p>
 *
 * @author Domagoj Lokner
 */
public class InvertibleBinaryOperation extends JButton implements InvertibleButton {

	private static final long serialVersionUID = 5461979175319233003L;

	/**
     * Calculator model.
     */
    private CalcModel calc;

    /**
     * Primary function of button.
     */
    private DoubleBinaryOperator function;

    /**
     * Inverse function of button.
     */
    private DoubleBinaryOperator inverse;

    /**
     * Flag for inverse operation.
     */
    private boolean isInverse;

    /**
     * Primary function button text.
     */
    private String functionText;

    /**
     * Inverse function button text.
     */
    private String inverseText;

    /**
     * Constructs {@code InvertibleUnaryOperation}.
     *
     * @param calc - calculator model that will perform operations.
     * @param function - primary function of button.
     * @param inverse - inverse function of button.
     */
    public InvertibleBinaryOperation(String functionText, String inverseText,
                                   DoubleBinaryOperator function,
                                   DoubleBinaryOperator inverse, CalcModel calc) {
        super(functionText);
        this.functionText = functionText;
        this.inverseText = inverseText;

        this.calc = calc;
        this.function = function;
        this.inverse = inverse;
        
        addActionListener(e -> {
            if(isInverse){
                calc.setPendingBinaryOperation(inverse);
            } else {
                calc.setPendingBinaryOperation(function);
            }
        });
    }

    @Override
    public void setInverse() {
        isInverse = true;
        setText(inverseText);
    }

    @Override
    public void setFunction() {
        isInverse = false;
        setText(functionText);
    }

    /**
     * Gets function button text.
     *
     * @return function text.
     */
    public String getFunctionText() {
        return functionText;
    }

    /**
     * Gets button text for inverse function.
     *
     * @return inverse function text.
     */
    public String getInverseText() {
        return inverseText;
    }

    /**
     * Gets calculator model.
     * 
     * @return calculator model.
     */
	public CalcModel getCalc() {
		return calc;
	}

	/**
	 * Gets function.
	 * 
	 * @return function.
	 */
	public DoubleBinaryOperator getFunction() {
		return function;
	}

	/**
	 * Gets inverse function.
	 * 
	 * @return inverse function.
	 */
	public DoubleBinaryOperator getInverse() {
		return inverse;
	}  
    
}
