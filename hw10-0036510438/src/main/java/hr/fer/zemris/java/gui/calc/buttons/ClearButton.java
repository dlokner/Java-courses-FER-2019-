package hr.fer.zemris.java.gui.calc.buttons;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

import javax.swing.*;

/**
 * Button which will erase the current number.
 *
 * @author Domagoj Lokner
 */
public class ClearButton extends JButton {

	private static final long serialVersionUID = 4596817552374946972L;
	
	/**
     * Calculator model.
     */
    CalcModel calc;

    /**
     * Constructs {@code ClearButton}.
     *
     * @param text - button text.
     * @param calc - calculator implementation.
     */
    public ClearButton(String text, CalcModel calc) {
        super(text);
        this.calc = calc;
        addActionListener(e -> calc.clear());
    }
}
