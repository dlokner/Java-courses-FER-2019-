package hr.fer.zemris.java.gui.calc.buttons;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

import javax.swing.*;
import java.util.function.DoubleBinaryOperator;

/**
 * Implements single calculator button that represents binary operator.
 *
 * <p>
 *     Constructor given binary operator will be set as operator in given {@code calcModel}
 *     every time when button is pressed.
 * </p>
 *
 * @author Domagoj Lokner
 */
public class BinaryOperatorButton extends JButton {

	private static final long serialVersionUID = 533625809997420653L;

	/**
     * Operator this button represent.
     */
    DoubleBinaryOperator operator;

    /**
     * Calculator model.
     */
    CalcModel calc;

    /**
     * Constructs {@code BinaryOperatorButton}.
     *
     * @param text - button text.
     * @param operator - binary operator.
     * @param calcModel - calculator implementation.
     */
    public BinaryOperatorButton(String text, DoubleBinaryOperator operator, CalcModel calcModel) {
        super(text);

        this.operator = operator;
        this.calc = calcModel;

        addActionListener(e -> calc.setPendingBinaryOperation(operator));
    }
}
