package hr.fer.zemris.java.gui.prim;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Collection of prime numbers.
 *
 * @author Domagoj Lokner
 */
public class PrimListModel implements ListModel<Long> {

    /**
     * Collection stores generated primes.
     */
    private List<Long> primes;

    /**
     * Collection that stores all registered listeners.
     */
    private List<ListDataListener> listeners = new ArrayList<>();

    /**
     * Constructs new {@code PrimListModel}-
     */
    public PrimListModel() {
        primes = new LinkedList<>();
        primes.add(1l);
    }

    /**
     * Generates next prime number.
     */
    public void next() {
        int size = getSize();
        long last = primes.get(primes.size()-1);

        if(last == 1l || last == 2l){
            ++last;
        } else {
            last += 2;
        }

        while(!isPrime(last)){
            last += 2;
        }

        primes.add(last);

        ListDataEvent event = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, size, size);
        for(ListDataListener l : listeners) {
            l.intervalAdded(event);
        }
    }

    /**
     * Checks if given number is prime.
     *
     * @param num - number to be checked.
     * @return {@code true} if number is prime otherwise {@code false}.
     */
    private boolean isPrime(long num) {
        for(long i = 2, n = (long)Math.sqrt(num); i <= n; ++i){
            if(num % i == 0){
                return false;
            }
        }
        return true;
    }

    /**
     * Gets list of primes.
     *
     * @return list of primes.
     */
    public List<Long> getPrimes() {
        return primes;
    }

    @Override
    public int getSize() {
        return primes.size();
    }

    @Override
    public Long getElementAt(int index) {
        return primes.get(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        listeners.add(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        listeners.remove(l);
    }
}
