package hr.fer.zemris.java.gui.calc.buttons;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

import javax.swing.*;
import java.util.function.DoubleFunction;

/**
 * Implements single calculator button that represents function.
 *
 * <p>
 *     Constructor given function will be performed in given {@code calcModel}
 *     every time when button is pressed.
 * </p>
 *
 * @author Domagoj Lokner
 */
public class FunctionButton extends JButton {

	private static final long serialVersionUID = -3271489079801923747L;

	/**
     * Operator this button represent.
     */
    DoubleFunction<Double> function;

    /**
     * Calculator model.
     */
    CalcModel calc;

    /**
     *
     * @param text
     * @param operator
     * @param calcModel
     */
    public FunctionButton(String text, DoubleFunction<Double> operator, CalcModel calcModel) {
        super(text);

        this.function = operator;
        this.calc = calcModel;

        addActionListener(e -> calc.setValue(function.apply(calc.getValue())));
    }
}
