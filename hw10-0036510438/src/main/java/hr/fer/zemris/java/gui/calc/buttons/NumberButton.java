package hr.fer.zemris.java.gui.calc.buttons;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

import javax.swing.*;

/**
 * Implements single number calculator button.
 *
 * <p>
 *      This implementation will call {@code insetDigit} method for {@code calcImpl}
 *      given through constructor. Constructor given {@code value} will be sent as
 *      {@code digit} argument to method.
 * </p>
 *
 * @author Domagoj Lokner
 */
public class NumberButton extends JButton {

	private static final long serialVersionUID = -8743010923059370227L;

	/**
     * Number value.
     */
    private int value;

    /**
     * Calculator model.
     */
    private CalcModel calcModel;

    /**
     * Constructs number button for calculator.
     *
     * @param text - button text.
     * @param value - number value. This value will be inserted to {@code calcModel}
     *                on every button action.
     * @param calcModel - {@code CalcModel} calculator model implementation in which
     *                    insertDigit method will be called for every button action.
     */
    public NumberButton(String text, int value, CalcModel calcModel) {
        super(text);

        this.value = value;
        this.calcModel = calcModel;

        addActionListener(e -> calcModel.insertDigit(value));
    }

    /**
     * Gets number value.
     * 
     * @return number value.
     */
	public int getValue() {
		return value;
	}

	/**
	 * Gets calculator model.
	 * 
	 * @return calculator model.
	 */
	public CalcModel getCalcModel() {
		return calcModel;
	}
    
    
}