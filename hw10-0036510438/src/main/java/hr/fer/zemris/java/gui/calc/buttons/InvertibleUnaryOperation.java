package hr.fer.zemris.java.gui.calc.buttons;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

import javax.swing.*;

import java.util.function.DoubleFunction;

/**
 * Objects represent calculator button performing invertible unary operation.
 *
 * @author Domagoj Lokner
 */
public class InvertibleUnaryOperation extends JButton implements InvertibleButton {

	private static final long serialVersionUID = 183028468017050803L;

	/**
     * Calculator model.
     */
    private CalcModel calc;

    /**
     * Primary function of button.
     */
    private DoubleFunction<Double> function;

    /**
     * Inverse function of button.
     */
    private DoubleFunction<Double> inverse;

    /**
     * Flag for inverse operation.
     */
    boolean isInverse;

    /**
     * Primary function button text.
     */
    String functionText;

    /**
     * Inverse function button text.
     */
    String inverseText;

    /**
     * Constructs {@code InvertibleUnaryOperation}.
     *
     * @param calc - calculator model that will perform operations.
     * @param function - primary function of button.
     * @param inverse - inverse function of button.
     */
    public InvertibleUnaryOperation(String functionText, String inverseText,
                                    DoubleFunction<Double> function,
                                    DoubleFunction<Double> inverse, CalcModel calc) {
        super(functionText);
        this.functionText = functionText;
        this.inverseText = inverseText;

        this.calc = calc;
        this.function = function;
        this.inverse = inverse;
        addActionListener(e -> {
            if(isInverse){
                setText(inverseText);
                performFunction(inverse);
            } else {
                setText(functionText);
                performFunction(function);
            }
        });
    }

    @Override
    public void setInverse() {
        isInverse = true;
        setText(inverseText);
    }

    @Override
    public void setFunction() {
        isInverse = false;
        setText(functionText);
    }

    /**
     * Perform given function.
     *
     * @param function - function to be performed.
     */
    private void performFunction(DoubleFunction<Double> function){
        calc.setValue(function.apply(calc.getValue()));
    }

    /**
     * Gets function button text.
     *
     * @return function text.
     */
    public String getFunctionText() {
        return functionText;
    }

    /**
     * Gets button text for inverse function.
     *
     * @return inverse function text.
     */
    public String getInverseText() {
        return inverseText;
    }
    
    /**
     * Gets calculator model.
     * 
     * @return calculator model.
     */
	public CalcModel getCalc() {
		return calc;
	}

	/**
	 * Gets function.
	 * 
	 * @return function.
	 */
	public DoubleFunction<Double> getFunction() {
		return function;
	}

	/**
	 * Gets inverse function.
	 * 
	 * @return inverse function.
	 */
	public DoubleFunction<Double> getInverse() {
		return inverse;
	}  
}
